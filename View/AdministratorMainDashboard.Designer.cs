﻿namespace RentMe_G4.View
{
    partial class AdministratorMainDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.Logoutbutton = new System.Windows.Forms.Button();
            this.AdministratorTabControl = new System.Windows.Forms.TabControl();
            this.addEmployeeTabPage = new System.Windows.Forms.TabPage();
            this.displayEmployeeTabPage = new System.Windows.Forms.TabPage();
            this.updateEmployeeTabPage = new System.Windows.Forms.TabPage();
            this.reportsTabPage = new System.Windows.Forms.TabPage();
            this.addEmployeeUserControl = new RentMe_G4.UserControls.AddEmployeeUserControl();
            this.displayEmployeesUserControl = new RentMe_G4.UserControls.DisplayEmployeesUserControl();
            this.updateEmployeeUserControl1 = new RentMe_G4.UserControls.UpdateEmployeeUserControl();
            this.adminReportsUserControl1 = new RentMe_G4.UserControls.AdminReportsUserControl();
            this.tableLayoutPanel1.SuspendLayout();
            this.AdministratorTabControl.SuspendLayout();
            this.addEmployeeTabPage.SuspendLayout();
            this.displayEmployeeTabPage.SuspendLayout();
            this.updateEmployeeTabPage.SuspendLayout();
            this.reportsTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 116F));
            this.tableLayoutPanel1.Controls.Add(this.usernameLabel, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Logoutbutton, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(776, 57);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.usernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameLabel.Location = new System.Drawing.Point(333, 0);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(324, 57);
            this.usernameLabel.TabIndex = 0;
            this.usernameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.usernameLabel.VisibleChanged += new System.EventHandler(this.SetUsernameLabel);
            // 
            // Logoutbutton
            // 
            this.Logoutbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Logoutbutton.Location = new System.Drawing.Point(663, 3);
            this.Logoutbutton.Name = "Logoutbutton";
            this.Logoutbutton.Size = new System.Drawing.Size(110, 51);
            this.Logoutbutton.TabIndex = 1;
            this.Logoutbutton.Text = "Logout";
            this.Logoutbutton.UseVisualStyleBackColor = true;
            this.Logoutbutton.Click += new System.EventHandler(this.LogoutClicked);
            // 
            // AdministratorTabControl
            // 
            this.AdministratorTabControl.Controls.Add(this.addEmployeeTabPage);
            this.AdministratorTabControl.Controls.Add(this.displayEmployeeTabPage);
            this.AdministratorTabControl.Controls.Add(this.updateEmployeeTabPage);
            this.AdministratorTabControl.Controls.Add(this.reportsTabPage);
            this.AdministratorTabControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdministratorTabControl.Location = new System.Drawing.Point(13, 85);
            this.AdministratorTabControl.Name = "AdministratorTabControl";
            this.AdministratorTabControl.SelectedIndex = 0;
            this.AdministratorTabControl.Size = new System.Drawing.Size(813, 490);
            this.AdministratorTabControl.TabIndex = 1;
            // 
            // addEmployeeTabPage
            // 
            this.addEmployeeTabPage.Controls.Add(this.addEmployeeUserControl);
            this.addEmployeeTabPage.Location = new System.Drawing.Point(4, 25);
            this.addEmployeeTabPage.Name = "addEmployeeTabPage";
            this.addEmployeeTabPage.Size = new System.Drawing.Size(805, 461);
            this.addEmployeeTabPage.TabIndex = 1;
            this.addEmployeeTabPage.Text = "Add Employee";
            this.addEmployeeTabPage.UseVisualStyleBackColor = true;
            // 
            // displayEmployeeTabPage
            // 
            this.displayEmployeeTabPage.Controls.Add(this.displayEmployeesUserControl);
            this.displayEmployeeTabPage.Location = new System.Drawing.Point(4, 25);
            this.displayEmployeeTabPage.Name = "displayEmployeeTabPage";
            this.displayEmployeeTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.displayEmployeeTabPage.Size = new System.Drawing.Size(805, 461);
            this.displayEmployeeTabPage.TabIndex = 0;
            this.displayEmployeeTabPage.Text = "Display Employees";
            this.displayEmployeeTabPage.UseVisualStyleBackColor = true;
            // 
            // updateEmployeeTabPage
            // 
            this.updateEmployeeTabPage.Controls.Add(this.updateEmployeeUserControl1);
            this.updateEmployeeTabPage.Location = new System.Drawing.Point(4, 25);
            this.updateEmployeeTabPage.Name = "updateEmployeeTabPage";
            this.updateEmployeeTabPage.Size = new System.Drawing.Size(805, 461);
            this.updateEmployeeTabPage.TabIndex = 2;
            this.updateEmployeeTabPage.Text = "Update Employee";
            this.updateEmployeeTabPage.UseVisualStyleBackColor = true;
            // 
            // reportsTabPage
            // 
            this.reportsTabPage.Controls.Add(this.adminReportsUserControl1);
            this.reportsTabPage.Location = new System.Drawing.Point(4, 25);
            this.reportsTabPage.Name = "reportsTabPage";
            this.reportsTabPage.Size = new System.Drawing.Size(805, 461);
            this.reportsTabPage.TabIndex = 3;
            this.reportsTabPage.Text = "Reports";
            this.reportsTabPage.UseVisualStyleBackColor = true;
            // 
            // addEmployeeUserControl
            // 
            this.addEmployeeUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addEmployeeUserControl.Location = new System.Drawing.Point(0, 0);
            this.addEmployeeUserControl.Margin = new System.Windows.Forms.Padding(4);
            this.addEmployeeUserControl.Name = "addEmployeeUserControl";
            this.addEmployeeUserControl.Size = new System.Drawing.Size(805, 461);
            this.addEmployeeUserControl.TabIndex = 0;
            // 
            // displayEmployeesUserControl
            // 
            this.displayEmployeesUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.displayEmployeesUserControl.Location = new System.Drawing.Point(3, 3);
            this.displayEmployeesUserControl.Margin = new System.Windows.Forms.Padding(4);
            this.displayEmployeesUserControl.Name = "displayEmployeesUserControl";
            this.displayEmployeesUserControl.Size = new System.Drawing.Size(799, 455);
            this.displayEmployeesUserControl.TabIndex = 0;
            // 
            // updateEmployeeUserControl1
            // 
            this.updateEmployeeUserControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updateEmployeeUserControl1.Location = new System.Drawing.Point(0, 0);
            this.updateEmployeeUserControl1.Margin = new System.Windows.Forms.Padding(4);
            this.updateEmployeeUserControl1.Name = "updateEmployeeUserControl1";
            this.updateEmployeeUserControl1.Size = new System.Drawing.Size(805, 461);
            this.updateEmployeeUserControl1.TabIndex = 0;
            // 
            // adminReportsUserControl1
            // 
            this.adminReportsUserControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.adminReportsUserControl1.Location = new System.Drawing.Point(0, 0);
            this.adminReportsUserControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.adminReportsUserControl1.Name = "adminReportsUserControl1";
            this.adminReportsUserControl1.Size = new System.Drawing.Size(805, 461);
            this.adminReportsUserControl1.TabIndex = 0;
            // 
            // AdministratorMainDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(838, 587);
            this.Controls.Add(this.AdministratorTabControl);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AdministratorMainDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rent Me - Administration";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.AdministratorTabControl.ResumeLayout(false);
            this.addEmployeeTabPage.ResumeLayout(false);
            this.displayEmployeeTabPage.ResumeLayout(false);
            this.updateEmployeeTabPage.ResumeLayout(false);
            this.reportsTabPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Button Logoutbutton;
        private System.Windows.Forms.TabControl AdministratorTabControl;
        private System.Windows.Forms.TabPage displayEmployeeTabPage;
        private System.Windows.Forms.TabPage addEmployeeTabPage;
        private System.Windows.Forms.TabPage updateEmployeeTabPage;
        private UserControls.DisplayEmployeesUserControl displayEmployeesUserControl;
        private UserControls.AddEmployeeUserControl addEmployeeUserControl;
        private UserControls.UpdateEmployeeUserControl updateEmployeeUserControl1;
        private System.Windows.Forms.TabPage reportsTabPage;
        private UserControls.AdminReportsUserControl adminReportsUserControl1;
    }
}