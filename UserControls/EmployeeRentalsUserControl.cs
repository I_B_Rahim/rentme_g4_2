﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using RentMe_G4.Model;
using RentMe_G4.Controller;
using RentMe_G4.View;
using System.Linq;

namespace RentMe_G4.UserControls
{
    public partial class EmployeeRentalsUserControl : UserControl
    {
        private string defaultCartLabelText;
        private string defaultMemberButtonLookupText;
        private List<Item> itemsInSystem;
        private List<int[]> itemsInCart;
        private User userOfCurrentCart;
        private UserController theUserController;
        private ItemController theItemController;
        private EmployeeMainDashboard employeeMainDashboard;

        /// <summary>
        /// Default constructor for the EmployeeRentalsUserControl
        /// </summary>
        public EmployeeRentalsUserControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Retrieves items in inventory
        /// </summary>
        /// <returns>The items in inventory</returns>
        public List<Item> GetItemsInSystem()
        {
            return this.itemsInSystem;
        }

        /// <summary>
        /// Retrieves items in the current cart
        /// </summary>
        /// <returns>Items in the current cart</returns>
        public List<int[]> GetItemsInCart()
        {
            return this.itemsInCart;
        }

        /// <summary>
        /// Retrievs this carts reference to the ItemController
        /// </summary>
        /// <returns>The ItemController to use</returns>
        public ItemController GetItemController()
        {
            return this.theItemController;
        }

        /// <summary>
        /// Retrieves a reference to the Member User for this cart
        /// </summary>
        /// <returns>The member user</returns>
        public User GetCartUser()
        {
            return this.userOfCurrentCart;
        }

        /// <summary>
        /// Retrieves a reference to the Employee managing this cart
        /// </summary>
        /// <returns>The employee managing the cart</returns>
        public Employee GetCurrentEmployee()
        {
            return employeeMainDashboard.theEmployee;
        }

        /// <summary>
        /// Resets the cart for a new to empty and reader for a new member
        /// </summary>
        public void ResetCart()
        {
            ItemToAddQuantityTextBox.Text = "";
            SerialNumberToAddTextBox.Text = "";
            MemberUserIDLookupTextBox.Text = "";
            CartMessageLabel.Text = defaultCartLabelText;
            CreateMemberCartButton.Text = defaultMemberButtonLookupText;
            FurnitureItemListView.Clear();
            CreateMemberCartButton.Click -= new EventHandler(this.ClearMemberCartButton_Click);
            CreateMemberCartButton.Click += new EventHandler(this.CreateMemberCartButton_Click);
            itemsInSystem = new List<Item>();
            ReviewCartButton.Hide();
            AddFurnitureTableLayout.Hide();
        }

        private void EmployeeRentalsUserControl_Load(object sender, EventArgs e)
        {
            defaultCartLabelText = CartMessageLabel.Text;
            defaultMemberButtonLookupText = CreateMemberCartButton.Text;
            theUserController = new UserController();
            theItemController = new ItemController();
            employeeMainDashboard = this.ParentForm as EmployeeMainDashboard;
        }

        private void ShowCartReview()
        {
            Form modalReviewForm = new EmployeeReviewRentalForm(this);
            modalReviewForm.ShowDialog();
        }

        private void MessageUserError(String theMessage)
        {
            MessageBox.Show(theMessage, "There was an error in your request", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void CreateNewCart()
        {
            itemsInCart = new List<int[]>();
            itemsInSystem = theItemController.GetListOfItems();
            UpdateItemListBox();
            AddFurnitureTableLayout.Show();
            ReviewCartButton.Show();
            CreateMemberCartButton.Text = "Reset Cart";
            CreateMemberCartButton.Click -= new EventHandler(this.CreateMemberCartButton_Click);
            CreateMemberCartButton.Click += new EventHandler(this.ClearMemberCartButton_Click);
            UpdateCartLabel();
        }

        private void FurnitureItemListView_ItemActivate(Object sender, EventArgs e)
        {
            SerialNumberToAddTextBox.Text = FurnitureItemListView.SelectedItems[0].Text;
            ItemToAddQuantityTextBox.Text = "";
        }

        /// <summary>
        /// Updates the list of available inventory
        /// </summary>
        public void UpdateItemListBox()
        {
            FurnitureItemListView.Clear();
            ColumnHeader headerSerialNumber = new ColumnHeader();
            headerSerialNumber.Text = "Serial Number";
            headerSerialNumber.Name = "SerialNumberColumn";
            FurnitureItemListView.Columns.Add(headerSerialNumber);
            ColumnHeader headerDescription = new ColumnHeader();
            headerDescription.Text = "Description";
            headerDescription.Name = "DescriptionColumn";
            FurnitureItemListView.Columns.Add(headerDescription);
            ColumnHeader headerStyle = new ColumnHeader();
            headerStyle.Text = "Style";
            headerStyle.Name = "StyleColumn";
            FurnitureItemListView.Columns.Add(headerStyle);
            ColumnHeader headerCategory = new ColumnHeader();
            headerCategory.Text = "Category";
            headerCategory.Name = "CategoryColumn";
            FurnitureItemListView.Columns.Add(headerCategory);
            ColumnHeader headerQuantity = new ColumnHeader();
            headerQuantity.Text = "Quantity";
            headerQuantity.Name = "QuantityColumn";
            FurnitureItemListView.Columns.Add(headerQuantity);
            ColumnHeader headerDailyRentalRate = new ColumnHeader();
            headerDailyRentalRate.Text = "Daily Rental Rate";
            headerDailyRentalRate.Name = "DailyRentalRateColumn";
            FurnitureItemListView.Columns.Add(headerDailyRentalRate);
            ColumnHeader headerDailyFineRate = new ColumnHeader();
            headerDailyFineRate.Text = "Daily Fine Rate";
            headerDailyFineRate.Name = "DailyFineRaterColumn";
            FurnitureItemListView.Columns.Add(headerDailyFineRate);
            FurnitureItemListView.Scrollable = true;
            FurnitureItemListView.View = System.Windows.Forms.View.Details;

            this.FurnitureItemListView.BeginUpdate();
            foreach (Item thisItem in itemsInSystem) {
                ListViewItem theListViewItem = new ListViewItem();
                theListViewItem.Text = thisItem.SerialNumber.ToString();
                theListViewItem.SubItems.Add(thisItem.Description.ToString());
                theListViewItem.SubItems.Add(thisItem.StyleType.ToString());
                theListViewItem.SubItems.Add(thisItem.CategoryName.ToString());
                theListViewItem.SubItems.Add(thisItem.Quantity.ToString());
                theListViewItem.SubItems.Add(thisItem.DailyRentalRate.ToString());
                theListViewItem.SubItems.Add(thisItem.DailyFineRate.ToString());
                FurnitureItemListView.Items.Add(theListViewItem);
            }

            foreach (ColumnHeader column in this.FurnitureItemListView.Columns)
            {
                column.Width = -2;
            }

            this.FurnitureItemListView.EndUpdate();
        }

        /// <summary>
        /// Updates the cart label with the current number of items in the cart
        /// </summary>
        public void UpdateCartLabel()
        {
            int itemTotalCount = 0;
            foreach(int[] cartItem in itemsInCart)
            {
                itemTotalCount += cartItem[1];
            }
            CartMessageLabel.Text = userOfCurrentCart.FirstName + " " + userOfCurrentCart.LastName + "'s Cart Total: " + itemTotalCount.ToString();
        }

        private void AddtoCart()
        {
            int quantity;
            bool itemExists = false;
            int existingItemIndex = -1;
            int existingQuantity = 0;
            if (!Int32.TryParse(ItemToAddQuantityTextBox.Text, out quantity))
            {
                MessageUserError("Quantity is not a valid number");
                return;
            }
            if (quantity < 1)
            {
                MessageUserError("There must be at least one item to add to the cart");
                return;
            }

            int serialNumber;
            if (!Int32.TryParse(SerialNumberToAddTextBox.Text, out serialNumber))
            {
                MessageUserError("Serial Number is not a valid number");
                return;
            }

            if (ValidateSelectionAndUpdateLocalQuantity(serialNumber, quantity))
            {
                int[] cartItem = new int[2];
                cartItem[0] = serialNumber;
                cartItem[1] = quantity;

                foreach (int[] item in itemsInCart)
                {
                    if (item[0] == serialNumber)
                    {
                        existingItemIndex = itemsInCart.FindIndex(foundItem => Enumerable.SequenceEqual(item, foundItem));
                        int[] existingItem = itemsInCart.Find(foundItem => Enumerable.SequenceEqual(item, foundItem));
                        existingQuantity = existingItem[1];
                        itemExists = true;
                    }
                }

                if (itemExists)
                {
                    itemsInCart.RemoveAt(existingItemIndex);
                    itemsInCart.Add(new int[] { serialNumber, existingQuantity + quantity });
                }
                else
                {
                    itemsInCart.Add(cartItem);
                }

                SerialNumberToAddTextBox.Text = "";
                ItemToAddQuantityTextBox.Text = "";
                UpdateItemListBox();
                UpdateCartLabel();
            }
        }

        /// <summary>
        /// Validates and updates the local quantity of items available based on what is in the cart
        /// </summary>
        /// <param name="serialNumber">The serial number of the item to update</param>
        /// <param name="quantity">The quantity of the item to update</param>
        /// <returns>If the update was executed successfully</returns>
        public bool ValidateSelectionAndUpdateLocalQuantity(int serialNumber, int quantity)
        {
            foreach(Item thisItem in itemsInSystem)
            {
                if (thisItem.SerialNumber == serialNumber)
                {
                    if (thisItem.Quantity >= quantity)
                    {
                        thisItem.Quantity -= quantity;
                        return true;
                    }
                    else
                    {
                        MessageUserError("There is not enough stock for the specified quantity");
                        return false;
                    }
                }
            }
            MessageUserError("The serial number provided was not found in inventory");
            return false;
        }

        private void SetupUser()
        {
            int userIDToGet;
            if (!Int32.TryParse(MemberUserIDLookupTextBox.Text, out userIDToGet))
            {
                MessageUserError("The ID is not a valid number");
                return;
            }

            if (userIDToGet < 0)
            {
                MessageUserError("A member ID must be greater than -1");
                return;
            }

            userOfCurrentCart = theUserController.GetUserForCartByID(userIDToGet);
            if (userOfCurrentCart.RoleID != 3)
            {
                MessageUserError("The selected memberID does not qualify to use a cart");
                userOfCurrentCart = null;
                return;
            }

            if (userOfCurrentCart == null)
            {
                MessageUserError("No user was found with the selected memberID.");
            }
            else
            {
                CreateNewCart();
            }

        }

        private void CreateMemberCartButton_Click(object sender, EventArgs e)
        {
            SetupUser();
        }

        private void ClearMemberCartButton_Click(object sender, EventArgs e)
        {
            ResetCart();
        }

        private void AddToCartButton_Click(object sender, EventArgs e)
        {
            AddtoCart();
        }

        private void ReviewCartButton_Click(object sender, EventArgs e)
        {
            ShowCartReview();
        }
    }
}
