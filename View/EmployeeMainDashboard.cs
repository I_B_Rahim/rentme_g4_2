﻿using RentMe_G4.Model;
using System;
using System.Windows.Forms;

namespace RentMe_G4.View
{
    /// <summary>
    /// This is the view for the employee's main dashboard
    /// </summary>
    public partial class EmployeeMainDashboard : Form
    {
        public LoginForm theLoginForm { get; set; }
        public Employee theEmployee { get; set; }

        /// <summary>
        /// Constructor used to initialize the class
        /// </summary>
        /// <param name="theEmployee">The employee currently using the dashboard</param>
        public EmployeeMainDashboard(Employee theEmployee)
        {
            InitializeComponent();
            this.theEmployee = theEmployee;
            employeeLogoutUserControl1.theMainDashboard = this;
            employeeLogoutUserControl1.DisplayValidUserName();
            try
            {
                ReturnItemsForm.employeeID = this.theEmployee.UserID;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        /// <summary>
        /// Sets this MainForm's reference to the login form, used to ensure program logs out and/or closes succesfully.
        /// </summary>
        /// <param name="theLoginForm">the LoginForm this MainForm will reference.</param>
        public void SetTheLoginForm(LoginForm theLoginForm)
        {
            this.theLoginForm = theLoginForm;
        }

        private void logOutLinedLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ReturnToLoginForm();
        }

        private void ReturnToLoginForm()
        {
            theEmployee = null;
            theLoginForm.ResetLoginForm();
            Hide();
        }

        private void MainDashboard_Closing(object sender, FormClosingEventArgs e)
        {
            theLoginForm.Close();
        }

        private void memberSearchUserControl3_Load(object sender, EventArgs e)
        {

        }

        private void SetMemberIDForRentalsAndReturnsHistory(object sender, EventArgs e)
        {
            try
            {
                if (employeeTabControl.SelectedTab == employeeTabControl.TabPages["rentals"])
                {
                    memberRentalsUserControl1.memberID = memberSearchUserControl3.memberSearchMemberID;
                    memberRentalsUserControl1.SetUpMemberRentalFieldsAndDataGridView();
                }
                else if (employeeTabControl.SelectedTab == employeeTabControl.TabPages["returnsTabPage"])
                {
                    memberReturnsUserControl1.memberID = memberSearchUserControl3.memberSearchMemberID;
                    memberReturnsUserControl1.SetUpMemberReturnsFieldsAndDataGridView();
                }
                else if (employeeTabControl.SelectedTab == employeeTabControl.TabPages["searchMember"])
                {
                    memberRentalsUserControl1.ClearListViewItems();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }
    }
}
