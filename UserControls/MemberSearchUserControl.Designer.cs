﻿namespace RentMe_G4.UserControls
{
    partial class MemberSearchUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label userIDLabel;
            System.Windows.Forms.Label date_of_birthLabel;
            System.Windows.Forms.Label first_nameLabel;
            System.Windows.Forms.Label last_nameLabel;
            System.Windows.Forms.Label phone_numberLabel;
            System.Windows.Forms.Label address1Label;
            System.Windows.Forms.Label address2Label;
            System.Windows.Forms.Label zipLabel;
            System.Windows.Forms.Label sexLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MemberSearchUserControl));
            this.userIDTextBox = new System.Windows.Forms.TextBox();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._cs6232_g4DataSet = new RentMe_G4._cs6232_g4DataSet();
            this.date_of_birthTextBox = new System.Windows.Forms.TextBox();
            this.first_nameTextBox = new System.Windows.Forms.TextBox();
            this.last_nameTextBox = new System.Windows.Forms.TextBox();
            this.phone_numberTextBox = new System.Windows.Forms.TextBox();
            this.address1TextBox = new System.Windows.Forms.TextBox();
            this.address2TextBox = new System.Windows.Forms.TextBox();
            this.zipTextBox = new System.Windows.Forms.TextBox();
            this.fillByToolStrip = new System.Windows.Forms.ToolStrip();
            this.userIDToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.userIDToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.fillByToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.fillByPhoneToolStrip = new System.Windows.Forms.ToolStrip();
            this.phone_numberToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.phone_numberToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.fillByPhoneToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.fillByNameToolStrip = new System.Windows.Forms.ToolStrip();
            this.nameToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.nameToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.fillByNameToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.editUserButton = new System.Windows.Forms.Button();
            this.addUserButton = new System.Windows.Forms.Button();
            this.sexTextBox = new System.Windows.Forms.TextBox();
            this.MemberInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.dateOfBirthFormat = new System.Windows.Forms.Label();
            this.searchCriteriaGroupBox = new System.Windows.Forms.GroupBox();
            this.phoneFormatLabel = new System.Windows.Forms.Label();
            this.nameFormatLabel = new System.Windows.Forms.Label();
            this.userTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.userTableAdapter();
            this.tableAdapterManager = new RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager();
            this.roleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.membersDataGridView = new System.Windows.Forms.DataGridView();
            this.userBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.memberLabel1 = new System.Windows.Forms.Label();
            this.memberLabel2 = new System.Windows.Forms.Label();
            this.memberLabel3 = new System.Windows.Forms.Label();
            this.userBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            userIDLabel = new System.Windows.Forms.Label();
            date_of_birthLabel = new System.Windows.Forms.Label();
            first_nameLabel = new System.Windows.Forms.Label();
            last_nameLabel = new System.Windows.Forms.Label();
            phone_numberLabel = new System.Windows.Forms.Label();
            address1Label = new System.Windows.Forms.Label();
            address2Label = new System.Windows.Forms.Label();
            zipLabel = new System.Windows.Forms.Label();
            sexLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).BeginInit();
            this.fillByToolStrip.SuspendLayout();
            this.fillByPhoneToolStrip.SuspendLayout();
            this.fillByNameToolStrip.SuspendLayout();
            this.MemberInfoGroupBox.SuspendLayout();
            this.searchCriteriaGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.roleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.membersDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingNavigator)).BeginInit();
            this.userBindingNavigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // userIDLabel
            // 
            userIDLabel.AutoSize = true;
            userIDLabel.Location = new System.Drawing.Point(16, 22);
            userIDLabel.Name = "userIDLabel";
            userIDLabel.Size = new System.Drawing.Size(21, 13);
            userIDLabel.TabIndex = 1;
            userIDLabel.Text = "ID:";
            // 
            // date_of_birthLabel
            // 
            date_of_birthLabel.AutoSize = true;
            date_of_birthLabel.Location = new System.Drawing.Point(428, 49);
            date_of_birthLabel.Name = "date_of_birthLabel";
            date_of_birthLabel.Size = new System.Drawing.Size(69, 13);
            date_of_birthLabel.TabIndex = 5;
            date_of_birthLabel.Text = "Date of Birth:";
            // 
            // first_nameLabel
            // 
            first_nameLabel.AutoSize = true;
            first_nameLabel.Location = new System.Drawing.Point(16, 48);
            first_nameLabel.Name = "first_nameLabel";
            first_nameLabel.Size = new System.Drawing.Size(58, 13);
            first_nameLabel.TabIndex = 7;
            first_nameLabel.Text = "First name:";
            // 
            // last_nameLabel
            // 
            last_nameLabel.AutoSize = true;
            last_nameLabel.Location = new System.Drawing.Point(222, 47);
            last_nameLabel.Name = "last_nameLabel";
            last_nameLabel.Size = new System.Drawing.Size(59, 13);
            last_nameLabel.TabIndex = 9;
            last_nameLabel.Text = "Last name:";
            // 
            // phone_numberLabel
            // 
            phone_numberLabel.AutoSize = true;
            phone_numberLabel.Location = new System.Drawing.Point(16, 74);
            phone_numberLabel.Name = "phone_numberLabel";
            phone_numberLabel.Size = new System.Drawing.Size(81, 13);
            phone_numberLabel.TabIndex = 11;
            phone_numberLabel.Text = "Phone Number:";
            // 
            // address1Label
            // 
            address1Label.AutoSize = true;
            address1Label.Location = new System.Drawing.Point(227, 74);
            address1Label.Name = "address1Label";
            address1Label.Size = new System.Drawing.Size(54, 13);
            address1Label.TabIndex = 13;
            address1Label.Text = "Address1:";
            // 
            // address2Label
            // 
            address2Label.AutoSize = true;
            address2Label.Location = new System.Drawing.Point(443, 71);
            address2Label.Name = "address2Label";
            address2Label.Size = new System.Drawing.Size(54, 13);
            address2Label.TabIndex = 15;
            address2Label.Text = "Address2:";
            // 
            // zipLabel
            // 
            zipLabel.AutoSize = true;
            zipLabel.Location = new System.Drawing.Point(16, 100);
            zipLabel.Name = "zipLabel";
            zipLabel.Size = new System.Drawing.Size(25, 13);
            zipLabel.TabIndex = 17;
            zipLabel.Text = "Zip:";
            // 
            // sexLabel
            // 
            sexLabel.AutoSize = true;
            sexLabel.Location = new System.Drawing.Point(227, 97);
            sexLabel.Name = "sexLabel";
            sexLabel.Size = new System.Drawing.Size(26, 13);
            sexLabel.TabIndex = 25;
            sexLabel.Text = "sex:";
            // 
            // userIDTextBox
            // 
            this.userIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "userID", true));
            this.userIDTextBox.Location = new System.Drawing.Point(100, 19);
            this.userIDTextBox.Name = "userIDTextBox";
            this.userIDTextBox.ReadOnly = true;
            this.userIDTextBox.Size = new System.Drawing.Size(52, 20);
            this.userIDTextBox.TabIndex = 2;
            this.userIDTextBox.TextChanged += new System.EventHandler(this.UserIDTextBox_TextChanged);
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataMember = "user";
            this.userBindingSource.DataSource = this._cs6232_g4DataSet;
            // 
            // _cs6232_g4DataSet
            // 
            this._cs6232_g4DataSet.DataSetName = "_cs6232_g4DataSet";
            this._cs6232_g4DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // date_of_birthTextBox
            // 
            this.date_of_birthTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "date_of_birth", true));
            this.date_of_birthTextBox.Location = new System.Drawing.Point(512, 45);
            this.date_of_birthTextBox.Name = "date_of_birthTextBox";
            this.date_of_birthTextBox.ReadOnly = true;
            this.date_of_birthTextBox.Size = new System.Drawing.Size(110, 20);
            this.date_of_birthTextBox.TabIndex = 6;
            // 
            // first_nameTextBox
            // 
            this.first_nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "first_name", true));
            this.first_nameTextBox.Location = new System.Drawing.Point(100, 45);
            this.first_nameTextBox.Name = "first_nameTextBox";
            this.first_nameTextBox.ReadOnly = true;
            this.first_nameTextBox.Size = new System.Drawing.Size(100, 20);
            this.first_nameTextBox.TabIndex = 8;
            // 
            // last_nameTextBox
            // 
            this.last_nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "last_name", true));
            this.last_nameTextBox.Location = new System.Drawing.Point(311, 44);
            this.last_nameTextBox.Name = "last_nameTextBox";
            this.last_nameTextBox.ReadOnly = true;
            this.last_nameTextBox.Size = new System.Drawing.Size(100, 20);
            this.last_nameTextBox.TabIndex = 10;
            // 
            // phone_numberTextBox
            // 
            this.phone_numberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "phone_number", true));
            this.phone_numberTextBox.Location = new System.Drawing.Point(100, 71);
            this.phone_numberTextBox.Name = "phone_numberTextBox";
            this.phone_numberTextBox.ReadOnly = true;
            this.phone_numberTextBox.Size = new System.Drawing.Size(100, 20);
            this.phone_numberTextBox.TabIndex = 12;
            // 
            // address1TextBox
            // 
            this.address1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "address1", true));
            this.address1TextBox.Location = new System.Drawing.Point(311, 71);
            this.address1TextBox.Name = "address1TextBox";
            this.address1TextBox.ReadOnly = true;
            this.address1TextBox.Size = new System.Drawing.Size(100, 20);
            this.address1TextBox.TabIndex = 14;
            // 
            // address2TextBox
            // 
            this.address2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "address2", true));
            this.address2TextBox.Location = new System.Drawing.Point(512, 73);
            this.address2TextBox.Name = "address2TextBox";
            this.address2TextBox.ReadOnly = true;
            this.address2TextBox.Size = new System.Drawing.Size(110, 20);
            this.address2TextBox.TabIndex = 16;
            // 
            // zipTextBox
            // 
            this.zipTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "zip", true));
            this.zipTextBox.Location = new System.Drawing.Point(100, 97);
            this.zipTextBox.Name = "zipTextBox";
            this.zipTextBox.ReadOnly = true;
            this.zipTextBox.Size = new System.Drawing.Size(100, 20);
            this.zipTextBox.TabIndex = 18;
            // 
            // fillByToolStrip
            // 
            this.fillByToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.fillByToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userIDToolStripLabel,
            this.userIDToolStripTextBox,
            this.fillByToolStripButton});
            this.fillByToolStrip.Location = new System.Drawing.Point(5, 16);
            this.fillByToolStrip.Name = "fillByToolStrip";
            this.fillByToolStrip.Size = new System.Drawing.Size(207, 25);
            this.fillByToolStrip.TabIndex = 21;
            this.fillByToolStrip.Text = "fillByToolStrip";
            // 
            // userIDToolStripLabel
            // 
            this.userIDToolStripLabel.Name = "userIDToolStripLabel";
            this.userIDToolStripLabel.Size = new System.Drawing.Size(47, 22);
            this.userIDToolStripLabel.Text = "User ID:";
            // 
            // userIDToolStripTextBox
            // 
            this.userIDToolStripTextBox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.userIDToolStripTextBox.Name = "userIDToolStripTextBox";
            this.userIDToolStripTextBox.Size = new System.Drawing.Size(100, 25);
            this.userIDToolStripTextBox.Click += new System.EventHandler(this.ClearSearchFields);
            // 
            // fillByToolStripButton
            // 
            this.fillByToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.fillByToolStripButton.Name = "fillByToolStripButton";
            this.fillByToolStripButton.Size = new System.Drawing.Size(46, 22);
            this.fillByToolStripButton.Text = "Search";
            this.fillByToolStripButton.Click += new System.EventHandler(this.FillByUserToolStripButton_Click);
            // 
            // fillByPhoneToolStrip
            // 
            this.fillByPhoneToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.fillByPhoneToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.phone_numberToolStripLabel,
            this.phone_numberToolStripTextBox,
            this.fillByPhoneToolStripButton});
            this.fillByPhoneToolStrip.Location = new System.Drawing.Point(237, 16);
            this.fillByPhoneToolStrip.Name = "fillByPhoneToolStrip";
            this.fillByPhoneToolStrip.Size = new System.Drawing.Size(214, 25);
            this.fillByPhoneToolStrip.TabIndex = 22;
            this.fillByPhoneToolStrip.Text = "fillByPhoneToolStrip";
            // 
            // phone_numberToolStripLabel
            // 
            this.phone_numberToolStripLabel.Name = "phone_numberToolStripLabel";
            this.phone_numberToolStripLabel.Size = new System.Drawing.Size(54, 22);
            this.phone_numberToolStripLabel.Text = "Phone #:";
            // 
            // phone_numberToolStripTextBox
            // 
            this.phone_numberToolStripTextBox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.phone_numberToolStripTextBox.Name = "phone_numberToolStripTextBox";
            this.phone_numberToolStripTextBox.Size = new System.Drawing.Size(100, 25);
            this.phone_numberToolStripTextBox.Click += new System.EventHandler(this.ClearSearchFields);
            // 
            // fillByPhoneToolStripButton
            // 
            this.fillByPhoneToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.fillByPhoneToolStripButton.Name = "fillByPhoneToolStripButton";
            this.fillByPhoneToolStripButton.Size = new System.Drawing.Size(46, 22);
            this.fillByPhoneToolStripButton.Text = "Search";
            this.fillByPhoneToolStripButton.Click += new System.EventHandler(this.FillByPhoneToolStripButton_Click);
            // 
            // fillByNameToolStrip
            // 
            this.fillByNameToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.fillByNameToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nameToolStripLabel,
            this.nameToolStripTextBox,
            this.fillByNameToolStripButton});
            this.fillByNameToolStrip.Location = new System.Drawing.Point(479, 16);
            this.fillByNameToolStrip.Name = "fillByNameToolStrip";
            this.fillByNameToolStrip.Size = new System.Drawing.Size(202, 25);
            this.fillByNameToolStrip.TabIndex = 23;
            this.fillByNameToolStrip.Text = "fillByNameToolStrip";
            // 
            // nameToolStripLabel
            // 
            this.nameToolStripLabel.Name = "nameToolStripLabel";
            this.nameToolStripLabel.Size = new System.Drawing.Size(42, 22);
            this.nameToolStripLabel.Text = "Name:";
            // 
            // nameToolStripTextBox
            // 
            this.nameToolStripTextBox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.nameToolStripTextBox.Name = "nameToolStripTextBox";
            this.nameToolStripTextBox.Size = new System.Drawing.Size(100, 25);
            this.nameToolStripTextBox.Click += new System.EventHandler(this.ClearSearchFields);
            // 
            // fillByNameToolStripButton
            // 
            this.fillByNameToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.fillByNameToolStripButton.Name = "fillByNameToolStripButton";
            this.fillByNameToolStripButton.Size = new System.Drawing.Size(46, 22);
            this.fillByNameToolStripButton.Text = "Search";
            this.fillByNameToolStripButton.Click += new System.EventHandler(this.FillByNameToolStripButton_Click);
            // 
            // editUserButton
            // 
            this.editUserButton.Location = new System.Drawing.Point(675, 141);
            this.editUserButton.Name = "editUserButton";
            this.editUserButton.Size = new System.Drawing.Size(75, 23);
            this.editUserButton.TabIndex = 4;
            this.editUserButton.Text = "Edit Member";
            this.editUserButton.UseVisualStyleBackColor = true;
            this.editUserButton.Click += new System.EventHandler(this.EditUserbutton_Click);
            // 
            // addUserButton
            // 
            this.addUserButton.Location = new System.Drawing.Point(675, 187);
            this.addUserButton.Name = "addUserButton";
            this.addUserButton.Size = new System.Drawing.Size(75, 23);
            this.addUserButton.TabIndex = 5;
            this.addUserButton.Text = "Add Member";
            this.addUserButton.UseVisualStyleBackColor = true;
            this.addUserButton.Click += new System.EventHandler(this.AddUserButton_Click);
            // 
            // sexTextBox
            // 
            this.sexTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "sex", true));
            this.sexTextBox.Location = new System.Drawing.Point(311, 97);
            this.sexTextBox.Name = "sexTextBox";
            this.sexTextBox.ReadOnly = true;
            this.sexTextBox.Size = new System.Drawing.Size(100, 20);
            this.sexTextBox.TabIndex = 26;
            // 
            // MemberInfoGroupBox
            // 
            this.MemberInfoGroupBox.Controls.Add(this.dateOfBirthFormat);
            this.MemberInfoGroupBox.Controls.Add(this.userIDTextBox);
            this.MemberInfoGroupBox.Controls.Add(sexLabel);
            this.MemberInfoGroupBox.Controls.Add(this.zipTextBox);
            this.MemberInfoGroupBox.Controls.Add(this.sexTextBox);
            this.MemberInfoGroupBox.Controls.Add(zipLabel);
            this.MemberInfoGroupBox.Controls.Add(this.phone_numberTextBox);
            this.MemberInfoGroupBox.Controls.Add(date_of_birthLabel);
            this.MemberInfoGroupBox.Controls.Add(phone_numberLabel);
            this.MemberInfoGroupBox.Controls.Add(this.date_of_birthTextBox);
            this.MemberInfoGroupBox.Controls.Add(address2Label);
            this.MemberInfoGroupBox.Controls.Add(this.first_nameTextBox);
            this.MemberInfoGroupBox.Controls.Add(this.address2TextBox);
            this.MemberInfoGroupBox.Controls.Add(first_nameLabel);
            this.MemberInfoGroupBox.Controls.Add(userIDLabel);
            this.MemberInfoGroupBox.Controls.Add(this.last_nameTextBox);
            this.MemberInfoGroupBox.Controls.Add(this.address1TextBox);
            this.MemberInfoGroupBox.Controls.Add(last_nameLabel);
            this.MemberInfoGroupBox.Controls.Add(address1Label);
            this.MemberInfoGroupBox.Location = new System.Drawing.Point(26, 92);
            this.MemberInfoGroupBox.Name = "MemberInfoGroupBox";
            this.MemberInfoGroupBox.Size = new System.Drawing.Size(643, 119);
            this.MemberInfoGroupBox.TabIndex = 27;
            this.MemberInfoGroupBox.TabStop = false;
            this.MemberInfoGroupBox.Text = "Member Info.";
            // 
            // dateOfBirthFormat
            // 
            this.dateOfBirthFormat.AutoSize = true;
            this.dateOfBirthFormat.Location = new System.Drawing.Point(513, 22);
            this.dateOfBirthFormat.Name = "dateOfBirthFormat";
            this.dateOfBirthFormat.Size = new System.Drawing.Size(109, 13);
            this.dateOfBirthFormat.TabIndex = 30;
            this.dateOfBirthFormat.Text = "(Format: mm/dd/yyyy)";
            // 
            // searchCriteriaGroupBox
            // 
            this.searchCriteriaGroupBox.Controls.Add(this.phoneFormatLabel);
            this.searchCriteriaGroupBox.Controls.Add(this.nameFormatLabel);
            this.searchCriteriaGroupBox.Controls.Add(this.fillByToolStrip);
            this.searchCriteriaGroupBox.Controls.Add(this.fillByPhoneToolStrip);
            this.searchCriteriaGroupBox.Controls.Add(this.fillByNameToolStrip);
            this.searchCriteriaGroupBox.Location = new System.Drawing.Point(13, 3);
            this.searchCriteriaGroupBox.Name = "searchCriteriaGroupBox";
            this.searchCriteriaGroupBox.Size = new System.Drawing.Size(733, 58);
            this.searchCriteriaGroupBox.TabIndex = 28;
            this.searchCriteriaGroupBox.TabStop = false;
            this.searchCriteriaGroupBox.Text = "Search Criteria";
            // 
            // phoneFormatLabel
            // 
            this.phoneFormatLabel.AutoSize = true;
            this.phoneFormatLabel.Location = new System.Drawing.Point(300, 41);
            this.phoneFormatLabel.Name = "phoneFormatLabel";
            this.phoneFormatLabel.Size = new System.Drawing.Size(111, 13);
            this.phoneFormatLabel.TabIndex = 32;
            this.phoneFormatLabel.Text = "(Format: 8889996666)";
            // 
            // nameFormatLabel
            // 
            this.nameFormatLabel.AutoSize = true;
            this.nameFormatLabel.Location = new System.Drawing.Point(533, 41);
            this.nameFormatLabel.Name = "nameFormatLabel";
            this.nameFormatLabel.Size = new System.Drawing.Size(118, 13);
            this.nameFormatLabel.TabIndex = 31;
            this.nameFormatLabel.Text = "(Format: Lname Fname)";
            // 
            // userTableAdapter
            // 
            this.userTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.categoryTableAdapter = null;
            this.tableAdapterManager.furniture_itemTableAdapter = null;
            this.tableAdapterManager.locationTableAdapter = null;
            this.tableAdapterManager.rental_transactionTableAdapter = null;
            this.tableAdapterManager.roleTableAdapter = null;
            this.tableAdapterManager.stateTableAdapter = null;
            this.tableAdapterManager.styleTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.userTableAdapter = this.userTableAdapter;
            // 
            // membersDataGridView
            // 
            this.membersDataGridView.AllowUserToAddRows = false;
            this.membersDataGridView.AllowUserToDeleteRows = false;
            this.membersDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.membersDataGridView.Location = new System.Drawing.Point(89, 219);
            this.membersDataGridView.Name = "membersDataGridView";
            this.membersDataGridView.ReadOnly = true;
            this.membersDataGridView.Size = new System.Drawing.Size(580, 94);
            this.membersDataGridView.TabIndex = 28;
            // 
            // memberLabel1
            // 
            this.memberLabel1.AutoSize = true;
            this.memberLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memberLabel1.Location = new System.Drawing.Point(29, 219);
            this.memberLabel1.Name = "memberLabel1";
            this.memberLabel1.Size = new System.Drawing.Size(35, 17);
            this.memberLabel1.TabIndex = 29;
            this.memberLabel1.Text = "Our";
            // 
            // memberLabel2
            // 
            this.memberLabel2.AutoSize = true;
            this.memberLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memberLabel2.Location = new System.Drawing.Point(15, 247);
            this.memberLabel2.Name = "memberLabel2";
            this.memberLabel2.Size = new System.Drawing.Size(58, 17);
            this.memberLabel2.TabIndex = 30;
            this.memberLabel2.Text = "Valued";
            // 
            // memberLabel3
            // 
            this.memberLabel3.AutoSize = true;
            this.memberLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memberLabel3.Location = new System.Drawing.Point(10, 274);
            this.memberLabel3.Name = "memberLabel3";
            this.memberLabel3.Size = new System.Drawing.Size(73, 17);
            this.memberLabel3.TabIndex = 31;
            this.memberLabel3.Text = "Members";
            // 
            // userBindingNavigator
            // 
            this.userBindingNavigator.AddNewItem = null;
            this.userBindingNavigator.BindingSource = this.userBindingSource;
            this.userBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.userBindingNavigator.CountItemFormat = "of {0} Members Found";
            this.userBindingNavigator.DeleteItem = null;
            this.userBindingNavigator.Dock = System.Windows.Forms.DockStyle.None;
            this.userBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem});
            this.userBindingNavigator.Location = new System.Drawing.Point(221, 64);
            this.userBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.userBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.userBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.userBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.userBindingNavigator.Name = "userBindingNavigator";
            this.userBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.userBindingNavigator.Size = new System.Drawing.Size(293, 25);
            this.userBindingNavigator.TabIndex = 32;
            this.userBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(125, 22);
            this.bindingNavigatorCountItem.Text = "of {0} Members Found";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // MemberSearchUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.userBindingNavigator);
            this.Controls.Add(this.memberLabel3);
            this.Controls.Add(this.memberLabel2);
            this.Controls.Add(this.memberLabel1);
            this.Controls.Add(this.membersDataGridView);
            this.Controls.Add(this.searchCriteriaGroupBox);
            this.Controls.Add(this.MemberInfoGroupBox);
            this.Controls.Add(this.addUserButton);
            this.Controls.Add(this.editUserButton);
            this.Name = "MemberSearchUserControl";
            this.Size = new System.Drawing.Size(766, 320);
            this.Load += new System.EventHandler(this.MemberSearchUserControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).EndInit();
            this.fillByToolStrip.ResumeLayout(false);
            this.fillByToolStrip.PerformLayout();
            this.fillByPhoneToolStrip.ResumeLayout(false);
            this.fillByPhoneToolStrip.PerformLayout();
            this.fillByNameToolStrip.ResumeLayout(false);
            this.fillByNameToolStrip.PerformLayout();
            this.MemberInfoGroupBox.ResumeLayout(false);
            this.MemberInfoGroupBox.PerformLayout();
            this.searchCriteriaGroupBox.ResumeLayout(false);
            this.searchCriteriaGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.roleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.membersDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingNavigator)).EndInit();
            this.userBindingNavigator.ResumeLayout(false);
            this.userBindingNavigator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private _cs6232_g4DataSet _cs6232_g4DataSet;
        private System.Windows.Forms.BindingSource userBindingSource;
        private _cs6232_g4DataSetTableAdapters.userTableAdapter userTableAdapter;
        private _cs6232_g4DataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox userIDTextBox;
        private System.Windows.Forms.TextBox date_of_birthTextBox;
        private System.Windows.Forms.TextBox first_nameTextBox;
        private System.Windows.Forms.TextBox last_nameTextBox;
        private System.Windows.Forms.TextBox phone_numberTextBox;
        private System.Windows.Forms.TextBox address1TextBox;
        private System.Windows.Forms.TextBox address2TextBox;
        private System.Windows.Forms.TextBox zipTextBox;
        private System.Windows.Forms.ToolStrip fillByToolStrip;
        private System.Windows.Forms.ToolStripLabel userIDToolStripLabel;
        private System.Windows.Forms.ToolStripTextBox userIDToolStripTextBox;
        private System.Windows.Forms.ToolStripButton fillByToolStripButton;
        private System.Windows.Forms.ToolStrip fillByPhoneToolStrip;
        private System.Windows.Forms.ToolStripLabel phone_numberToolStripLabel;
        private System.Windows.Forms.ToolStripTextBox phone_numberToolStripTextBox;
        private System.Windows.Forms.ToolStripButton fillByPhoneToolStripButton;
        private System.Windows.Forms.ToolStrip fillByNameToolStrip;
        private System.Windows.Forms.ToolStripLabel nameToolStripLabel;
        private System.Windows.Forms.ToolStripTextBox nameToolStripTextBox;
        private System.Windows.Forms.ToolStripButton fillByNameToolStripButton;
        private System.Windows.Forms.Button editUserButton;
        private System.Windows.Forms.Button addUserButton;
        private System.Windows.Forms.TextBox sexTextBox;
        private System.Windows.Forms.GroupBox MemberInfoGroupBox;
        private System.Windows.Forms.Label dateOfBirthFormat;
        private System.Windows.Forms.GroupBox searchCriteriaGroupBox;
        private System.Windows.Forms.BindingSource roleBindingSource;
        private System.Windows.Forms.Label phoneFormatLabel;
        private System.Windows.Forms.Label nameFormatLabel;
        private System.Windows.Forms.BindingSource userBindingSource1;
        private System.Windows.Forms.DataGridView membersDataGridView;
        private System.Windows.Forms.Label memberLabel1;
        private System.Windows.Forms.Label memberLabel2;
        private System.Windows.Forms.Label memberLabel3;
        private System.Windows.Forms.BindingNavigator userBindingNavigator;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
    }
}
