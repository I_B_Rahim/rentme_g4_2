﻿using System;
using System.Windows.Forms;
using RentMe_G4.View;

namespace RentMe_G4.UserControls
{
    /// <summary>
    /// User control for the logout panel
    /// </summary>
    public partial class EmployeeLogoutUserControl : UserControl
    {
        public EmployeeMainDashboard theMainDashboard { get; set; }

        /// <summary>
        /// Default Constructor for EmployeeLogin.
        /// </summary>
        public EmployeeLogoutUserControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Show the loggedin username in the MainDashboard
        /// </summary>
        public void DisplayValidUserName()
        {
            if (theMainDashboard.theEmployee.UserName == null)
            {
                MessageBox.Show("A user must be logged in to use this form.");
            }
            else
            {
                UserMessageLabel.Text = theMainDashboard.theEmployee.FirstName + " "  + theMainDashboard.theEmployee.LastName + ", logged in as " + theMainDashboard.theEmployee.UserName;
            }
        }

        private void ReturnToLoginForm()
        {
            theMainDashboard.theEmployee = null;
            theMainDashboard.theLoginForm.ResetLoginForm();
            theMainDashboard.Hide();
        }

        private void LogoutButton_Click(object sender, EventArgs e)
        {
            ReturnToLoginForm();
        }
    }
}
