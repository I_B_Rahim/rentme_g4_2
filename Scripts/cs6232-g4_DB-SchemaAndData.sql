﻿USE [master]
GO
/****** Object:  Database [cs6232-g4]    Script Date: 4/26/2020 2:08:49 PM ******/
CREATE DATABASE [cs6232-g4]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'cs6232-g4', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\cs6232-g4.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'cs6232-g4_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\cs6232-g4_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [cs6232-g4] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [cs6232-g4].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [cs6232-g4] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [cs6232-g4] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [cs6232-g4] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [cs6232-g4] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [cs6232-g4] SET ARITHABORT OFF 
GO
ALTER DATABASE [cs6232-g4] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [cs6232-g4] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [cs6232-g4] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [cs6232-g4] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [cs6232-g4] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [cs6232-g4] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [cs6232-g4] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [cs6232-g4] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [cs6232-g4] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [cs6232-g4] SET  DISABLE_BROKER 
GO
ALTER DATABASE [cs6232-g4] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [cs6232-g4] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [cs6232-g4] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [cs6232-g4] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [cs6232-g4] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [cs6232-g4] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [cs6232-g4] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [cs6232-g4] SET RECOVERY FULL 
GO
ALTER DATABASE [cs6232-g4] SET  MULTI_USER 
GO
ALTER DATABASE [cs6232-g4] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [cs6232-g4] SET DB_CHAINING OFF 
GO
ALTER DATABASE [cs6232-g4] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [cs6232-g4] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [cs6232-g4] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'cs6232-g4', N'ON'
GO
ALTER DATABASE [cs6232-g4] SET QUERY_STORE = OFF
GO
USE [cs6232-g4]
GO
/****** Object:  UserDefinedFunction [dbo].[getReturnAmountDue]    Script Date: 4/26/2020 2:08:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jake Spain
-- Create date: 2/23/2020
-- Description:	Calculates the amount due for a returned item
-- =============================================
CREATE FUNCTION [dbo].[getReturnAmountDue] 
(
	-- Add the parameters for the function here
	@transactionID int,
	@serial_number int,
	@return_date datetime,
	@quantity int
)
RETURNS decimal(9, 2)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @due_date datetime
	DECLARE @return_days_difference int
	DECLARE @daily_fine_rate decimal(9, 2)
	DECLARE @amount_due decimal(9, 2)

	-- Add the T-SQL statements to compute the return value here
	SET @due_date = (
	  SELECT due_date
      FROM rental
      WHERE transactionID = @transactionID
      AND serial_number = @serial_number
	)

	SET @return_days_difference = DATEDIFF(DAY, @due_date, @return_date)

	SET @daily_fine_rate = (
	  SELECT daily_fine_rate
      FROM furniture_kind fk
      JOIN furniture_item fi
      ON fk.furnitureID = fi.furnitureID
      WHERE fi.serial_number = @serial_number
	)

	SET @amount_due = @return_days_difference * @daily_fine_rate * @quantity

	-- Return the result of the function
	RETURN @amount_due

END
GO
/****** Object:  Table [dbo].[category]    Script Date: 4/26/2020 2:08:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[category](
	[categoryID] [int] IDENTITY(1,1) NOT NULL,
	[category_name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_category] PRIMARY KEY CLUSTERED 
(
	[categoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[credential]    Script Date: 4/26/2020 2:08:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[credential](
	[userID] [int] NOT NULL,
	[username] [varchar](50) NOT NULL,
	[password] [varchar](100) NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_credential] PRIMARY KEY CLUSTERED 
(
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[furniture_item]    Script Date: 4/26/2020 2:08:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[furniture_item](
	[serial_number] [int] NOT NULL,
	[description] [varchar](50) NOT NULL,
	[categoryID] [int] NOT NULL,
	[styleID] [int] NOT NULL,
	[daily_rental_rate] [decimal](9, 2) NOT NULL,
	[daily_fine_rate] [decimal](9, 2) NOT NULL,
	[quantity] [int] NOT NULL,
 CONSTRAINT [PK_furniture_item] PRIMARY KEY CLUSTERED 
(
	[serial_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[location]    Script Date: 4/26/2020 2:08:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[location](
	[zip] [char](5) NOT NULL,
	[city] [varchar](50) NOT NULL,
	[state] [char](2) NOT NULL,
 CONSTRAINT [PK_location] PRIMARY KEY CLUSTERED 
(
	[zip] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rental_item]    Script Date: 4/26/2020 2:08:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rental_item](
	[itemID] [int] IDENTITY(1,1) NOT NULL,
	[rentalID] [int] NOT NULL,
	[serial_number] [int] NOT NULL,
	[quantity] [int] NOT NULL,
 CONSTRAINT [PK_rental_item] PRIMARY KEY CLUSTERED 
(
	[itemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rental_transaction]    Script Date: 4/26/2020 2:08:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rental_transaction](
	[rentalID] [int] IDENTITY(1,1) NOT NULL,
	[member_userID] [int] NOT NULL,
	[employee_userID] [int] NOT NULL,
	[rental_date] [datetime] NOT NULL,
	[due_date] [date] NOT NULL,
 CONSTRAINT [PK_rental_transaction] PRIMARY KEY CLUSTERED 
(
	[rentalID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[return_item]    Script Date: 4/26/2020 2:08:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[return_item](
	[itemID] [int] NOT NULL,
	[returnID] [int] NOT NULL,
	[quantity] [int] NOT NULL,
	[amount_due] [decimal](9, 2) NOT NULL,
	[amount_paid] [decimal](9, 2) NOT NULL,
	[amount_refunded] [decimal](9, 2) NOT NULL,
 CONSTRAINT [PK_return_item] PRIMARY KEY CLUSTERED 
(
	[itemID] ASC,
	[returnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[return_transaction]    Script Date: 4/26/2020 2:08:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[return_transaction](
	[returnID] [int] IDENTITY(1,1) NOT NULL,
	[employee_userID] [int] NOT NULL,
	[return_date] [datetime] NOT NULL,
 CONSTRAINT [PK_return_transaction] PRIMARY KEY CLUSTERED 
(
	[returnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[role]    Script Date: 4/26/2020 2:08:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[role](
	[roleID] [int] IDENTITY(1,1) NOT NULL,
	[type] [varchar](50) NOT NULL,
 CONSTRAINT [PK_role] PRIMARY KEY CLUSTERED 
(
	[roleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[state]    Script Date: 4/26/2020 2:08:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[state](
	[code] [char](2) NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_state] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[style]    Script Date: 4/26/2020 2:08:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[style](
	[styleID] [int] IDENTITY(1,1) NOT NULL,
	[style_type] [varchar](50) NOT NULL,
 CONSTRAINT [PK_style] PRIMARY KEY CLUSTERED 
(
	[styleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user]    Script Date: 4/26/2020 2:08:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user](
	[userID] [int] IDENTITY(1,1) NOT NULL,
	[roleID] [int] NOT NULL,
	[date_of_birth] [date] NOT NULL,
	[first_name] [varchar](50) NOT NULL,
	[last_name] [varchar](50) NOT NULL,
	[phone_number] [char](10) NOT NULL,
	[address1] [varchar](50) NOT NULL,
	[address2] [varchar](50) NULL,
	[zip] [char](5) NOT NULL,
	[sex] [char](1) NOT NULL,
 CONSTRAINT [PK_employee] PRIMARY KEY CLUSTERED 
(
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[category] ON 
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (1, N'Bean Bag')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (2, N'Bed')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (3, N'Book Shelf')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (4, N'Chair')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (5, N'Chest')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (6, N'Coat Rack')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (7, N'Couch')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (8, N'Crib')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (9, N'Desk')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (10, N'Dining Table')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (11, N'Dresser')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (12, N'Futon')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (13, N'Hammock')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (14, N'Hat Stand')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (15, N'Hutch')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (16, N'Love Seat')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (17, N'Ottoman')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (18, N'Stool')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (19, N'Vanity')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (20, N'Wine Rack')
GO
INSERT [dbo].[category] ([categoryID], [category_name]) VALUES (21, N'Workbench')
GO
SET IDENTITY_INSERT [dbo].[category] OFF
GO
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (1, N'jdoe1', N't+3h8M2HSFbd8vxyd0G3xw==', 1)
GO
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (2, N'zstuttard1', N't+3h8M2HSFbd8vxyd0G3xw==', 1)
GO
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (3, N'gscotford1', N't+3h8M2HSFbd8vxyd0G3xw==', 1)
GO
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (4, N'mpettiward1', N't+3h8M2HSFbd8vxyd0G3xw==', 1)
GO
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (5, N'djoutapaitis1', N't+3h8M2HSFbd8vxyd0G3xw==', 1)
GO
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (6, N'jweymont1', N't+3h8M2HSFbd8vxyd0G3xw==', 1)
GO
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (7, N'fcoan1', N't+3h8M2HSFbd8vxyd0G3xw==', 1)
GO
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (8, N'ocolvine1', N't+3h8M2HSFbd8vxyd0G3xw==', 1)
GO
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (9, N'klemmen1', N't+3h8M2HSFbd8vxyd0G3xw==', 1)
GO
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (10, N'dveale1', N't+3h8M2HSFbd8vxyd0G3xw==', 1)
GO
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (11, N'htibbs1', N't+3h8M2HSFbd8vxyd0G3xw==', 1)
GO
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (12, N'ebosson1', N't+3h8M2HSFbd8vxyd0G3xw==', 1)
GO
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (13, N'cpoland1', N't+3h8M2HSFbd8vxyd0G3xw==', 1)
GO
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (14, N'divanyushin1', N't+3h8M2HSFbd8vxyd0G3xw==', 1)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (5613, N'incubate strategic eyeballs', 19, 13, CAST(2.67 AS Decimal(9, 2)), CAST(3.20 AS Decimal(9, 2)), 20)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (11811, N'envisioneer wireless content', 5, 3, CAST(2.90 AS Decimal(9, 2)), CAST(3.48 AS Decimal(9, 2)), 20)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (12627, N'brand holistic functionalities', 1, 2, CAST(4.74 AS Decimal(9, 2)), CAST(5.69 AS Decimal(9, 2)), 17)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (18642, N'orchestrate one-to-one channels', 18, 14, CAST(2.50 AS Decimal(9, 2)), CAST(3.00 AS Decimal(9, 2)), 14)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (27449, N'optimize distributed networks', 13, 3, CAST(3.95 AS Decimal(9, 2)), CAST(4.74 AS Decimal(9, 2)), 5)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (29364, N'syndicate end-to-end infomediaries', 12, 6, CAST(4.59 AS Decimal(9, 2)), CAST(5.51 AS Decimal(9, 2)), 8)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (30806, N'leverage proactive markets', 1, 10, CAST(2.82 AS Decimal(9, 2)), CAST(3.38 AS Decimal(9, 2)), 17)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (32085, N'benchmark bleeding-edge portals', 19, 4, CAST(2.12 AS Decimal(9, 2)), CAST(2.54 AS Decimal(9, 2)), 20)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (34899, N'aggregate wireless bandwidth', 3, 2, CAST(3.63 AS Decimal(9, 2)), CAST(4.36 AS Decimal(9, 2)), 9)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (36270, N'disintermediate sexy ROI', 14, 3, CAST(4.24 AS Decimal(9, 2)), CAST(5.09 AS Decimal(9, 2)), 9)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (41435, N'unleash interactive infrastructures', 7, 6, CAST(2.74 AS Decimal(9, 2)), CAST(3.29 AS Decimal(9, 2)), 14)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (42295, N'leverage open-source infrastructures', 21, 7, CAST(1.47 AS Decimal(9, 2)), CAST(1.76 AS Decimal(9, 2)), 16)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (42940, N'facilitate dynamic methodologies', 11, 13, CAST(4.93 AS Decimal(9, 2)), CAST(5.92 AS Decimal(9, 2)), 9)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (42949, N'revolutionize out-of-the-box initiatives', 14, 8, CAST(3.83 AS Decimal(9, 2)), CAST(4.60 AS Decimal(9, 2)), 3)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (43459, N'seize e-business e-markets', 8, 12, CAST(3.60 AS Decimal(9, 2)), CAST(4.32 AS Decimal(9, 2)), 19)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (50116, N'harness scalable e-commerce', 15, 7, CAST(3.06 AS Decimal(9, 2)), CAST(3.67 AS Decimal(9, 2)), 5)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (50437, N'visualize strategic infrastructures', 12, 3, CAST(3.35 AS Decimal(9, 2)), CAST(4.02 AS Decimal(9, 2)), 9)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (55215, N'incentivize efficient synergies', 17, 2, CAST(4.53 AS Decimal(9, 2)), CAST(5.44 AS Decimal(9, 2)), 8)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (57538, N'drive scalable paradigms', 11, 13, CAST(3.56 AS Decimal(9, 2)), CAST(4.27 AS Decimal(9, 2)), 14)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (57591, N'syndicate robust relationships', 18, 9, CAST(4.12 AS Decimal(9, 2)), CAST(4.94 AS Decimal(9, 2)), 1)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (60005, N'brand real-time niches', 4, 5, CAST(3.97 AS Decimal(9, 2)), CAST(4.76 AS Decimal(9, 2)), 2)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (70182, N'exploit killer users', 17, 12, CAST(3.39 AS Decimal(9, 2)), CAST(4.07 AS Decimal(9, 2)), 3)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (76047, N'exploit strategic supply-chains', 1, 10, CAST(3.87 AS Decimal(9, 2)), CAST(4.64 AS Decimal(9, 2)), 14)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (83068, N'unleash bleeding-edge technologies', 7, 11, CAST(2.89 AS Decimal(9, 2)), CAST(3.47 AS Decimal(9, 2)), 11)
GO
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (93498, N'harness clicks-and-mortar experiences', 15, 6, CAST(3.10 AS Decimal(9, 2)), CAST(3.72 AS Decimal(9, 2)), 1)
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'01760', N'Natick', N'MA')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'02101', N'Boston', N'MA')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'08601', N'Trenton', N'NJ')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'12084', N'Albany', N'NY')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'17025', N'Harrisburg', N'PA')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'21401', N'Annapolis', N'MD')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'22434', N'San Diego', N'CA')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'27513', N'Raleigh', N'NC')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'29401', N'Charleston', N'WV')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'30118', N'Carrollton', N'GA')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'30301', N'Atlanta', N'GA')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'32301', N'Tallahassee', N'FL')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'32789', N'Orlando', N'FL')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'33101', N'Miami', N'FL')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'40018', N'Louisville', N'KY')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'43004', N'Columbus', N'OH')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'48864', N'Lansing', N'MI')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'65043', N'Jefferson City', N'MO')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'73008', N'Oklahoma City', N'OK')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'73301', N'Austin', N'TX')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'77001', N'Houston', N'TX')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'77351', N'Livingston', N'TX')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'85001', N'Phoenix', N'AZ')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'94016', N'San Francisco', N'CA')
GO
INSERT [dbo].[location] ([zip], [city], [state]) VALUES (N'94203', N'Sacramento', N'CA')
GO
SET IDENTITY_INSERT [dbo].[rental_item] ON 
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (1, 1, 55215, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (2, 2, 36270, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (3, 3, 42949, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (4, 4, 60005, 2)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (5, 4, 70182, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (6, 5, 57538, 5)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (7, 5, 42949, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (8, 6, 55215, 5)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (9, 7, 29364, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (10, 7, 11811, 4)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (11, 7, 60005, 2)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (12, 8, 42940, 5)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (13, 8, 36270, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (14, 9, 42940, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (15, 10, 27449, 5)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (16, 11, 36270, 4)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (17, 12, 18642, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (18, 12, 70182, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (19, 12, 93498, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (20, 13, 50116, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (21, 14, 30806, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (22, 14, 57591, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (23, 15, 18642, 4)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (24, 15, 60005, 2)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (25, 15, 5613, 5)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (26, 16, 5613, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (27, 16, 41435, 4)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (28, 17, 41435, 2)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (29, 18, 50437, 4)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (30, 18, 34899, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (31, 18, 42295, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (32, 19, 70182, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (33, 20, 43459, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (34, 20, 57538, 4)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (35, 21, 93498, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (36, 22, 50437, 4)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (37, 23, 70182, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (38, 24, 57591, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (39, 24, 41435, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (40, 25, 60005, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (41, 25, 42940, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (42, 25, 34899, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (43, 26, 5613, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (44, 26, 30806, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (45, 27, 29364, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (46, 27, 42949, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (47, 27, 42940, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (48, 28, 57538, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (49, 28, 18642, 2)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (50, 29, 42940, 2)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (51, 30, 34899, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (52, 30, 18642, 4)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (53, 30, 42940, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (54, 31, 42940, 2)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (55, 32, 5613, 5)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (56, 33, 30806, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (57, 33, 5613, 5)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (58, 33, 50116, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (59, 34, 27449, 5)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (60, 34, 41435, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (61, 35, 60005, 2)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (62, 36, 42949, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (63, 36, 29364, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (64, 36, 11811, 5)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (65, 37, 29364, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (66, 37, 50116, 5)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (67, 38, 34899, 5)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (68, 39, 11811, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (69, 40, 30806, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (70, 41, 29364, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (71, 41, 36270, 5)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (72, 41, 41435, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (73, 42, 83068, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (74, 43, 29364, 3)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (75, 44, 41435, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (76, 45, 50437, 5)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (77, 45, 57591, 1)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (78, 46, 76047, 5)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (79, 47, 30806, 5)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (80, 48, 32085, 5)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (81, 48, 76047, 5)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (82, 48, 36270, 4)
GO
INSERT [dbo].[rental_item] ([itemID], [rentalID], [serial_number], [quantity]) VALUES (83, 49, 57538, 2)
GO
SET IDENTITY_INSERT [dbo].[rental_item] OFF
GO
SET IDENTITY_INSERT [dbo].[rental_transaction] ON 
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (1, 30, 11, CAST(N'2015-03-14T00:00:00.000' AS DateTime), CAST(N'2015-04-14' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (2, 32, 14, CAST(N'2015-03-24T00:00:00.000' AS DateTime), CAST(N'2015-04-24' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (3, 29, 5, CAST(N'2015-04-28T00:00:00.000' AS DateTime), CAST(N'2015-05-28' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (4, 28, 5, CAST(N'2015-07-26T00:00:00.000' AS DateTime), CAST(N'2015-08-26' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (5, 28, 14, CAST(N'2015-10-15T00:00:00.000' AS DateTime), CAST(N'2015-11-15' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (6, 15, 6, CAST(N'2015-12-10T00:00:00.000' AS DateTime), CAST(N'2016-01-10' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (7, 29, 8, CAST(N'2016-01-08T00:00:00.000' AS DateTime), CAST(N'2016-02-08' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (8, 26, 8, CAST(N'2016-03-07T00:00:00.000' AS DateTime), CAST(N'2016-04-07' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (9, 28, 10, CAST(N'2016-05-10T00:00:00.000' AS DateTime), CAST(N'2016-06-10' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (10, 20, 14, CAST(N'2016-05-21T00:00:00.000' AS DateTime), CAST(N'2016-06-21' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (11, 32, 12, CAST(N'2016-06-03T00:00:00.000' AS DateTime), CAST(N'2016-07-03' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (12, 26, 11, CAST(N'2016-06-17T00:00:00.000' AS DateTime), CAST(N'2016-07-17' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (13, 23, 9, CAST(N'2016-08-12T00:00:00.000' AS DateTime), CAST(N'2016-09-12' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (14, 23, 12, CAST(N'2016-09-10T00:00:00.000' AS DateTime), CAST(N'2016-10-10' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (15, 26, 5, CAST(N'2016-10-21T00:00:00.000' AS DateTime), CAST(N'2016-11-21' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (16, 20, 5, CAST(N'2017-02-06T00:00:00.000' AS DateTime), CAST(N'2017-03-06' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (17, 33, 13, CAST(N'2017-04-14T00:00:00.000' AS DateTime), CAST(N'2017-05-14' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (18, 33, 7, CAST(N'2017-04-22T00:00:00.000' AS DateTime), CAST(N'2017-05-22' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (19, 15, 8, CAST(N'2017-04-24T00:00:00.000' AS DateTime), CAST(N'2017-05-24' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (20, 25, 10, CAST(N'2017-05-16T00:00:00.000' AS DateTime), CAST(N'2017-06-16' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (21, 34, 9, CAST(N'2017-07-01T00:00:00.000' AS DateTime), CAST(N'2017-06-01' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (22, 17, 13, CAST(N'2017-07-03T00:00:00.000' AS DateTime), CAST(N'2017-08-03' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (23, 31, 10, CAST(N'2017-07-22T00:00:00.000' AS DateTime), CAST(N'2017-08-22' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (24, 32, 12, CAST(N'2017-08-26T00:00:00.000' AS DateTime), CAST(N'2017-09-26' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (25, 15, 14, CAST(N'2017-12-03T00:00:00.000' AS DateTime), CAST(N'2018-01-03' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (26, 33, 12, CAST(N'2017-12-07T00:00:00.000' AS DateTime), CAST(N'2018-01-07' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (27, 27, 6, CAST(N'2017-12-10T00:00:00.000' AS DateTime), CAST(N'2018-01-10' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (28, 23, 13, CAST(N'2017-12-27T00:00:00.000' AS DateTime), CAST(N'2018-01-27' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (29, 26, 14, CAST(N'2018-02-08T00:00:00.000' AS DateTime), CAST(N'2018-03-08' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (30, 32, 12, CAST(N'2018-02-23T00:00:00.000' AS DateTime), CAST(N'2018-03-23' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (31, 24, 13, CAST(N'2018-04-27T00:00:00.000' AS DateTime), CAST(N'2018-05-27' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (32, 33, 10, CAST(N'2018-05-17T00:00:00.000' AS DateTime), CAST(N'2018-06-17' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (33, 34, 12, CAST(N'2018-05-29T00:00:00.000' AS DateTime), CAST(N'2018-06-29' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (34, 23, 10, CAST(N'2018-07-03T00:00:00.000' AS DateTime), CAST(N'2018-08-03' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (35, 22, 14, CAST(N'2018-08-12T00:00:00.000' AS DateTime), CAST(N'2018-09-12' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (36, 15, 5, CAST(N'2018-08-28T00:00:00.000' AS DateTime), CAST(N'2018-09-28' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (37, 29, 5, CAST(N'2018-09-28T00:00:00.000' AS DateTime), CAST(N'2018-10-28' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (38, 30, 9, CAST(N'2018-10-01T00:00:00.000' AS DateTime), CAST(N'2018-11-01' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (39, 34, 6, CAST(N'2018-10-21T00:00:00.000' AS DateTime), CAST(N'2018-11-21' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (40, 28, 10, CAST(N'2018-10-26T00:00:00.000' AS DateTime), CAST(N'2018-11-26' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (41, 23, 12, CAST(N'2018-12-20T00:00:00.000' AS DateTime), CAST(N'2019-01-20' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (42, 20, 14, CAST(N'2018-12-23T00:00:00.000' AS DateTime), CAST(N'2019-01-23' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (43, 31, 13, CAST(N'2018-12-25T00:00:00.000' AS DateTime), CAST(N'2019-01-25' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (44, 27, 7, CAST(N'2018-12-30T00:00:00.000' AS DateTime), CAST(N'2019-01-30' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (45, 20, 13, CAST(N'2019-02-07T00:00:00.000' AS DateTime), CAST(N'2019-03-07' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (46, 21, 12, CAST(N'2019-04-13T00:00:00.000' AS DateTime), CAST(N'2019-05-13' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (47, 19, 10, CAST(N'2019-04-22T00:00:00.000' AS DateTime), CAST(N'2019-05-22' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (48, 16, 6, CAST(N'2019-05-04T00:00:00.000' AS DateTime), CAST(N'2019-06-04' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (49, 23, 14, CAST(N'2019-07-19T00:00:00.000' AS DateTime), CAST(N'2019-08-19' AS Date))
GO
INSERT [dbo].[rental_transaction] ([rentalID], [member_userID], [employee_userID], [rental_date], [due_date]) VALUES (50, 17, 10, CAST(N'2019-11-13T00:00:00.000' AS DateTime), CAST(N'2019-12-13' AS Date))
GO
SET IDENTITY_INSERT [dbo].[rental_transaction] OFF
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (1, 1, 3, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(27.20 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (2, 2, 1, CAST(45.81 AS Decimal(9, 2)), CAST(45.81 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (3, 3, 3, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(55.20 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (4, 4, 2, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(76.16 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (5, 4, 3, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(97.68 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (6, 5, 5, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (7, 6, 3, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(69.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (8, 7, 5, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(54.40 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (9, 8, 3, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(165.30 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (10, 8, 4, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(139.20 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (11, 8, 2, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(95.20 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (12, 9, 5, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(177.60 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (13, 10, 3, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (14, 11, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(5.29 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (15, 12, 5, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(189.60 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (16, 13, 4, CAST(40.72 AS Decimal(9, 2)), CAST(40.72 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (17, 14, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(30.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (18, 14, 3, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(122.10 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (19, 14, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(37.20 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (20, 15, 1, CAST(11.01 AS Decimal(9, 2)), CAST(11.01 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (21, 16, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (22, 17, 1, CAST(49.40 AS Decimal(9, 2)), CAST(49.40 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (23, 18, 4, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(24.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (24, 18, 2, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(19.04 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (25, 19, 5, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (26, 20, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(9.60 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (27, 21, 4, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (28, 22, 2, CAST(65.80 AS Decimal(9, 2)), CAST(65.80 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (29, 23, 4, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(160.80 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (30, 23, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(43.60 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (31, 23, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(17.60 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (32, 24, 3, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(12.21 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (33, 25, 3, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(77.76 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (34, 25, 4, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(102.48 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (35, 26, 1, CAST(22.32 AS Decimal(9, 2)), CAST(22.32 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (36, 27, 2, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(16.08 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (36, 28, 2, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (37, 29, 3, CAST(36.63 AS Decimal(9, 2)), CAST(36.63 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (38, 30, 1, CAST(19.76 AS Decimal(9, 2)), CAST(19.76 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (39, 31, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(26.32 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (40, 32, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(47.60 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (41, 33, 3, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (42, 33, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (43, 34, 3, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(86.40 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (44, 35, 3, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (45, 36, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(5.51 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (46, 36, 3, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(13.80 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (47, 37, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (48, 38, 1, CAST(25.62 AS Decimal(9, 2)), CAST(25.62 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (49, 38, 2, CAST(36.00 AS Decimal(9, 2)), CAST(36.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (50, 39, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (50, 40, 1, CAST(47.36 AS Decimal(9, 2)), CAST(47.36 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (51, 41, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (52, 41, 4, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (53, 42, 3, CAST(35.52 AS Decimal(9, 2)), CAST(35.52 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (54, 43, 2, CAST(11.84 AS Decimal(9, 2)), CAST(11.84 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (55, 44, 5, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (56, 45, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (57, 45, 5, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (58, 46, 3, CAST(55.05 AS Decimal(9, 2)), CAST(55.05 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (59, 47, 5, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(165.90 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (60, 48, 3, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (61, 49, 2, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (62, 50, 3, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(69.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (63, 50, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(27.55 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (64, 50, 5, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(87.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (65, 51, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(55.10 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (66, 51, 5, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(183.50 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (67, 52, 5, CAST(196.20 AS Decimal(9, 2)), CAST(196.20 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (68, 53, 1, CAST(31.32 AS Decimal(9, 2)), CAST(31.32 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (69, 54, 1, CAST(27.40 AS Decimal(9, 2)), CAST(27.40 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (70, 55, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(33.06 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (71, 56, 5, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (72, 56, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (73, 57, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(34.70 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (74, 58, 3, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(132.24 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (75, 59, 1, CAST(3.29 AS Decimal(9, 2)), CAST(3.29 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (76, 60, 5, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (77, 61, 1, CAST(14.82 AS Decimal(9, 2)), CAST(14.82 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (78, 62, 5, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (79, 63, 5, CAST(152.10 AS Decimal(9, 2)), CAST(152.10 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (80, 64, 5, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(63.50 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (81, 65, 5, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (82, 65, 4, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
GO
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (83, 66, 2, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(17.08 AS Decimal(9, 2)))
GO
SET IDENTITY_INSERT [dbo].[return_transaction] ON 
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (1, 6, CAST(N'2015-04-09T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (2, 5, CAST(N'2015-05-03T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (3, 7, CAST(N'2015-05-24T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (4, 8, CAST(N'1905-06-13T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (5, 10, CAST(N'2015-11-15T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (6, 10, CAST(N'2015-11-20T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (7, 6, CAST(N'2016-01-08T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (8, 9, CAST(N'2016-01-29T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (9, 14, CAST(N'2016-04-01T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (10, 9, CAST(N'2016-04-07T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (11, 10, CAST(N'2016-06-09T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (12, 14, CAST(N'2016-06-13T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (13, 9, CAST(N'2016-07-05T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (14, 13, CAST(N'2016-07-07T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (15, 13, CAST(N'2016-09-15T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (16, 11, CAST(N'2016-10-10T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (17, 13, CAST(N'2016-10-20T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (18, 13, CAST(N'2016-11-19T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (19, 13, CAST(N'2016-11-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (20, 10, CAST(N'2017-03-03T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (21, 11, CAST(N'2017-03-06T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (22, 6, CAST(N'2017-05-24T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (23, 11, CAST(N'2017-05-12T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (24, 5, CAST(N'2017-05-23T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (25, 11, CAST(N'2017-06-10T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (26, 10, CAST(N'2017-06-07T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (27, 9, CAST(N'2017-08-01T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (28, 13, CAST(N'2017-08-03T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (29, 7, CAST(N'2017-08-25T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (30, 14, CAST(N'2017-09-30T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (31, 10, CAST(N'2017-12-26T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (32, 12, CAST(N'2017-12-28T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (33, 12, CAST(N'2018-01-07T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (34, 13, CAST(N'2018-01-01T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (35, 6, CAST(N'2018-01-10T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (36, 13, CAST(N'2018-01-26T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (37, 13, CAST(N'2018-01-27T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (38, 5, CAST(N'2018-03-14T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (39, 9, CAST(N'2018-03-23T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (40, 12, CAST(N'2018-03-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (41, 13, CAST(N'2018-05-27T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (42, 13, CAST(N'2018-05-29T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (43, 10, CAST(N'2018-06-18T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (44, 13, CAST(N'2018-06-29T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (45, 10, CAST(N'2018-08-03T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (46, 13, CAST(N'2018-08-08T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (47, 9, CAST(N'2018-09-05T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (48, 9, CAST(N'2018-09-12T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (49, 6, CAST(N'2018-09-28T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (50, 5, CAST(N'2018-10-23T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (51, 5, CAST(N'2018-10-22T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (52, 14, CAST(N'2018-11-30T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (53, 12, CAST(N'2018-12-05T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (54, 5, CAST(N'2019-01-28T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (55, 7, CAST(N'2019-01-17T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (56, 14, CAST(N'2019-01-23T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (57, 7, CAST(N'2019-01-15T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (58, 14, CAST(N'2019-01-22T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (59, 5, CAST(N'2019-03-08T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (60, 6, CAST(N'2019-05-13T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (61, 12, CAST(N'2019-05-16T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (62, 6, CAST(N'2019-05-22T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (63, 8, CAST(N'2019-06-13T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (64, 8, CAST(N'2019-08-14T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (65, 8, CAST(N'2019-08-19T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[return_transaction] ([returnID], [employee_userID], [return_date]) VALUES (66, 6, CAST(N'2019-12-11T00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[return_transaction] OFF
GO
SET IDENTITY_INSERT [dbo].[role] ON 
GO
INSERT [dbo].[role] ([roleID], [type]) VALUES (1, N'administrator')
GO
INSERT [dbo].[role] ([roleID], [type]) VALUES (2, N'employee')
GO
INSERT [dbo].[role] ([roleID], [type]) VALUES (3, N'customer')
GO
SET IDENTITY_INSERT [dbo].[role] OFF
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'AK', N'Alaska')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'AL', N'Alabama')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'AR', N'Arkansas')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'AZ', N'Arizona')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'CA', N'California')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'CO', N'Colorado')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'CT', N'Connecticut')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'DE', N'Delaware')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'FL', N'Florida')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'GA', N'Georgia')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'HI', N'Hawaii')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'IA', N'Iowa')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'ID', N'Idaho')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'IL', N'Illinois')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'IN', N'Indiana')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'KS', N'Kansas')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'KY', N'Kentucky')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'LA', N'Louisiana')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'MA', N'Massachusetts')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'MD', N'Maryland')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'ME', N'Maine')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'MI', N'Michigan')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'MN', N'Minnesota')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'MO', N'Missouri')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'MS', N'Mississippi')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'MT', N'Montana')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'NC', N'North Carolina')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'ND', N'North Dakota')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'NE', N'Nebraska')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'NH', N'New Hampshire')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'NJ', N'New Jersey')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'NM', N'New Mexico')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'NV', N'Nevada')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'NY', N'New York')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'OH', N'Ohio')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'OK', N'Oklahoma')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'OR', N'Oregon')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'PA', N'Pennsylvania')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'RI', N'Rhode Island')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'SC', N'South Carolina')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'SD', N'South Dakota')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'TN', N'Tennessee')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'TX', N'Texas')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'UT', N'Utah')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'VA', N'Virginia')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'VT', N'Vermont')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'WA', N'Washington')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'WI', N'Wisconsin')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'WV', N'West Virginia')
GO
INSERT [dbo].[state] ([code], [name]) VALUES (N'WY', N'Wyoming')
GO
SET IDENTITY_INSERT [dbo].[style] ON 
GO
INSERT [dbo].[style] ([styleID], [style_type]) VALUES (1, N'Bohemian')
GO
INSERT [dbo].[style] ([styleID], [style_type]) VALUES (2, N'Coastal/Hamptons')
GO
INSERT [dbo].[style] ([styleID], [style_type]) VALUES (3, N'Contemporary')
GO
INSERT [dbo].[style] ([styleID], [style_type]) VALUES (4, N'French Country')
GO
INSERT [dbo].[style] ([styleID], [style_type]) VALUES (5, N'Hollywood Glam')
GO
INSERT [dbo].[style] ([styleID], [style_type]) VALUES (6, N'Industrial')
GO
INSERT [dbo].[style] ([styleID], [style_type]) VALUES (7, N'Mid-Century Modern')
GO
INSERT [dbo].[style] ([styleID], [style_type]) VALUES (8, N'Minimalist')
GO
INSERT [dbo].[style] ([styleID], [style_type]) VALUES (9, N'Modern')
GO
INSERT [dbo].[style] ([styleID], [style_type]) VALUES (10, N'Rustic')
GO
INSERT [dbo].[style] ([styleID], [style_type]) VALUES (11, N'Scandanavian')
GO
INSERT [dbo].[style] ([styleID], [style_type]) VALUES (12, N'Shabby Chic')
GO
INSERT [dbo].[style] ([styleID], [style_type]) VALUES (13, N'Traditional')
GO
INSERT [dbo].[style] ([styleID], [style_type]) VALUES (14, N'Transitional')
GO
SET IDENTITY_INSERT [dbo].[style] OFF
GO
SET IDENTITY_INSERT [dbo].[user] ON 
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (1, 1, CAST(N'1995-09-27' AS Date), N'Jane', N'Doe', N'6027589812', N'120 Dryden Place', NULL, N'01760', N'F')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (2, 1, CAST(N'1990-09-27' AS Date), N'Zelda', N'Stuttard', N'9126604920', N'50 Lumin Street', NULL, N'30118', N'M')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, 1, CAST(N'1982-09-27' AS Date), N'Gibbie', N'Scotford', N'9894772454', N'897 Kinan Circle', NULL, N'94203', N'F')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (4, 1, CAST(N'1985-09-27' AS Date), N'Margarita', N'Pettiward', N'9189666561', N'135 Rossbury Trail', NULL, N'33101', N'F')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (5, 2, CAST(N'1970-08-30' AS Date), N'Dell', N'Joutapaitis', N'6027619812', N'8744 Lighthouse Bay Way', NULL, N'85001', N'M')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (6, 2, CAST(N'1979-10-11' AS Date), N'Jeffry', N'Weymont', N'5858705203', N'203 Buena Vista Circle', NULL, N'12084', N'M')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (7, 2, CAST(N'1969-01-23' AS Date), N'Filippo', N'Coan', N'7063870403', N'7701 Dryden Street', N'Apt 456', N'30301', N'M')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (8, 2, CAST(N'2002-10-31' AS Date), N'Osbert', N'Colvine', N'3148836056', N'72 Meadow Vale Plaza', NULL, N'65043', N'M')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (9, 2, CAST(N'1971-10-28' AS Date), N'Konrad', N'Lemmen', N'4433631375', N'69 Comanche Plaza', NULL, N'21401', N'M')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (10, 2, CAST(N'1997-12-17' AS Date), N'Derick', N'Veale', N'9126605320', N'53475 Buhler Avenue', NULL, N'30118', N'M')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (11, 2, CAST(N'1980-08-19' AS Date), N'Harman', N'Tibbs', N'8283571904', N'89 Kinsman Street', N'Apt 312', N'27513', N'F')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (12, 2, CAST(N'1993-06-11' AS Date), N'Ebba', N'Bosson', N'9734772454', N'53097 Cody Terrace', NULL, N'08601', N'F')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (13, 2, CAST(N'1973-02-26' AS Date), N'Cahra', N'Poland', N'9187666561', N'0 Loomis Place', NULL, N'73008', N'F')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (14, 2, CAST(N'1970-05-01' AS Date), N'Delaney', N'Ivanyushin', N'4153454902', N'13 Roxbury Center', NULL, N'94203', N'M')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (15, 3, CAST(N'1990-07-13' AS Date), N'Kippy', N'Gritland', N'2095050739', N'8189 Service Road', NULL, N'94203', N'F')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (16, 3, CAST(N'1976-10-05' AS Date), N'Susana', N'Attenbrow', N'2028322317', N'662 Autumn Leaf Court', NULL, N'48864', N'F')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (17, 3, CAST(N'1975-10-02' AS Date), N'Bordy', N'Cestard', N'7063636895', N'331 Thackeray Trail', NULL, N'30301', N'M')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (18, 3, CAST(N'1994-07-02' AS Date), N'Ronica', N'Balentyne', N'4804839835', N'51 Fisk Court', NULL, N'85001', N'F')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (19, 3, CAST(N'1960-01-05' AS Date), N'Trenton', N'Blaycock', N'2397526625', N'0673 Fairfield Park', N'Apt 701a', N'32301', N'M')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (20, 3, CAST(N'1965-06-08' AS Date), N'Jilly', N'Skingle', N'7142000413', N'44638 Kenwood Circle', NULL, N'94016', N'F')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (21, 3, CAST(N'1982-07-20' AS Date), N'Cosme', N'Favelle', N'4053954322', N'7783 Village Green Parkway', NULL, N'73008', N'M')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (22, 3, CAST(N'1975-03-19' AS Date), N'Kai', N'Satterthwaite', N'7143097365', N'763 Fieldstone Center', NULL, N'22434', N'M')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (23, 3, CAST(N'1990-03-03' AS Date), N'Rosie', N'Heugel', N'8599905723', N'57 Surrey Trail', NULL, N'40018', N'F')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (24, 3, CAST(N'1982-05-09' AS Date), N'Lucas', N'Batte', N'7139464786', N'6832 7th Point', NULL, N'73301', N'M')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (25, 3, CAST(N'1978-03-15' AS Date), N'Harriett', N'Artrick', N'5138136137', N'9 Dunning Junction', NULL, N'43004', N'F')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (26, 3, CAST(N'1983-07-14' AS Date), N'Cornelius', N'Gusticke', N'9789263346', N'408 Loomis Road', N'Apt 503', N'02101', N'M')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (27, 3, CAST(N'1962-07-06' AS Date), N'Brigid', N'Drysdale', N'9166582448', N'839 Prairieview Road', NULL, N'01760', N'F')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (28, 3, CAST(N'1983-12-23' AS Date), N'Avie', N'Mandrey', N'7133321756', N'44349 Reindahl Point', NULL, N'77001', N'F')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (29, 3, CAST(N'1990-09-21' AS Date), N'Tallie', N'Rheam', N'9417455386', N'4 Prairieview Plaza', NULL, N'32789', N'F')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (30, 3, CAST(N'1993-09-21' AS Date), N'Porty', N'Dabinett', N'9176980727', N'04 Alpine Alley', N'Apt 212', N'33101', N'M')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (31, 3, CAST(N'1987-04-07' AS Date), N'Audrey', N'Stirland', N'7131053203', N'7714 Cody Way', NULL, N'77351', N'F')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (32, 3, CAST(N'1973-02-09' AS Date), N'Althea', N'Yurkov', N'8167689917', N'61147 Johnson Alley', NULL, N'65043', N'M')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (33, 3, CAST(N'1988-11-18' AS Date), N'Lynn', N'Tubble', N'3042384626', N'21491 Clemons Circle', NULL, N'29401', N'F')
GO
INSERT [dbo].[user] ([userID], [roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (34, 3, CAST(N'1983-08-27' AS Date), N'Land', N'Hallsworth', N'8146053181', N'2618 Daystar Pass', NULL, N'17025', N'M')
GO
SET IDENTITY_INSERT [dbo].[user] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_category]    Script Date: 4/26/2020 2:08:50 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_category] ON [dbo].[category]
(
	[category_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_credential_username]    Script Date: 4/26/2020 2:08:50 PM ******/
ALTER TABLE [dbo].[credential] ADD  CONSTRAINT [IX_credential_username] UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_credential]    Script Date: 4/26/2020 2:08:50 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_credential] ON [dbo].[credential]
(
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_style]    Script Date: 4/26/2020 2:08:50 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_style] ON [dbo].[style]
(
	[style_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[credential] ADD  CONSTRAINT [DF_credential_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[credential]  WITH CHECK ADD  CONSTRAINT [FK_credential_user] FOREIGN KEY([userID])
REFERENCES [dbo].[user] ([userID])
GO
ALTER TABLE [dbo].[credential] CHECK CONSTRAINT [FK_credential_user]
GO
ALTER TABLE [dbo].[furniture_item]  WITH CHECK ADD  CONSTRAINT [FK_furniture_item_category] FOREIGN KEY([categoryID])
REFERENCES [dbo].[category] ([categoryID])
GO
ALTER TABLE [dbo].[furniture_item] CHECK CONSTRAINT [FK_furniture_item_category]
GO
ALTER TABLE [dbo].[furniture_item]  WITH CHECK ADD  CONSTRAINT [FK_furniture_item_style] FOREIGN KEY([styleID])
REFERENCES [dbo].[style] ([styleID])
GO
ALTER TABLE [dbo].[furniture_item] CHECK CONSTRAINT [FK_furniture_item_style]
GO
ALTER TABLE [dbo].[location]  WITH CHECK ADD  CONSTRAINT [FK_location_state] FOREIGN KEY([state])
REFERENCES [dbo].[state] ([code])
GO
ALTER TABLE [dbo].[location] CHECK CONSTRAINT [FK_location_state]
GO
ALTER TABLE [dbo].[rental_item]  WITH CHECK ADD  CONSTRAINT [FK_rental_item_furniture_item] FOREIGN KEY([serial_number])
REFERENCES [dbo].[furniture_item] ([serial_number])
GO
ALTER TABLE [dbo].[rental_item] CHECK CONSTRAINT [FK_rental_item_furniture_item]
GO
ALTER TABLE [dbo].[rental_item]  WITH CHECK ADD  CONSTRAINT [FK_rental_item_rental_transaction] FOREIGN KEY([rentalID])
REFERENCES [dbo].[rental_transaction] ([rentalID])
GO
ALTER TABLE [dbo].[rental_item] CHECK CONSTRAINT [FK_rental_item_rental_transaction]
GO
ALTER TABLE [dbo].[rental_transaction]  WITH CHECK ADD  CONSTRAINT [FK_rental_transaction_employeeID] FOREIGN KEY([employee_userID])
REFERENCES [dbo].[user] ([userID])
GO
ALTER TABLE [dbo].[rental_transaction] CHECK CONSTRAINT [FK_rental_transaction_employeeID]
GO
ALTER TABLE [dbo].[rental_transaction]  WITH CHECK ADD  CONSTRAINT [FK_rental_transaction_memberID] FOREIGN KEY([member_userID])
REFERENCES [dbo].[user] ([userID])
GO
ALTER TABLE [dbo].[rental_transaction] CHECK CONSTRAINT [FK_rental_transaction_memberID]
GO
ALTER TABLE [dbo].[return_item]  WITH CHECK ADD  CONSTRAINT [FK_return_item_rental_item] FOREIGN KEY([itemID])
REFERENCES [dbo].[rental_item] ([itemID])
GO
ALTER TABLE [dbo].[return_item] CHECK CONSTRAINT [FK_return_item_rental_item]
GO
ALTER TABLE [dbo].[return_item]  WITH CHECK ADD  CONSTRAINT [FK_return_item_return_transaction] FOREIGN KEY([returnID])
REFERENCES [dbo].[return_transaction] ([returnID])
GO
ALTER TABLE [dbo].[return_item] CHECK CONSTRAINT [FK_return_item_return_transaction]
GO
ALTER TABLE [dbo].[return_transaction]  WITH CHECK ADD  CONSTRAINT [FK_return_transaction_employeeID] FOREIGN KEY([employee_userID])
REFERENCES [dbo].[user] ([userID])
GO
ALTER TABLE [dbo].[return_transaction] CHECK CONSTRAINT [FK_return_transaction_employeeID]
GO
ALTER TABLE [dbo].[user]  WITH CHECK ADD  CONSTRAINT [FK_employee_location1] FOREIGN KEY([zip])
REFERENCES [dbo].[location] ([zip])
GO
ALTER TABLE [dbo].[user] CHECK CONSTRAINT [FK_employee_location1]
GO
ALTER TABLE [dbo].[user]  WITH CHECK ADD  CONSTRAINT [FK_user_role] FOREIGN KEY([roleID])
REFERENCES [dbo].[role] ([roleID])
GO
ALTER TABLE [dbo].[user] CHECK CONSTRAINT [FK_user_role]
GO
/****** Object:  StoredProcedure [dbo].[GetPopularFurniture]    Script Date: 4/26/2020 2:08:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPopularFurniture]
@start_date DATETIME,
@end_date DATETIME

AS
	/** Overrides date parameters for testing
	SET @start_date = '2015-03-28';
	SET @end_date = '2015-12-10';
	**/

	DECLARE @total_rentals SMALLINT;
	SET @total_rentals = (
		SELECT COUNT(*) 
		FROM rental_item AS ri
			LEFT JOIN rental_transaction AS rt ON ri.rentalID = rt.rentalID
		WHERE rt.rental_date >= @start_date AND rt.rental_date <= @end_date
	)

	SELECT fi.serial_number 'SerialNumber', c.category_name 'Category', fi.description 'Description',
		(SELECT COUNT(*) 
		 FROM rental_item AS ri1 
			LEFT JOIN rental_transaction AS rt1 ON ri1.rentalID = rt1.rentalID
		 WHERE ri1.serial_number = fi.serial_number
			AND rt1.rental_date >= @start_date AND rt1.rental_date <= @end_date AND (SELECT COUNT(*) FROM rental_item WHERE ri.serial_number = fi.serial_number) > 1) AS 'TimesRented',
		 @total_rentals AS 'TotalRentals',
		 CAST(((SELECT COUNT(*) 
		 FROM rental_item AS ri1 
			LEFT JOIN rental_transaction AS rt1 ON ri1.rentalID = rt1.rentalID
		 WHERE ri1.serial_number = fi.serial_number
			AND rt1.rental_date >= @start_date AND rt1.rental_date <= @end_date AND (SELECT COUNT(*) FROM rental_item WHERE ri.serial_number = fi.serial_number) > 1) / CAST(@total_rentals AS FLOAT) * 100) AS INT) AS 'PercentOfTotalRentals',
		 (SELECT COUNT(*)
			FROM furniture_item AS fi
				LEFT JOIN rental_item AS ri ON ri.serial_number = fi.serial_number
					LEFT JOIN rental_transaction AS rt ON rt.rentalID = ri.rentalID
						LEFT JOIN "user" AS u ON rt.member_userID = u.userID 
			WHERE rt.rental_date >= @start_date AND rt.rental_date <= @end_date 
				AND (SELECT COUNT(*) FROM rental_item WHERE ri.serial_number = fi.serial_number) > 1
				AND (FLOOR(DATEDIFF(DAY, u.date_of_birth, rt.rental_date) / 365) BETWEEN 18 AND 29)
		 ) 'PercentBetween18And29'
	FROM furniture_item AS fi
		LEFT JOIN category AS c ON fi.categoryID = c.categoryID
			LEFT JOIN rental_item AS ri ON ri.serial_number = fi.serial_number
				LEFT JOIN rental_transaction AS rt ON rt.rentalID = ri.rentalID
	WHERE rt.rental_date >= @start_date AND rt.rental_date <= @end_date AND 
		(
		SELECT COUNT(*) 
		FROM rental_item AS ri2
			LEFT JOIN rental_transaction AS rt2 ON rt2.rentalID = ri2.rentalID
		WHERE ri2.serial_number = fi.serial_number AND rt2.rental_date >= @start_date AND rt2.rental_date <= @end_date
		) > 1
	GROUP BY fi.serial_number, fi.description, c.category_name, ri.serial_number
RETURN
GO
USE [master]
GO
ALTER DATABASE [cs6232-g4] SET  READ_WRITE 
GO
