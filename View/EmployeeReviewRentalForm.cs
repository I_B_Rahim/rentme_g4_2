﻿using RentMe_G4.UserControls;
using System;
using System.Windows.Forms;
using RentMe_G4.Model;
using System.Collections.Generic;
using RentMe_G4.Controller;

namespace RentMe_G4.View
{
    public partial class EmployeeReviewRentalForm : Form
    {
        private EmployeeRentalsUserControl theEmployeeRentalsUserControl;
        private ItemController ItemController;
        private int DaysToRent;

        /// <summary>
        /// Constructor for the Cart Review form
        /// </summary>
        /// <param name="theUserControl">A refrence to the order form, to obtain needed information from it</param>
        public EmployeeReviewRentalForm(EmployeeRentalsUserControl theUserControl)
        {
            InitializeComponent();
            theEmployeeRentalsUserControl = theUserControl;
            DisplayHeaderInformation();
            if (this.DaysToRent == 0)
            {
                this.DaysToRent = 30;
            }
            this.returnDateTimePicker.Value = DateTime.Today.AddDays(this.DaysToRent);
            DisplayCartReview(this.DaysToRent);
            this.ItemController = new ItemController();
        }

        private void DisplayHeaderInformation()
        {
            OrderForLabel.Text += " " + theEmployeeRentalsUserControl.GetCartUser().FirstName
                + " " + theEmployeeRentalsUserControl.GetCartUser().LastName
                + ", ID: " + theEmployeeRentalsUserControl.GetCartUser().UserID.ToString();
        }

        private void DisplayCartReview(int daysToRent)
        {
            double totalDue = 0;
            
            this.returnDateTimePicker.MinDate = DateTime.Today.AddDays(1);

            CartItemListView.Clear();
            ColumnHeader headerQuantity = new ColumnHeader();
            headerQuantity.Text = "Quantity";
            headerQuantity.Name = "QuantityColumn";
            CartItemListView.Columns.Add(headerQuantity);
            ColumnHeader headerSerialNumber = new ColumnHeader();
            headerSerialNumber.Text = "Serial Number";
            headerSerialNumber.Name = "SerialNumberColumn";
            CartItemListView.Columns.Add(headerSerialNumber);
            ColumnHeader headerDescription = new ColumnHeader();
            headerDescription.Text = "Description";
            headerDescription.Name = "DescriptionColumn";
            CartItemListView.Columns.Add(headerDescription);
            ColumnHeader headerStyle = new ColumnHeader();
            headerStyle.Text = "Style";
            headerStyle.Name = "StyleColumn";
            CartItemListView.Columns.Add(headerStyle);
            ColumnHeader headerCategory = new ColumnHeader();
            headerCategory.Text = "Category";
            headerCategory.Name = "CategoryColumn";
            CartItemListView.Columns.Add(headerCategory);
            ColumnHeader headerDailyRentalRate = new ColumnHeader();
            headerDailyRentalRate.Text = "Daily Rental Rate";
            headerDailyRentalRate.Name = "DailyRentalRateColumn";
            CartItemListView.Columns.Add(headerDailyRentalRate);
            ColumnHeader headerDailyFineRate = new ColumnHeader();
            headerDailyFineRate.Text = "Daily Fine Rate";
            headerDailyFineRate.Name = "DailyFineRaterColumn";
            CartItemListView.Columns.Add(headerDailyFineRate);
            CartItemListView.Scrollable = true;
            CartItemListView.View = System.Windows.Forms.View.Details;
            this.CartItemListView.BeginUpdate();

            foreach (int[] cartItem in theEmployeeRentalsUserControl.GetItemsInCart())
            {
                foreach (Item inventoryItem in theEmployeeRentalsUserControl.GetItemsInSystem())
                {
                    if (cartItem[0] == inventoryItem.SerialNumber)
                    {
                        ListViewItem theListViewItem = new ListViewItem();
                        theListViewItem.Text = cartItem[1].ToString();
                        theListViewItem.SubItems.Add(inventoryItem.SerialNumber.ToString());
                        theListViewItem.SubItems.Add(inventoryItem.Description.ToString());
                        theListViewItem.SubItems.Add(inventoryItem.StyleType.ToString());
                        theListViewItem.SubItems.Add(inventoryItem.CategoryName.ToString());
                        theListViewItem.SubItems.Add(inventoryItem.DailyRentalRate.ToString());
                        theListViewItem.SubItems.Add(inventoryItem.DailyFineRate.ToString());
                        CartItemListView.Items.Add(theListViewItem);
                        totalDue += inventoryItem.DailyRentalRate * cartItem[1] * daysToRent;
                    }
                }
            }

            foreach (ColumnHeader column in this.CartItemListView.Columns)
            {
                column.Width = -2;
            }

            this.CartItemListView.EndUpdate();

            DateTime dueDate = DateTime.Now.AddDays(daysToRent);
            this.dueDateTextBox.Text = dueDate.ToString("MM/dd/yyyy");

            this.totalDueTextBox.Text = "$" + string.Format("{0:0.00}", totalDue);

            TimeSpan daysDifference = this.returnDateTimePicker.Value - DateTime.Today;
            this.DaysToRent = daysDifference.Days;
        }

        private void CancelReviewOrderButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SubmitOrderButton_Click(object sender, EventArgs e)
        {
            DialogResult submitOrder = MessageBox.Show("Are you sure you want to submit this order?", "Order Submission", MessageBoxButtons.YesNo);
            if (submitOrder == DialogResult.Yes)
            {
                SubmitOrder();
            }
        }

        private void SubmitOrder()
        {
            theEmployeeRentalsUserControl.GetItemController().SubmitOrder(
                       theEmployeeRentalsUserControl.GetItemsInCart(),
                       theEmployeeRentalsUserControl.GetCartUser().UserID,
                       theEmployeeRentalsUserControl.GetCurrentEmployee().UserID,
                       this.DaysToRent
                   );
            theEmployeeRentalsUserControl.ResetCart();
            MessageBox.Show("This rental transaction is complete", "Transaciton Status", MessageBoxButtons.OK);
            this.Close();
        }

        private void UpdateRentalDueDateAndTotal(object sender, EventArgs e)
        {
            TimeSpan daysDifference = this.returnDateTimePicker.Value - DateTime.Today;
            int daysToRent = daysDifference.Days;
            this.DisplayCartReview(daysToRent);
        }

        private void CloseUpRentalDueDateAndTotal(object sender, EventArgs e)
        {
            this.returnDateTimePicker.ValueChanged += UpdateRentalDueDateAndTotal;
            UpdateRentalDueDateAndTotal(sender, e);
        }

        private void DropDownRentalDueDateAndTotal(object sender, EventArgs e)
        {
            this.returnDateTimePicker.ValueChanged -= UpdateRentalDueDateAndTotal;
        }

        private void DoNotManuallyChangeDate(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void UpdateQuantity(object sender, LabelEditEventArgs e)
        {
            List<int[]> itemsInCart = theEmployeeRentalsUserControl.GetItemsInCart();
            try
            {
                if (e.Label != null)
                {
                    int quantityInStock = this.ItemController.GetQuantityBySerial(itemsInCart[e.Item][0]);
                    if (!Int32.TryParse(e.Label, out int result))
                    {
                        e.CancelEdit = true;
                        throw new ArgumentException("Quantity must be a valid integer.");
                    }
                    else if (Int32.Parse(e.Label) > quantityInStock)
                    {
                        e.CancelEdit = true;
                        throw new ArgumentException("Quantity must be a less than or equal to " + quantityInStock);
                    }
                    else
                    {
                        int serialNumber = itemsInCart[e.Item][0];
                        int originalQuantity = itemsInCart[e.Item][1];
                        itemsInCart.RemoveAt(e.Item);
                        itemsInCart.Add(new int[] { serialNumber, Int32.Parse(e.Label) });
                        TimeSpan daysDifference = this.returnDateTimePicker.Value - DateTime.Today;
                        int daysToRent = daysDifference.Days;
                        this.DisplayCartReview(daysToRent);
                        // new quantity - original quantity
                        int quantityDifference = Int32.Parse(e.Label) - originalQuantity;
                        theEmployeeRentalsUserControl.ValidateSelectionAndUpdateLocalQuantity(serialNumber, quantityDifference);
                        theEmployeeRentalsUserControl.UpdateItemListBox();
                        theEmployeeRentalsUserControl.UpdateCartLabel();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error has occurred: \n" + ex.Message, "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
