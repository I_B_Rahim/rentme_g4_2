﻿using System;
using System.Security.Cryptography;
using System.Windows.Forms;
using RentMe_G4.Controller;
using RentMe_G4.Model;

namespace RentMe_G4.UserControls
{
    /// <summary>
    /// This class handles the view for adding employees
    /// </summary>
    public partial class AddEmployeeUserControl : UserControl
    {
        private readonly UserController UserController;

        /// <summary>
        /// Constructor used to initialize the form
        /// </summary>
        public AddEmployeeUserControl()
        {
            InitializeComponent();
            this.UserController = new UserController();
            this.SetupForm();
        }

        private void AddEmployeeUserControl_Load(object sender, EventArgs e)
        {
            this.ClearForm();
        }

        private void ClearForm()
        {
            this.first_nameTextBox.Clear();
            this.last_nameTextBox.Clear();
            this.date_of_birthTextBox.Clear();
            this.phone_numberTextBox.Clear();
            this.address1TextBox.Clear();
            this.address2TextBox.Clear();
            this.usernameTextBox.Clear();
            this.passwordTextBox.Clear();
            this.sexComboBox.ResetText();
            this.codeComboBox.ResetText();
            this.cityTextBox.Clear();
            this.zipTextBox.Clear();
        }

        private void SetupForm()
        {
            this.sexComboBox.Items.Add("Male");
            this.sexComboBox.Items.Add("Female");

            this.codeComboBox.DataSource = this.UserController.GetListOfStates();
            this.sexComboBox.SelectedIndex = 1;
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            this.ClearForm();
        }

        private void CreateButton_Click(object sender, EventArgs e)
        {
            try
            {
                User employee = new User();

                employee.RoleID = 2;
                employee.Status = true;
                employee.FirstName = this.first_nameTextBox.Text;
                employee.LastName = this.last_nameTextBox.Text;
                employee.DateOfBirth = DateTime.Parse(this.date_of_birthTextBox.Text);
                employee.Address1 = this.address1TextBox.Text;

                if (this.address2TextBox.Text != "")
                {
                    employee.Address2 = this.address2TextBox.Text;
                }
                else
                {
                    employee.Address2 = "";
                }

                if (!Int32.TryParse(this.phone_numberTextBox.Text, out _) || this.phone_numberTextBox.Text.Length > 10 || this.phone_numberTextBox.Text.Length < 10)
                {
                    throw new ArgumentException("Phone number must be 10 digits.");
                }
                else
                {
                    employee.PhoneNumber = this.phone_numberTextBox.Text;
                }

                if (!Int32.TryParse(this.zipTextBox.Text, out _) || this.zipTextBox.Text.Length > 5 || this.zipTextBox.Text.Length < 5)
                {
                    throw new ArgumentException("Zip must be a 5 digit zip code.");
                }
                else
                {
                    employee.City = this.cityTextBox.Text;
                    if (!this.UserController.GetZipExists(this.zipTextBox.Text))
                    {
                        this.UserController.AddLocation(this.zipTextBox.Text, this.cityTextBox.Text, this.codeComboBox.SelectedItem.ToString());
                    }
                    employee.Zip = this.zipTextBox.Text;
                }

                if (this.sexComboBox.Text == "Male")
                {
                    employee.Sex = char.Parse("M");
                }
                else
                {
                    employee.Sex = char.Parse("F");
                }

                int userID = 0;

                if (this.UserController.GetUsernameExists(this.usernameTextBox.Text))
                {
                    throw new ArgumentException("The username " + this.usernameTextBox.Text + " already exists. Please choose a different one.");
                }
                else
                {
                    employee.Username = this.usernameTextBox.Text;
                }

                employee.Password = Encryption.EncryptPassword(this.passwordTextBox.Text);

                this.UserController.AddEmployee(employee, out userID);

                MessageBox.Show("The employee has been successfully added!\n\nYour User ID is: " + userID, "Success",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error has occurred: \n" + ex.Message, "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
