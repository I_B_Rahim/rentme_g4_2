﻿namespace RentMe_G4.UserControls
{
    partial class AddEmployeeUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label date_of_birthLabel;
            System.Windows.Forms.Label first_nameLabel;
            System.Windows.Forms.Label last_nameLabel;
            System.Windows.Forms.Label phone_numberLabel;
            System.Windows.Forms.Label address1Label;
            System.Windows.Forms.Label address2Label;
            System.Windows.Forms.Label zipLabel;
            System.Windows.Forms.Label sexLabel;
            System.Windows.Forms.Label usernameLabel;
            System.Windows.Forms.Label passwordLabel;
            System.Windows.Forms.Label stateLabel;
            System.Windows.Forms.Label cityLabel;
            this.date_of_birthTextBox = new System.Windows.Forms.TextBox();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._cs6232_g4DataSet = new RentMe_G4._cs6232_g4DataSet();
            this.first_nameTextBox = new System.Windows.Forms.TextBox();
            this.last_nameTextBox = new System.Windows.Forms.TextBox();
            this.phone_numberTextBox = new System.Windows.Forms.TextBox();
            this.address1TextBox = new System.Windows.Forms.TextBox();
            this.address2TextBox = new System.Windows.Forms.TextBox();
            this.addEmployeeTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.sexComboBox = new System.Windows.Forms.ComboBox();
            this.welcomeLabel = new System.Windows.Forms.Label();
            this.usernameTextBox = new System.Windows.Forms.TextBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.createButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.codeComboBox = new System.Windows.Forms.ComboBox();
            this.stateBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cityTextBox = new System.Windows.Forms.TextBox();
            this.zipTextBox = new System.Windows.Forms.TextBox();
            this.userTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.userTableAdapter();
            this.tableAdapterManager = new RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager();
            this.stateTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.stateTableAdapter();
            date_of_birthLabel = new System.Windows.Forms.Label();
            first_nameLabel = new System.Windows.Forms.Label();
            last_nameLabel = new System.Windows.Forms.Label();
            phone_numberLabel = new System.Windows.Forms.Label();
            address1Label = new System.Windows.Forms.Label();
            address2Label = new System.Windows.Forms.Label();
            zipLabel = new System.Windows.Forms.Label();
            sexLabel = new System.Windows.Forms.Label();
            usernameLabel = new System.Windows.Forms.Label();
            passwordLabel = new System.Windows.Forms.Label();
            stateLabel = new System.Windows.Forms.Label();
            cityLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).BeginInit();
            this.addEmployeeTableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // date_of_birthLabel
            // 
            date_of_birthLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            date_of_birthLabel.AutoSize = true;
            date_of_birthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            date_of_birthLabel.Location = new System.Drawing.Point(22, 112);
            date_of_birthLabel.Name = "date_of_birthLabel";
            date_of_birthLabel.Size = new System.Drawing.Size(91, 17);
            date_of_birthLabel.TabIndex = 5;
            date_of_birthLabel.Text = "Date of Birth:";
            // 
            // first_nameLabel
            // 
            first_nameLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            first_nameLabel.AutoSize = true;
            first_nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            first_nameLabel.Location = new System.Drawing.Point(33, 67);
            first_nameLabel.Name = "first_nameLabel";
            first_nameLabel.Size = new System.Drawing.Size(80, 17);
            first_nameLabel.TabIndex = 7;
            first_nameLabel.Text = "First Name:";
            // 
            // last_nameLabel
            // 
            last_nameLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            last_nameLabel.AutoSize = true;
            last_nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            last_nameLabel.Location = new System.Drawing.Point(299, 67);
            last_nameLabel.Name = "last_nameLabel";
            last_nameLabel.Size = new System.Drawing.Size(80, 17);
            last_nameLabel.TabIndex = 9;
            last_nameLabel.Text = "Last Name:";
            // 
            // phone_numberLabel
            // 
            phone_numberLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            phone_numberLabel.AutoSize = true;
            phone_numberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            phone_numberLabel.Location = new System.Drawing.Point(272, 112);
            phone_numberLabel.Name = "phone_numberLabel";
            phone_numberLabel.Size = new System.Drawing.Size(107, 17);
            phone_numberLabel.TabIndex = 11;
            phone_numberLabel.Text = "Phone Number:";
            // 
            // address1Label
            // 
            address1Label.Anchor = System.Windows.Forms.AnchorStyles.Right;
            address1Label.AutoSize = true;
            address1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            address1Label.Location = new System.Drawing.Point(37, 153);
            address1Label.Name = "address1Label";
            address1Label.Size = new System.Drawing.Size(76, 17);
            address1Label.TabIndex = 13;
            address1Label.Text = "Address 1:";
            // 
            // address2Label
            // 
            address2Label.Anchor = System.Windows.Forms.AnchorStyles.Right;
            address2Label.AutoSize = true;
            address2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            address2Label.Location = new System.Drawing.Point(37, 193);
            address2Label.Name = "address2Label";
            address2Label.Size = new System.Drawing.Size(76, 17);
            address2Label.TabIndex = 15;
            address2Label.Text = "Address 2:";
            // 
            // zipLabel
            // 
            zipLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            zipLabel.AutoSize = true;
            zipLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            zipLabel.Location = new System.Drawing.Point(44, 232);
            zipLabel.Name = "zipLabel";
            zipLabel.Size = new System.Drawing.Size(69, 17);
            zipLabel.TabIndex = 22;
            zipLabel.Text = "Zip Code:";
            // 
            // sexLabel
            // 
            sexLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            sexLabel.AutoSize = true;
            sexLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sexLabel.Location = new System.Drawing.Point(344, 232);
            sexLabel.Name = "sexLabel";
            sexLabel.Size = new System.Drawing.Size(35, 17);
            sexLabel.TabIndex = 23;
            sexLabel.Text = "Sex:";
            // 
            // usernameLabel
            // 
            usernameLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            usernameLabel.AutoSize = true;
            usernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            usernameLabel.Location = new System.Drawing.Point(36, 271);
            usernameLabel.Name = "usernameLabel";
            usernameLabel.Size = new System.Drawing.Size(77, 17);
            usernameLabel.TabIndex = 25;
            usernameLabel.Text = "Username:";
            // 
            // passwordLabel
            // 
            passwordLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            passwordLabel.AutoSize = true;
            passwordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            passwordLabel.Location = new System.Drawing.Point(306, 271);
            passwordLabel.Name = "passwordLabel";
            passwordLabel.Size = new System.Drawing.Size(73, 17);
            passwordLabel.TabIndex = 26;
            passwordLabel.Text = "Password:";
            // 
            // stateLabel
            // 
            stateLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            stateLabel.AutoSize = true;
            stateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            stateLabel.Location = new System.Drawing.Point(68, 311);
            stateLabel.Name = "stateLabel";
            stateLabel.Size = new System.Drawing.Size(45, 17);
            stateLabel.TabIndex = 29;
            stateLabel.Text = "State:";
            // 
            // cityLabel
            // 
            cityLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            cityLabel.AutoSize = true;
            cityLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cityLabel.Location = new System.Drawing.Point(344, 311);
            cityLabel.Name = "cityLabel";
            cityLabel.Size = new System.Drawing.Size(35, 17);
            cityLabel.TabIndex = 31;
            cityLabel.Text = "City:";
            // 
            // date_of_birthTextBox
            // 
            this.date_of_birthTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.date_of_birthTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "date_of_birth", true));
            this.date_of_birthTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.date_of_birthTextBox.Location = new System.Drawing.Point(119, 109);
            this.date_of_birthTextBox.Name = "date_of_birthTextBox";
            this.date_of_birthTextBox.Size = new System.Drawing.Size(121, 23);
            this.date_of_birthTextBox.TabIndex = 2;
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataMember = "user";
            this.userBindingSource.DataSource = this._cs6232_g4DataSet;
            // 
            // _cs6232_g4DataSet
            // 
            this._cs6232_g4DataSet.DataSetName = "_cs6232_g4DataSet";
            this._cs6232_g4DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // first_nameTextBox
            // 
            this.first_nameTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.first_nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "first_name", true));
            this.first_nameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.first_nameTextBox.Location = new System.Drawing.Point(119, 64);
            this.first_nameTextBox.Name = "first_nameTextBox";
            this.first_nameTextBox.Size = new System.Drawing.Size(121, 23);
            this.first_nameTextBox.TabIndex = 0;
            // 
            // last_nameTextBox
            // 
            this.last_nameTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.last_nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "last_name", true));
            this.last_nameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.last_nameTextBox.Location = new System.Drawing.Point(385, 64);
            this.last_nameTextBox.Name = "last_nameTextBox";
            this.last_nameTextBox.Size = new System.Drawing.Size(121, 23);
            this.last_nameTextBox.TabIndex = 1;
            // 
            // phone_numberTextBox
            // 
            this.phone_numberTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.phone_numberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "phone_number", true));
            this.phone_numberTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phone_numberTextBox.Location = new System.Drawing.Point(385, 109);
            this.phone_numberTextBox.Name = "phone_numberTextBox";
            this.phone_numberTextBox.Size = new System.Drawing.Size(121, 23);
            this.phone_numberTextBox.TabIndex = 3;
            // 
            // address1TextBox
            // 
            this.address1TextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.addEmployeeTableLayoutPanel.SetColumnSpan(this.address1TextBox, 2);
            this.address1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "address1", true));
            this.address1TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.address1TextBox.Location = new System.Drawing.Point(119, 150);
            this.address1TextBox.Name = "address1TextBox";
            this.address1TextBox.Size = new System.Drawing.Size(260, 23);
            this.address1TextBox.TabIndex = 4;
            // 
            // address2TextBox
            // 
            this.address2TextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.addEmployeeTableLayoutPanel.SetColumnSpan(this.address2TextBox, 2);
            this.address2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "address2", true));
            this.address2TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.address2TextBox.Location = new System.Drawing.Point(119, 190);
            this.address2TextBox.Name = "address2TextBox";
            this.address2TextBox.Size = new System.Drawing.Size(260, 23);
            this.address2TextBox.TabIndex = 5;
            // 
            // addEmployeeTableLayoutPanel
            // 
            this.addEmployeeTableLayoutPanel.ColumnCount = 4;
            this.addEmployeeTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.76098F));
            this.addEmployeeTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56.23902F));
            this.addEmployeeTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 117F));
            this.addEmployeeTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 186F));
            this.addEmployeeTableLayoutPanel.Controls.Add(sexLabel, 2, 5);
            this.addEmployeeTableLayoutPanel.Controls.Add(this.sexComboBox, 3, 5);
            this.addEmployeeTableLayoutPanel.Controls.Add(zipLabel, 0, 5);
            this.addEmployeeTableLayoutPanel.Controls.Add(this.welcomeLabel, 0, 0);
            this.addEmployeeTableLayoutPanel.Controls.Add(first_nameLabel, 0, 1);
            this.addEmployeeTableLayoutPanel.Controls.Add(this.first_nameTextBox, 1, 1);
            this.addEmployeeTableLayoutPanel.Controls.Add(last_nameLabel, 2, 1);
            this.addEmployeeTableLayoutPanel.Controls.Add(this.last_nameTextBox, 3, 1);
            this.addEmployeeTableLayoutPanel.Controls.Add(date_of_birthLabel, 0, 2);
            this.addEmployeeTableLayoutPanel.Controls.Add(this.date_of_birthTextBox, 1, 2);
            this.addEmployeeTableLayoutPanel.Controls.Add(phone_numberLabel, 2, 2);
            this.addEmployeeTableLayoutPanel.Controls.Add(this.phone_numberTextBox, 3, 2);
            this.addEmployeeTableLayoutPanel.Controls.Add(address1Label, 0, 3);
            this.addEmployeeTableLayoutPanel.Controls.Add(address2Label, 0, 4);
            this.addEmployeeTableLayoutPanel.Controls.Add(this.address1TextBox, 1, 3);
            this.addEmployeeTableLayoutPanel.Controls.Add(this.address2TextBox, 1, 4);
            this.addEmployeeTableLayoutPanel.Controls.Add(usernameLabel, 0, 6);
            this.addEmployeeTableLayoutPanel.Controls.Add(passwordLabel, 2, 6);
            this.addEmployeeTableLayoutPanel.Controls.Add(this.usernameTextBox, 1, 6);
            this.addEmployeeTableLayoutPanel.Controls.Add(this.passwordTextBox, 3, 6);
            this.addEmployeeTableLayoutPanel.Controls.Add(this.createButton, 1, 8);
            this.addEmployeeTableLayoutPanel.Controls.Add(this.clearButton, 2, 8);
            this.addEmployeeTableLayoutPanel.Controls.Add(stateLabel, 0, 7);
            this.addEmployeeTableLayoutPanel.Controls.Add(this.codeComboBox, 1, 7);
            this.addEmployeeTableLayoutPanel.Controls.Add(cityLabel, 2, 7);
            this.addEmployeeTableLayoutPanel.Controls.Add(this.cityTextBox, 3, 7);
            this.addEmployeeTableLayoutPanel.Controls.Add(this.zipTextBox, 1, 5);
            this.addEmployeeTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addEmployeeTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.addEmployeeTableLayoutPanel.Name = "addEmployeeTableLayoutPanel";
            this.addEmployeeTableLayoutPanel.RowCount = 9;
            this.addEmployeeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.54331F));
            this.addEmployeeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.45669F));
            this.addEmployeeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.addEmployeeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.addEmployeeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.addEmployeeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.addEmployeeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.addEmployeeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.addEmployeeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.addEmployeeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.addEmployeeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.addEmployeeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.addEmployeeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.addEmployeeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.addEmployeeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.addEmployeeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.addEmployeeTableLayoutPanel.Size = new System.Drawing.Size(569, 371);
            this.addEmployeeTableLayoutPanel.TabIndex = 21;
            // 
            // sexComboBox
            // 
            this.sexComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.sexComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "sex", true));
            this.sexComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sexComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sexComboBox.FormattingEnabled = true;
            this.sexComboBox.Location = new System.Drawing.Point(385, 228);
            this.sexComboBox.Name = "sexComboBox";
            this.sexComboBox.Size = new System.Drawing.Size(121, 24);
            this.sexComboBox.TabIndex = 7;
            // 
            // welcomeLabel
            // 
            this.welcomeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.welcomeLabel.AutoSize = true;
            this.addEmployeeTableLayoutPanel.SetColumnSpan(this.welcomeLabel, 4);
            this.welcomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.welcomeLabel.Location = new System.Drawing.Point(24, 0);
            this.welcomeLabel.Name = "welcomeLabel";
            this.welcomeLabel.Size = new System.Drawing.Size(520, 53);
            this.welcomeLabel.TabIndex = 0;
            this.welcomeLabel.Text = "Please Complete the Information Below to Add a New Employee";
            this.welcomeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.usernameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "first_name", true));
            this.usernameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameTextBox.Location = new System.Drawing.Point(119, 268);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.Size = new System.Drawing.Size(121, 23);
            this.usernameTextBox.TabIndex = 8;
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.passwordTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "first_name", true));
            this.passwordTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordTextBox.Location = new System.Drawing.Point(385, 268);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.PasswordChar = '*';
            this.passwordTextBox.Size = new System.Drawing.Size(121, 23);
            this.passwordTextBox.TabIndex = 9;
            // 
            // createButton
            // 
            this.createButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.createButton.Location = new System.Drawing.Point(153, 344);
            this.createButton.Name = "createButton";
            this.createButton.Size = new System.Drawing.Size(75, 23);
            this.createButton.TabIndex = 12;
            this.createButton.Text = "Create";
            this.createButton.UseVisualStyleBackColor = true;
            this.createButton.Click += new System.EventHandler(this.CreateButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.clearButton.Location = new System.Drawing.Point(286, 344);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(75, 23);
            this.clearButton.TabIndex = 13;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // codeComboBox
            // 
            this.codeComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.codeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.stateBindingSource, "code", true));
            this.codeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.codeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.codeComboBox.FormattingEnabled = true;
            this.codeComboBox.Location = new System.Drawing.Point(119, 307);
            this.codeComboBox.Name = "codeComboBox";
            this.codeComboBox.Size = new System.Drawing.Size(121, 24);
            this.codeComboBox.TabIndex = 10;
            // 
            // stateBindingSource
            // 
            this.stateBindingSource.DataMember = "state";
            this.stateBindingSource.DataSource = this._cs6232_g4DataSet;
            // 
            // cityTextBox
            // 
            this.cityTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cityTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "first_name", true));
            this.cityTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cityTextBox.Location = new System.Drawing.Point(385, 308);
            this.cityTextBox.Name = "cityTextBox";
            this.cityTextBox.Size = new System.Drawing.Size(121, 23);
            this.cityTextBox.TabIndex = 11;
            // 
            // zipTextBox
            // 
            this.zipTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.zipTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "first_name", true));
            this.zipTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zipTextBox.Location = new System.Drawing.Point(119, 229);
            this.zipTextBox.Name = "zipTextBox";
            this.zipTextBox.Size = new System.Drawing.Size(121, 23);
            this.zipTextBox.TabIndex = 6;
            // 
            // userTableAdapter
            // 
            this.userTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.categoryTableAdapter = null;
            this.tableAdapterManager.furniture_itemTableAdapter = null;
            this.tableAdapterManager.locationTableAdapter = null;
            this.tableAdapterManager.rental_itemTableAdapter = null;
            this.tableAdapterManager.rental_transactionTableAdapter = null;
            this.tableAdapterManager.return_itemTableAdapter = null;
            this.tableAdapterManager.return_transactionTableAdapter = null;
            this.tableAdapterManager.roleTableAdapter = null;
            this.tableAdapterManager.stateTableAdapter = null;
            this.tableAdapterManager.styleTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.userTableAdapter = this.userTableAdapter;
            // 
            // stateTableAdapter
            // 
            this.stateTableAdapter.ClearBeforeFill = true;
            // 
            // AddEmployeeUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.addEmployeeTableLayoutPanel);
            this.Name = "AddEmployeeUserControl";
            this.Size = new System.Drawing.Size(569, 371);
            this.VisibleChanged += new System.EventHandler(this.AddEmployeeUserControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).EndInit();
            this.addEmployeeTableLayoutPanel.ResumeLayout(false);
            this.addEmployeeTableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private _cs6232_g4DataSet _cs6232_g4DataSet;
        private System.Windows.Forms.BindingSource userBindingSource;
        private _cs6232_g4DataSetTableAdapters.userTableAdapter userTableAdapter;
        private _cs6232_g4DataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox date_of_birthTextBox;
        private System.Windows.Forms.TextBox first_nameTextBox;
        private System.Windows.Forms.TextBox last_nameTextBox;
        private System.Windows.Forms.TextBox phone_numberTextBox;
        private System.Windows.Forms.TextBox address1TextBox;
        private System.Windows.Forms.TextBox address2TextBox;
        private System.Windows.Forms.TableLayoutPanel addEmployeeTableLayoutPanel;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Label welcomeLabel;
        private System.Windows.Forms.Button createButton;
        private System.Windows.Forms.ComboBox sexComboBox;
        private System.Windows.Forms.TextBox usernameTextBox;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.ComboBox codeComboBox;
        private System.Windows.Forms.BindingSource stateBindingSource;
        private System.Windows.Forms.TextBox cityTextBox;
        private _cs6232_g4DataSetTableAdapters.stateTableAdapter stateTableAdapter;
        private System.Windows.Forms.TextBox zipTextBox;
    }
}
