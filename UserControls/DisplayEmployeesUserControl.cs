﻿using System;
using System.Windows.Forms;
using RentMe_G4.Controller;
using RentMe_G4.Model;

namespace RentMe_G4.UserControls
{
    /// <summary>
    /// User control for displaying employees
    /// </summary>
    public partial class DisplayEmployeesUserControl : UserControl
    {
        private readonly UserController UserController;

        /// <summary>
        /// Constructor used to initialize the class
        /// </summary>
        public DisplayEmployeesUserControl()
        {
            InitializeComponent();
            this.UserController = new UserController();
        }

        private void LoadDisplayEmployees(object sender, EventArgs e)
        {
            this.RefreshListView();
        }

        private void RefreshListView()
        {
            this.displayEmployeeListView.Clear();
            this.displayEmployeeListView.View = System.Windows.Forms.View.Details;

            this.displayEmployeeListView.Columns.Add("UserID");
            this.displayEmployeeListView.Columns.Add("First Name");
            this.displayEmployeeListView.Columns.Add("Last Name");
            this.displayEmployeeListView.Columns.Add("Username");
            this.displayEmployeeListView.Columns.Add("Phone Number");
            this.displayEmployeeListView.Columns.Add("Date of Birth");
            this.displayEmployeeListView.Columns.Add("Address");
            this.displayEmployeeListView.Columns.Add("Sex");
            this.displayEmployeeListView.Columns.Add("Is Active");

            try
            {
                this.displayEmployeeListView.BeginUpdate();
                
                foreach (User user in this.UserController.GetEmployees())
                {
                    int userID = user.UserID;
                    string firstName = user.FirstName;
                    string lastName = user.LastName;
                    string username = user.Username;
                    string phoneNumber = user.PhoneNumber;
                    DateTime dateOfBirth = user.DateOfBirth;
                    string address = user.Address1 + " " + user.Address2 +
                        ", " + user.Zip;
                    char sex = user.Sex;
                    bool status = user.Status;

                    ListViewItem employee = new ListViewItem(new[] { userID.ToString(), firstName, lastName, username, phoneNumber.ToString(), dateOfBirth.ToString("MM/dd/yyyy"), address, sex.ToString(), status.ToString() });
                    this.displayEmployeeListView.Items.Add(employee);
                }

                foreach (ColumnHeader column in this.displayEmployeeListView.Columns)
                {
                    column.Width = -2;
                }

                this.displayEmployeeListView.EndUpdate();
        }
            catch (Exception ex)
            {
                MessageBox.Show("An error has occurred: \n" + ex.Message, "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
}
    }
}
