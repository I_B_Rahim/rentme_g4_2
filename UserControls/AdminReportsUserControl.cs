﻿using System;
using System.Windows.Forms;

namespace RentMe_G4.UserControls
{
    public partial class AdminReportsUserControl : UserControl
    {
        public AdminReportsUserControl()
        {
            InitializeComponent();
            StartDateTimePicker.Format = DateTimePickerFormat.Custom;
            StartDateTimePicker.CustomFormat = "MM-dd-yyyy";
            EndDateTimePicker.Format = DateTimePickerFormat.Custom;
            EndDateTimePicker.CustomFormat = "MM-dd-yyyy";
            StartDateTimePicker.Value = new DateTime(2015, 03, 28);
            EndDateTimePicker.Value = new DateTime(2015, 12, 10);
        }

        private void LoadFurnitureReport(object sender, EventArgs e)
        {
            try
            {
                this.getPopularFurnitureTableAdapter.Fill(this._cs6232_g4DataSet2.GetPopularFurniture, StartDateTimePicker.Value, EndDateTimePicker.Value);
                this.PopularFurnitureReport.LocalReport.DisplayName = "Popular_Furniture_for_" + StartDateTimePicker.Value.ToShortDateString() + "_" + EndDateTimePicker.Value.ToShortDateString();
                this.PopularFurnitureReport.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "The Report Failed to Load", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
