﻿namespace RentMe_G4.View
{
    partial class RentalItemsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._cs6232_g4DataSet = new RentMe_G4._cs6232_g4DataSet();
            this.rental_itemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rental_itemTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.rental_itemTableAdapter();
            this.tableAdapterManager = new RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager();
            this.rental_itemDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.selectItemBtn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.viewItemDetails = new System.Windows.Forms.DataGridViewButtonColumn();
            this.RentalItemsLabel = new System.Windows.Forms.Label();
            this.RentalItemsRentalIDtxtbox = new System.Windows.Forms.TextBox();
            this.itemsSelectedLabel = new System.Windows.Forms.Label();
            this.itemsListView = new System.Windows.Forms.ListView();
            this.rentalItemsAccept = new System.Windows.Forms.Button();
            this.rentalItemsCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rental_itemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rental_itemDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // _cs6232_g4DataSet
            // 
            this._cs6232_g4DataSet.DataSetName = "_cs6232_g4DataSet";
            this._cs6232_g4DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rental_itemBindingSource
            // 
            this.rental_itemBindingSource.DataMember = "rental_item";
            this.rental_itemBindingSource.DataSource = this._cs6232_g4DataSet;
            // 
            // rental_itemTableAdapter
            // 
            this.rental_itemTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.categoryTableAdapter = null;
            this.tableAdapterManager.furniture_itemTableAdapter = null;
            this.tableAdapterManager.locationTableAdapter = null;
            this.tableAdapterManager.rental_itemTableAdapter = this.rental_itemTableAdapter;
            this.tableAdapterManager.rental_transactionTableAdapter = null;
            this.tableAdapterManager.return_itemTableAdapter = null;
            this.tableAdapterManager.return_transactionTableAdapter = null;
            this.tableAdapterManager.roleTableAdapter = null;
            this.tableAdapterManager.stateTableAdapter = null;
            this.tableAdapterManager.styleTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.userTableAdapter = null;
            // 
            // rental_itemDataGridView
            // 
            this.rental_itemDataGridView.AllowUserToAddRows = false;
            this.rental_itemDataGridView.AllowUserToDeleteRows = false;
            this.rental_itemDataGridView.AutoGenerateColumns = false;
            this.rental_itemDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rental_itemDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.selectItemBtn,
            this.viewItemDetails});
            this.rental_itemDataGridView.DataSource = this.rental_itemBindingSource;
            this.rental_itemDataGridView.Location = new System.Drawing.Point(12, 53);
            this.rental_itemDataGridView.Name = "rental_itemDataGridView";
            this.rental_itemDataGridView.ReadOnly = true;
            this.rental_itemDataGridView.RowHeadersVisible = false;
            this.rental_itemDataGridView.Size = new System.Drawing.Size(571, 190);
            this.rental_itemDataGridView.TabIndex = 1;
            this.rental_itemDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Rental_itemDataGridView_CellContentClick);
            this.rental_itemDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.Rental_itemDataGridView_DataError);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "itemID";
            this.dataGridViewTextBoxColumn1.HeaderText = "Item ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 70;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "rentalID";
            this.dataGridViewTextBoxColumn2.HeaderText = "Rental ID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 80;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "serial_number";
            this.dataGridViewTextBoxColumn3.HeaderText = "Serial Number";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "quantity";
            this.dataGridViewTextBoxColumn4.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // selectItemBtn
            // 
            this.selectItemBtn.HeaderText = "";
            this.selectItemBtn.Name = "selectItemBtn";
            this.selectItemBtn.ReadOnly = true;
            this.selectItemBtn.Text = "Select/Deselect";
            this.selectItemBtn.UseColumnTextForButtonValue = true;
            // 
            // viewItemDetails
            // 
            this.viewItemDetails.HeaderText = "";
            this.viewItemDetails.Name = "viewItemDetails";
            this.viewItemDetails.ReadOnly = true;
            this.viewItemDetails.Text = "View Item Details";
            this.viewItemDetails.UseColumnTextForButtonValue = true;
            // 
            // RentalItemsLabel
            // 
            this.RentalItemsLabel.AutoSize = true;
            this.RentalItemsLabel.Location = new System.Drawing.Point(12, 21);
            this.RentalItemsLabel.Name = "RentalItemsLabel";
            this.RentalItemsLabel.Size = new System.Drawing.Size(139, 13);
            this.RentalItemsLabel.TabIndex = 2;
            this.RentalItemsLabel.Text = "Items Rented For Rental ID:";
            // 
            // RentalItemsRentalIDtxtbox
            // 
            this.RentalItemsRentalIDtxtbox.Location = new System.Drawing.Point(157, 18);
            this.RentalItemsRentalIDtxtbox.Name = "RentalItemsRentalIDtxtbox";
            this.RentalItemsRentalIDtxtbox.ReadOnly = true;
            this.RentalItemsRentalIDtxtbox.Size = new System.Drawing.Size(49, 20);
            this.RentalItemsRentalIDtxtbox.TabIndex = 3;
            // 
            // itemsSelectedLabel
            // 
            this.itemsSelectedLabel.AutoSize = true;
            this.itemsSelectedLabel.Location = new System.Drawing.Point(589, 53);
            this.itemsSelectedLabel.Name = "itemsSelectedLabel";
            this.itemsSelectedLabel.Size = new System.Drawing.Size(149, 13);
            this.itemsSelectedLabel.TabIndex = 6;
            this.itemsSelectedLabel.Text = "Item ID\'s Selected For Return:";
            // 
            // itemsListView
            // 
            this.itemsListView.HideSelection = false;
            this.itemsListView.Location = new System.Drawing.Point(620, 75);
            this.itemsListView.Name = "itemsListView";
            this.itemsListView.Size = new System.Drawing.Size(80, 168);
            this.itemsListView.TabIndex = 5;
            this.itemsListView.Tag = "itemsListView";
            this.itemsListView.UseCompatibleStateImageBehavior = false;
            this.itemsListView.View = System.Windows.Forms.View.List;
            // 
            // rentalItemsAccept
            // 
            this.rentalItemsAccept.Location = new System.Drawing.Point(620, 262);
            this.rentalItemsAccept.Name = "rentalItemsAccept";
            this.rentalItemsAccept.Size = new System.Drawing.Size(80, 23);
            this.rentalItemsAccept.TabIndex = 8;
            this.rentalItemsAccept.Text = "Accept";
            this.rentalItemsAccept.UseVisualStyleBackColor = true;
            this.rentalItemsAccept.Click += new System.EventHandler(this.RentalItemsAccept_Click);
            // 
            // rentalItemsCancel
            // 
            this.rentalItemsCancel.Location = new System.Drawing.Point(12, 262);
            this.rentalItemsCancel.Name = "rentalItemsCancel";
            this.rentalItemsCancel.Size = new System.Drawing.Size(75, 23);
            this.rentalItemsCancel.TabIndex = 9;
            this.rentalItemsCancel.Text = "Cancel";
            this.rentalItemsCancel.UseVisualStyleBackColor = true;
            this.rentalItemsCancel.Click += new System.EventHandler(this.RentalItemsCancel_Click);
            // 
            // RentalItemsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 308);
            this.Controls.Add(this.rentalItemsCancel);
            this.Controls.Add(this.rentalItemsAccept);
            this.Controls.Add(this.itemsSelectedLabel);
            this.Controls.Add(this.itemsListView);
            this.Controls.Add(this.RentalItemsRentalIDtxtbox);
            this.Controls.Add(this.RentalItemsLabel);
            this.Controls.Add(this.rental_itemDataGridView);
            this.Name = "RentalItemsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rental Items Form";
            this.Load += new System.EventHandler(this.RentalItemsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rental_itemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rental_itemDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private _cs6232_g4DataSet _cs6232_g4DataSet;
        private System.Windows.Forms.BindingSource rental_itemBindingSource;
        private _cs6232_g4DataSetTableAdapters.rental_itemTableAdapter rental_itemTableAdapter;
        private _cs6232_g4DataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView rental_itemDataGridView;
        private System.Windows.Forms.Label RentalItemsLabel;
        private System.Windows.Forms.TextBox RentalItemsRentalIDtxtbox;
        private System.Windows.Forms.Label itemsSelectedLabel;
        private System.Windows.Forms.ListView itemsListView;
        private System.Windows.Forms.Button rentalItemsAccept;
        private System.Windows.Forms.Button rentalItemsCancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewButtonColumn selectItemBtn;
        private System.Windows.Forms.DataGridViewButtonColumn viewItemDetails;
    }
}