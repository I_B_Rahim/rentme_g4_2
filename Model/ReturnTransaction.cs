﻿using System;


namespace RentMe_G4.Model
{
    /// <summary>
    /// This class models a return transaction
    /// </summary>
    public class ReturnTransaction
    {
        private int returnID;
        private int employeeID;
        private DateTime returnDate;

        /// <summary>
        /// Accessor and modifier for returnID
        /// </summary>
        public int ReturnID
        {
            get
            {
                return this.returnID;
            }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentException("returnID cannot be less than 1", "returnID error");
                }
                this.returnID = value;
            }
        }

        /// <summary>
        /// Accessor and modifier for employeeID
        /// </summary>
        public int EmployeeID
        {
            get
            {
                return this.employeeID;
            }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentException("employeeID cannot be less than 1", "employeeID error");
                }
                this.employeeID = value;
            }
        }

        /// <summary>
        /// Accessor and modifier for returnDate
        /// </summary>
        public DateTime ReturnDate
        {
            get
            {
                return this.returnDate;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("returnDate cannot be null", "returnDate error");
                }
                this.returnDate = value;
            }
        }
    }
}
