﻿using System;

namespace RentMe_G4.Model
{
    /// <summary>
    /// This class models a user
    /// </summary>
    public class User
    {
        private int userID;
        private int roleID;
        private DateTime dateOfBirth;
        private string firstName;
        private string lastName;
        private string phoneNumber;
        private string address1;
        private string address2;
        private string zip;
        private string username;
        public bool Status { get; set; }
        private string password;
        public string city;
        public string state;

        /// <summary>
        /// Accessor and modifier for the UserID
        /// </summary>
        public int UserID
        {
            get
            {
                return this.userID;
            }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentException("userID cannot be less than 1", "userID error");
                }
                this.userID = value;
            }
        }

        /// <summary>
        /// Accessor and modifier for RoleID
        /// </summary>
        public int RoleID
        {
            get
            {
                return this.roleID;
            }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentException("role cannot be less than 1", "role error");
                }
                this.roleID = value;
            }
        }

        /// <summary>
        /// Accessor and modifier for DateOfBirth
        /// </summary>
        public DateTime DateOfBirth
        {
            get
            {
                return this.dateOfBirth;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("dateOfBirth cannot be null", "dateOfBirth error");
                }
                this.dateOfBirth = value;
            }
        }

        /// <summary>
        /// Accessor and modifier for FirstName
        /// </summary>
        public string FirstName
        {
            get
            {
                return this.firstName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("firstName cannot be null or empty", "firstName error");
                }
                this.firstName = value;
            }
        }

        /// <summary>
        /// Accessor and modifier for LastName
        /// </summary>
        public string LastName
        {
            get
            {
                return this.lastName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("lastName cannot be null or empty", "lastName error");
                }
                this.lastName = value;
            }
        }
        /// <summary>
        /// Accessor and modifier for PhoneNumber
        /// </summary>
        public string PhoneNumber
        {
            get
            {
                return this.phoneNumber;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("phoneNumber cannot be null or empty", "phoneNumber error");
                }
                this.phoneNumber = value;
            }
        }

        /// <summary>
        /// Accessor and modifier for Address1
        /// </summary>
        public string Address1
        {
            get
            {
                return this.address1;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("address1 cannot be null or empty", "address1 error");
                }
                this.address1 = value;
            }
        }

        /// <summary>
        /// Accessor and modifier for Address2
        /// </summary>
        public string Address2
        {
            get
            {
                return this.address2;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    this.address2 = "";
                }
                else
                {
                    this.address2 = value;
                }
                
            }
        }

        /// <summary>
        /// Accessor and modifier for Zip
        /// </summary>
        public string Zip
        {
            get
            {
                return this.zip;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("zip cannot be null or empty", "zip error");
                }
                this.zip = value;
            }
        }

        /// <summary>
        /// Accessor and modifier for City
        /// </summary>
        public string City
        {
            get
            {
                return this.city;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("city cannot be null or empty", "city error");
                }
                this.city = value;
            }
        }

        /// <summary>
        /// Accessor and modifier for State
        /// </summary>
        public string State
        {
            get
            {
                return this.state;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("state cannot be null or empty", "state error");
                }
                this.state = value;
            }
        }

        /// <summary>
        /// Accessor and modifier for Sex
        /// </summary>
        public char Sex { get; set; }

        /// <summary>
        /// Accessor and modifier for Username
        /// </summary>
        public string Username
        {
            get
            {
                return this.username;
            }

            set
            {
                if (this.roleID < 3 && string.IsNullOrEmpty(value)) {
                    throw new ArgumentException("Administrators and employess must provide a username");
                }
                this.username = value;
            }
        }

        /// <summary>
        /// Accessor and modifier for Password
        /// </summary>
        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                if (this.roleID < 3 && string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Administrators and employess must provide a password");
                }
                this.password = value;
            }
        }
    }
}
