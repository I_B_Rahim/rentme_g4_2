﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentMe_G4.View
{
    public partial class MemberRentalsForm : Form
    {
        public MemberRentalsForm()
        {
            InitializeComponent();
        }

        private void rental_transactionBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.rental_transactionBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this._cs6232_g4DataSet);

        }

        private void MemberRentalsForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the '_cs6232_g4DataSet.rental_transaction' table. You can move, or remove it, as needed.
            this.rental_transactionTableAdapter.Fill(this._cs6232_g4DataSet.rental_transaction);

        }
    }
}
