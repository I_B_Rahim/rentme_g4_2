﻿using RentMe_G4.View;
using System;
using System.Windows.Forms;

namespace RentMe_G4
{
    /// <summary>
    /// The main class for the program
    /// </summary>
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LoginForm());
        }
    }
}
