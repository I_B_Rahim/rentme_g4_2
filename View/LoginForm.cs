﻿using RentMe_G4.Controller;
using RentMe_G4.Model;
using System;
using System.Windows.Forms;

namespace RentMe_G4.View
{

    /// <summary>
    /// The LoginForm is the first view for the RentMe application. A valid user's credentials must be entered here to acces the MainForm.
    /// </summary>
    public partial class LoginForm : Form
    {
        EmployeeMainDashboard theMainDashboard;
        EmployeeController theEmployeeController;
        private AdministratorMainDashboard AdministratorMainDashboard;
        private UserController UserController;

        /// <summary>
        /// Constructor for the LoginForm.
        /// </summary>
        public LoginForm()
        {
            InitializeComponent();
            loginMessageLabel.Text = "";
            theEmployeeController = new EmployeeController();
            this.AdministratorMainDashboard = new AdministratorMainDashboard();
            this.UserController = new UserController();
        }

        /// <summary>
        /// Allows the MainForm to initialize a reset of this LoginForm when logging out of the Main program.
        /// </summary>
        public void ResetLoginForm()
        {
            usernameTextBox.Text = "";
            passwordTextBox.Text = "";
            ClearLoginMessage();
            Show();
        }

        private void ClearLoginMessage()
        {
            loginMessageLabel.Text = "";
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            string attemptedPassword = Encryption.EncryptPassword(this.passwordTextBox.Text);
            Employee theEmployee = theEmployeeController.GetEmployeeByUserNameAndPassword(usernameTextBox.Text, attemptedPassword);

            if (theEmployee != null)
            {
                Hide();
                SetupTheMainDashBoard(theEmployee);
                theMainDashboard.SetTheLoginForm(this);
                theMainDashboard.Show();
            }
            else if (this.UserController.IsValidAdministrator(usernameTextBox.Text, attemptedPassword)) {
                this.AdministratorMainDashboard.username = usernameTextBox.Text;
                this.Hide();
                this.usernameTextBox.Clear();
                this.passwordTextBox.Clear();
                DialogResult result = AdministratorMainDashboard.ShowDialog();
                this.ShowAdministratorMainDashboard(result);
            }
            else
            {
                loginMessageLabel.Text = "invalid username/password";
                loginMessageLabel.ForeColor = System.Drawing.Color.Red;
            }
        }

        private void ShowAdministratorMainDashboard(DialogResult result)
        {
            if (result == DialogResult.OK)
            {
                this.Show();
            }
            else
            {
                Application.Exit();
            }
        }

        private void SetupTheMainDashBoard(Employee theEmployee)
        {
            theMainDashboard = new EmployeeMainDashboard(theEmployee);
        }

        private void UsernameTextBox_TextChanged(object sender, EventArgs e)
        {
            ClearLoginMessage();
        }

        private void PasswordTextBox_TextChanged(object sender, EventArgs e)
        {
            ClearLoginMessage();
        }
    }
}
