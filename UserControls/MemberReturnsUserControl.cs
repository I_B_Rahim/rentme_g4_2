﻿using System;
using System.Collections.Generic;

using System.Windows.Forms;
using RentMe_G4.Controller;
using RentMe_G4.Model;
using RentMe_G4.View;

namespace RentMe_G4.UserControls
{
    /// <summary>
    /// This class creates a user control to view the return history for a member
    /// </summary>
    public partial class MemberReturnsUserControl : UserControl
    {
        public string memberID;

        /// <summary>
        /// initializes the MemberReturnUserControl
        /// </summary>
        public MemberReturnsUserControl()
        {
            InitializeComponent();
        }

        private void MemberReturnsUserControl_Load(object sender, EventArgs e)
        {
            this.SetUpMemberReturnsFieldsAndDataGridView();
        }

        /// <summary>
        /// This method allows to setup the values for the member info fields and the data for the data grid view
        /// </summary>
        public void SetUpMemberReturnsFieldsAndDataGridView()
        {
            try
            {
                if (string.IsNullOrEmpty(memberID))
                {
                    memberErrorlabel.Visible = true;
                    this.userTableAdapter.FillBy(this._cs6232_g4DataSet.user, 0);
                    return_transactionDataGridView.Rows.Clear();
                    return_transactionDataGridView.Refresh();
                }
                else
                {
                    return_transactionDataGridView.Rows.Clear();
                    return_transactionDataGridView.Refresh();
                    memberErrorlabel.Visible = false;
                    this.userTableAdapter.FillBy(this._cs6232_g4DataSet.user, ((int)(System.Convert.ChangeType(memberID, typeof(int)))));
                    this.PopulateTheDataGridView();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        /// <summary>
        /// Populates the DataGridView
        /// </summary>
        private void PopulateTheDataGridView()
        {
            List<ReturnTransaction> listOfMembersReturns = new List<ReturnTransaction>();
            listOfMembersReturns = UserController.GetMemberReturnTransactionHistory(int.Parse(memberID));
            for (int index = 0; index < listOfMembersReturns.Count; index++)
            {
                int indexRow = return_transactionDataGridView.Rows.Add();
                return_transactionDataGridView.Rows[indexRow].Cells[0].Value = listOfMembersReturns[index].ReturnID;
                return_transactionDataGridView.Rows[indexRow].Cells[1].Value = listOfMembersReturns[index].EmployeeID;
                return_transactionDataGridView.Rows[indexRow].Cells[2].Value = listOfMembersReturns[index].ReturnDate;
            }
        }

        private void Return_transactionDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 3)
                {
                    int rowIndex = e.RowIndex;
                    DataGridViewRow row = return_transactionDataGridView.Rows[rowIndex];
                    DataGridViewCell cell = row.Cells[0];
                    int returnID = (int)cell.Value;
                    using (ItemsInReturnHistoryForm itemsInReturnHistory = new ItemsInReturnHistoryForm())
                    {
                        itemsInReturnHistory.Tag = returnID;
                        itemsInReturnHistory.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void Return_transactionDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            int row = e.RowIndex + 1;
            string errorMessage = "A data error occurred.\n" +
                "Row: " + row + "\n" +
                "Error: " + e.Exception.Message;
            MessageBox.Show(errorMessage, "Data Error");
        }
    }
}
