﻿namespace RentMe_G4.UserControls
{
    partial class AdminReportsUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this._cs6232_g4DataSet2 = new RentMe_G4._cs6232_g4DataSet2();
            this.PopularFurnitureReport = new Microsoft.Reporting.WinForms.ReportViewer();
            this.StartDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.EndDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.StartDateTimePickerLabel = new System.Windows.Forms.Label();
            this.EndDateTimePickerLabel = new System.Windows.Forms.Label();
            this.LookupDateRangeButton = new System.Windows.Forms.Button();
            this.DateRangeControlsTabelLayout = new System.Windows.Forms.TableLayoutPanel();
            this.getPopularFurnitureBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.getPopularFurnitureTableAdapter = new RentMe_G4._cs6232_g4DataSet2TableAdapters.GetPopularFurnitureTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet2)).BeginInit();
            this.DateRangeControlsTabelLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.getPopularFurnitureBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // _cs6232_g4DataSet2
            // 
            this._cs6232_g4DataSet2.DataSetName = "_cs6232_g4DataSet2";
            this._cs6232_g4DataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // PopularFurnitureReport
            // 
            this.PopularFurnitureReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource1.Name = "GetPopularFurniture";
            reportDataSource1.Value = this.getPopularFurnitureBindingSource;
            this.PopularFurnitureReport.LocalReport.DataSources.Add(reportDataSource1);
            this.PopularFurnitureReport.LocalReport.ReportEmbeddedResource = "RentMe_G4.Reports.AdminFurnitureReport.rdlc";
            this.PopularFurnitureReport.Location = new System.Drawing.Point(4, 64);
            this.PopularFurnitureReport.Name = "PopularFurnitureReport";
            this.PopularFurnitureReport.ServerReport.BearerToken = null;
            this.PopularFurnitureReport.Size = new System.Drawing.Size(602, 294);
            this.PopularFurnitureReport.TabIndex = 0;
            // 
            // StartDateTimePicker
            // 
            this.StartDateTimePicker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.StartDateTimePicker.Location = new System.Drawing.Point(57, 17);
            this.StartDateTimePicker.Name = "StartDateTimePicker";
            this.StartDateTimePicker.Size = new System.Drawing.Size(196, 20);
            this.StartDateTimePicker.TabIndex = 1;
            // 
            // EndDateTimePicker
            // 
            this.EndDateTimePicker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.EndDateTimePicker.Location = new System.Drawing.Point(306, 17);
            this.EndDateTimePicker.Name = "EndDateTimePicker";
            this.EndDateTimePicker.Size = new System.Drawing.Size(199, 20);
            this.EndDateTimePicker.TabIndex = 2;
            // 
            // StartDateTimePickerLabel
            // 
            this.StartDateTimePickerLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.StartDateTimePickerLabel.AutoSize = true;
            this.StartDateTimePickerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartDateTimePickerLabel.Location = new System.Drawing.Point(9, 10);
            this.StartDateTimePickerLabel.Name = "StartDateTimePickerLabel";
            this.StartDateTimePickerLabel.Size = new System.Drawing.Size(42, 34);
            this.StartDateTimePickerLabel.TabIndex = 3;
            this.StartDateTimePickerLabel.Text = "Start Date";
            // 
            // EndDateTimePickerLabel
            // 
            this.EndDateTimePickerLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.EndDateTimePickerLabel.AutoSize = true;
            this.EndDateTimePickerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EndDateTimePickerLabel.Location = new System.Drawing.Point(262, 10);
            this.EndDateTimePickerLabel.Name = "EndDateTimePickerLabel";
            this.EndDateTimePickerLabel.Size = new System.Drawing.Size(38, 34);
            this.EndDateTimePickerLabel.TabIndex = 4;
            this.EndDateTimePickerLabel.Text = "End Date";
            // 
            // LookupDateRangeButton
            // 
            this.LookupDateRangeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LookupDateRangeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LookupDateRangeButton.Location = new System.Drawing.Point(519, 3);
            this.LookupDateRangeButton.Name = "LookupDateRangeButton";
            this.LookupDateRangeButton.Size = new System.Drawing.Size(78, 48);
            this.LookupDateRangeButton.TabIndex = 5;
            this.LookupDateRangeButton.Text = "Look Up";
            this.LookupDateRangeButton.UseVisualStyleBackColor = true;
            this.LookupDateRangeButton.Click += new System.EventHandler(this.LoadFurnitureReport);
            // 
            // DateRangeControlsTabelLayout
            // 
            this.DateRangeControlsTabelLayout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DateRangeControlsTabelLayout.ColumnCount = 6;
            this.DateRangeControlsTabelLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.39918F));
            this.DateRangeControlsTabelLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 78.60082F));
            this.DateRangeControlsTabelLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.DateRangeControlsTabelLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 205F));
            this.DateRangeControlsTabelLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.DateRangeControlsTabelLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 83F));
            this.DateRangeControlsTabelLayout.Controls.Add(this.StartDateTimePickerLabel, 0, 0);
            this.DateRangeControlsTabelLayout.Controls.Add(this.StartDateTimePicker, 1, 0);
            this.DateRangeControlsTabelLayout.Controls.Add(this.EndDateTimePicker, 3, 0);
            this.DateRangeControlsTabelLayout.Controls.Add(this.EndDateTimePickerLabel, 2, 0);
            this.DateRangeControlsTabelLayout.Controls.Add(this.LookupDateRangeButton, 5, 0);
            this.DateRangeControlsTabelLayout.Location = new System.Drawing.Point(5, 4);
            this.DateRangeControlsTabelLayout.Margin = new System.Windows.Forms.Padding(1);
            this.DateRangeControlsTabelLayout.MaximumSize = new System.Drawing.Size(600, 54);
            this.DateRangeControlsTabelLayout.MinimumSize = new System.Drawing.Size(600, 54);
            this.DateRangeControlsTabelLayout.Name = "DateRangeControlsTabelLayout";
            this.DateRangeControlsTabelLayout.RowCount = 1;
            this.DateRangeControlsTabelLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.DateRangeControlsTabelLayout.Size = new System.Drawing.Size(600, 54);
            this.DateRangeControlsTabelLayout.TabIndex = 6;
            // 
            // getPopularFurnitureBindingSource
            // 
            this.getPopularFurnitureBindingSource.DataMember = "GetPopularFurniture";
            this.getPopularFurnitureBindingSource.DataSource = this._cs6232_g4DataSet2;
            // 
            // getPopularFurnitureTableAdapter
            // 
            this.getPopularFurnitureTableAdapter.ClearBeforeFill = true;
            // 
            // AdminReportsUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.DateRangeControlsTabelLayout);
            this.Controls.Add(this.PopularFurnitureReport);
            this.Name = "AdminReportsUserControl";
            this.Size = new System.Drawing.Size(613, 397);
            this.VisibleChanged += new System.EventHandler(this.LoadFurnitureReport);
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet2)).EndInit();
            this.DateRangeControlsTabelLayout.ResumeLayout(false);
            this.DateRangeControlsTabelLayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.getPopularFurnitureBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer PopularFurnitureReport;
        private System.Windows.Forms.DateTimePicker StartDateTimePicker;
        private System.Windows.Forms.DateTimePicker EndDateTimePicker;
        private System.Windows.Forms.Label StartDateTimePickerLabel;
        private System.Windows.Forms.Label EndDateTimePickerLabel;
        private System.Windows.Forms.Button LookupDateRangeButton;
        private System.Windows.Forms.TableLayoutPanel DateRangeControlsTabelLayout;
        private _cs6232_g4DataSet2 _cs6232_g4DataSet2;
        private System.Windows.Forms.BindingSource getPopularFurnitureBindingSource;
        private _cs6232_g4DataSet2TableAdapters.GetPopularFurnitureTableAdapter getPopularFurnitureTableAdapter;
    }
}
