﻿using RentMe_G4.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace RentMe_G4.DAL
{
    ///<summary>
    /// The DAL that interacts with ReturnTransaction object
    /// </summary>
    public class ReturnTransactionDAL
    {
        private static SqlConnection connectionForReturn;
        private static SqlCommand returnCommand;
        private static SqlTransaction returnTransaction;
        private static int returnID;

        /// <summary>
        /// This method returns the last returnID in the return transaction table
        /// </summary>
        /// <returns>Returns the last returnID</returns>
        public static int GetLastReturnID()
        {
            int returnID = 0;
            string selectStatement =
                "SELECT TOP 1 [returnID] " +
                "FROM [return_transaction] " +
                "ORDER BY [returnID] DESC ";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            returnID = (int)reader["returnID"];
                        }
                    }
                }
            }
            return returnID;
        }

        /// <summary>
        /// Gets the quantity of the item rented
        /// </summary>
        /// <param name="itemID">itemID to get quantity</param>
        /// <returns>quantity</returns>
        public static int GetQuantityFromRental(int itemID)
        {
            int quantitiy = 0;
            string selectStatement =
                "SELECT TOP 1 [quantity] " +
                "FROM [rental_item] " +
                "WHERE itemID = @itemID " +
                "ORDER BY [itemID] ";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@itemID", itemID);
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            quantitiy = (int)reader["quantity"];
                        }
                    }
                }
            }
            return quantitiy;
        }

        /// <summary>
        /// Gets the quantities of the item returned
        /// </summary>
        /// <param name="itemID">itemID to get quantity</param>
        /// <returns>quantity</returns>
        public static int GetQuantityFromReturn(int itemID)
        {
            int quantitiy = 0;
            string selectStatement =
                "SELECT TOP 1 SUM(quantity) quantity " +
                "FROM[return_item] " +
                "WHERE itemID = @itemID " +
                "GROUP BY itemID " +
                "ORDER BY[itemID] ";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@itemID", itemID);
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            quantitiy = (int)reader["quantity"];
                        }
                    }
                }
            }
            return quantitiy;
        }

        /// <summary>
        /// Gets the returned quantity
        /// </summary>
        /// <param name="itemID">itemID to get quantity</param>
        /// <returns>returned quantity</returns>
        public static int GetReturnedQuantity(int itemID)
        {
            int quantitiy = 0;
            string selectStatement =
                "SELECT SUM(reti.quantity) as returned_quantity " +
                "FROM [rental_item] ri " +
                "JOIN [return_item] reti " +
                "ON ri.itemID = reti.itemID " +
                "WHERE reti.itemID = @itemID " +
                "GROUP BY reti.itemID " +
                "ORDER BY reti.[itemID] ";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@itemID", itemID);
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            quantitiy = (int)reader["returned_quantity"];
                        }
                    }
                }
            }
            return quantitiy;
        }

        /// <summary>
        /// Gets the return transaction history by member id 
        /// </summary>
        /// <param name="memberID">member ID to get return history</param>
        /// <returns>return transaction list</returns>
        public static List<ReturnTransaction> GetMemberReturnTransactionHistory(int memberID)
        {
            List<ReturnTransaction> listOfMembersReturns = new List<ReturnTransaction>();
            string selectStatement =
                "SELECT DISTINCT rt.returnID, rt.employee_userID, rt.return_date " +
                "FROM[return_transaction] rt " +
                "JOIN[return_item] ri " +
                "ON rt.returnID = ri.returnID " +
                "JOIN rental_item rei " +
                "ON ri.itemID = rei.itemID " +
                "JOIN rental_transaction ret " +
                "ON rei.rentalID = ret.rentalID " +
                "WHERE member_userID = @memberID " +
                "ORDER BY rt.returnID ";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@memberID", memberID);
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ReturnTransaction returnTransaction = new ReturnTransaction();
                            returnTransaction.ReturnID = (int)reader["returnID"];
                            returnTransaction.EmployeeID = (int)reader["employee_userID"];
                            returnTransaction.ReturnDate = (DateTime)reader["return_date"];
                            listOfMembersReturns.Add(returnTransaction);
                        }
                    }
                }
            }
            return listOfMembersReturns;
        }

        /// <summary>
        /// Executes a return transaction adding a data into the return transaction table and return items table
        /// </summary>
        /// <param name="aReturnTransactionToAdd">the return transaction to added to the return transaction table</param>
        /// <param name="listOfReturnItems">items to return</param>
        /// <returns>true if return transaction has been executed</returns>
        public static bool ExecuteAReturn(ReturnTransaction aReturnTransactionToAdd, List<ReturnItem> listOfReturnItems)
        {

            returnID = 0;
            using (connectionForReturn = Cs6232_g4DBConnection.GetConnection())
            {

                connectionForReturn.Open();
                returnTransaction = connectionForReturn.BeginTransaction();

                using (returnCommand = new SqlCommand())
                {
                    returnCommand.Connection = connectionForReturn;
                    returnCommand.Transaction = returnTransaction;

                    bool returnTransactionTableAdded = AddReturnTransaction(aReturnTransactionToAdd);
                    bool updatedFurnitureAndReturnItemAdded = false;
                    foreach (ReturnItem item in listOfReturnItems)
                    {
                        updatedFurnitureAndReturnItemAdded = false;
                        if (!UpdateFurnitureQuantity(item) || !AddReturnItem(item))
                        {
                            break;
                        }
                        else
                        {
                            updatedFurnitureAndReturnItemAdded = true;
                        }
                    }
                    if (updatedFurnitureAndReturnItemAdded && returnTransactionTableAdded)
                    {
                        returnTransaction.Commit();
                        return true;
                    }
                    else
                    {
                        returnTransaction.Rollback();
                        return false;
                    }
                }
            }
        }

        /// <summary>
        /// Adds a returnTransaction to the database and returns true if added or false otherwise
        /// </summary>
        /// <param name="aReturnTransactionToAdd">the returnTransaction to be added</param>
        /// <returns>true if returnTransaction was added, false otherwise</returns>
        public static bool AddReturnTransaction(ReturnTransaction aReturnTransactionToAdd)
        {
            bool returnAdded;
            string insertStatement =
                "INSERT [return_transaction] " +
                  "(employee_userID, return_date) " +
                "VALUES ( @employee_userID, @return_date)";

            using (SqlCommand insertCommand = new SqlCommand(insertStatement, connectionForReturn))
            {
                insertCommand.Transaction = returnTransaction;
                insertCommand.Parameters.Clear();
                //insertCommand.Parameters.AddWithValue("@returnID", returnTransaction.ReturnID);
                insertCommand.Parameters.AddWithValue("@employee_userID", aReturnTransactionToAdd.EmployeeID);
                insertCommand.Parameters.AddWithValue("@return_date", aReturnTransactionToAdd.ReturnDate);

                int count = insertCommand.ExecuteNonQuery();
                insertCommand.CommandText =
                "SELECT IDENT_CURRENT('return_transaction') FROM [cs6232-g4].[dbo].return_transaction";
                returnID = Convert.ToInt32(insertCommand.ExecuteScalar());
                if (count > 0)
                {
                    returnAdded = true;
                }
                else
                {
                    returnAdded = false;
                }
            }
            return returnAdded;
        }

        /// <summary>
        /// Adds a returnItem to the database and returns true if added or false otherwise
        /// </summary>
        /// <param name="returnItem">the returnItem to be added</param>
        public static bool AddReturnItem(ReturnItem returnItem)
        {
            string insertStatement =
                "INSERT [return_item] " +
                  "(itemID, returnID, quantity, amount_due, amount_paid, amount_refunded) " +
                "VALUES ( @itemID, @returnID, @quantity, @amount_due, @amount_paid, @amount_refunded)";

            using (SqlCommand insertCommand = new SqlCommand(insertStatement, connectionForReturn))
            {
                insertCommand.Transaction = returnTransaction;
                insertCommand.Parameters.Clear();
                insertCommand.Parameters.AddWithValue("@itemID", returnItem.ItemID);
                insertCommand.Parameters.AddWithValue("@returnID", returnID);
                insertCommand.Parameters.AddWithValue("@quantity", returnItem.Quantity);
                insertCommand.Parameters.AddWithValue("@amount_due", returnItem.Amount_due);
                insertCommand.Parameters.AddWithValue("@amount_paid", returnItem.Amount_paid);
                insertCommand.Parameters.AddWithValue("@amount_refunded", returnItem.Amount_refunded);

                int count = insertCommand.ExecuteNonQuery();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Updates the store quantity for the furniture
        /// </summary>
        /// <param name="returnItem">the returnItem to be added</param>
        /// <returns>true if update was applied, false otherwise</returns>
        public static bool UpdateFurnitureQuantity(ReturnItem returnItem)
        {
            int serial_number = ReturnItemDAL.GetSerialNumberByItemID(returnItem.ItemID);
            string updateStatement =
                "UPDATE furniture_item "
                + "SET quantity = quantity + @quantity "
                + "WHERE serial_number = @serial_number";

            using (SqlCommand updateCommand = new SqlCommand(updateStatement, connectionForReturn))
            {
                updateCommand.Transaction = returnTransaction;
                updateCommand.Parameters.Clear();
                updateCommand.Parameters.AddWithValue("@serial_number", serial_number);
                updateCommand.Parameters.AddWithValue("@quantity", returnItem.Quantity);

                int count = updateCommand.ExecuteNonQuery();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Returns daily rental rate for the itemID
        /// </summary>
        /// <param name="itemID">to get the daily rental rate</param>
        /// <returns>daily rental rate</returns>
        public static decimal GetDailyRentalRate(int itemID)
        {
            decimal daily_rental_rate = 0;
            string selectStatement =
                "SELECT fi.daily_rental_rate " +
                "FROM rental_item rei " +
                "JOIN furniture_item fi " +
                "ON rei.serial_number = fi.serial_number " +
                "WHERE rei.itemID = @itemID " +
                "ORDER BY rei.itemID ";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@itemID", itemID);
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            daily_rental_rate = (decimal)reader["daily_rental_rate"];
                        }
                    }
                }
            }
            return daily_rental_rate;
        }

        /// <summary>
        /// Returns daily fine rate for the itemID
        /// </summary>
        /// <param name="itemID">to get the daily fine rate</param>
        /// <returns>daily fine rate</returns>
        public static decimal GetDailyFineRate(int itemID)
        {
            decimal daily_fine_rate = 0;
            string selectStatement =
                "SELECT fi.daily_fine_rate " +
                "FROM  rental_item rei " +
                "JOIN furniture_item fi " +
                "ON rei.serial_number = fi.serial_number " +
                "WHERE rei.itemID = @itemID " +
                "ORDER BY rei.itemID ";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@itemID", itemID);
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            daily_fine_rate = (decimal)reader["daily_fine_rate"];
                        }
                    }
                }
            }
            return daily_fine_rate;
        }

        /// <summary>
        /// Returns rental period
        /// </summary>
        /// <param name="itemID">to get the rental period</param>
        /// <returns>rental period</returns>
        public static int GetRentalPeriod(int itemID)
        {
            int rental_period = 0;
            string selectStatement =
                "SELECT ri.itemID, DATEDIFF(day, rnt.rental_date, rnt.due_date) as rental_period " +
                "FROM rental_item ri " +
                "JOIN rental_transaction rnt " +
                "ON ri.rentalID = rnt.rentalID " +
                "WHERE ri.itemID = @itemID " +
                "ORDER BY ri.itemID ";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@itemID", itemID);
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            rental_period = (int)reader["rental_period"];
                        }
                    }
                }
            }
            return rental_period;
        }

        /// <summary>
        /// Returns rental date
        /// </summary>
        /// <param name="itemID">to get the rental period</param>
        /// <returns>rental date</returns>
        public static DateTime GetRentalDate(int itemID)
        {
            DateTime rental_date = DateTime.Now;
            string selectStatement =
                "SELECT TOP 1 rnt.rental_date " +
                "FROM rental_item ri " +
                "JOIN rental_transaction rnt " +
                "ON ri.rentalID = rnt.rentalID " +
                "WHERE ri.itemID = @itemID ";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@itemID", itemID);
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            rental_date = (DateTime)reader["rental_date"];
                        }
                    }
                }
            }
            return rental_date;
        }

    }


}
