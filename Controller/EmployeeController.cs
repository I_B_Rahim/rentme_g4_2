﻿using RentMe_G4.Model;
using RentMeG4.DAL;

namespace RentMe_G4.Controller
{
    class EmployeeController
    {
        private EmployeeDBDAL employeeDBSource;

        /// <summary>
        /// Constructor for the Employee Controller Class
        /// </summary>
        public EmployeeController()
        {
            employeeDBSource = new EmployeeDBDAL();
        }

        /// <summary>
        /// Returns the employee found by username and password
        /// </summary>
        /// <param name="userName">The username to match</param>
        /// <param name="password">The password to match</param>
        /// <returns>The employee found by username and password</returns>
        public Employee GetEmployeeByUserNameAndPassword(string userName, string password)
        {
            return employeeDBSource.GetEmployeeByUsernameAndPassword(userName, password);
        }
    }
}
