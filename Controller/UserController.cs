﻿using RentMe_G4.DAL;
using RentMe_G4.Model;
using System;
using System.Collections.Generic;

namespace RentMe_G4.Controller
{
    ///<summary>
    /// The controller class for accessing data From the cs6232-g4; it mediates b/w the cs6232-g4Data and the view
    /// </summary>
    public class UserController
    {
        private UserDAL UserDBSource;

        /// <summary>
        /// Constructor to setup the controller class
        /// </summary>
        public UserController()
        {
            this.UserDBSource = new UserDAL();
        }

        /// <summary>
        /// Returns a user 
        /// </summary>
        /// <param name="userID">ID of the user information to get</param>
        /// <returns>A user</returns>
        public User GetUserByUserID(int userID)
        {
            return UserDAL.GetUserByUserID(userID);
        }
        public User GetUserForCartByID(int userID)
        {
            return UserDAL.GetUserForCartByID(userID);
        }

        /// <summary>
        /// Returns userID by the phone number
        /// </summary>
        /// <param name="phone_number">the phone_number for the user</param>
        /// <returns>userID</returns>
        public int GetUserIDByPhone(string phone_number)
        {
            return UserDAL.GetUserIDByPhone(phone_number);
        }

        /// <summary>
        /// Returns the List of members 
        /// </summary>
        /// <returns>Members </returns>
        public List<User> GetListOfMembers()
        {
            return UserDAL.GetListOfMembers();
        }

        /// <summary>
        /// Gets the list of zip codes available to use
        /// </summary>
        /// <returns>List of zipcodes</returns>
        public List<string> GetListOfZip()
        {
            return UserDAL.GetListOfZip();
        }

        /// <summary>
        /// Returns if the credentials passed are valid and if the user is an administrator
        /// </summary>
        /// <param name="username">The username for authentication</param>
        /// <param name="password">The password for authentication</param>
        /// <returns>If the credentials passed are valid and if the user is an administrator</returns>
        public bool IsValidAdministrator(string username, string password)
        {
            return this.UserDBSource.IsValidAdministrator(username, password);
        }

        /// <summary>
        /// Returns a list of users who are employees
        /// </summary>
        /// <returns>A list of users who are employees</returns>
        public List<User> GetEmployees()
        {
            return this.UserDBSource.GetEmployees();
        }

        /// <summary>
        /// Adds an employee with credentials to the database
        /// </summary>
        /// <param name="employee">The employee to insert</param>
        /// <returns>If the insters were successfully executed</returns>
        public bool AddEmployee(User employee, out int userID)
        {
            return this.UserDBSource.AddEmployee(employee, out userID);
        }

        /// <summary>
        /// Returns if the userID exists and if it is an employee account
        /// </summary>
        /// <param name="userID">The userID to check</param>
        /// <returns>If the userID exists and if it is an employee account</returns>
        public bool GetEmployeeExists(int userID)
        {
            return this.UserDBSource.GetEmployeeExists(userID);
        }

        /// <summary>
        /// Updates an employee
        /// </summary>
        /// <param name="employee">The employee to update</param>
        /// <returns>If the update successfully executed</returns>
        public bool UpdateEmployee(User employee)
        {
            return this.UserDBSource.UpdateEmployee(employee);
        }

        /// <summary>
        /// Returns a list of states
        /// </summary>
        /// <returns>A list of states</returns>
        public List<string> GetListOfStates()
        {
            return this.UserDBSource.GetListOfStates();
        }

        /// <summary>
        /// Return true if zip code exists in location
        /// </summary>
        /// <param name="zip"></param>
        /// <returns>If zip code exists in location</returns>
        public bool GetZipExists(string zip)
        {
            return this.UserDBSource.GetZipExists(zip);
        }

        /// <summary>
        /// Adds a location to the database
        /// </summary>
        /// <param name="zip">The zip code to insert</param>
        /// <param name="city">The city to insert</param>
        /// <param name="state">The state to insert</param>
        /// <returns>If the query was executed successfully</returns>
        public bool AddLocation(string zip, string city, string state)
        {
            return this.UserDBSource.AddLocation(zip, city, state);
        }

        /// <summary>
        /// This method returns the last returnID in the return transaction table
        /// </summary>
        /// <returns>Returns the last returnID</returns>
        public static int GetLastReturnID()
        {
            return ReturnTransactionDAL.GetLastReturnID();
        }

        /// <summary>
        /// Adds a returnTransaction to the database and returns true if added or false otherwise
        /// </summary>
        /// <param name="returnTransaction">the returnTransaction to be added</param>
        /// <returns>true if returnTransaction was added, false otherwise</returns>
        public static bool AddReturnTransaction(ReturnTransaction returnTransaction)
        {
            return ReturnTransactionDAL.AddReturnTransaction(returnTransaction);
        }

        /// <summary>
        /// Adds a returnItem to the database and returns true if added or false otherwise
        /// </summary>
        /// <param name="returnItem">the returnItem to be added</param>
        /// <returns>true if returnItem was added, false otherwise</returns>
        public static void AddReturnItem(ReturnItem returnItem)
        {
            ReturnTransactionDAL.AddReturnItem(returnItem);
        }

        /// <summary>
        /// Executes a return transaction adding a data into the return transaction table and return items table
        /// </summary>
        /// <param name="aReturnTransactionToAdd">the return transaction to added to the return transaction table</param>
        /// <param name="listOfReturnItems">items to return</param>
        /// <returns>true if return transaction has been executed</returns>
        public static bool ExecuteAReturn(ReturnTransaction aReturnTransactionToAdd, List<ReturnItem> listOfReturnItems)
        {
            return ReturnTransactionDAL.ExecuteAReturn(aReturnTransactionToAdd, listOfReturnItems);
        }

        /// <summary>
        /// Gets the quantity of the item rented
        /// </summary>
        /// <param name="itemID">itemID to get quantity</param>
        /// <returns>quantity</returns>
        public static int GetQuantityFromRental(int itemID)
        {
            return ReturnTransactionDAL.GetQuantityFromRental(itemID);
        }

        /// <summary>
        /// Gets the return transaction history by member id 
        /// </summary>
        /// <param name="memberID">member ID to get return history</param>
        /// <returns>return transaction list</returns>
        public static List<ReturnTransaction> GetMemberReturnTransactionHistory(int memberID)
        {
            return ReturnTransactionDAL.GetMemberReturnTransactionHistory(memberID);
        }

        /// <summary>
        /// Gets the serial number of the item 
        /// </summary>
        /// <param name="itemID">itemID to get serial number</param>
        /// <returns>serial number</returns>
        public static int GetSerialNumberByItemID(int itemID)
        {
            return ReturnItemDAL.GetSerialNumberByItemID(itemID);
        }

        /// <summary>
        /// Returns daily rental rate for the itemID
        /// </summary>
        /// <param name="itemID">to get the daily rental rate</param>
        /// <returns>daily rental rate</returns>
        public static decimal GetDailyRentalRate(int itemID)
        {
            return ReturnTransactionDAL.GetDailyRentalRate(itemID);
        }

        /// <summary>
        /// Returns daily fine rate for the itemID
        /// </summary>
        /// <param name="itemID">to get the daily fine rate</param>
        /// <returns>daily fine rate</returns>
        public static decimal GetDailyFineRate(int itemID)
        {
            return ReturnTransactionDAL.GetDailyFineRate(itemID);
        }

        /// <summary>
        /// Returns rental period
        /// </summary>
        /// <param name="itemID">to get the rental period</param>
        /// <returns>rental period</returns>
        public static int GetRentalPeriod(int itemID)
        {
            return ReturnTransactionDAL.GetRentalPeriod(itemID);
        }

        /// <summary>
        /// Returns if a username already exists
        /// </summary>
        /// <param name="username">The username to check</param>
        /// <returns>If a username already exists</returns>
        public bool GetUsernameExists(string username)
        {
            return this.UserDBSource.GetUsernameExists(username);
        }

        /// <summary>
        /// Gets the returned quantity
        /// </summary>
        /// <param name="itemID">itemID to get quantity</param>
        /// <returns>returned quantity</returns>
        public static int GetReturnedQuantity(int itemID)
        {
            return ReturnTransactionDAL.GetReturnedQuantity(itemID);
        }

        /// <summary>
        /// Gets the quantities of the item returned
        /// </summary>
        /// <param name="itemID">itemID to get quantity</param>
        /// <returns>quantity</returns>
        public static int GetQuantityFromReturn(int itemID)
        {
            return ReturnTransactionDAL.GetQuantityFromReturn(itemID);
        }

        /// <summary>
        /// Returns rental date
        /// </summary>
        /// <param name="itemID">to get the rental period</param>
        /// <returns>rental date</returns>
        public static DateTime GetRentalDate(int itemID)
        {
            return ReturnTransactionDAL.GetRentalDate(itemID);
        }
    }
}
