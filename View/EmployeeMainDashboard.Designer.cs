﻿namespace RentMe_G4.View
{
    partial class EmployeeMainDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.employeeTabControl = new System.Windows.Forms.TabControl();
            this.searchMember = new System.Windows.Forms.TabPage();
            this.memberSearchUserControl3 = new RentMe_G4.UserControls.MemberSearchUserControl();
            this.searchFurniture = new System.Windows.Forms.TabPage();
            this.searchFurnitureUserControl1 = new RentMe_G4.UserControls.SearchFurnitureUserControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.employeeRentalsUserControl1 = new RentMe_G4.UserControls.EmployeeRentalsUserControl();
            this.rentals = new System.Windows.Forms.TabPage();
            this.memberRentalsUserControl1 = new RentMe_G4.UserControls.MemberRentalsUserControl();
            this.returnsTabPage = new System.Windows.Forms.TabPage();
            this.memberReturnsUserControl1 = new RentMe_G4.UserControls.MemberReturnsUserControl();
            this.employeeLogoutUserControl1 = new RentMe_G4.UserControls.EmployeeLogoutUserControl();
            this.memberSearchUserControl2 = new RentMe_G4.UserControls.MemberSearchUserControl();
            this.employeeTabControl.SuspendLayout();
            this.searchMember.SuspendLayout();
            this.searchFurniture.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.rentals.SuspendLayout();
            this.returnsTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // employeeTabControl
            // 
            this.employeeTabControl.Controls.Add(this.searchMember);
            this.employeeTabControl.Controls.Add(this.searchFurniture);
            this.employeeTabControl.Controls.Add(this.tabPage1);
            this.employeeTabControl.Controls.Add(this.rentals);
            this.employeeTabControl.Controls.Add(this.returnsTabPage);
            this.employeeTabControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.employeeTabControl.Location = new System.Drawing.Point(0, 97);
            this.employeeTabControl.Name = "employeeTabControl";
            this.employeeTabControl.SelectedIndex = 0;
            this.employeeTabControl.Size = new System.Drawing.Size(800, 353);
            this.employeeTabControl.TabIndex = 0;
            this.employeeTabControl.SelectedIndexChanged += new System.EventHandler(this.SetMemberIDForRentalsAndReturnsHistory);
            // 
            // searchMember
            // 
            this.searchMember.Controls.Add(this.memberSearchUserControl3);
            this.searchMember.Location = new System.Drawing.Point(4, 22);
            this.searchMember.Name = "searchMember";
            this.searchMember.Padding = new System.Windows.Forms.Padding(3);
            this.searchMember.Size = new System.Drawing.Size(792, 327);
            this.searchMember.TabIndex = 0;
            this.searchMember.Text = "Search Member";
            this.searchMember.UseVisualStyleBackColor = true;
            // 
            // memberSearchUserControl3
            // 
            this.memberSearchUserControl3.Location = new System.Drawing.Point(3, 3);
            this.memberSearchUserControl3.Name = "memberSearchUserControl3";
            this.memberSearchUserControl3.Size = new System.Drawing.Size(781, 333);
            this.memberSearchUserControl3.TabIndex = 0;
            this.memberSearchUserControl3.Load += new System.EventHandler(this.memberSearchUserControl3_Load);
            // 
            // searchFurniture
            // 
            this.searchFurniture.Controls.Add(this.searchFurnitureUserControl1);
            this.searchFurniture.Location = new System.Drawing.Point(4, 22);
            this.searchFurniture.Name = "searchFurniture";
            this.searchFurniture.Size = new System.Drawing.Size(792, 327);
            this.searchFurniture.TabIndex = 1;
            this.searchFurniture.Text = "Search Furniture";
            this.searchFurniture.UseVisualStyleBackColor = true;
            // 
            // searchFurnitureUserControl1
            // 
            this.searchFurnitureUserControl1.Location = new System.Drawing.Point(3, 3);
            this.searchFurnitureUserControl1.Name = "searchFurnitureUserControl1";
            this.searchFurnitureUserControl1.Size = new System.Drawing.Size(786, 321);
            this.searchFurnitureUserControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.employeeRentalsUserControl1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(792, 327);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Rent Furniture";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // employeeRentalsUserControl1
            // 
            this.employeeRentalsUserControl1.Location = new System.Drawing.Point(3, 3);
            this.employeeRentalsUserControl1.Name = "employeeRentalsUserControl1";
            this.employeeRentalsUserControl1.Size = new System.Drawing.Size(786, 321);
            this.employeeRentalsUserControl1.TabIndex = 0;
            // 
            // rentals
            // 
            this.rentals.Controls.Add(this.memberRentalsUserControl1);
            this.rentals.Location = new System.Drawing.Point(4, 22);
            this.rentals.Name = "rentals";
            this.rentals.Size = new System.Drawing.Size(792, 327);
            this.rentals.TabIndex = 2;
            this.rentals.Text = "Rentals";
            this.rentals.UseVisualStyleBackColor = true;
            // 
            // memberRentalsUserControl1
            // 
            this.memberRentalsUserControl1.Location = new System.Drawing.Point(8, 0);
            this.memberRentalsUserControl1.Name = "memberRentalsUserControl1";
            this.memberRentalsUserControl1.Size = new System.Drawing.Size(766, 359);
            this.memberRentalsUserControl1.TabIndex = 0;
            // 
            // returnsTabPage
            // 
            this.returnsTabPage.Controls.Add(this.memberReturnsUserControl1);
            this.returnsTabPage.Location = new System.Drawing.Point(4, 22);
            this.returnsTabPage.Name = "returnsTabPage";
            this.returnsTabPage.Size = new System.Drawing.Size(792, 327);
            this.returnsTabPage.TabIndex = 3;
            this.returnsTabPage.Text = "Returns";
            this.returnsTabPage.UseVisualStyleBackColor = true;
            // 
            // memberReturnsUserControl1
            // 
            this.memberReturnsUserControl1.Location = new System.Drawing.Point(6, 0);
            this.memberReturnsUserControl1.Name = "memberReturnsUserControl1";
            this.memberReturnsUserControl1.Size = new System.Drawing.Size(783, 320);
            this.memberReturnsUserControl1.TabIndex = 0;
            // 
            // employeeLogoutUserControl1
            // 
            this.employeeLogoutUserControl1.Location = new System.Drawing.Point(97, -2);
            this.employeeLogoutUserControl1.Name = "employeeLogoutUserControl1";
            this.employeeLogoutUserControl1.Size = new System.Drawing.Size(703, 93);
            this.employeeLogoutUserControl1.TabIndex = 1;
            this.employeeLogoutUserControl1.theMainDashboard = null;
            // 
            // memberSearchUserControl2
            // 
            this.memberSearchUserControl2.Location = new System.Drawing.Point(8, 6);
            this.memberSearchUserControl2.Name = "memberSearchUserControl2";
            this.memberSearchUserControl2.Size = new System.Drawing.Size(778, 333);
            this.memberSearchUserControl2.TabIndex = 0;
            // 
            // EmployeeMainDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.employeeLogoutUserControl1);
            this.Controls.Add(this.employeeTabControl);
            this.Name = "EmployeeMainDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Employee Page";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainDashboard_Closing);
            this.employeeTabControl.ResumeLayout(false);
            this.searchMember.ResumeLayout(false);
            this.searchFurniture.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.rentals.ResumeLayout(false);
            this.returnsTabPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl employeeTabControl;
        private System.Windows.Forms.TabPage searchMember;
        private System.Windows.Forms.TabPage searchFurniture;
        private UserControls.MemberSearchUserControl memberSearchUserControl2;
        private UserControls.MemberSearchUserControl memberSearchUserControl3;
        private UserControls.EmployeeLogoutUserControl employeeLogoutUserControl1;
        private UserControls.SearchFurnitureUserControl searchFurnitureUserControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private UserControls.EmployeeRentalsUserControl employeeRentalsUserControl1;
        private System.Windows.Forms.TabPage rentals;
        private UserControls.MemberRentalsUserControl memberRentalsUserControl1;
        private System.Windows.Forms.TabPage returnsTabPage;
        private UserControls.MemberReturnsUserControl memberReturnsUserControl1;
    }
}