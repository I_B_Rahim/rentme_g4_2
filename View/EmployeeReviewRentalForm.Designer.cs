﻿namespace RentMe_G4.View
{
    partial class EmployeeReviewRentalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ReviewOrderLabel = new System.Windows.Forms.Label();
            this.OrderForLabel = new System.Windows.Forms.Label();
            this.CartItemListView = new System.Windows.Forms.ListView();
            this.SubmitOrderButton = new System.Windows.Forms.Button();
            this.CancelReviewOrderButton = new System.Windows.Forms.Button();
            this.dueDateLabel = new System.Windows.Forms.Label();
            this.rentalTotalLabel = new System.Windows.Forms.Label();
            this.dueDateTextBox = new System.Windows.Forms.TextBox();
            this.totalDueTextBox = new System.Windows.Forms.TextBox();
            this.returnDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.chooseReturnDateLabel = new System.Windows.Forms.Label();
            this.changeQuantityLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ReviewOrderLabel
            // 
            this.ReviewOrderLabel.AutoSize = true;
            this.ReviewOrderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReviewOrderLabel.Location = new System.Drawing.Point(12, 9);
            this.ReviewOrderLabel.Name = "ReviewOrderLabel";
            this.ReviewOrderLabel.Size = new System.Drawing.Size(162, 29);
            this.ReviewOrderLabel.TabIndex = 0;
            this.ReviewOrderLabel.Text = "Review Order";
            // 
            // OrderForLabel
            // 
            this.OrderForLabel.AutoSize = true;
            this.OrderForLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OrderForLabel.Location = new System.Drawing.Point(17, 42);
            this.OrderForLabel.Name = "OrderForLabel";
            this.OrderForLabel.Size = new System.Drawing.Size(66, 17);
            this.OrderForLabel.TabIndex = 1;
            this.OrderForLabel.Text = "Order for";
            // 
            // CartItemListView
            // 
            this.CartItemListView.FullRowSelect = true;
            this.CartItemListView.HideSelection = false;
            this.CartItemListView.LabelEdit = true;
            this.CartItemListView.Location = new System.Drawing.Point(20, 125);
            this.CartItemListView.Name = "CartItemListView";
            this.CartItemListView.Size = new System.Drawing.Size(622, 381);
            this.CartItemListView.TabIndex = 2;
            this.CartItemListView.UseCompatibleStateImageBehavior = false;
            this.CartItemListView.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.UpdateQuantity);
            // 
            // SubmitOrderButton
            // 
            this.SubmitOrderButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubmitOrderButton.Location = new System.Drawing.Point(533, 512);
            this.SubmitOrderButton.Name = "SubmitOrderButton";
            this.SubmitOrderButton.Size = new System.Drawing.Size(109, 37);
            this.SubmitOrderButton.TabIndex = 3;
            this.SubmitOrderButton.Text = "Submit Order";
            this.SubmitOrderButton.UseVisualStyleBackColor = true;
            this.SubmitOrderButton.Click += new System.EventHandler(this.SubmitOrderButton_Click);
            // 
            // CancelReviewOrderButton
            // 
            this.CancelReviewOrderButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelReviewOrderButton.Location = new System.Drawing.Point(452, 512);
            this.CancelReviewOrderButton.Name = "CancelReviewOrderButton";
            this.CancelReviewOrderButton.Size = new System.Drawing.Size(75, 37);
            this.CancelReviewOrderButton.TabIndex = 4;
            this.CancelReviewOrderButton.Text = "Cancel";
            this.CancelReviewOrderButton.UseVisualStyleBackColor = true;
            this.CancelReviewOrderButton.Click += new System.EventHandler(this.CancelReviewOrderButton_Click);
            // 
            // dueDateLabel
            // 
            this.dueDateLabel.AutoSize = true;
            this.dueDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dueDateLabel.Location = new System.Drawing.Point(395, 18);
            this.dueDateLabel.Name = "dueDateLabel";
            this.dueDateLabel.Size = new System.Drawing.Size(114, 17);
            this.dueDateLabel.TabIndex = 5;
            this.dueDateLabel.Text = "All Items Due By:";
            // 
            // rentalTotalLabel
            // 
            this.rentalTotalLabel.AutoSize = true;
            this.rentalTotalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rentalTotalLabel.Location = new System.Drawing.Point(390, 47);
            this.rentalTotalLabel.Name = "rentalTotalLabel";
            this.rentalTotalLabel.Size = new System.Drawing.Size(119, 17);
            this.rentalTotalLabel.TabIndex = 6;
            this.rentalTotalLabel.Text = "Rental Total Due:";
            // 
            // dueDateTextBox
            // 
            this.dueDateTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dueDateTextBox.Location = new System.Drawing.Point(515, 15);
            this.dueDateTextBox.Name = "dueDateTextBox";
            this.dueDateTextBox.ReadOnly = true;
            this.dueDateTextBox.Size = new System.Drawing.Size(127, 23);
            this.dueDateTextBox.TabIndex = 7;
            // 
            // totalDueTextBox
            // 
            this.totalDueTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalDueTextBox.Location = new System.Drawing.Point(515, 44);
            this.totalDueTextBox.Name = "totalDueTextBox";
            this.totalDueTextBox.ReadOnly = true;
            this.totalDueTextBox.Size = new System.Drawing.Size(127, 23);
            this.totalDueTextBox.TabIndex = 8;
            // 
            // returnDateTimePicker
            // 
            this.returnDateTimePicker.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.returnDateTimePicker.Location = new System.Drawing.Point(157, 69);
            this.returnDateTimePicker.Name = "returnDateTimePicker";
            this.returnDateTimePicker.Size = new System.Drawing.Size(181, 20);
            this.returnDateTimePicker.TabIndex = 9;
            this.returnDateTimePicker.CloseUp += new System.EventHandler(this.CloseUpRentalDueDateAndTotal);
            this.returnDateTimePicker.DropDown += new System.EventHandler(this.DropDownRentalDueDateAndTotal);
            this.returnDateTimePicker.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DoNotManuallyChangeDate);
            // 
            // chooseReturnDateLabel
            // 
            this.chooseReturnDateLabel.AutoSize = true;
            this.chooseReturnDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chooseReturnDateLabel.Location = new System.Drawing.Point(17, 69);
            this.chooseReturnDateLabel.Name = "chooseReturnDateLabel";
            this.chooseReturnDateLabel.Size = new System.Drawing.Size(134, 17);
            this.chooseReturnDateLabel.TabIndex = 10;
            this.chooseReturnDateLabel.Text = "Choose return date:";
            // 
            // changeQuantityLabel
            // 
            this.changeQuantityLabel.AutoSize = true;
            this.changeQuantityLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeQuantityLabel.Location = new System.Drawing.Point(17, 96);
            this.changeQuantityLabel.Name = "changeQuantityLabel";
            this.changeQuantityLabel.Size = new System.Drawing.Size(583, 17);
            this.changeQuantityLabel.TabIndex = 11;
            this.changeQuantityLabel.Text = "Note: To change the quantity of an item select a row below, then click a second t" +
    "ime to edit:";
            // 
            // EmployeeReviewRentalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 561);
            this.ControlBox = false;
            this.Controls.Add(this.changeQuantityLabel);
            this.Controls.Add(this.chooseReturnDateLabel);
            this.Controls.Add(this.returnDateTimePicker);
            this.Controls.Add(this.totalDueTextBox);
            this.Controls.Add(this.dueDateTextBox);
            this.Controls.Add(this.rentalTotalLabel);
            this.Controls.Add(this.dueDateLabel);
            this.Controls.Add(this.CancelReviewOrderButton);
            this.Controls.Add(this.SubmitOrderButton);
            this.Controls.Add(this.CartItemListView);
            this.Controls.Add(this.OrderForLabel);
            this.Controls.Add(this.ReviewOrderLabel);
            this.Name = "EmployeeReviewRentalForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EmployeeReviewRentalForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ReviewOrderLabel;
        private System.Windows.Forms.Label OrderForLabel;
        private System.Windows.Forms.ListView CartItemListView;
        private System.Windows.Forms.Button SubmitOrderButton;
        private System.Windows.Forms.Button CancelReviewOrderButton;
        private System.Windows.Forms.Label dueDateLabel;
        private System.Windows.Forms.Label rentalTotalLabel;
        private System.Windows.Forms.TextBox dueDateTextBox;
        private System.Windows.Forms.TextBox totalDueTextBox;
        private System.Windows.Forms.DateTimePicker returnDateTimePicker;
        private System.Windows.Forms.Label chooseReturnDateLabel;
        private System.Windows.Forms.Label changeQuantityLabel;
    }
}