﻿using System;
using System.Windows.Forms;

namespace RentMe_G4.View
{
    /// <summary>
    /// This class handles the view for the administrator's main dashboard
    /// </summary>
    public partial class AdministratorMainDashboard : Form
    {
        public string username { get; set; }

        /// <summary>
        /// Constructor used to initialize the form
        /// </summary>
        public AdministratorMainDashboard()
        {
            InitializeComponent();
        }

        private void SetUsernameLabel(object sender, EventArgs e)
        {
            usernameLabel.Text = "Hi, " + this.username;
        }

        private void LogoutClicked(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
    }
}
