﻿namespace RentMe_G4.View
{
    partial class ReturnItemsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label returnIDLabel;
            System.Windows.Forms.Label employee_userIDLabel;
            System.Windows.Forms.Label return_dateLabel;
            this.returnIDTextBox = new System.Windows.Forms.TextBox();
            this.return_transactionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._cs6232_g4DataSet = new RentMe_G4._cs6232_g4DataSet();
            this.employeeIDTextBox = new System.Windows.Forms.TextBox();
            this.return_dateTextBox = new System.Windows.Forms.TextBox();
            this.returnBtn = new System.Windows.Forms.Button();
            this.cancelReturnBtn = new System.Windows.Forms.Button();
            this.return_itemDataGridView = new System.Windows.Forms.DataGridView();
            this.itemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.returnID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amount_due = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amount_paid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amountRefunded = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.viewItemDetails = new System.Windows.Forms.DataGridViewButtonColumn();
            this.detailReturnItemsLabel = new System.Windows.Forms.Label();
            this.totalCostTxtbox = new System.Windows.Forms.TextBox();
            this.totalCostLabel = new System.Windows.Forms.Label();
            this.balanceLabel = new System.Windows.Forms.Label();
            this.amountOwedTxtbox = new System.Windows.Forms.TextBox();
            this.calculateCostBalancebtn = new System.Windows.Forms.Button();
            this.TransactionInfo = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.refundTxtbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.return_transactionTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.return_transactionTableAdapter();
            this.tableAdapterManager = new RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager();
            this.return_itemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.return_itemTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.return_itemTableAdapter();
            returnIDLabel = new System.Windows.Forms.Label();
            employee_userIDLabel = new System.Windows.Forms.Label();
            return_dateLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.return_transactionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.return_itemDataGridView)).BeginInit();
            this.TransactionInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.return_itemBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // returnIDLabel
            // 
            returnIDLabel.AutoSize = true;
            returnIDLabel.Location = new System.Drawing.Point(21, 29);
            returnIDLabel.Name = "returnIDLabel";
            returnIDLabel.Size = new System.Drawing.Size(56, 13);
            returnIDLabel.TabIndex = 1;
            returnIDLabel.Text = "Return ID:";
            // 
            // employee_userIDLabel
            // 
            employee_userIDLabel.AutoSize = true;
            employee_userIDLabel.Location = new System.Drawing.Point(178, 29);
            employee_userIDLabel.Name = "employee_userIDLabel";
            employee_userIDLabel.Size = new System.Drawing.Size(70, 13);
            employee_userIDLabel.TabIndex = 3;
            employee_userIDLabel.Text = "Employee ID:";
            // 
            // return_dateLabel
            // 
            return_dateLabel.AutoSize = true;
            return_dateLabel.Location = new System.Drawing.Point(349, 29);
            return_dateLabel.Name = "return_dateLabel";
            return_dateLabel.Size = new System.Drawing.Size(68, 13);
            return_dateLabel.TabIndex = 5;
            return_dateLabel.Text = "Return Date:";
            // 
            // returnIDTextBox
            // 
            this.returnIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.return_transactionBindingSource, "returnID", true));
            this.returnIDTextBox.Location = new System.Drawing.Point(83, 26);
            this.returnIDTextBox.Name = "returnIDTextBox";
            this.returnIDTextBox.ReadOnly = true;
            this.returnIDTextBox.Size = new System.Drawing.Size(56, 20);
            this.returnIDTextBox.TabIndex = 2;
            // 
            // return_transactionBindingSource
            // 
            this.return_transactionBindingSource.DataMember = "return_transaction";
            this.return_transactionBindingSource.DataSource = this._cs6232_g4DataSet;
            // 
            // _cs6232_g4DataSet
            // 
            this._cs6232_g4DataSet.DataSetName = "_cs6232_g4DataSet";
            this._cs6232_g4DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // employeeIDTextBox
            // 
            this.employeeIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.return_transactionBindingSource, "employee_userID", true));
            this.employeeIDTextBox.Location = new System.Drawing.Point(254, 26);
            this.employeeIDTextBox.Name = "employeeIDTextBox";
            this.employeeIDTextBox.ReadOnly = true;
            this.employeeIDTextBox.Size = new System.Drawing.Size(56, 20);
            this.employeeIDTextBox.TabIndex = 4;
            // 
            // return_dateTextBox
            // 
            this.return_dateTextBox.Location = new System.Drawing.Point(429, 26);
            this.return_dateTextBox.Name = "return_dateTextBox";
            this.return_dateTextBox.Size = new System.Drawing.Size(132, 20);
            this.return_dateTextBox.TabIndex = 6;
            this.return_dateTextBox.TextChanged += new System.EventHandler(this.Return_dateTextBox_TextChanged);
            // 
            // returnBtn
            // 
            this.returnBtn.Enabled = false;
            this.returnBtn.Location = new System.Drawing.Point(678, 236);
            this.returnBtn.Name = "returnBtn";
            this.returnBtn.Size = new System.Drawing.Size(61, 23);
            this.returnBtn.TabIndex = 7;
            this.returnBtn.Text = "Return";
            this.returnBtn.UseVisualStyleBackColor = true;
            this.returnBtn.Click += new System.EventHandler(this.ReturnBtn_Click);
            // 
            // cancelReturnBtn
            // 
            this.cancelReturnBtn.Location = new System.Drawing.Point(26, 246);
            this.cancelReturnBtn.Name = "cancelReturnBtn";
            this.cancelReturnBtn.Size = new System.Drawing.Size(72, 23);
            this.cancelReturnBtn.TabIndex = 8;
            this.cancelReturnBtn.Text = "Cancel";
            this.cancelReturnBtn.UseVisualStyleBackColor = true;
            this.cancelReturnBtn.Click += new System.EventHandler(this.CancelReturnBtn_Click);
            // 
            // return_itemDataGridView
            // 
            this.return_itemDataGridView.AllowUserToAddRows = false;
            this.return_itemDataGridView.AllowUserToDeleteRows = false;
            this.return_itemDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.return_itemDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.itemID,
            this.returnID,
            this.quantity,
            this.amount_due,
            this.amount_paid,
            this.amountRefunded,
            this.viewItemDetails});
            this.return_itemDataGridView.Location = new System.Drawing.Point(26, 113);
            this.return_itemDataGridView.Name = "return_itemDataGridView";
            this.return_itemDataGridView.RowHeadersVisible = false;
            this.return_itemDataGridView.Size = new System.Drawing.Size(568, 127);
            this.return_itemDataGridView.TabIndex = 9;
            this.return_itemDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Return_itemDataGridView_CellClick);
            this.return_itemDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Return_itemDataGridView_CellContentClick);
            this.return_itemDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.Return_itemDataGridView_DataError);
            // 
            // itemID
            // 
            this.itemID.HeaderText = "Item ID";
            this.itemID.Name = "itemID";
            this.itemID.Width = 50;
            // 
            // returnID
            // 
            this.returnID.HeaderText = "Return ID";
            this.returnID.Name = "returnID";
            this.returnID.Width = 60;
            // 
            // quantity
            // 
            this.quantity.HeaderText = "Quantity";
            this.quantity.Name = "quantity";
            this.quantity.Width = 50;
            // 
            // amount_due
            // 
            this.amount_due.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.amount_due.HeaderText = "Amount Due";
            this.amount_due.Name = "amount_due";
            // 
            // amount_paid
            // 
            this.amount_paid.HeaderText = "Amount Paid";
            this.amount_paid.Name = "amount_paid";
            // 
            // amountRefunded
            // 
            this.amountRefunded.HeaderText = "Refund";
            this.amountRefunded.Name = "amountRefunded";
            // 
            // viewItemDetails
            // 
            this.viewItemDetails.HeaderText = "";
            this.viewItemDetails.Name = "viewItemDetails";
            this.viewItemDetails.Text = "View Item Details";
            this.viewItemDetails.UseColumnTextForButtonValue = true;
            // 
            // detailReturnItemsLabel
            // 
            this.detailReturnItemsLabel.AutoSize = true;
            this.detailReturnItemsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.detailReturnItemsLabel.Location = new System.Drawing.Point(23, 86);
            this.detailReturnItemsLabel.Name = "detailReturnItemsLabel";
            this.detailReturnItemsLabel.Size = new System.Drawing.Size(313, 13);
            this.detailReturnItemsLabel.TabIndex = 10;
            this.detailReturnItemsLabel.Text = "Please specify the quantity and the amount to be paid";
            // 
            // totalCostTxtbox
            // 
            this.totalCostTxtbox.Location = new System.Drawing.Point(678, 152);
            this.totalCostTxtbox.Name = "totalCostTxtbox";
            this.totalCostTxtbox.ReadOnly = true;
            this.totalCostTxtbox.Size = new System.Drawing.Size(60, 20);
            this.totalCostTxtbox.TabIndex = 11;
            // 
            // totalCostLabel
            // 
            this.totalCostLabel.AutoSize = true;
            this.totalCostLabel.Location = new System.Drawing.Point(600, 155);
            this.totalCostLabel.Name = "totalCostLabel";
            this.totalCostLabel.Size = new System.Drawing.Size(58, 13);
            this.totalCostLabel.TabIndex = 12;
            this.totalCostLabel.Text = "Total Cost:";
            // 
            // balanceLabel
            // 
            this.balanceLabel.AutoSize = true;
            this.balanceLabel.Location = new System.Drawing.Point(600, 181);
            this.balanceLabel.Name = "balanceLabel";
            this.balanceLabel.Size = new System.Drawing.Size(77, 13);
            this.balanceLabel.TabIndex = 14;
            this.balanceLabel.Text = "Amount Owed:";
            // 
            // amountOwedTxtbox
            // 
            this.amountOwedTxtbox.Location = new System.Drawing.Point(678, 178);
            this.amountOwedTxtbox.Name = "amountOwedTxtbox";
            this.amountOwedTxtbox.ReadOnly = true;
            this.amountOwedTxtbox.Size = new System.Drawing.Size(61, 20);
            this.amountOwedTxtbox.TabIndex = 13;
            // 
            // calculateCostBalancebtn
            // 
            this.calculateCostBalancebtn.Location = new System.Drawing.Point(660, 113);
            this.calculateCostBalancebtn.Name = "calculateCostBalancebtn";
            this.calculateCostBalancebtn.Size = new System.Drawing.Size(79, 23);
            this.calculateCostBalancebtn.TabIndex = 15;
            this.calculateCostBalancebtn.Text = "Re-Calculate";
            this.calculateCostBalancebtn.UseVisualStyleBackColor = true;
            this.calculateCostBalancebtn.Click += new System.EventHandler(this.CalculateBtn_Click);
            // 
            // TransactionInfo
            // 
            this.TransactionInfo.Controls.Add(this.label3);
            this.TransactionInfo.Controls.Add(this.returnIDTextBox);
            this.TransactionInfo.Controls.Add(returnIDLabel);
            this.TransactionInfo.Controls.Add(this.employeeIDTextBox);
            this.TransactionInfo.Controls.Add(employee_userIDLabel);
            this.TransactionInfo.Controls.Add(this.return_dateTextBox);
            this.TransactionInfo.Controls.Add(return_dateLabel);
            this.TransactionInfo.Location = new System.Drawing.Point(26, 12);
            this.TransactionInfo.Name = "TransactionInfo";
            this.TransactionInfo.Size = new System.Drawing.Size(712, 54);
            this.TransactionInfo.TabIndex = 16;
            this.TransactionInfo.TabStop = false;
            this.TransactionInfo.Text = "Transaction Info.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(441, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Format: mm/dd/yyyy";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(600, 207);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Refund:";
            // 
            // refundTxtbox
            // 
            this.refundTxtbox.Location = new System.Drawing.Point(678, 204);
            this.refundTxtbox.Name = "refundTxtbox";
            this.refundTxtbox.ReadOnly = true;
            this.refundTxtbox.Size = new System.Drawing.Size(61, 20);
            this.refundTxtbox.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(387, 246);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(207, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Must re-calculate to activate return";
            // 
            // return_transactionTableAdapter
            // 
            this.return_transactionTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.categoryTableAdapter = null;
            this.tableAdapterManager.furniture_itemTableAdapter = null;
            this.tableAdapterManager.locationTableAdapter = null;
            this.tableAdapterManager.rental_itemTableAdapter = null;
            this.tableAdapterManager.rental_transactionTableAdapter = null;
            this.tableAdapterManager.return_itemTableAdapter = null;
            this.tableAdapterManager.return_transactionTableAdapter = this.return_transactionTableAdapter;
            this.tableAdapterManager.roleTableAdapter = null;
            this.tableAdapterManager.stateTableAdapter = null;
            this.tableAdapterManager.styleTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.userTableAdapter = null;
            // 
            // return_itemBindingSource
            // 
            this.return_itemBindingSource.DataMember = "return_item";
            this.return_itemBindingSource.DataSource = this._cs6232_g4DataSet;
            // 
            // return_itemTableAdapter
            // 
            this.return_itemTableAdapter.ClearBeforeFill = true;
            // 
            // ReturnItemsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 281);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.refundTxtbox);
            this.Controls.Add(this.TransactionInfo);
            this.Controls.Add(this.calculateCostBalancebtn);
            this.Controls.Add(this.balanceLabel);
            this.Controls.Add(this.amountOwedTxtbox);
            this.Controls.Add(this.totalCostLabel);
            this.Controls.Add(this.totalCostTxtbox);
            this.Controls.Add(this.detailReturnItemsLabel);
            this.Controls.Add(this.return_itemDataGridView);
            this.Controls.Add(this.cancelReturnBtn);
            this.Controls.Add(this.returnBtn);
            this.Name = "ReturnItemsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Return Items Page";
            this.Load += new System.EventHandler(this.ReturnItemsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.return_transactionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.return_itemDataGridView)).EndInit();
            this.TransactionInfo.ResumeLayout(false);
            this.TransactionInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.return_itemBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private _cs6232_g4DataSet _cs6232_g4DataSet;
        private System.Windows.Forms.BindingSource return_transactionBindingSource;
        private _cs6232_g4DataSetTableAdapters.return_transactionTableAdapter return_transactionTableAdapter;
        private _cs6232_g4DataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox returnIDTextBox;
        private System.Windows.Forms.TextBox employeeIDTextBox;
        private System.Windows.Forms.TextBox return_dateTextBox;
        private System.Windows.Forms.Button returnBtn;
        private System.Windows.Forms.Button cancelReturnBtn;
        private System.Windows.Forms.BindingSource return_itemBindingSource;
        private _cs6232_g4DataSetTableAdapters.return_itemTableAdapter return_itemTableAdapter;
        private System.Windows.Forms.DataGridView return_itemDataGridView;
        private System.Windows.Forms.Label detailReturnItemsLabel;
        private System.Windows.Forms.TextBox totalCostTxtbox;
        private System.Windows.Forms.Label totalCostLabel;
        private System.Windows.Forms.Label balanceLabel;
        private System.Windows.Forms.TextBox amountOwedTxtbox;
        private System.Windows.Forms.Button calculateCostBalancebtn;
        private System.Windows.Forms.GroupBox TransactionInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox refundTxtbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemID;
        private System.Windows.Forms.DataGridViewTextBoxColumn returnID;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn amount_due;
        private System.Windows.Forms.DataGridViewTextBoxColumn amount_paid;
        private System.Windows.Forms.DataGridViewTextBoxColumn amountRefunded;
        private System.Windows.Forms.DataGridViewButtonColumn viewItemDetails;
    }
}