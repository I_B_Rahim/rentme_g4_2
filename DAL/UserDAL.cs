﻿using RentMe_G4.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace RentMe_G4.DAL
{
    ///<summary>
    /// The DAL that interacts with User object
    /// this gets out list of user 
    /// </summary>
    public class UserDAL
    {
        /// <summary>
        /// This returns the list of members 
        /// </summary>
        /// <returns>Returns all current members</returns>
        public static List<User> GetListOfMembers()
        {
            List<User> userList = new List<User>();
            string selectStatement =
                "SELECT * " +
                "FROM [user] " +
                "WHERE roleID = 3 " +
                "ORDER BY userID "
                ;

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            User user = new User
                            {
                                UserID = (int)reader["userID"],
                                RoleID = (int)reader["roleID"],
                                DateOfBirth = Convert.ToDateTime(reader["date_of_birth"])
                            };
                            ;
                            user.FirstName = reader["first_name"].ToString();
                            user.LastName = reader["last_name"].ToString();
                            user.PhoneNumber = reader["phone_number"].ToString();
                            user.Address1 = reader["address1"].ToString();
                            user.Address2 = reader["address1"].ToString();
                            user.Zip = reader["zip"].ToString();
                            user.Sex = char.Parse(reader["sex"].ToString());
                            userList.Add(user);
                        }
                    }
                }
            }

            return userList;
        }

        /// <summary>
        /// This method returns the list of acceptable zipcodes
        /// </summary>
        /// <returns>Returns the list of zipcodes</returns>
        public static List<string> GetListOfZip()
        {
            List<string> listOfZip = new List<string>();
            string selectStatement =
                "SELECT DISTINCT [zip] " +
                "FROM [cs6232-g4].[dbo].[user] ";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string zip;
                            zip = reader["zip"].ToString();
                            listOfZip.Add(zip);
                        }
                    }
                }
            }
            return listOfZip;
        }

        public static User GetUserForCartByID(int userID)
        {
            User user = new User();
            string selectStatement =
                "SELECT * " +
                "FROM [user] u " +
                "WHERE u.userID = @userID";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.Add("@userID", SqlDbType.Int).Value = userID;
                    using (SqlDataReader reader = selectCommand.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        while (reader.Read())
                        {
                            user.UserID = (int)reader["userID"];
                            user.RoleID = (int)reader["roleID"];
                            user.DateOfBirth = Convert.ToDateTime(reader["date_of_birth"]); ;
                            user.FirstName = reader["first_name"].ToString();
                            user.LastName = reader["last_name"].ToString();
                            user.PhoneNumber = reader["phone_number"].ToString();
                            user.Address1 = reader["address1"].ToString();
                            user.Address2 = reader["address2"].ToString();
                            user.Zip = reader["zip"].ToString();
                            user.Sex = char.Parse(reader["sex"].ToString());
                        }
                    }
                }
            }
            return user;
        }

        /// <summary>
        /// Returns user by the userID
        /// </summary>
        /// <param name="userID">the userID for the user</param>
        /// <returns>User</returns>
        public static User GetUserByUserID(int userID)
        {
            User user = new User();
            string selectStatement =
                "SELECT * " +
                "FROM [user] u " +
                "JOIN [credential] c " +
                "ON u.userID = c.userID " +
                "WHERE u.userID = @userID";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.Add("@userID", SqlDbType.Int).Value = userID;
                    using (SqlDataReader reader = selectCommand.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        while (reader.Read())
                        {
                            user.UserID = (int)reader["userID"];
                            user.RoleID = (int)reader["roleID"];
                            user.DateOfBirth = Convert.ToDateTime(reader["date_of_birth"]); ;
                            user.FirstName = reader["first_name"].ToString();
                            user.LastName = reader["last_name"].ToString();
                            user.PhoneNumber = reader["phone_number"].ToString();
                            user.Address1 = reader["address1"].ToString();
                            user.Address2 = reader["address2"].ToString();
                            user.Zip = reader["zip"].ToString();
                            user.Sex = char.Parse(reader["sex"].ToString());
                            user.Status = bool.Parse(reader["is_active"].ToString());
                        }
                    }
                }

                string selectLocationStatement =
                    "SELECT * " +
                    "FROM [location] " +
                    "WHERE zip = @zip";

                using (SqlCommand selectLocationCommand = new SqlCommand(selectLocationStatement, connection))
                {
                    selectLocationCommand.Parameters.AddWithValue("@zip", user.Zip);
                    using (SqlDataReader reader = selectLocationCommand.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        while (reader.Read())
                        {
                            user.City = reader["city"].ToString();
                            user.State = reader["state"].ToString();
                        }
                    }
                }
            }
            return user;
        }

        /// <summary>
        /// Returns userID by the phone number
        /// </summary>
        /// <param name="phone_number">the phone_number for the user</param>
        /// <returns>userID</returns>
        public static int GetUserIDByPhone(string phone_number)
        {
            User user = new User();
            string selectStatement =
                "SELECT * " +
                "FROM [user]  " +
                "WHERE phone_number = @phone_number";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@phone_number", phone_number);
                    using (SqlDataReader reader = selectCommand.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        while (reader.Read())
                        {
                            user.UserID = (int)reader["userID"];
                        }
                    }
                }
            }
            return user.UserID;
        }

        /// <summary>
        /// Returns if the credentials passed are valid and if the user is an administrator
        /// </summary>
        /// <param name="username">The username for authentication</param>
        /// <param name="password">The password for authentication</param>
        /// <returns>If the credentials passed are valid and if the user is an administrator</returns>
        public bool IsValidAdministrator(string username, string password)
        {
            string selectStatement =
                "SELECT c.username " +
                "FROM [credential] c " +
                "JOIN[user] u " +
                "ON c.userID = u.userID " +
                "JOIN[role] r " +
                "ON u.roleID = r.roleID " +
                "WHERE username = @username AND password = @password " +
                "AND r.type = 'administrator'";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();
                SqlCommand selectCommand;
                using (selectCommand = new SqlCommand(selectStatement, connection))
                    selectCommand.Parameters.AddWithValue("@username", username);
                selectCommand.Parameters.AddWithValue("@password", password);
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }

        }

        /// <summary>
        /// Returns a list of users who are employees
        /// </summary>
        /// <returns>A list of users who are employees</returns>
        public List<User> GetEmployees()
        {
            List<User> employeesList = new List<User>();

            string selectStatement =
                "SELECT u.userID AS userID, first_name, last_name, username, phone_number, date_of_birth, " +
                "address1, address2, zip, sex, is_active " +
                "FROM[user] u " +
                "JOIN[role] r " +
                "ON u.roleID = r.roleID " +
                "JOIN[credential] c " +
                "ON u.userID = c.userID " +
                "WHERE r.type = 'Employee'";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            User user = new User
                            {
                                UserID = Int32.Parse(reader["userID"].ToString()),
                                FirstName = reader["first_name"].ToString(),
                                LastName = reader["last_name"].ToString(),
                                Username = reader["username"].ToString(),
                                PhoneNumber = reader["phone_number"].ToString(),
                                DateOfBirth = (DateTime)reader["date_of_birth"],
                                Address1 = reader["address1"].ToString(),
                                Address2 = reader["address2"].ToString(),
                                Zip = reader["zip"].ToString(),
                                Sex = char.Parse(reader["sex"].ToString()),
                                Status = Convert.ToBoolean(reader["is_active"])
                            };

                            employeesList.Add(user);
                        }
                    }
                }
            }

            return employeesList;
        }

        /// <summary>
        /// Adds an employee with credentials to the database
        /// </summary>
        /// <param name="employee">The employee to insert</param>
        /// <returns>If the insters were successfully executed</returns>
        public bool AddEmployee(User employee, out int outID)
        {
            int insert = 0;
            int userID = 0;
            outID = 0;

            string insertUserStatement =
                    "INSERT INTO [user] (roleID, date_of_birth, first_name, last_name, phone_number, address1, address2, zip, sex) " +
                    "VALUES (@roleID, @date_of_birth, @first_name, @last_name, @phone_number, @address1, @address2, @zip, @sex)";

            string selectUserIDStatement = "SELECT @@IDENTITY AS UserID FROM [user]";

            string insertCredentialStatement =
                "INSERT INTO [credential] (userID, username, password) " +
                "VALUES (@userID, @username, @password)";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                SqlCommand insertUserCommand;
                using (insertUserCommand = new SqlCommand(insertUserStatement, connection))
                    insertUserCommand.Parameters.AddWithValue("@roleID", employee.RoleID);
                insertUserCommand.Parameters.AddWithValue("@date_of_birth", employee.DateOfBirth);
                insertUserCommand.Parameters.AddWithValue("@first_name", employee.FirstName);
                insertUserCommand.Parameters.AddWithValue("@last_name", employee.LastName);
                insertUserCommand.Parameters.AddWithValue("@phone_number", employee.PhoneNumber);
                insertUserCommand.Parameters.AddWithValue("@address1", employee.Address1);
                insertUserCommand.Parameters.AddWithValue("@address2", employee.Address2);
                insertUserCommand.Parameters.AddWithValue("@zip", employee.Zip);
                insertUserCommand.Parameters.AddWithValue("@sex", employee.Sex);
                insertUserCommand.ExecuteNonQuery();

                SqlCommand selectUserIDCommand;
                using (selectUserIDCommand = new SqlCommand(selectUserIDStatement, connection))
                {
                    using (SqlDataReader reader = selectUserIDCommand.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            userID = Int32.Parse(reader["UserID"].ToString());
                            outID = userID;
                        }
                    }
                }

                SqlCommand insertCredentialCommand;
                using (insertCredentialCommand = new SqlCommand(insertCredentialStatement, connection))
                    insertCredentialCommand.Parameters.AddWithValue("@userID", userID);
                insertCredentialCommand.Parameters.AddWithValue("@username", employee.Username);
                insertCredentialCommand.Parameters.AddWithValue("@password", employee.Password);
                insertCredentialCommand.ExecuteNonQuery();
            }

            if (insert > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns if the userID exists and if it is an employee account
        /// </summary>
        /// <param name="userID">The userID to check</param>
        /// <returns>If the userID exists and if it is an employee account</returns>
        public bool GetEmployeeExists(int userID)
        {
            string selectStatement =
                "SELECT userID " +
                "FROM [user] u " +
                "JOIN[role] r " +
                "ON u.roleID = r.roleID " +
                "WHERE userID = @userID " +
                "AND r.type = 'employee'";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();
                SqlCommand selectCommand;
                using (selectCommand = new SqlCommand(selectStatement, connection))
                    selectCommand.Parameters.AddWithValue("@userID", userID);
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Updates an employee
        /// </summary>
        /// <param name="employee">The employee to update</param>
        /// <returns>If the update successfully executed</returns>
        public bool UpdateEmployee(User employee)
        {
            int insert = 0;

            string updateUserStatement =
                    "UPDATE [user] SET " +
                    "date_of_birth = @date_of_birth, " +
                    "first_name = @first_name, " +
                    "last_name = @last_name, " +
                    "phone_number = @phone_number, " +
                    "address1 = @address1, " +
                    "address2 = @address2, " +
                    "zip = @zip, " +
                    "sex = @sex " +
                    "WHERE userID = @userID";

            string updateStatusStatement =
                "UPDATE [credential] SET is_active = @status " +
                "WHERE userID = @userID";

            string updateCredentialStatement =
                "UPDATE [credential] SET password = @password " +
                "WHERE userID = @userID";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                SqlCommand updateUserCommand;
                using (updateUserCommand = new SqlCommand(updateUserStatement, connection))
                    updateUserCommand.Parameters.AddWithValue("@date_of_birth", employee.DateOfBirth);
                updateUserCommand.Parameters.AddWithValue("@userID", employee.UserID);
                updateUserCommand.Parameters.AddWithValue("@first_name", employee.FirstName);
                updateUserCommand.Parameters.AddWithValue("@last_name", employee.LastName);
                updateUserCommand.Parameters.AddWithValue("@phone_number", employee.PhoneNumber);
                updateUserCommand.Parameters.AddWithValue("@address1", employee.Address1);
                updateUserCommand.Parameters.AddWithValue("@address2", employee.Address2);
                updateUserCommand.Parameters.AddWithValue("@zip", employee.Zip);
                updateUserCommand.Parameters.AddWithValue("@sex", employee.Sex);
                insert = updateUserCommand.ExecuteNonQuery();

                SqlCommand updateStatusCommand;
                using (updateStatusCommand = new SqlCommand(updateStatusStatement, connection))
                    updateStatusCommand.Parameters.AddWithValue("@userID", employee.UserID);
                updateStatusCommand.Parameters.AddWithValue("@status", employee.Status);
                updateStatusCommand.ExecuteNonQuery();

                Console.WriteLine(employee.Password);

                if (employee.Password != null)
                {
                    SqlCommand updateCredentialCommand;
                    using (updateCredentialCommand = new SqlCommand(updateCredentialStatement, connection))
                        updateCredentialCommand.Parameters.AddWithValue("@userID", employee.UserID);
                    updateCredentialCommand.Parameters.AddWithValue("@password", employee.Password);
                    updateCredentialCommand.ExecuteNonQuery();
                }
            }


            if (insert > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns a list of states
        /// </summary>
        /// <returns>A list of states</returns>
        public List<string> GetListOfStates()
        {
            List<string> listOfStates = new List<string>();
            string selectStatement =
                "SELECT code " +
                "FROM [state]";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string state;
                            state = reader["code"].ToString();
                            listOfStates.Add(state);
                        }
                    }
                }
            }
            return listOfStates;
        }

        /// <summary>
        /// Return true if zip code exists in location
        /// </summary>
        /// <param name="zip"></param>
        /// <returns>If zip code exists in location</returns>
        public bool GetZipExists(string zip)
        {
            string selectStatement =
                "SELECT zip " +
                "FROM [location] " +
                "WHERE zip = @zip";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();
                SqlCommand selectCommand;
                using (selectCommand = new SqlCommand(selectStatement, connection))
                    selectCommand.Parameters.AddWithValue("@zip", zip);
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Adds a location to the database
        /// </summary>
        /// <param name="zip">The zip code to insert</param>
        /// <param name="city">The city to insert</param>
        /// <param name="state">The state to insert</param>
        /// <returns>If the query was executed successfully</returns>
        public bool AddLocation(string zip, string city, string state)
        {
            int insert = 0;

            string insertLocationStatement =
                    "INSERT INTO [location] (zip, city, state) " +
                    "VALUES (@zip, @city, @state)";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                SqlCommand insertLocationCommand;
                using (insertLocationCommand = new SqlCommand(insertLocationStatement, connection))
                    insertLocationCommand.Parameters.AddWithValue("@zip", zip);
                insertLocationCommand.Parameters.AddWithValue("@city", city);
                insertLocationCommand.Parameters.AddWithValue("@state", state);
                insertLocationCommand.ExecuteNonQuery();
            }

            if (insert > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns if a username already exists
        /// </summary>
        /// <param name="username">The username to check</param>
        /// <returns>If a username already exists</returns>
        public bool GetUsernameExists(string username)
        {
            string selectStatement =
                "SELECT username " +
                "FROM [credential] " +
                "WHERE username = @username";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();
                SqlCommand selectCommand;
                using (selectCommand = new SqlCommand(selectStatement, connection))
                    selectCommand.Parameters.AddWithValue("@username", username);
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
        }
    }
}
