﻿namespace RentMe_G4.UserControls
{
    partial class UpdateEmployeeUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label userIDLabel;
            System.Windows.Forms.Label date_of_birthLabel;
            System.Windows.Forms.Label first_nameLabel;
            System.Windows.Forms.Label last_nameLabel;
            System.Windows.Forms.Label phone_numberLabel;
            System.Windows.Forms.Label address1Label;
            System.Windows.Forms.Label address2Label;
            System.Windows.Forms.Label zipLabel;
            System.Windows.Forms.Label sexLabel;
            System.Windows.Forms.Label codeLabel;
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.codeComboBox = new System.Windows.Forms.ComboBox();
            this.stateBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._cs6232_g4DataSet = new RentMe_G4._cs6232_g4DataSet();
            this.userIDTextBox = new System.Windows.Forms.TextBox();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.first_nameTextBox = new System.Windows.Forms.TextBox();
            this.last_nameTextBox = new System.Windows.Forms.TextBox();
            this.date_of_birthTextBox = new System.Windows.Forms.TextBox();
            this.phone_numberTextBox = new System.Windows.Forms.TextBox();
            this.address1TextBox = new System.Windows.Forms.TextBox();
            this.getUserButton = new System.Windows.Forms.Button();
            this.sexComboBox = new System.Windows.Forms.ComboBox();
            this.address2TextBox = new System.Windows.Forms.TextBox();
            this.resetPasswordLabel = new System.Windows.Forms.Label();
            this.resetPasswrodTextBox = new System.Windows.Forms.TextBox();
            this.statusLabel = new System.Windows.Forms.Label();
            this.statusCheckBox = new System.Windows.Forms.CheckBox();
            this.clearButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.cityLabel = new System.Windows.Forms.Label();
            this.cityTextBox = new System.Windows.Forms.TextBox();
            this.zipTextBox = new System.Windows.Forms.TextBox();
            this.userTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.userTableAdapter();
            this.tableAdapterManager = new RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager();
            this.stateTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.stateTableAdapter();
            userIDLabel = new System.Windows.Forms.Label();
            date_of_birthLabel = new System.Windows.Forms.Label();
            first_nameLabel = new System.Windows.Forms.Label();
            last_nameLabel = new System.Windows.Forms.Label();
            phone_numberLabel = new System.Windows.Forms.Label();
            address1Label = new System.Windows.Forms.Label();
            address2Label = new System.Windows.Forms.Label();
            zipLabel = new System.Windows.Forms.Label();
            sexLabel = new System.Windows.Forms.Label();
            codeLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // userIDLabel
            // 
            userIDLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            userIDLabel.AutoSize = true;
            userIDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            userIDLabel.Location = new System.Drawing.Point(58, 15);
            userIDLabel.Name = "userIDLabel";
            userIDLabel.Size = new System.Drawing.Size(59, 17);
            userIDLabel.TabIndex = 0;
            userIDLabel.Text = "User ID:";
            // 
            // date_of_birthLabel
            // 
            date_of_birthLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            date_of_birthLabel.AutoSize = true;
            date_of_birthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            date_of_birthLabel.Location = new System.Drawing.Point(26, 103);
            date_of_birthLabel.Name = "date_of_birthLabel";
            date_of_birthLabel.Size = new System.Drawing.Size(91, 17);
            date_of_birthLabel.TabIndex = 4;
            date_of_birthLabel.Text = "Date of Birth:";
            // 
            // first_nameLabel
            // 
            first_nameLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            first_nameLabel.AutoSize = true;
            first_nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            first_nameLabel.Location = new System.Drawing.Point(37, 61);
            first_nameLabel.Name = "first_nameLabel";
            first_nameLabel.Size = new System.Drawing.Size(80, 17);
            first_nameLabel.TabIndex = 6;
            first_nameLabel.Text = "First Name:";
            // 
            // last_nameLabel
            // 
            last_nameLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            last_nameLabel.AutoSize = true;
            last_nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            last_nameLabel.Location = new System.Drawing.Point(303, 61);
            last_nameLabel.Name = "last_nameLabel";
            last_nameLabel.Size = new System.Drawing.Size(80, 17);
            last_nameLabel.TabIndex = 8;
            last_nameLabel.Text = "Last Name:";
            // 
            // phone_numberLabel
            // 
            phone_numberLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            phone_numberLabel.AutoSize = true;
            phone_numberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            phone_numberLabel.Location = new System.Drawing.Point(276, 103);
            phone_numberLabel.Name = "phone_numberLabel";
            phone_numberLabel.Size = new System.Drawing.Size(107, 17);
            phone_numberLabel.TabIndex = 10;
            phone_numberLabel.Text = "Phone Number:";
            // 
            // address1Label
            // 
            address1Label.Anchor = System.Windows.Forms.AnchorStyles.Right;
            address1Label.AutoSize = true;
            address1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            address1Label.Location = new System.Drawing.Point(41, 141);
            address1Label.Name = "address1Label";
            address1Label.Size = new System.Drawing.Size(76, 17);
            address1Label.TabIndex = 12;
            address1Label.Text = "Address 1:";
            // 
            // address2Label
            // 
            address2Label.Anchor = System.Windows.Forms.AnchorStyles.Right;
            address2Label.AutoSize = true;
            address2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            address2Label.Location = new System.Drawing.Point(41, 182);
            address2Label.Name = "address2Label";
            address2Label.Size = new System.Drawing.Size(76, 17);
            address2Label.TabIndex = 14;
            address2Label.Text = "Address 2:";
            // 
            // zipLabel
            // 
            zipLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            zipLabel.AutoSize = true;
            zipLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            zipLabel.Location = new System.Drawing.Point(48, 225);
            zipLabel.Name = "zipLabel";
            zipLabel.Size = new System.Drawing.Size(69, 17);
            zipLabel.TabIndex = 16;
            zipLabel.Text = "Zip Code:";
            // 
            // sexLabel
            // 
            sexLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            sexLabel.AutoSize = true;
            sexLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sexLabel.Location = new System.Drawing.Point(348, 225);
            sexLabel.Name = "sexLabel";
            sexLabel.Size = new System.Drawing.Size(35, 17);
            sexLabel.TabIndex = 18;
            sexLabel.Text = "Sex:";
            // 
            // codeLabel
            // 
            codeLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            codeLabel.AutoSize = true;
            codeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            codeLabel.Location = new System.Drawing.Point(72, 275);
            codeLabel.Name = "codeLabel";
            codeLabel.Size = new System.Drawing.Size(45, 17);
            codeLabel.TabIndex = 30;
            codeLabel.Text = "State:";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 4;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.43206F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.56794F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 139F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel.Controls.Add(codeLabel, 0, 6);
            this.tableLayoutPanel.Controls.Add(this.codeComboBox, 1, 6);
            this.tableLayoutPanel.Controls.Add(userIDLabel, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.userIDTextBox, 1, 0);
            this.tableLayoutPanel.Controls.Add(first_nameLabel, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.first_nameTextBox, 1, 1);
            this.tableLayoutPanel.Controls.Add(last_nameLabel, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.last_nameTextBox, 3, 1);
            this.tableLayoutPanel.Controls.Add(date_of_birthLabel, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.date_of_birthTextBox, 1, 2);
            this.tableLayoutPanel.Controls.Add(phone_numberLabel, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.phone_numberTextBox, 3, 2);
            this.tableLayoutPanel.Controls.Add(address1Label, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.address1TextBox, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.getUserButton, 3, 0);
            this.tableLayoutPanel.Controls.Add(zipLabel, 0, 5);
            this.tableLayoutPanel.Controls.Add(sexLabel, 2, 5);
            this.tableLayoutPanel.Controls.Add(this.sexComboBox, 3, 5);
            this.tableLayoutPanel.Controls.Add(address2Label, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.address2TextBox, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.resetPasswordLabel, 3, 3);
            this.tableLayoutPanel.Controls.Add(this.resetPasswrodTextBox, 3, 4);
            this.tableLayoutPanel.Controls.Add(this.statusLabel, 0, 7);
            this.tableLayoutPanel.Controls.Add(this.statusCheckBox, 1, 7);
            this.tableLayoutPanel.Controls.Add(this.cityLabel, 2, 6);
            this.tableLayoutPanel.Controls.Add(this.cityTextBox, 3, 6);
            this.tableLayoutPanel.Controls.Add(this.zipTextBox, 1, 5);
            this.tableLayoutPanel.Controls.Add(this.updateButton, 2, 7);
            this.tableLayoutPanel.Controls.Add(this.clearButton, 3, 7);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 8;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.9434F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.0566F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(567, 367);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // codeComboBox
            // 
            this.codeComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.codeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.stateBindingSource, "code", true));
            this.codeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.codeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.codeComboBox.FormattingEnabled = true;
            this.codeComboBox.Location = new System.Drawing.Point(123, 271);
            this.codeComboBox.Name = "codeComboBox";
            this.codeComboBox.Size = new System.Drawing.Size(121, 24);
            this.codeComboBox.TabIndex = 11;
            // 
            // stateBindingSource
            // 
            this.stateBindingSource.DataMember = "state";
            this.stateBindingSource.DataSource = this._cs6232_g4DataSet;
            // 
            // _cs6232_g4DataSet
            // 
            this._cs6232_g4DataSet.DataSetName = "_cs6232_g4DataSet";
            this._cs6232_g4DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // userIDTextBox
            // 
            this.userIDTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tableLayoutPanel.SetColumnSpan(this.userIDTextBox, 2);
            this.userIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "userID", true));
            this.userIDTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userIDTextBox.Location = new System.Drawing.Point(123, 12);
            this.userIDTextBox.Name = "userIDTextBox";
            this.userIDTextBox.Size = new System.Drawing.Size(257, 23);
            this.userIDTextBox.TabIndex = 0;
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataMember = "user";
            this.userBindingSource.DataSource = this._cs6232_g4DataSet;
            // 
            // first_nameTextBox
            // 
            this.first_nameTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.first_nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "first_name", true));
            this.first_nameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.first_nameTextBox.Location = new System.Drawing.Point(123, 58);
            this.first_nameTextBox.Name = "first_nameTextBox";
            this.first_nameTextBox.Size = new System.Drawing.Size(118, 23);
            this.first_nameTextBox.TabIndex = 2;
            // 
            // last_nameTextBox
            // 
            this.last_nameTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.last_nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "last_name", true));
            this.last_nameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.last_nameTextBox.Location = new System.Drawing.Point(389, 58);
            this.last_nameTextBox.Name = "last_nameTextBox";
            this.last_nameTextBox.Size = new System.Drawing.Size(121, 23);
            this.last_nameTextBox.TabIndex = 3;
            // 
            // date_of_birthTextBox
            // 
            this.date_of_birthTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.date_of_birthTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "date_of_birth", true));
            this.date_of_birthTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.date_of_birthTextBox.Location = new System.Drawing.Point(123, 100);
            this.date_of_birthTextBox.Name = "date_of_birthTextBox";
            this.date_of_birthTextBox.Size = new System.Drawing.Size(118, 23);
            this.date_of_birthTextBox.TabIndex = 4;
            // 
            // phone_numberTextBox
            // 
            this.phone_numberTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.phone_numberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "phone_number", true));
            this.phone_numberTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phone_numberTextBox.Location = new System.Drawing.Point(389, 100);
            this.phone_numberTextBox.Name = "phone_numberTextBox";
            this.phone_numberTextBox.Size = new System.Drawing.Size(121, 23);
            this.phone_numberTextBox.TabIndex = 5;
            // 
            // address1TextBox
            // 
            this.address1TextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tableLayoutPanel.SetColumnSpan(this.address1TextBox, 2);
            this.address1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "address1", true));
            this.address1TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.address1TextBox.Location = new System.Drawing.Point(123, 138);
            this.address1TextBox.Name = "address1TextBox";
            this.address1TextBox.Size = new System.Drawing.Size(257, 23);
            this.address1TextBox.TabIndex = 6;
            // 
            // getUserButton
            // 
            this.getUserButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.getUserButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getUserButton.Location = new System.Drawing.Point(434, 8);
            this.getUserButton.Name = "getUserButton";
            this.getUserButton.Size = new System.Drawing.Size(85, 30);
            this.getUserButton.TabIndex = 1;
            this.getUserButton.Text = "Get";
            this.getUserButton.UseVisualStyleBackColor = true;
            this.getUserButton.Click += new System.EventHandler(this.GetUserButton_Click);
            // 
            // sexComboBox
            // 
            this.sexComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.sexComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "sex", true));
            this.sexComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sexComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sexComboBox.FormattingEnabled = true;
            this.sexComboBox.Location = new System.Drawing.Point(389, 221);
            this.sexComboBox.Name = "sexComboBox";
            this.sexComboBox.Size = new System.Drawing.Size(121, 24);
            this.sexComboBox.TabIndex = 10;
            // 
            // address2TextBox
            // 
            this.address2TextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tableLayoutPanel.SetColumnSpan(this.address2TextBox, 2);
            this.address2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "address2", true));
            this.address2TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.address2TextBox.Location = new System.Drawing.Point(123, 179);
            this.address2TextBox.Name = "address2TextBox";
            this.address2TextBox.Size = new System.Drawing.Size(257, 23);
            this.address2TextBox.TabIndex = 7;
            // 
            // resetPasswordLabel
            // 
            this.resetPasswordLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.resetPasswordLabel.AutoSize = true;
            this.resetPasswordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetPasswordLabel.Location = new System.Drawing.Point(419, 152);
            this.resetPasswordLabel.Name = "resetPasswordLabel";
            this.resetPasswordLabel.Size = new System.Drawing.Size(114, 17);
            this.resetPasswordLabel.TabIndex = 25;
            this.resetPasswordLabel.Text = "Reset Password:";
            // 
            // resetPasswrodTextBox
            // 
            this.resetPasswrodTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.resetPasswrodTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetPasswrodTextBox.Location = new System.Drawing.Point(397, 179);
            this.resetPasswrodTextBox.Name = "resetPasswrodTextBox";
            this.resetPasswrodTextBox.PasswordChar = '*';
            this.resetPasswrodTextBox.Size = new System.Drawing.Size(159, 23);
            this.resetPasswrodTextBox.TabIndex = 8;
            // 
            // statusLabel
            // 
            this.statusLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.statusLabel.AutoSize = true;
            this.statusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusLabel.Location = new System.Drawing.Point(65, 331);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(52, 17);
            this.statusLabel.TabIndex = 23;
            this.statusLabel.Text = "Status:";
            // 
            // statusCheckBox
            // 
            this.statusCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.statusCheckBox.AutoSize = true;
            this.statusCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusCheckBox.Location = new System.Drawing.Point(123, 326);
            this.statusCheckBox.Name = "statusCheckBox";
            this.statusCheckBox.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.statusCheckBox.Size = new System.Drawing.Size(65, 26);
            this.statusCheckBox.TabIndex = 13;
            this.statusCheckBox.Text = "Active";
            this.statusCheckBox.UseVisualStyleBackColor = true;
            // 
            // clearButton
            // 
            this.clearButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.clearButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearButton.Location = new System.Drawing.Point(436, 325);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(80, 29);
            this.clearButton.TabIndex = 15;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.updateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateButton.Location = new System.Drawing.Point(275, 325);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(82, 29);
            this.updateButton.TabIndex = 14;
            this.updateButton.Text = "Update";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // cityLabel
            // 
            this.cityLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cityLabel.AutoSize = true;
            this.cityLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cityLabel.Location = new System.Drawing.Point(348, 275);
            this.cityLabel.Name = "cityLabel";
            this.cityLabel.Size = new System.Drawing.Size(35, 17);
            this.cityLabel.TabIndex = 29;
            this.cityLabel.Text = "City:";
            // 
            // cityTextBox
            // 
            this.cityTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cityTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cityTextBox.Location = new System.Drawing.Point(389, 272);
            this.cityTextBox.Name = "cityTextBox";
            this.cityTextBox.Size = new System.Drawing.Size(121, 23);
            this.cityTextBox.TabIndex = 12;
            // 
            // zipTextBox
            // 
            this.zipTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.zipTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zipTextBox.Location = new System.Drawing.Point(123, 222);
            this.zipTextBox.Name = "zipTextBox";
            this.zipTextBox.Size = new System.Drawing.Size(118, 23);
            this.zipTextBox.TabIndex = 9;
            // 
            // userTableAdapter
            // 
            this.userTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.categoryTableAdapter = null;
            this.tableAdapterManager.furniture_itemTableAdapter = null;
            this.tableAdapterManager.locationTableAdapter = null;
            this.tableAdapterManager.rental_itemTableAdapter = null;
            this.tableAdapterManager.rental_transactionTableAdapter = null;
            this.tableAdapterManager.return_itemTableAdapter = null;
            this.tableAdapterManager.return_transactionTableAdapter = null;
            this.tableAdapterManager.roleTableAdapter = null;
            this.tableAdapterManager.stateTableAdapter = null;
            this.tableAdapterManager.styleTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.userTableAdapter = this.userTableAdapter;
            // 
            // stateTableAdapter
            // 
            this.stateTableAdapter.ClearBeforeFill = true;
            // 
            // UpdateEmployeeUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "UpdateEmployeeUserControl";
            this.Size = new System.Drawing.Size(567, 367);
            this.VisibleChanged += new System.EventHandler(this.UpdateEmployeeUserControl_Load);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private _cs6232_g4DataSet _cs6232_g4DataSet;
        private System.Windows.Forms.BindingSource userBindingSource;
        private _cs6232_g4DataSetTableAdapters.userTableAdapter userTableAdapter;
        private _cs6232_g4DataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox phone_numberTextBox;
        private System.Windows.Forms.TextBox address1TextBox;
        private System.Windows.Forms.TextBox address2TextBox;
        private System.Windows.Forms.ComboBox sexComboBox;
        private System.Windows.Forms.TextBox userIDTextBox;
        private System.Windows.Forms.TextBox date_of_birthTextBox;
        private System.Windows.Forms.TextBox first_nameTextBox;
        private System.Windows.Forms.TextBox last_nameTextBox;
        private System.Windows.Forms.Button getUserButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.CheckBox statusCheckBox;
        private System.Windows.Forms.Label resetPasswordLabel;
        private System.Windows.Forms.TextBox resetPasswrodTextBox;
        private System.Windows.Forms.Label cityLabel;
        private System.Windows.Forms.TextBox cityTextBox;
        private System.Windows.Forms.ComboBox codeComboBox;
        private System.Windows.Forms.BindingSource stateBindingSource;
        private _cs6232_g4DataSetTableAdapters.stateTableAdapter stateTableAdapter;
        private System.Windows.Forms.TextBox zipTextBox;
    }
}
