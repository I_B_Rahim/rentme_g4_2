﻿using System;


namespace RentMe_G4.Model
{
    /// <summary>
    /// This class models a return item
    /// </summary>
    public class ReturnItem
    {

        private int itemID;
        private int returnID;
        private int quantity;

        /// <summary>
        /// Accessor and modifier for ItemID
        /// </summary>
        public int ItemID
        {
            get
            {
                return this.itemID;
            }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentException("itemID cannot be less than 1", "itemID error");
                }
                this.itemID = value;
            }
        }

        /// <summary>
        /// Accessor and modifier for returnID
        /// </summary>
        public int ReturnID
        {
            get
            {
                return this.returnID;
            }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentException("returnID cannot be less than 1", "returnID error");
                }
                this.returnID = value;
            }
        }

        /// <summary>
        /// Accessor and modifier for quantity
        /// </summary>
        public int Quantity
        {
            get
            {
                return this.quantity;
            }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentException("returnID cannot be less than 1", "returnID error");
                }
                this.quantity = value;
            }
        }

        /// <summary>
        /// Accessor and modifier for Amount_due
        /// </summary>
        public decimal Amount_due { get; set; }

        /// <summary>
        /// Accessor and modifier for Amount_paid
        /// </summary>
        public decimal Amount_paid { get; set; }

        /// <summary>
        /// Accessor and modifier for Amount refunded
        /// </summary>
        public decimal Amount_refunded { get; set; }
    }
}
