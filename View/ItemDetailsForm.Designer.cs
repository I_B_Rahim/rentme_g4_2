﻿namespace RentMe_G4.View
{
    partial class ItemDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label serial_numberLabel;
            System.Windows.Forms.Label descriptionLabel;
            System.Windows.Forms.Label categoryIDLabel;
            System.Windows.Forms.Label styleIDLabel;
            System.Windows.Forms.Label daily_rental_rateLabel;
            System.Windows.Forms.Label daily_fine_rateLabel;
            System.Windows.Forms.Label quantityLabel;
            System.Windows.Forms.Label style_typeLabel;
            System.Windows.Forms.Label category_nameLabel;
            System.Windows.Forms.Label itemDetailsLabel;
            this._cs6232_g4DataSet = new RentMe_G4._cs6232_g4DataSet();
            this.furniture_itemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.furniture_itemTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.furniture_itemTableAdapter();
            this.tableAdapterManager = new RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager();
            this.serial_numberTextBox = new System.Windows.Forms.TextBox();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.categoryIDTextBox = new System.Windows.Forms.TextBox();
            this.styleIDTextBox = new System.Windows.Forms.TextBox();
            this.daily_rental_rateTextBox = new System.Windows.Forms.TextBox();
            this.daily_fine_rateTextBox = new System.Windows.Forms.TextBox();
            this.quantityTextBox = new System.Windows.Forms.TextBox();
            this.Okbtn = new System.Windows.Forms.Button();
            this.styleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.styleTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.styleTableAdapter();
            this.style_typeTextBox = new System.Windows.Forms.TextBox();
            this.categoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.categoryTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.categoryTableAdapter();
            this.category_nameTextBox = new System.Windows.Forms.TextBox();
            serial_numberLabel = new System.Windows.Forms.Label();
            descriptionLabel = new System.Windows.Forms.Label();
            categoryIDLabel = new System.Windows.Forms.Label();
            styleIDLabel = new System.Windows.Forms.Label();
            daily_rental_rateLabel = new System.Windows.Forms.Label();
            daily_fine_rateLabel = new System.Windows.Forms.Label();
            quantityLabel = new System.Windows.Forms.Label();
            style_typeLabel = new System.Windows.Forms.Label();
            category_nameLabel = new System.Windows.Forms.Label();
            itemDetailsLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.furniture_itemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.styleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // serial_numberLabel
            // 
            serial_numberLabel.AutoSize = true;
            serial_numberLabel.Location = new System.Drawing.Point(79, 76);
            serial_numberLabel.Name = "serial_numberLabel";
            serial_numberLabel.Size = new System.Drawing.Size(74, 13);
            serial_numberLabel.TabIndex = 1;
            serial_numberLabel.Text = "Serial number:";
            // 
            // descriptionLabel
            // 
            descriptionLabel.AutoSize = true;
            descriptionLabel.Location = new System.Drawing.Point(79, 102);
            descriptionLabel.Name = "descriptionLabel";
            descriptionLabel.Size = new System.Drawing.Size(63, 13);
            descriptionLabel.TabIndex = 3;
            descriptionLabel.Text = "Description:";
            // 
            // categoryIDLabel
            // 
            categoryIDLabel.AutoSize = true;
            categoryIDLabel.Location = new System.Drawing.Point(79, 128);
            categoryIDLabel.Name = "categoryIDLabel";
            categoryIDLabel.Size = new System.Drawing.Size(66, 13);
            categoryIDLabel.TabIndex = 5;
            categoryIDLabel.Text = "Category ID:";
            // 
            // styleIDLabel
            // 
            styleIDLabel.AutoSize = true;
            styleIDLabel.Location = new System.Drawing.Point(79, 154);
            styleIDLabel.Name = "styleIDLabel";
            styleIDLabel.Size = new System.Drawing.Size(47, 13);
            styleIDLabel.TabIndex = 7;
            styleIDLabel.Text = "Style ID:";
            // 
            // daily_rental_rateLabel
            // 
            daily_rental_rateLabel.AutoSize = true;
            daily_rental_rateLabel.Location = new System.Drawing.Point(79, 180);
            daily_rental_rateLabel.Name = "daily_rental_rateLabel";
            daily_rental_rateLabel.Size = new System.Drawing.Size(83, 13);
            daily_rental_rateLabel.TabIndex = 9;
            daily_rental_rateLabel.Text = "Daily rental rate:";
            // 
            // daily_fine_rateLabel
            // 
            daily_fine_rateLabel.AutoSize = true;
            daily_fine_rateLabel.Location = new System.Drawing.Point(79, 206);
            daily_fine_rateLabel.Name = "daily_fine_rateLabel";
            daily_fine_rateLabel.Size = new System.Drawing.Size(74, 13);
            daily_fine_rateLabel.TabIndex = 11;
            daily_fine_rateLabel.Text = "Daily fine rate:";
            // 
            // quantityLabel
            // 
            quantityLabel.AutoSize = true;
            quantityLabel.Location = new System.Drawing.Point(79, 232);
            quantityLabel.Name = "quantityLabel";
            quantityLabel.Size = new System.Drawing.Size(49, 13);
            quantityLabel.TabIndex = 13;
            quantityLabel.Text = "Quantity:";
            // 
            // style_typeLabel
            // 
            style_typeLabel.AutoSize = true;
            style_typeLabel.Location = new System.Drawing.Point(218, 154);
            style_typeLabel.Name = "style_typeLabel";
            style_typeLabel.Size = new System.Drawing.Size(34, 13);
            style_typeLabel.TabIndex = 18;
            style_typeLabel.Text = "Type:";
            // 
            // _cs6232_g4DataSet
            // 
            this._cs6232_g4DataSet.DataSetName = "_cs6232_g4DataSet";
            this._cs6232_g4DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // furniture_itemBindingSource
            // 
            this.furniture_itemBindingSource.DataMember = "furniture_item";
            this.furniture_itemBindingSource.DataSource = this._cs6232_g4DataSet;
            // 
            // furniture_itemTableAdapter
            // 
            this.furniture_itemTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.categoryTableAdapter = null;
            this.tableAdapterManager.furniture_itemTableAdapter = this.furniture_itemTableAdapter;
            this.tableAdapterManager.locationTableAdapter = null;
            this.tableAdapterManager.rental_itemTableAdapter = null;
            this.tableAdapterManager.rental_transactionTableAdapter = null;
            this.tableAdapterManager.return_itemTableAdapter = null;
            this.tableAdapterManager.return_transactionTableAdapter = null;
            this.tableAdapterManager.roleTableAdapter = null;
            this.tableAdapterManager.stateTableAdapter = null;
            this.tableAdapterManager.styleTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.userTableAdapter = null;
            // 
            // serial_numberTextBox
            // 
            this.serial_numberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.furniture_itemBindingSource, "serial_number", true));
            this.serial_numberTextBox.Location = new System.Drawing.Point(166, 73);
            this.serial_numberTextBox.Name = "serial_numberTextBox";
            this.serial_numberTextBox.ReadOnly = true;
            this.serial_numberTextBox.Size = new System.Drawing.Size(215, 20);
            this.serial_numberTextBox.TabIndex = 2;
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.furniture_itemBindingSource, "description", true));
            this.descriptionTextBox.Location = new System.Drawing.Point(166, 99);
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.ReadOnly = true;
            this.descriptionTextBox.Size = new System.Drawing.Size(215, 20);
            this.descriptionTextBox.TabIndex = 4;
            // 
            // categoryIDTextBox
            // 
            this.categoryIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.furniture_itemBindingSource, "categoryID", true));
            this.categoryIDTextBox.Location = new System.Drawing.Point(166, 125);
            this.categoryIDTextBox.Name = "categoryIDTextBox";
            this.categoryIDTextBox.ReadOnly = true;
            this.categoryIDTextBox.Size = new System.Drawing.Size(40, 20);
            this.categoryIDTextBox.TabIndex = 6;
            // 
            // styleIDTextBox
            // 
            this.styleIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.furniture_itemBindingSource, "styleID", true));
            this.styleIDTextBox.Location = new System.Drawing.Point(166, 151);
            this.styleIDTextBox.Name = "styleIDTextBox";
            this.styleIDTextBox.ReadOnly = true;
            this.styleIDTextBox.Size = new System.Drawing.Size(40, 20);
            this.styleIDTextBox.TabIndex = 8;
            // 
            // daily_rental_rateTextBox
            // 
            this.daily_rental_rateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.furniture_itemBindingSource, "daily_rental_rate", true));
            this.daily_rental_rateTextBox.Location = new System.Drawing.Point(166, 177);
            this.daily_rental_rateTextBox.Name = "daily_rental_rateTextBox";
            this.daily_rental_rateTextBox.ReadOnly = true;
            this.daily_rental_rateTextBox.Size = new System.Drawing.Size(40, 20);
            this.daily_rental_rateTextBox.TabIndex = 10;
            // 
            // daily_fine_rateTextBox
            // 
            this.daily_fine_rateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.furniture_itemBindingSource, "daily_fine_rate", true));
            this.daily_fine_rateTextBox.Location = new System.Drawing.Point(166, 203);
            this.daily_fine_rateTextBox.Name = "daily_fine_rateTextBox";
            this.daily_fine_rateTextBox.ReadOnly = true;
            this.daily_fine_rateTextBox.Size = new System.Drawing.Size(40, 20);
            this.daily_fine_rateTextBox.TabIndex = 12;
            // 
            // quantityTextBox
            // 
            this.quantityTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.furniture_itemBindingSource, "quantity", true));
            this.quantityTextBox.Location = new System.Drawing.Point(166, 229);
            this.quantityTextBox.Name = "quantityTextBox";
            this.quantityTextBox.ReadOnly = true;
            this.quantityTextBox.Size = new System.Drawing.Size(40, 20);
            this.quantityTextBox.TabIndex = 14;
            // 
            // Okbtn
            // 
            this.Okbtn.Location = new System.Drawing.Point(306, 226);
            this.Okbtn.Name = "Okbtn";
            this.Okbtn.Size = new System.Drawing.Size(75, 23);
            this.Okbtn.TabIndex = 15;
            this.Okbtn.Text = "OK";
            this.Okbtn.UseVisualStyleBackColor = true;
            this.Okbtn.Click += new System.EventHandler(this.Okbtn_Click);
            // 
            // styleBindingSource
            // 
            this.styleBindingSource.DataMember = "style";
            this.styleBindingSource.DataSource = this._cs6232_g4DataSet;
            // 
            // styleTableAdapter
            // 
            this.styleTableAdapter.ClearBeforeFill = true;
            // 
            // style_typeTextBox
            // 
            this.style_typeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.styleBindingSource, "style_type", true));
            this.style_typeTextBox.Location = new System.Drawing.Point(262, 151);
            this.style_typeTextBox.Name = "style_typeTextBox";
            this.style_typeTextBox.ReadOnly = true;
            this.style_typeTextBox.Size = new System.Drawing.Size(119, 20);
            this.style_typeTextBox.TabIndex = 19;
            // 
            // categoryBindingSource
            // 
            this.categoryBindingSource.DataMember = "category";
            this.categoryBindingSource.DataSource = this._cs6232_g4DataSet;
            // 
            // categoryTableAdapter
            // 
            this.categoryTableAdapter.ClearBeforeFill = true;
            // 
            // category_nameLabel
            // 
            category_nameLabel.AutoSize = true;
            category_nameLabel.Location = new System.Drawing.Point(218, 128);
            category_nameLabel.Name = "category_nameLabel";
            category_nameLabel.Size = new System.Drawing.Size(38, 13);
            category_nameLabel.TabIndex = 22;
            category_nameLabel.Text = "Name:";
            // 
            // category_nameTextBox
            // 
            this.category_nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.categoryBindingSource, "category_name", true));
            this.category_nameTextBox.Location = new System.Drawing.Point(262, 125);
            this.category_nameTextBox.Name = "category_nameTextBox";
            this.category_nameTextBox.ReadOnly = true;
            this.category_nameTextBox.Size = new System.Drawing.Size(119, 20);
            this.category_nameTextBox.TabIndex = 23;
            // 
            // itemDetailsLabel
            // 
            itemDetailsLabel.AutoSize = true;
            itemDetailsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            itemDetailsLabel.Location = new System.Drawing.Point(190, 33);
            itemDetailsLabel.Name = "itemDetailsLabel";
            itemDetailsLabel.Size = new System.Drawing.Size(106, 20);
            itemDetailsLabel.TabIndex = 24;
            itemDetailsLabel.Text = "Item Details";
            // 
            // ItemDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 316);
            this.Controls.Add(itemDetailsLabel);
            this.Controls.Add(category_nameLabel);
            this.Controls.Add(this.category_nameTextBox);
            this.Controls.Add(style_typeLabel);
            this.Controls.Add(this.style_typeTextBox);
            this.Controls.Add(this.Okbtn);
            this.Controls.Add(serial_numberLabel);
            this.Controls.Add(this.serial_numberTextBox);
            this.Controls.Add(descriptionLabel);
            this.Controls.Add(this.descriptionTextBox);
            this.Controls.Add(categoryIDLabel);
            this.Controls.Add(this.categoryIDTextBox);
            this.Controls.Add(styleIDLabel);
            this.Controls.Add(this.styleIDTextBox);
            this.Controls.Add(daily_rental_rateLabel);
            this.Controls.Add(this.daily_rental_rateTextBox);
            this.Controls.Add(daily_fine_rateLabel);
            this.Controls.Add(this.daily_fine_rateTextBox);
            this.Controls.Add(quantityLabel);
            this.Controls.Add(this.quantityTextBox);
            this.Name = "ItemDetailsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Items Details Page";
            this.Load += new System.EventHandler(this.ItemDetailsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.furniture_itemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.styleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private _cs6232_g4DataSet _cs6232_g4DataSet;
        private System.Windows.Forms.BindingSource furniture_itemBindingSource;
        private _cs6232_g4DataSetTableAdapters.furniture_itemTableAdapter furniture_itemTableAdapter;
        private _cs6232_g4DataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox serial_numberTextBox;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.TextBox categoryIDTextBox;
        private System.Windows.Forms.TextBox styleIDTextBox;
        private System.Windows.Forms.TextBox daily_rental_rateTextBox;
        private System.Windows.Forms.TextBox daily_fine_rateTextBox;
        private System.Windows.Forms.TextBox quantityTextBox;
        private System.Windows.Forms.Button Okbtn;
        private System.Windows.Forms.BindingSource styleBindingSource;
        private _cs6232_g4DataSetTableAdapters.styleTableAdapter styleTableAdapter;
        private System.Windows.Forms.TextBox style_typeTextBox;
        private System.Windows.Forms.BindingSource categoryBindingSource;
        private _cs6232_g4DataSetTableAdapters.categoryTableAdapter categoryTableAdapter;
        private System.Windows.Forms.TextBox category_nameTextBox;
    }
}