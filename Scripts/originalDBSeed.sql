﻿INSERT [dbo].[state] ([code], [name]) VALUES ('AL', 'Alabama')
INSERT [dbo].[state] ([code], [name]) VALUES ('AK', 'Alaska')
INSERT [dbo].[state] ([code], [name]) VALUES ('AZ', 'Arizona')
INSERT [dbo].[state] ([code], [name]) VALUES ('AR', 'Arkansas')
INSERT [dbo].[state] ([code], [name]) VALUES ('CA', 'California')
INSERT [dbo].[state] ([code], [name]) VALUES ('CO', 'Colorado')
INSERT [dbo].[state] ([code], [name]) VALUES ('CT', 'Connecticut')
INSERT [dbo].[state] ([code], [name]) VALUES ('DE', 'Delaware')
INSERT [dbo].[state] ([code], [name]) VALUES ('FL', 'Florida')
INSERT [dbo].[state] ([code], [name]) VALUES ('GA', 'Georgia')
INSERT [dbo].[state] ([code], [name]) VALUES ('HI', 'Hawaii')
INSERT [dbo].[state] ([code], [name]) VALUES ('ID', 'Idaho')
INSERT [dbo].[state] ([code], [name]) VALUES ('IL', 'Illinois')
INSERT [dbo].[state] ([code], [name]) VALUES ('IN', 'Indiana')
INSERT [dbo].[state] ([code], [name]) VALUES ('IA', 'Iowa')
INSERT [dbo].[state] ([code], [name]) VALUES ('KS', 'Kansas')
INSERT [dbo].[state] ([code], [name]) VALUES ('KY', 'Kentucky')
INSERT [dbo].[state] ([code], [name]) VALUES ('LA', 'Louisiana')
INSERT [dbo].[state] ([code], [name]) VALUES ('ME', 'Maine')
INSERT [dbo].[state] ([code], [name]) VALUES ('MD', 'Maryland')
INSERT [dbo].[state] ([code], [name]) VALUES ('MA', 'Massachusetts')
INSERT [dbo].[state] ([code], [name]) VALUES ('MI', 'Michigan')
INSERT [dbo].[state] ([code], [name]) VALUES ('MN', 'Minnesota')
INSERT [dbo].[state] ([code], [name]) VALUES ('MS', 'Mississippi')
INSERT [dbo].[state] ([code], [name]) VALUES ('MO', 'Missouri')
INSERT [dbo].[state] ([code], [name]) VALUES ('MT', 'Montana')
INSERT [dbo].[state] ([code], [name]) VALUES ('NE', 'Nebraska')
INSERT [dbo].[state] ([code], [name]) VALUES ('NV', 'Nevada')
INSERT [dbo].[state] ([code], [name]) VALUES ('NH', 'New Hampshire')
INSERT [dbo].[state] ([code], [name]) VALUES ('NJ', 'New Jersey')
INSERT [dbo].[state] ([code], [name]) VALUES ('NM', 'New Mexico')
INSERT [dbo].[state] ([code], [name]) VALUES ('NY', 'New York')
INSERT [dbo].[state] ([code], [name]) VALUES ('NC', 'North Carolina')
INSERT [dbo].[state] ([code], [name]) VALUES ('ND', 'North Dakota')
INSERT [dbo].[state] ([code], [name]) VALUES ('OH', 'Ohio')
INSERT [dbo].[state] ([code], [name]) VALUES ('OK', 'Oklahoma')
INSERT [dbo].[state] ([code], [name]) VALUES ('OR', 'Oregon')
INSERT [dbo].[state] ([code], [name]) VALUES ('PA', 'Pennsylvania')
INSERT [dbo].[state] ([code], [name]) VALUES ('RI', 'Rhode Island')
INSERT [dbo].[state] ([code], [name]) VALUES ('SC', 'South Carolina')
INSERT [dbo].[state] ([code], [name]) VALUES ('SD', 'South Dakota')
INSERT [dbo].[state] ([code], [name]) VALUES ('TN', 'Tennessee')
INSERT [dbo].[state] ([code], [name]) VALUES ('TX', 'Texas')
INSERT [dbo].[state] ([code], [name]) VALUES ('UT', 'Utah')
INSERT [dbo].[state] ([code], [name]) VALUES ('VT', 'Vermont')
INSERT [dbo].[state] ([code], [name]) VALUES ('VA', 'Virginia')
INSERT [dbo].[state] ([code], [name]) VALUES ('WA', 'Washington')
INSERT [dbo].[state] ([code], [name]) VALUES ('WV', 'West Virginia')
INSERT [dbo].[state] ([code], [name]) VALUES ('WI', 'Wisconsin')
INSERT [dbo].[state] ([code], [name]) VALUES ('WY', 'Wyoming')

INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('01760', 'Natick', 'MA')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('02101', 'Boston', 'MA')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('08601', 'Trenton', 'NJ')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('12084', 'Albany', 'NY')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('17025', 'Harrisburg', 'PA')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('21401', 'Annapolis', 'MD')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('22434', 'San Diego', 'CA')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('27513', 'Raleigh', 'NC')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('29401', 'Charleston', 'WV')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('30118', 'Carrollton', 'GA')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('30301', 'Atlanta', 'GA')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('32301', 'Tallahassee', 'FL')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('32789', 'Orlando', 'FL')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('33101', 'Miami', 'FL')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('40018', 'Louisville', 'KY')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('43004', 'Columbus', 'OH')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('48864', 'Lansing', 'MI')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('65043', 'Jefferson City', 'MO')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('73008', 'Oklahoma City', 'OK')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('73301', 'Austin', 'TX')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('77001', 'Houston', 'TX')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('77351', 'Livingston', 'TX')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('85001', 'Phoenix', 'AZ')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('94016', 'San Francisco', 'CA')
INSERT [dbo].[location] ([zip], [city], [state]) VALUES ('94203', 'Sacramento', 'CA')

INSERT [dbo].[role] ([type]) VALUES ('administrator')
INSERT [dbo].[role] ([type]) VALUES ('employee')
INSERT [dbo].[role] ([type]) VALUES ('customer')

INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [zip], [sex]) VALUES (1, '1973-09-27', 'Jane', 'Doe', '6027589812', '120 Dryden Place', '01760', 'F')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [zip], [sex]) VALUES (1, '1982-09-27', 'Zelda', 'Stuttard', '9126604920', '50 Lumin Street', '30118', 'M')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [zip], [sex]) VALUES (1, '1965-09-27', 'Gibbie', 'Scotford', '9894772454', '897 Kinan Circle', '94203', 'F')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [zip], [sex]) VALUES (1, '1985-09-27', 'Margarita', 'Pettiward', '9189666561', '135 Rossbury Trail', '33101', 'F')

INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (1, 'jdoe1', 't+3h8M2HSFbd8vxyd0G3xw==', 1)
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (2, 'zstuttard1', 't+3h8M2HSFbd8vxyd0G3xw==', 1)
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (3, 'gscotford1', 't+3h8M2HSFbd8vxyd0G3xw==', 1)
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (4, 'mpettiward1', 't+3h8M2HSFbd8vxyd0G3xw==', 1)

INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (2, '1970-08-30', 'Dell', 'Joutapaitis', '6027619812', '8744 Lighthouse Bay Way', NULL, '85001', 'M')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (2, '1979-10-11', 'Jeffry', 'Weymont', '5858705203', '203 Buena Vista Circle', NULL, '12084', 'M')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (2, '1969-01-23', 'Filippo', 'Coan', '7063870403', '7701 Dryden Street', 'Apt 456', '30301', 'M')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (2, '1959-10-31', 'Osbert', 'Colvine', '3148836056', '72 Meadow Vale Plaza', NULL, '65043', 'M')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (2, '1971-10-28', 'Konrad', 'Lemmen', '4433631375', '69 Comanche Plaza', NULL, '21401', 'M')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (2, '1956-12-17', 'Derick', 'Veale', '9126605320', '53475 Buhler Avenue', NULL, '30118', 'M')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (2, '1980-08-19', 'Harman', 'Tibbs', '8283571904', '89 Kinsman Street', 'Apt 312', '27513', 'F')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (2, '1956-06-11', 'Ebba', 'Bosson', '9734772454', '53097 Cody Terrace', NULL, '08601', 'F')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (2, '1973-02-26', 'Cahra', 'Poland', '9187666561', '0 Loomis Place', NULL, '73008', 'F')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (2, '1970-05-01', 'Delaney', 'Ivanyushin', '4153454902', '13 Roxbury Center', NULL, '94203', 'M')

INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (5, 'djoutapaitis1', 't+3h8M2HSFbd8vxyd0G3xw==', 1)
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (6, 'jweymont1', 't+3h8M2HSFbd8vxyd0G3xw==', 1)
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (7, 'fcoan1', 't+3h8M2HSFbd8vxyd0G3xw==', 1)
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (8, 'ocolvine1', 't+3h8M2HSFbd8vxyd0G3xw==', 1)
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (9, 'klemmen1', 't+3h8M2HSFbd8vxyd0G3xw==', 1)
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (10, 'dveale1', 't+3h8M2HSFbd8vxyd0G3xw==', 1)
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (11, 'htibbs1', 't+3h8M2HSFbd8vxyd0G3xw==', 1)
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (12, 'ebosson1', 't+3h8M2HSFbd8vxyd0G3xw==', 1)
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (13, 'cpoland1', 't+3h8M2HSFbd8vxyd0G3xw==', 1)
INSERT [dbo].[credential] ([userID], [username], [password], [is_active]) VALUES (14, 'divanyushin1', 't+3h8M2HSFbd8vxyd0G3xw==', 1)

INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1960-07-13', 'Kippy', 'Gritland', '2095050739', '8189 Service Road', NULL, '94203', 'F')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1976-10-05', 'Susana', 'Attenbrow', '2028322317', '662 Autumn Leaf Court', NULL, '48864', 'F')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1960-10-02', 'Bordy', 'Cestard', '7063636895', '331 Thackeray Trail', NULL, '30301', 'M')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1956-07-02', 'Ronica', 'Balentyne', '4804839835', '51 Fisk Court', NULL, '85001', 'F')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1960-01-05', 'Trenton', 'Blaycock', '2397526625', '0673 Fairfield Park', 'Apt 701a', '32301', 'M')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1965-06-08', 'Jilly', 'Skingle', '7142000413', '44638 Kenwood Circle', NULL, '94016', 'F')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1982-07-20', 'Cosme', 'Favelle', '4053954322', '7783 Village Green Parkway', NULL, '73008', 'M')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1975-03-19', 'Kai', 'Satterthwaite', '7143097365', '763 Fieldstone Center', NULL, '22434', 'M')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1990-03-03', 'Rosie', 'Heugel', '8599905723', '57 Surrey Trail', NULL, '40018', 'F')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1982-05-09', 'Lucas', 'Batte', '7139464786', '6832 7th Point', NULL, '73301', 'M')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1978-03-15', 'Harriett', 'Artrick', '5138136137', '9 Dunning Junction', NULL, '43004', 'F')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1983-07-14', 'Cornelius', 'Gusticke', '9789263346', '408 Loomis Road', 'Apt 503', '02101', 'M')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1962-07-06', 'Brigid', 'Drysdale', '9166582448', '839 Prairieview Road', NULL, '01760', 'F')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1983-12-23', 'Avie', 'Mandrey', '7133321756', '44349 Reindahl Point', NULL, '77001', 'F')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1960-09-21', 'Tallie', 'Rheam', '9417455386', '4 Prairieview Plaza', NULL, '32789', 'F')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1965-03-18', 'Porty', 'Dabinett', '9176980727', '04 Alpine Alley', 'Apt 212', '33101', 'M')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1987-04-07', 'Audrey', 'Stirland', '7131053203', '7714 Cody Way', NULL, '77351', 'F')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1973-02-09', 'Althea', 'Yurkov', '8167689917', '61147 Johnson Alley', NULL, '65043', 'M')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1988-11-18', 'Lynn', 'Tubble', '3042384626', '21491 Clemons Circle', NULL, '29401', 'F')
INSERT [dbo].[user] ([roleID], [date_of_birth], [first_name], [last_name], [phone_number], [address1], [address2], [zip], [sex]) VALUES (3, '1983-08-27', 'Land', 'Hallsworth', '8146053181', '2618 Daystar Pass', NULL, '17025', 'M')

INSERT [dbo].[category] ([category_name]) VALUES ('Bean Bag')
INSERT [dbo].[category] ([category_name]) VALUES ('Bed')
INSERT [dbo].[category] ([category_name]) VALUES ('Book Shelf')
INSERT [dbo].[category] ([category_name]) VALUES ('Chair')
INSERT [dbo].[category] ([category_name]) VALUES ('Chest')
INSERT [dbo].[category] ([category_name]) VALUES ('Coat Rack')
INSERT [dbo].[category] ([category_name]) VALUES ('Couch')
INSERT [dbo].[category] ([category_name]) VALUES ('Crib')
INSERT [dbo].[category] ([category_name]) VALUES ('Desk')
INSERT [dbo].[category] ([category_name]) VALUES ('Dining Table')
INSERT [dbo].[category] ([category_name]) VALUES ('Dresser')
INSERT [dbo].[category] ([category_name]) VALUES ('Futon')
INSERT [dbo].[category] ([category_name]) VALUES ('Hammock')
INSERT [dbo].[category] ([category_name]) VALUES ('Hat Stand')
INSERT [dbo].[category] ([category_name]) VALUES ('Hutch')
INSERT [dbo].[category] ([category_name]) VALUES ('Love Seat')
INSERT [dbo].[category] ([category_name]) VALUES ('Ottoman')
INSERT [dbo].[category] ([category_name]) VALUES ('Stool')
INSERT [dbo].[category] ([category_name]) VALUES ('Vanity')
INSERT [dbo].[category] ([category_name]) VALUES ('Wine Rack')
INSERT [dbo].[category] ([category_name]) VALUES ('Workbench')

INSERT [dbo].[style] ([style_type]) VALUES ('Bohemian')
INSERT [dbo].[style] ([style_type]) VALUES ('Coastal/Hamptons')
INSERT [dbo].[style] ([style_type]) VALUES ('Contemporary')
INSERT [dbo].[style] ([style_type]) VALUES ('French Country')
INSERT [dbo].[style] ([style_type]) VALUES ('Hollywood Glam')
INSERT [dbo].[style] ([style_type]) VALUES ('Industrial')
INSERT [dbo].[style] ([style_type]) VALUES ('Mid-Century Modern')
INSERT [dbo].[style] ([style_type]) VALUES ('Minimalist')
INSERT [dbo].[style] ([style_type]) VALUES ('Modern')
INSERT [dbo].[style] ([style_type]) VALUES ('Rustic')
INSERT [dbo].[style] ([style_type]) VALUES ('Scandanavian')
INSERT [dbo].[style] ([style_type]) VALUES ('Shabby Chic')
INSERT [dbo].[style] ([style_type]) VALUES ('Traditional')
INSERT [dbo].[style] ([style_type]) VALUES ('Transitional')

INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (5613, 'incubate strategic eyeballs', 19, 13, '2.67', '3.20', '20')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (11811, 'envisioneer wireless content', 5, 3, '2.90', '3.48', '20')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (12627, 'brand holistic functionalities', 1, 2, '4.74', '5.69', '17')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (18642, 'orchestrate one-to-one channels', 18, 14, '2.50', '3.00', '14')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (27449, 'optimize distributed networks', 13, 3, '3.95', '4.74', '5')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (29364, 'syndicate end-to-end infomediaries', 12, 6, '4.59', '5.51', '8')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (30806, 'leverage proactive markets', 1, 10, '2.82', '3.38', '17')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (32085, 'benchmark bleeding-edge portals', 19, 4, '2.12', '2.54', '20')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (34899, 'aggregate wireless bandwidth', 3, 2, '3.63', '4.36', '9')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (36270, 'disintermediate sexy ROI', 14, 3, '4.24', '5.09', '9')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (41435, 'unleash interactive infrastructures', 7, 6, '2.74', '3.29', '14')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (42295, 'leverage open-source infrastructures', 21, 7, '1.47', '1.76', '16')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (42940, 'facilitate dynamic methodologies', 11, 13, '4.93', '5.92', '9')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (42949, 'revolutionize out-of-the-box initiatives', 14, 8, '3.83', '4.60', '3')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (43459, 'seize e-business e-markets', 8, 12, '3.60', '4.32', '19')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (50116, 'harness scalable e-commerce', 15, 7, '3.06', '3.67', '5')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (50437, 'visualize strategic infrastructures', 12, 3, '3.35', '4.02', '9')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (55215, 'incentivize efficient synergies', 17, 2, '4.53', '5.44', '8')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (57538, 'drive scalable paradigms', 11, 13, '3.56', '4.27', '14')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (57591, 'syndicate robust relationships', 18, 9, '4.12', '4.94', '1')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (60005, 'brand real-time niches', 4, 5, '3.97', '4.76', '2')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (70182, 'exploit killer users', 17, 12, '3.39', '4.07', '3')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (76047, 'exploit strategic supply-chains', 1, 10, '3.87', '4.64', '14')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (83068, 'unleash bleeding-edge technologies', 7, 11, '2.89', '3.47', '11')
INSERT [dbo].[furniture_item] ([serial_number], [description], [categoryID], [styleID], [daily_rental_rate], [daily_fine_rate], [quantity]) VALUES (93498, 'harness clicks-and-mortar experiences', 15, 6, '3.10', '3.72', '1')

INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (30, 11, '2015-03-14', '2015-04-14');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (1, 55215, 3);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (6, '2015-04-09');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (1, 1, 3, '0.00', '0.00', '27.20');

INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (32, 14, '2015-03-24', '2015-04-24');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (2, 36270, 1);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (5, '2015-05-03');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (2, 2, 1, '45.81', '45.81', '0.00');

INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (29, 5, '2015-04-28', '2015-05-28');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (3, 42949, 3);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (7, '2015-05-24');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (3, 3, 3, '0.00', '0.00', '55.20');

INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (28, 5, '2015-07-26', '2015-08-26');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (4, 60005, 2);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (4, 70182, 3);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (8, 2015-08-18);
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (4, 4, 2, '0.00', '0.00', '76.16');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (5, 4, 3, '0.00', '0.00', '97.68');

INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (28, 14, '2015-10-15', '2015-11-15');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (5, 57538, 5);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (5, 42949, 3);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (10, '2015-11-15');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (6, 5, 5, '0.00', '0.00', '0.00');
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (10, '2015-11-20');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (7, 6, 3, '0.00', '0.00', '69.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (15, 6, '2015-12-10', '2016-01-10');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (6, 55215, 5);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (6, '2016-01-08');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (8, 7, 5, '0.00', '0.00', '54.40');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (29, 8, '2016-01-08', '2016-02-08');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (7, 29364, 3);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (7, 11811, 4);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (7, 60005, 2);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (9, '2016-01-29');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (9, 8, 3, '0.00', '0.00', '165.30');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (10, 8, 4, '0.00', '0.00', '139.20');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (11, 8, 2, '0.00', '0.00', '95.20');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (26, 8, '2016-03-07', '2016-04-07');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (8, 42940, 5);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (8, 36270, 3);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (14, '2016-04-01');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (12, 9, 5, '0.00', '0.00', '177.60');
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (9, '2016-04-07');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (13, 10, 3, '0.00', '0.00', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (28, 10, '2016-05-10', '2016-06-10');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (9, 42940, 1);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (10, '2016-06-09');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (14, 11, 1, '0.00', '0.00', '5.29');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (20, 14, '2016-05-21', '2016-06-21');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (10, 27449, 5);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (14, '2016-06-13');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (15, 12, 5, '0.00', '0.00', '189.60');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (32, 12, '2016-06-03', '2016-07-03');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (11, 36270, 4);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (9, '2016-07-05');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (16, 13, 4, '40.72', '40.72', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (26, 11, '2016-06-17', '2016-07-17');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (12, 18642, 1);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (12, 70182, 3);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (12, 93498, 1);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (13, '2016-07-07');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (17, 14, 1, '0.00', '0.00', '30.00');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (18, 14, 3, '0.00', '0.00', '122.10');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (19, 14, 1, '0.00', '0.00', '37.20');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (23, 9, '2016-08-12', '2016-09-12');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (13, 50116, 1);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (13, '2016-09-15');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (20, 15, 1, '11.01', '11.01', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (23, 12, '2016-09-10', '2016-10-10');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (14, 30806, 1);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (14, 57591, 1);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (11, '2016-10-10');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (21, 16, 1, '0.00', '0.00', '0.00');
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (13, '2016-10-20');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (22, 17, 1, '49.40', '49.40', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (26, 5, '2016-10-21', '2016-11-21');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (15, 18642, 4);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (15, 60005, 2);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (15, 5613, 5);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (13, '2016-11-19');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (23, 18, 4, '0.00', '0.00', '24.00');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (24, 18, 2, '0.00', '0.00', '19.04');
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (13, '2016-11-21');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (25, 19, 5, '0.00', '0.00', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (20, 5, '2017-02-06', '2017-03-06');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (16, 5613, 1);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (16, 41435, 4);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (10, '2017-03-03');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (26, 20, 1, '0.00', '0.00', '9.60');
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (11, '2017-03-06');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (27, 21, 4, '0.00', '0.00', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (33, 13, '2017-04-14', '2017-05-14');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (17, 41435, 2);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (6, '2017-05-24');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (28, 22, 2, '65.80', '65.80', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (33, 7, '2017-04-22', '2017-05-22');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (18, 50437, 4);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (18, 34899, 1);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (18, 42295, 1);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (11, '2017-05-12');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (29, 23, 4, '0.00', '0.00', '160.80');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (30, 23, 1, '0.00', '0.00', '43.60');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (31, 23, 1, '0.00', '0.00', '17.60');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (15, 8, '2017-04-24', '2017-05-24');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (19, 70182, 3);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (5, '2017-05-23');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (32, 24, 3, '0.00', '0.00', '12.21');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (25, 10, '2017-05-16', '2017-06-16');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (20, 43459, 3);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (20, 57538, 4);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (11, '2017-06-10');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (33, 25, 3, '0.00', '0.00', '77.76');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (34, 25, 4, '0.00', '0.00', '102.48');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (34, 9, '2017-07-01', '2017-06-01');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (21, 93498, 1);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (10, '2017-06-07');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (35, 26, 1, '22.32', '22.32', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (17, 13, '2017-07-03', '2017-08-03');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (22, 50437, 4);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (9, '2017-08-01');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (36, 27, 2, '0.00', '0.00', '16.08');
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (13, '2017-08-03');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (36, 28, 2, '0.00', '0.00', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (31, 10, '2017-07-22', '2017-08-22');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (23, 70182, 3);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (7, '2017-08-25');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (37, 29, 3, '36.63', '36.63', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (32, 12, '2017-08-26', '2017-09-26');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (24, 57591, 1);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (14, '2017-09-30');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (38, 30, 1, '19.76', '19.76', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (15, 14, '2017-12-03', '2018-01-03');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (24, 41435, 1);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (10, '2017-12-26');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (39, 31, 1, '0.00', '0.00', '26.32');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (33, 12, '2017-12-07', '2018-01-07');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (25, 60005, 1);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (25, 42940, 3);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (25, 34899, 1);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (12, '2017-12-28');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (40, 32, 1, '0.00', '0.00', '47.60');
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (12, '2018-01-07');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (41, 33, 3, '0.00', '0.00', '0.00');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (42, 33, 1, '0.00', '0.00', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (27, 6, '2017-12-10', '2018-01-10');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (26, 5613, 3);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (26, 30806, 3);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (13, '2018-01-01');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (43, 34, 3, '0.00', '0.00', '86.40');
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (6, '2018-01-10');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (44, 35, 3, '0.00', '0.00', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (23, 13, '2017-12-27', '2018-01-27');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (27, 29364, 1);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (27, 42949, 3);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (27, 42940, 1);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (13, '2018-01-26');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (45, 36, 1, '0.00', '0.00', '5.51');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (46, 36, 3, '0.00', '0.00', '13.80');
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (13, '2018-01-27');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (47, 37, 1, '0.00', '0.00', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (26, 14, '2018-02-08', '2018-03-08');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (28, 57538, 1);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (28, 18642, 2);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (5, '2018-03-14');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (48, 38, 1, '25.62', '25.62', '0.00');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (49, 38, 2, '36.00', '36.00', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (32, 12, '2018-02-23', '2018-03-23');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (29, 42940, 2);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (9, '2018-03-23');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (50, 39, 1, '0.00', '0.00', '0.00');
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (12, '2018-03-31');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (50, 40, 1, '47.36', '47.36', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (24, 13, '2018-04-27', '2018-05-27');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (30, 34899, 1);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (30, 18642, 4);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (30, 42940, 3);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (13, '2018-05-27');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (51, 41, 1, '0.00', '0.00', '0.00');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (52, 41, 4, '0.00', '0.00', '0.00');
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (13, '2018-05-29');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (53, 42, 3, '35.52', '35.52', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (33, 10, '2018-05-17', '2018-06-17');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (31, 42940, 2);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (10, '2018-06-18');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (54, 43, 2, '11.84', '11.84', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (34, 12, '2018-05-29', '2018-06-29');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (32, 5613, 5);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (13, '2018-06-29');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (55, 44, 5, '0.00', '0.00', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (23, 10, '2018-07-03', '2018-08-03');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (33, 30806, 1);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (33, 5613, 5);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (33, 50116, 3);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (10, '2018-08-03');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (56, 45, 1, '0.00', '0.00', '0.00');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (57, 45, 5, '0.00', '0.00', '0.00');
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (13, '2018-08-08');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (58, 46, 3, '55.05', '55.05', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (22, 14, '2018-08-12', '2018-09-12');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (34, 27449, 5);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (34, 41435, 3);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (9, '2018-09-05');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (59, 47, 5, '0.00', '0.00', '165.90');
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (9, '2018-09-12');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (60, 48, 3, '0.00', '0.00', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (15, 5, '2018-08-28', '2018-09-28');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (35, 60005, 2);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (6, '2018-09-28');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (61, 49, 2, '0.00', '0.00', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (29, 5, '2018-09-28', '2018-10-28');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (36, 42949, 3);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (36, 29364, 1);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (36, 11811, 5);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (5, '2018-10-23');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (62, 50, 3, '0.00', '0.00', '69.00');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (63, 50, 1, '0.00', '0.00', '27.55');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (64, 50, 5, '0.00', '0.00', '87.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (30, 9, '2018-10-01', '2018-11-01');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (37, 29364, 1);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (37, 50116, 5);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (5, '2018-10-22');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (65, 51, 1, '0.00', '0.00', '55.10');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (66, 51, 5, '0.00', '0.00', '183.50');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (34, 6, '2018-10-21', '2018-11-21');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (38, 34899, 5);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (14, '2018-11-30');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (67, 52, 5, '196.20', '196.20', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (28, 10, '2018-10-26', '2018-11-26');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (39, 11811, 1);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (12, '2018-12-05');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (68, 53, 1, '31.32', '31.32', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (23, 12, '2018-12-20', '2019-01-20');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (40, 30806, 1);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (5, '2019-01-28');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (69, 54, 1, '27.40', '27.40', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (20, 14, '2018-12-23', '2019-01-23');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (41, 29364, 1);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (41, 36270, 5);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (41, 41435, 1);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (7, '2019-01-17');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (70, 55, 1, '0.00', '0.00', '33.06');
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (14, '2019-01-23');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (71, 56, 5, '0.00', '0.00', '0.00');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (72, 56, 1, '0.00', '0.00', '0.00');

INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (31, 13, '2018-12-25', '2019-01-25');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (42, 83068, 1);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (7, '2019-01-15');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (73, 57, 1, '0.00', '0.00', '34.70');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (27, 7, '2018-12-30', '2019-01-30');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (43, 29364, 3);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (14, '2019-01-22');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (74, 58, 3, '0.00', '0.00', '132.24');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (20, 13, '2019-02-07', '2019-03-07');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (44, 41435, 1);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (5, '2019-03-08');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (75, 59, 1, '3.29', '3.29', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (21, 12, '2019-04-13', '2019-05-13');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (45, 50437, 5);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (45, 57591, 1);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (6, '2019-05-13');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (76, 60, 5, '0.00', '0.00', '0.00');
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (12, '2019-05-16');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (77, 61, 1, '14.82', '14.82', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (19, 10, '2019-04-22', '2019-05-22');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (46, 76047, 5);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (6, '2019-05-22');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (78, 62, 5, '0.00', '0.00', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (16, 6, '2019-05-04', '2019-06-04');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (47, 30806, 5);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (8, '2019-06-13');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (79, 63, 5, '152.10', '152.10', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (23, 14, '2019-07-19', '2019-08-19');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (48, 32085, 5);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (48, 76047, 5);
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (48, 36270, 4);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (8, '2019-08-14');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (80, 64, 5, '0.00', '0.00', '63.50');
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (8, '2019-08-19');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (81, 65, 5, '0.00', '0.00', '0.00');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (82, 65, 4, '0.00', '0.00', '0.00');


INSERT [dbo].[rental_transaction] ([member_userID], [employee_userID], [rental_date], [due_date]) VALUES (17, 10, '2019-11-13', '2019-12-13');
INSERT [dbo].[rental_item] ([rentalID], [serial_number], [quantity]) VALUES (49, 57538, 2);
INSERT [dbo].[return_transaction] ([employee_userID], [return_date]) VALUES (6, '2019-12-11');
INSERT [dbo].[return_item] ([itemID], [returnID], [quantity], [amount_due], [amount_paid], [amount_refunded]) VALUES (83, 66, 2, '0.00', '0.00', '17.08');