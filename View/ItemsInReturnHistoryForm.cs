﻿using System;

using System.Windows.Forms;

namespace RentMe_G4.View
{
    /// <summary>
    /// Allows to create the ItemsInReturnHistoryForm
    /// </summary>
    public partial class ItemsInReturnHistoryForm : Form
    {

        /// <summary>
        /// Initializes the ItemsInReturnHistoryForm
        /// </summary>
        public ItemsInReturnHistoryForm()
        {
            InitializeComponent();
        }

        private void PopulateTheDataGridView()
        {
            try
            {
                returnedIDtxtbox.Text = this.Tag.ToString();
                this.return_itemTableAdapter.GetItemsByReturnID(this._cs6232_g4DataSet.return_item, ((int)(System.Convert.ChangeType(this.Tag.ToString(), typeof(int)))));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }

        private void ItemsInReturnHistoryForm_Load(object sender, EventArgs e)
        {
            this.PopulateTheDataGridView();
        }

        private void Okbtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void Return_itemDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 6)
                {
                    int rowIndex = e.RowIndex;
                    DataGridViewRow row = return_itemDataGridView.Rows[rowIndex];
                    DataGridViewCell cell = row.Cells[0];
                    int itemID = (int)cell.Value;
                    using (ItemDetailsForm itemDetails = new ItemDetailsForm())
                    {
                        itemDetails.Tag = itemID;
                        itemDetails.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }

        private void Return_itemDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            int row = e.RowIndex + 1;
            string errorMessage = "A data error occurred.\n" +
                "Row: " + row + "\n" +
                "Error: " + e.Exception.Message;
            MessageBox.Show(errorMessage, "Data Error");
        }
    }
}
