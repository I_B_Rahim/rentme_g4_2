﻿namespace RentMe_G4.UserControls
{
    partial class MemberRentalsUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label memberIDLabel;
            System.Windows.Forms.Label first_nameLabel;
            this.rental_transactionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._cs6232_g4DataSet = new RentMe_G4._cs6232_g4DataSet();
            this.rental_transactionDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ViewRentalItemsButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.makeReturnBtn = new System.Windows.Forms.Button();
            this.allItemsListView = new System.Windows.Forms.ListView();
            this.itemsSelectedLabel = new System.Windows.Forms.Label();
            this.memberErrorlabel = new System.Windows.Forms.Label();
            this.rentalsLabel = new System.Windows.Forms.Label();
            this.memberInfo = new System.Windows.Forms.GroupBox();
            this.last_nameTextBox = new System.Windows.Forms.TextBox();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.first_nameTextBox = new System.Windows.Forms.TextBox();
            this.memberIDTextBox = new System.Windows.Forms.TextBox();
            this.rental_transactionTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.rental_transactionTableAdapter();
            this.tableAdapterManager = new RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager();
            this.userTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.userTableAdapter();
            this.clearListBtn = new System.Windows.Forms.Button();
            this.removeItemBtn = new System.Windows.Forms.Button();
            memberIDLabel = new System.Windows.Forms.Label();
            first_nameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.rental_transactionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rental_transactionDataGridView)).BeginInit();
            this.memberInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // memberIDLabel
            // 
            memberIDLabel.AutoSize = true;
            memberIDLabel.Location = new System.Drawing.Point(6, 26);
            memberIDLabel.Name = "memberIDLabel";
            memberIDLabel.Size = new System.Drawing.Size(21, 13);
            memberIDLabel.TabIndex = 0;
            memberIDLabel.Text = "ID:";
            // 
            // first_nameLabel
            // 
            first_nameLabel.AutoSize = true;
            first_nameLabel.Location = new System.Drawing.Point(6, 56);
            first_nameLabel.Name = "first_nameLabel";
            first_nameLabel.Size = new System.Drawing.Size(38, 13);
            first_nameLabel.TabIndex = 2;
            first_nameLabel.Text = "Name:";
            // 
            // rental_transactionBindingSource
            // 
            this.rental_transactionBindingSource.DataMember = "rental_transaction";
            this.rental_transactionBindingSource.DataSource = this._cs6232_g4DataSet;
            // 
            // _cs6232_g4DataSet
            // 
            this._cs6232_g4DataSet.DataSetName = "_cs6232_g4DataSet";
            this._cs6232_g4DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rental_transactionDataGridView
            // 
            this.rental_transactionDataGridView.AllowUserToAddRows = false;
            this.rental_transactionDataGridView.AllowUserToDeleteRows = false;
            this.rental_transactionDataGridView.AutoGenerateColumns = false;
            this.rental_transactionDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rental_transactionDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.ViewRentalItemsButton});
            this.rental_transactionDataGridView.DataSource = this.rental_transactionBindingSource;
            this.rental_transactionDataGridView.Location = new System.Drawing.Point(6, 127);
            this.rental_transactionDataGridView.Name = "rental_transactionDataGridView";
            this.rental_transactionDataGridView.ReadOnly = true;
            this.rental_transactionDataGridView.RowHeadersVisible = false;
            this.rental_transactionDataGridView.Size = new System.Drawing.Size(620, 154);
            this.rental_transactionDataGridView.TabIndex = 1;
            this.rental_transactionDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Rental_transactionDataGridView_CellContentClick);
            this.rental_transactionDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.Rental_transactionDataGridView_DataError);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "rentalID";
            this.dataGridViewTextBoxColumn1.HeaderText = "Rental ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "member_userID";
            this.dataGridViewTextBoxColumn2.HeaderText = "Member ID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "employee_userID";
            this.dataGridViewTextBoxColumn3.HeaderText = "Employee ID";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "rental_date";
            this.dataGridViewTextBoxColumn4.HeaderText = "Rental Date";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "due_date";
            this.dataGridViewTextBoxColumn5.HeaderText = "Due Date";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // ViewRentalItemsButton
            // 
            this.ViewRentalItemsButton.HeaderText = "";
            this.ViewRentalItemsButton.Name = "ViewRentalItemsButton";
            this.ViewRentalItemsButton.ReadOnly = true;
            this.ViewRentalItemsButton.Text = "View Rental Items";
            this.ViewRentalItemsButton.UseColumnTextForButtonValue = true;
            // 
            // makeReturnBtn
            // 
            this.makeReturnBtn.Location = new System.Drawing.Point(6, 287);
            this.makeReturnBtn.Name = "makeReturnBtn";
            this.makeReturnBtn.Size = new System.Drawing.Size(77, 23);
            this.makeReturnBtn.TabIndex = 2;
            this.makeReturnBtn.Text = "Make Return";
            this.makeReturnBtn.UseVisualStyleBackColor = true;
            this.makeReturnBtn.Click += new System.EventHandler(this.MakeReturnBtn_Click);
            // 
            // allItemsListView
            // 
            this.allItemsListView.HideSelection = false;
            this.allItemsListView.Location = new System.Drawing.Point(673, 127);
            this.allItemsListView.Name = "allItemsListView";
            this.allItemsListView.Size = new System.Drawing.Size(87, 154);
            this.allItemsListView.TabIndex = 3;
            this.allItemsListView.Tag = "allItemsListView";
            this.allItemsListView.UseCompatibleStateImageBehavior = false;
            this.allItemsListView.View = System.Windows.Forms.View.List;
            // 
            // itemsSelectedLabel
            // 
            this.itemsSelectedLabel.AutoSize = true;
            this.itemsSelectedLabel.Location = new System.Drawing.Point(641, 111);
            this.itemsSelectedLabel.Name = "itemsSelectedLabel";
            this.itemsSelectedLabel.Size = new System.Drawing.Size(125, 13);
            this.itemsSelectedLabel.TabIndex = 4;
            this.itemsSelectedLabel.Text = "ID\'s of all items Selected:";
            // 
            // memberErrorlabel
            // 
            this.memberErrorlabel.AutoSize = true;
            this.memberErrorlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memberErrorlabel.Location = new System.Drawing.Point(584, 33);
            this.memberErrorlabel.Name = "memberErrorlabel";
            this.memberErrorlabel.Size = new System.Drawing.Size(179, 22);
            this.memberErrorlabel.TabIndex = 5;
            this.memberErrorlabel.Text = "No member selected!";
            this.memberErrorlabel.Visible = false;
            // 
            // rentalsLabel
            // 
            this.rentalsLabel.AutoSize = true;
            this.rentalsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rentalsLabel.Location = new System.Drawing.Point(291, 108);
            this.rentalsLabel.Name = "rentalsLabel";
            this.rentalsLabel.Size = new System.Drawing.Size(50, 13);
            this.rentalsLabel.TabIndex = 6;
            this.rentalsLabel.Text = "Rentals";
            // 
            // memberInfo
            // 
            this.memberInfo.Controls.Add(this.last_nameTextBox);
            this.memberInfo.Controls.Add(first_nameLabel);
            this.memberInfo.Controls.Add(this.first_nameTextBox);
            this.memberInfo.Controls.Add(memberIDLabel);
            this.memberInfo.Controls.Add(this.memberIDTextBox);
            this.memberInfo.Location = new System.Drawing.Point(6, 12);
            this.memberInfo.Name = "memberInfo";
            this.memberInfo.Size = new System.Drawing.Size(222, 93);
            this.memberInfo.TabIndex = 7;
            this.memberInfo.TabStop = false;
            this.memberInfo.Text = "Member Info.";
            // 
            // last_nameTextBox
            // 
            this.last_nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "last_name", true));
            this.last_nameTextBox.Location = new System.Drawing.Point(125, 53);
            this.last_nameTextBox.Name = "last_nameTextBox";
            this.last_nameTextBox.ReadOnly = true;
            this.last_nameTextBox.Size = new System.Drawing.Size(77, 20);
            this.last_nameTextBox.TabIndex = 9;
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataMember = "user";
            this.userBindingSource.DataSource = this._cs6232_g4DataSet;
            // 
            // first_nameTextBox
            // 
            this.first_nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "first_name", true));
            this.first_nameTextBox.Location = new System.Drawing.Point(53, 53);
            this.first_nameTextBox.Name = "first_nameTextBox";
            this.first_nameTextBox.ReadOnly = true;
            this.first_nameTextBox.Size = new System.Drawing.Size(60, 20);
            this.first_nameTextBox.TabIndex = 3;
            // 
            // memberIDTextBox
            // 
            this.memberIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "userID", true));
            this.memberIDTextBox.Location = new System.Drawing.Point(53, 23);
            this.memberIDTextBox.Name = "memberIDTextBox";
            this.memberIDTextBox.ReadOnly = true;
            this.memberIDTextBox.Size = new System.Drawing.Size(35, 20);
            this.memberIDTextBox.TabIndex = 1;
            // 
            // rental_transactionTableAdapter
            // 
            this.rental_transactionTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.categoryTableAdapter = null;
            this.tableAdapterManager.furniture_itemTableAdapter = null;
            this.tableAdapterManager.locationTableAdapter = null;
            this.tableAdapterManager.rental_itemTableAdapter = null;
            this.tableAdapterManager.rental_transactionTableAdapter = this.rental_transactionTableAdapter;
            this.tableAdapterManager.return_itemTableAdapter = null;
            this.tableAdapterManager.return_transactionTableAdapter = null;
            this.tableAdapterManager.roleTableAdapter = null;
            this.tableAdapterManager.stateTableAdapter = null;
            this.tableAdapterManager.styleTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.userTableAdapter = null;
            // 
            // userTableAdapter
            // 
            this.userTableAdapter.ClearBeforeFill = true;
            // 
            // clearListBtn
            // 
            this.clearListBtn.Location = new System.Drawing.Point(626, 287);
            this.clearListBtn.Name = "clearListBtn";
            this.clearListBtn.Size = new System.Drawing.Size(41, 23);
            this.clearListBtn.TabIndex = 10;
            this.clearListBtn.Text = "Clear ";
            this.clearListBtn.UseVisualStyleBackColor = true;
            this.clearListBtn.Click += new System.EventHandler(this.ClearListBtn_Click);
            // 
            // removeItemBtn
            // 
            this.removeItemBtn.Location = new System.Drawing.Point(673, 287);
            this.removeItemBtn.Name = "removeItemBtn";
            this.removeItemBtn.Size = new System.Drawing.Size(87, 23);
            this.removeItemBtn.TabIndex = 11;
            this.removeItemBtn.Text = "Remove Item";
            this.removeItemBtn.UseVisualStyleBackColor = true;
            this.removeItemBtn.Click += new System.EventHandler(this.RemoveItemBtn_Click);
            // 
            // MemberRentalsUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.removeItemBtn);
            this.Controls.Add(this.clearListBtn);
            this.Controls.Add(this.memberInfo);
            this.Controls.Add(this.rentalsLabel);
            this.Controls.Add(this.memberErrorlabel);
            this.Controls.Add(this.itemsSelectedLabel);
            this.Controls.Add(this.allItemsListView);
            this.Controls.Add(this.makeReturnBtn);
            this.Controls.Add(this.rental_transactionDataGridView);
            this.Name = "MemberRentalsUserControl";
            this.Size = new System.Drawing.Size(766, 320);
            this.Load += new System.EventHandler(this.MemberRentalsUserControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rental_transactionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rental_transactionDataGridView)).EndInit();
            this.memberInfo.ResumeLayout(false);
            this.memberInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private _cs6232_g4DataSet _cs6232_g4DataSet;
        private System.Windows.Forms.BindingSource rental_transactionBindingSource;
        private _cs6232_g4DataSetTableAdapters.rental_transactionTableAdapter rental_transactionTableAdapter;
        private _cs6232_g4DataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView rental_transactionDataGridView;
        private System.Windows.Forms.Button makeReturnBtn;
        private System.Windows.Forms.ListView allItemsListView;
        private System.Windows.Forms.Label itemsSelectedLabel;
        private System.Windows.Forms.Label memberErrorlabel;
        private System.Windows.Forms.Label rentalsLabel;
        private System.Windows.Forms.GroupBox memberInfo;
        private System.Windows.Forms.TextBox first_nameTextBox;
        private System.Windows.Forms.BindingSource userBindingSource;
        private System.Windows.Forms.TextBox memberIDTextBox;
        private _cs6232_g4DataSetTableAdapters.userTableAdapter userTableAdapter;
        private System.Windows.Forms.TextBox last_nameTextBox;
        private System.Windows.Forms.Button clearListBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewButtonColumn ViewRentalItemsButton;
        private System.Windows.Forms.Button removeItemBtn;
    }
}
