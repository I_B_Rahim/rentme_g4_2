﻿using RentMe_G4.Controller;
using System;

using System.Windows.Forms;

namespace RentMe_G4.View
{
    /// <summary>
    /// Allows to create the ItemDetailsForm
    /// </summary>
    public partial class ItemDetailsForm : Form
    {
        /// <summary>
        /// Initializes the ItemDetailsForm
        /// </summary>
        public ItemDetailsForm()
        {
            InitializeComponent();
        }


        private void ItemDetailsForm_Load(object sender, EventArgs e)
        {
            try
            {

                int serialNumber = UserController.GetSerialNumberByItemID(int.Parse(this.Tag.ToString()));
                this.furniture_itemTableAdapter.FillBySerialNumber(this._cs6232_g4DataSet.furniture_item, serialNumber);
                this.styleTableAdapter.FillByStyleID(this._cs6232_g4DataSet.style, ((int)(System.Convert.ChangeType(styleIDTextBox.Text, typeof(int)))));
                this.categoryTableAdapter.FillByCategoryID(this._cs6232_g4DataSet.category, ((int)(System.Convert.ChangeType(categoryIDTextBox.Text, typeof(int)))));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }

        private void Okbtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
