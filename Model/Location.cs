﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentMe_G4.Model
{
    /// <summary>
    /// This class models a location
    /// </summary>
    public class Location
    {
        private string zip;
        private string city;
        private string state;

        /// <summary>
        /// Accessor and modifier for zip
        /// </summary>
        public string Zip
        {
            get
            {
                return this.zip;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("zip cannot be null or empty", "zip error");
                }
                this.zip = value;
            }
        }

        /// <summary>
        /// Accessor and modifier for city
        /// </summary>
        public string City
        {
            get
            {
                return this.city;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("city cannot be null or empty", "city error");
                }
                this.city = value;
            }
        }

        /// <summary>
        /// Accessor and modifier for state
        /// </summary>
        public string State
        {
            get
            {
                return this.state;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("state cannot be null or empty", "state error");
                }
                this.state = value;
            }
        }
    }
}
