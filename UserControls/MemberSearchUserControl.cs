﻿using RentMe_G4.Model;
using RentMe_G4.Controller;
using RentMe_G4.View;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace RentMe_G4.UserControls
{
    /// <summary>
    /// This is class stores the methods that will be used to display the search feature and the options to edit and add members 
    /// </summary>
    public partial class MemberSearchUserControl : UserControl
    {
        private readonly UserController userController;
        private List<User> ListOfMembers;
        public string memberSearchMemberID;

        /// <summary>
        /// This method initializes the user control component and the instance variable userController
        /// </summary>
        public MemberSearchUserControl()
        {
            this.userController = new UserController();

            InitializeComponent();
        }

        private void MemberSearchUserControl_Load(object sender, EventArgs e)
        {
            this.SetUpDataGridView();
            editUserButton.Enabled = false;
        }

        /// <summary>
        /// Sets up the DataGridView
        /// </summary>
        private void SetUpDataGridView()
        {
            try
            {
                this.ListOfMembers = userController.GetListOfMembers();
                membersDataGridView.DataSource = ListOfMembers;
                membersDataGridView.Columns["City"].Visible = false;
                membersDataGridView.Columns["State"].Visible = false;
                membersDataGridView.Columns["Username"].Visible = false;
                membersDataGridView.Columns["Status"].Visible = false;
                membersDataGridView.Columns["Password"].Visible = false;
                membersDataGridView.Columns["RoleID"].Visible = false;
                membersDataGridView.Columns["DateOfBirth"].Visible = false;
                membersDataGridView.Columns["address1"].Visible = false;
                membersDataGridView.Columns["address2"].Visible = false;
                membersDataGridView.Columns["zip"].Visible = false;
                membersDataGridView.Columns["LastName"].Width = 120;
                //membersDataGridView.Columns["sex"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }

        private void ClearSearchFields()
        {
            userIDToolStripTextBox.Text = "";
            phone_numberToolStripTextBox.Text = "";
            nameToolStripTextBox.Text = "";
        }

        /// <summary>
        /// Checks if the userIdTextBox is blank, then the appropriate message is sent
        /// </summary>
        private void NoUserFound()
        {
            try
            {
                if (string.IsNullOrEmpty(userIDTextBox.Text))
                {
                    MessageBox.Show("No member was found for the search criteria", "No member Found");
                    editUserButton.Enabled = false;
                } else
                {
                    editUserButton.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }

        private void FillByUserToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.userTableAdapter.FillBy(this._cs6232_g4DataSet.user, ((int)(System.Convert.ChangeType(userIDToolStripTextBox.Text, typeof(int)))));
                this.NoUserFound();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }

        private void FillByPhoneToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.userTableAdapter.FillByPhone(this._cs6232_g4DataSet.user, phone_numberToolStripTextBox.Text);
                this.NoUserFound();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }

        private void FillByNameToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.userTableAdapter.FillByName(this._cs6232_g4DataSet.user, nameToolStripTextBox.Text);
                this.NoUserFound();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }

        private void EditUserbutton_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(userIDTextBox.Text))
                {
                    MessageBox.Show("Please find a user using the search criteria before editing.", "User Search Error");
                }
                else
                {
                    using (MemberForm editMemberForm = new MemberForm())
                    {
                        editMemberForm.addMember = false;
                        editMemberForm.userID = int.Parse(userIDTextBox.Text);
                        DialogResult result = editMemberForm.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            MessageBox.Show("The Member With ID: " + userIDTextBox.Text + ", Was Successfully Updated.", "Member Updated");
                            int userID = Convert.ToInt32(editMemberForm.userID);
                            this.userTableAdapter.FillByUserID(this._cs6232_g4DataSet.user, userID);
                            this.SetUpDataGridView();
                        }
                        else
                        {
                            MessageBox.Show("The member was not Updated.", "Update Canceled");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }


        }

        private void AddUserButton_Click(object sender, EventArgs e)
        {
            try
            {
                MemberForm addMemberForm = new MemberForm();
                addMemberForm.addMember = true;
                DialogResult result = addMemberForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    int newUserID = userController.GetUserIDByPhone(addMemberForm.userPhone);
                    MessageBox.Show("The Member With ID: " + newUserID + ", Was Successfully Created.", "Member Created");

                    this.userTableAdapter.FillByPhone(this._cs6232_g4DataSet.user, addMemberForm.userPhone);
                    this.SetUpDataGridView();
                    editUserButton.Enabled = true;
                }
                else
                {
                    MessageBox.Show("The member was not Added.", "Registration Canceled");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void ClearSearchFields(object sender, EventArgs e)
        {
            this.ClearSearchFields();
        }

        private void UserIDTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(userIDTextBox.Text))
                {
                    memberSearchMemberID = null;
                }
                else
                {
                    memberSearchMemberID = userIDTextBox.Text;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }
    }
}
