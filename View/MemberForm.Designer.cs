﻿namespace RentMe_G4.View
{
    partial class MemberForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label phone_numberLabel;
            System.Windows.Forms.Label address1Label;
            System.Windows.Forms.Label zipLabel;
            System.Windows.Forms.Label sexLabel;
            System.Windows.Forms.Label last_nameLabel;
            System.Windows.Forms.Label first_nameLabel;
            System.Windows.Forms.Label userIDLabel;
            System.Windows.Forms.Label roleIDLabel;
            System.Windows.Forms.Label date_of_birthLabel1;
            System.Windows.Forms.Label cityLabel;
            System.Windows.Forms.Label stateLabel;
            System.Windows.Forms.Label label2;
            this.first_nameTextBox = new System.Windows.Forms.TextBox();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._cs6232_g4DataSet = new RentMe_G4._cs6232_g4DataSet();
            this.last_nameTextBox = new System.Windows.Forms.TextBox();
            this.phone_numberTextBox = new System.Windows.Forms.TextBox();
            this.address1TextBox = new System.Windows.Forms.TextBox();
            this.address2TextBox = new System.Windows.Forms.TextBox();
            this.zipTextBox = new System.Windows.Forms.TextBox();
            this.acceptMemberFormButton = new System.Windows.Forms.Button();
            this.cancelMemberFormButton = new System.Windows.Forms.Button();
            this.sexTextBox = new System.Windows.Forms.TextBox();
            this.memberFormHeading = new System.Windows.Forms.Label();
            this.contactInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.zipLocationLabel = new System.Windows.Forms.Label();
            this.locationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.stateComboBox = new System.Windows.Forms.ComboBox();
            this.stateBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._cs6232_g4DataSet1 = new RentMe_G4._cs6232_g4DataSet();
            this.cityTextBox = new System.Windows.Forms.TextBox();
            this.phoneFormatLabel = new System.Windows.Forms.Label();
            this.userIDTextBox = new System.Windows.Forms.TextBox();
            this.roleIDTextBox = new System.Windows.Forms.TextBox();
            this.dateOfBirthFormat = new System.Windows.Forms.Label();
            this.type = new System.Windows.Forms.Label();
            this.typetxtbox = new System.Windows.Forms.TextBox();
            this.sexComboBox = new System.Windows.Forms.ComboBox();
            this.date_of_birthDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.userTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.userTableAdapter();
            this.tableAdapterManager = new RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager();
            this.locationTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.locationTableAdapter();
            this.stateTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.stateTableAdapter();
            phone_numberLabel = new System.Windows.Forms.Label();
            address1Label = new System.Windows.Forms.Label();
            zipLabel = new System.Windows.Forms.Label();
            sexLabel = new System.Windows.Forms.Label();
            last_nameLabel = new System.Windows.Forms.Label();
            first_nameLabel = new System.Windows.Forms.Label();
            userIDLabel = new System.Windows.Forms.Label();
            roleIDLabel = new System.Windows.Forms.Label();
            date_of_birthLabel1 = new System.Windows.Forms.Label();
            cityLabel = new System.Windows.Forms.Label();
            stateLabel = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).BeginInit();
            this.contactInfoGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.locationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // phone_numberLabel
            // 
            phone_numberLabel.AutoSize = true;
            phone_numberLabel.Location = new System.Drawing.Point(9, 22);
            phone_numberLabel.Name = "phone_numberLabel";
            phone_numberLabel.Size = new System.Drawing.Size(51, 13);
            phone_numberLabel.TabIndex = 11;
            phone_numberLabel.Text = "Phone #:";
            // 
            // address1Label
            // 
            address1Label.AutoSize = true;
            address1Label.Location = new System.Drawing.Point(9, 48);
            address1Label.Name = "address1Label";
            address1Label.Size = new System.Drawing.Size(38, 13);
            address1Label.TabIndex = 13;
            address1Label.Text = "Street:";
            // 
            // zipLabel
            // 
            zipLabel.AutoSize = true;
            zipLabel.Location = new System.Drawing.Point(9, 103);
            zipLabel.Name = "zipLabel";
            zipLabel.Size = new System.Drawing.Size(25, 13);
            zipLabel.TabIndex = 17;
            zipLabel.Text = "Zip:";
            // 
            // sexLabel
            // 
            sexLabel.AutoSize = true;
            sexLabel.Location = new System.Drawing.Point(360, 169);
            sexLabel.Name = "sexLabel";
            sexLabel.Size = new System.Drawing.Size(28, 13);
            sexLabel.TabIndex = 26;
            sexLabel.Text = "Sex:";
            // 
            // last_nameLabel
            // 
            last_nameLabel.AutoSize = true;
            last_nameLabel.Location = new System.Drawing.Point(32, 136);
            last_nameLabel.Name = "last_nameLabel";
            last_nameLabel.Size = new System.Drawing.Size(61, 13);
            last_nameLabel.TabIndex = 9;
            last_nameLabel.Text = "Last Name:";
            // 
            // first_nameLabel
            // 
            first_nameLabel.AutoSize = true;
            first_nameLabel.Location = new System.Drawing.Point(32, 110);
            first_nameLabel.Name = "first_nameLabel";
            first_nameLabel.Size = new System.Drawing.Size(60, 13);
            first_nameLabel.TabIndex = 7;
            first_nameLabel.Text = "First Name:";
            // 
            // userIDLabel
            // 
            userIDLabel.AutoSize = true;
            userIDLabel.Location = new System.Drawing.Point(33, 56);
            userIDLabel.Name = "userIDLabel";
            userIDLabel.Size = new System.Drawing.Size(46, 13);
            userIDLabel.TabIndex = 31;
            userIDLabel.Text = "User ID:";
            // 
            // roleIDLabel
            // 
            roleIDLabel.AutoSize = true;
            roleIDLabel.Location = new System.Drawing.Point(381, 52);
            roleIDLabel.Name = "roleIDLabel";
            roleIDLabel.Size = new System.Drawing.Size(46, 13);
            roleIDLabel.TabIndex = 33;
            roleIDLabel.Text = "Role ID:";
            // 
            // date_of_birthLabel1
            // 
            date_of_birthLabel1.AutoSize = true;
            date_of_birthLabel1.Location = new System.Drawing.Point(31, 170);
            date_of_birthLabel1.Name = "date_of_birthLabel1";
            date_of_birthLabel1.Size = new System.Drawing.Size(71, 13);
            date_of_birthLabel1.TabIndex = 42;
            date_of_birthLabel1.Text = "Date Of Birth:";
            // 
            // cityLabel
            // 
            cityLabel.AutoSize = true;
            cityLabel.Location = new System.Drawing.Point(151, 103);
            cityLabel.Name = "cityLabel";
            cityLabel.Size = new System.Drawing.Size(26, 13);
            cityLabel.TabIndex = 45;
            cityLabel.Text = "city:";
            // 
            // stateLabel
            // 
            stateLabel.AutoSize = true;
            stateLabel.Location = new System.Drawing.Point(305, 103);
            stateLabel.Name = "stateLabel";
            stateLabel.Size = new System.Drawing.Size(33, 13);
            stateLabel.TabIndex = 46;
            stateLabel.Text = "state:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(9, 74);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(55, 13);
            label2.TabIndex = 50;
            label2.Text = "Apt/Suite:";
            // 
            // first_nameTextBox
            // 
            this.first_nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "first_name", true));
            this.first_nameTextBox.Location = new System.Drawing.Point(116, 107);
            this.first_nameTextBox.Name = "first_nameTextBox";
            this.first_nameTextBox.Size = new System.Drawing.Size(375, 20);
            this.first_nameTextBox.TabIndex = 1;
            this.first_nameTextBox.Tag = "First Name";
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataMember = "user";
            this.userBindingSource.DataSource = this._cs6232_g4DataSet;
            // 
            // _cs6232_g4DataSet
            // 
            this._cs6232_g4DataSet.DataSetName = "_cs6232_g4DataSet";
            this._cs6232_g4DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // last_nameTextBox
            // 
            this.last_nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "last_name", true));
            this.last_nameTextBox.Location = new System.Drawing.Point(116, 133);
            this.last_nameTextBox.Name = "last_nameTextBox";
            this.last_nameTextBox.Size = new System.Drawing.Size(375, 20);
            this.last_nameTextBox.TabIndex = 2;
            this.last_nameTextBox.Tag = "Last Name";
            // 
            // phone_numberTextBox
            // 
            this.phone_numberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "phone_number", true));
            this.phone_numberTextBox.Location = new System.Drawing.Point(93, 19);
            this.phone_numberTextBox.Name = "phone_numberTextBox";
            this.phone_numberTextBox.Size = new System.Drawing.Size(95, 20);
            this.phone_numberTextBox.TabIndex = 5;
            this.phone_numberTextBox.Tag = "Phone Number";
            // 
            // address1TextBox
            // 
            this.address1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "address1", true));
            this.address1TextBox.Location = new System.Drawing.Point(93, 45);
            this.address1TextBox.Name = "address1TextBox";
            this.address1TextBox.Size = new System.Drawing.Size(357, 20);
            this.address1TextBox.TabIndex = 6;
            this.address1TextBox.Tag = "Address 1";
            // 
            // address2TextBox
            // 
            this.address2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "address2", true));
            this.address2TextBox.Location = new System.Drawing.Point(93, 71);
            this.address2TextBox.Name = "address2TextBox";
            this.address2TextBox.Size = new System.Drawing.Size(357, 20);
            this.address2TextBox.TabIndex = 7;
            this.address2TextBox.Tag = "Address 2";
            // 
            // zipTextBox
            // 
            this.zipTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "zip", true));
            this.zipTextBox.Location = new System.Drawing.Point(93, 100);
            this.zipTextBox.Name = "zipTextBox";
            this.zipTextBox.Size = new System.Drawing.Size(42, 20);
            this.zipTextBox.TabIndex = 8;
            this.zipTextBox.Tag = "Zipcode";
            this.zipTextBox.TextChanged += new System.EventHandler(this.ZipTextBox_TextChanged);
            // 
            // acceptMemberFormButton
            // 
            this.acceptMemberFormButton.Location = new System.Drawing.Point(73, 401);
            this.acceptMemberFormButton.Name = "acceptMemberFormButton";
            this.acceptMemberFormButton.Size = new System.Drawing.Size(75, 23);
            this.acceptMemberFormButton.TabIndex = 10;
            this.acceptMemberFormButton.Text = "Accept";
            this.acceptMemberFormButton.UseVisualStyleBackColor = true;
            this.acceptMemberFormButton.Click += new System.EventHandler(this.AcceptMemberFormButton_Click);
            // 
            // cancelMemberFormButton
            // 
            this.cancelMemberFormButton.Location = new System.Drawing.Point(394, 401);
            this.cancelMemberFormButton.Name = "cancelMemberFormButton";
            this.cancelMemberFormButton.Size = new System.Drawing.Size(75, 23);
            this.cancelMemberFormButton.TabIndex = 11;
            this.cancelMemberFormButton.Text = "Cancel";
            this.cancelMemberFormButton.UseVisualStyleBackColor = true;
            this.cancelMemberFormButton.Click += new System.EventHandler(this.CancelMemberFormButton_Click);
            // 
            // sexTextBox
            // 
            this.sexTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "sex", true));
            this.sexTextBox.Location = new System.Drawing.Point(470, 166);
            this.sexTextBox.Name = "sexTextBox";
            this.sexTextBox.ReadOnly = true;
            this.sexTextBox.Size = new System.Drawing.Size(25, 20);
            this.sexTextBox.TabIndex = 4;
            this.sexTextBox.Tag = "Sex";
            // 
            // memberFormHeading
            // 
            this.memberFormHeading.AutoSize = true;
            this.memberFormHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memberFormHeading.Location = new System.Drawing.Point(180, 9);
            this.memberFormHeading.Name = "memberFormHeading";
            this.memberFormHeading.Size = new System.Drawing.Size(178, 18);
            this.memberFormHeading.TabIndex = 28;
            this.memberFormHeading.Text = "Customer Registration";
            // 
            // contactInfoGroupBox
            // 
            this.contactInfoGroupBox.Controls.Add(this.zipLocationLabel);
            this.contactInfoGroupBox.Controls.Add(label2);
            this.contactInfoGroupBox.Controls.Add(this.label1);
            this.contactInfoGroupBox.Controls.Add(stateLabel);
            this.contactInfoGroupBox.Controls.Add(this.stateComboBox);
            this.contactInfoGroupBox.Controls.Add(cityLabel);
            this.contactInfoGroupBox.Controls.Add(this.cityTextBox);
            this.contactInfoGroupBox.Controls.Add(this.phoneFormatLabel);
            this.contactInfoGroupBox.Controls.Add(this.phone_numberTextBox);
            this.contactInfoGroupBox.Controls.Add(this.zipTextBox);
            this.contactInfoGroupBox.Controls.Add(this.address2TextBox);
            this.contactInfoGroupBox.Controls.Add(this.address1TextBox);
            this.contactInfoGroupBox.Controls.Add(address1Label);
            this.contactInfoGroupBox.Controls.Add(phone_numberLabel);
            this.contactInfoGroupBox.Controls.Add(zipLabel);
            this.contactInfoGroupBox.Location = new System.Drawing.Point(35, 225);
            this.contactInfoGroupBox.Name = "contactInfoGroupBox";
            this.contactInfoGroupBox.Size = new System.Drawing.Size(481, 170);
            this.contactInfoGroupBox.TabIndex = 9;
            this.contactInfoGroupBox.TabStop = false;
            this.contactInfoGroupBox.Text = "Contact Info.";
            // 
            // zipLocationLabel
            // 
            this.zipLocationLabel.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.locationBindingSource, "zip", true));
            this.zipLocationLabel.Location = new System.Drawing.Point(127, 136);
            this.zipLocationLabel.Name = "zipLocationLabel";
            this.zipLocationLabel.Size = new System.Drawing.Size(100, 23);
            this.zipLocationLabel.TabIndex = 51;
            this.zipLocationLabel.Text = "blank";
            // 
            // locationBindingSource
            // 
            this.locationBindingSource.DataMember = "location";
            this.locationBindingSource.DataSource = this._cs6232_g4DataSet;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 136);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 48;
            this.label1.Text = "Your zipcode is currently";
            // 
            // stateComboBox
            // 
            this.stateComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.locationBindingSource, "state", true));
            this.stateComboBox.DataSource = this.stateBindingSource;
            this.stateComboBox.DisplayMember = "name";
            this.stateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stateComboBox.FormattingEnabled = true;
            this.stateComboBox.Location = new System.Drawing.Point(344, 99);
            this.stateComboBox.Name = "stateComboBox";
            this.stateComboBox.Size = new System.Drawing.Size(106, 21);
            this.stateComboBox.TabIndex = 47;
            this.stateComboBox.ValueMember = "code";
            // 
            // stateBindingSource
            // 
            this.stateBindingSource.DataMember = "state";
            this.stateBindingSource.DataSource = this._cs6232_g4DataSet1;
            // 
            // _cs6232_g4DataSet1
            // 
            this._cs6232_g4DataSet1.DataSetName = "_cs6232_g4DataSet";
            this._cs6232_g4DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cityTextBox
            // 
            this.cityTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.locationBindingSource, "city", true));
            this.cityTextBox.Location = new System.Drawing.Point(183, 100);
            this.cityTextBox.Name = "cityTextBox";
            this.cityTextBox.Size = new System.Drawing.Size(90, 20);
            this.cityTextBox.TabIndex = 46;
            this.cityTextBox.Tag = "City";
            // 
            // phoneFormatLabel
            // 
            this.phoneFormatLabel.AutoSize = true;
            this.phoneFormatLabel.Location = new System.Drawing.Point(194, 22);
            this.phoneFormatLabel.Name = "phoneFormatLabel";
            this.phoneFormatLabel.Size = new System.Drawing.Size(111, 13);
            this.phoneFormatLabel.TabIndex = 43;
            this.phoneFormatLabel.Text = "(Format: 8886667777)";
            // 
            // userIDTextBox
            // 
            this.userIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "userID", true));
            this.userIDTextBox.Location = new System.Drawing.Point(95, 52);
            this.userIDTextBox.Name = "userIDTextBox";
            this.userIDTextBox.ReadOnly = true;
            this.userIDTextBox.Size = new System.Drawing.Size(53, 20);
            this.userIDTextBox.TabIndex = 32;
            this.userIDTextBox.Tag = "User ID";
            // 
            // roleIDTextBox
            // 
            this.roleIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "roleID", true));
            this.roleIDTextBox.Location = new System.Drawing.Point(436, 49);
            this.roleIDTextBox.Name = "roleIDTextBox";
            this.roleIDTextBox.ReadOnly = true;
            this.roleIDTextBox.Size = new System.Drawing.Size(53, 20);
            this.roleIDTextBox.TabIndex = 34;
            this.roleIDTextBox.Tag = "Role ";
            // 
            // dateOfBirthFormat
            // 
            this.dateOfBirthFormat.AutoSize = true;
            this.dateOfBirthFormat.Location = new System.Drawing.Point(113, 186);
            this.dateOfBirthFormat.Name = "dateOfBirthFormat";
            this.dateOfBirthFormat.Size = new System.Drawing.Size(109, 13);
            this.dateOfBirthFormat.TabIndex = 39;
            this.dateOfBirthFormat.Text = "(Format: mm/dd/yyyy)";
            // 
            // type
            // 
            this.type.AutoSize = true;
            this.type.Location = new System.Drawing.Point(381, 80);
            this.type.Name = "type";
            this.type.Size = new System.Drawing.Size(34, 13);
            this.type.TabIndex = 40;
            this.type.Text = "Type:";
            // 
            // typetxtbox
            // 
            this.typetxtbox.Location = new System.Drawing.Point(436, 76);
            this.typetxtbox.Name = "typetxtbox";
            this.typetxtbox.ReadOnly = true;
            this.typetxtbox.Size = new System.Drawing.Size(53, 20);
            this.typetxtbox.TabIndex = 41;
            this.typetxtbox.Text = "Member";
            // 
            // sexComboBox
            // 
            this.sexComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sexComboBox.FormattingEnabled = true;
            this.sexComboBox.Location = new System.Drawing.Point(394, 166);
            this.sexComboBox.Name = "sexComboBox";
            this.sexComboBox.Size = new System.Drawing.Size(70, 21);
            this.sexComboBox.TabIndex = 42;
            this.sexComboBox.SelectedIndexChanged += new System.EventHandler(this.SexComboBox_SelectedIndexChanged);
            // 
            // date_of_birthDateTimePicker
            // 
            this.date_of_birthDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.userBindingSource, "date_of_birth", true));
            this.date_of_birthDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.date_of_birthDateTimePicker.Location = new System.Drawing.Point(116, 166);
            this.date_of_birthDateTimePicker.Name = "date_of_birthDateTimePicker";
            this.date_of_birthDateTimePicker.Size = new System.Drawing.Size(100, 20);
            this.date_of_birthDateTimePicker.TabIndex = 43;
            this.date_of_birthDateTimePicker.Tag = "Date Of Birth";
            this.date_of_birthDateTimePicker.Value = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            // 
            // userTableAdapter
            // 
            this.userTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.categoryTableAdapter = null;
            this.tableAdapterManager.furniture_itemTableAdapter = null;
            this.tableAdapterManager.locationTableAdapter = null;
            this.tableAdapterManager.roleTableAdapter = null;
            this.tableAdapterManager.stateTableAdapter = null;
            this.tableAdapterManager.styleTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.userTableAdapter = this.userTableAdapter;
            // 
            // locationTableAdapter
            // 
            this.locationTableAdapter.ClearBeforeFill = true;
            // 
            // stateTableAdapter
            // 
            this.stateTableAdapter.ClearBeforeFill = true;
            // 
            // MemberForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 450);
            this.Controls.Add(date_of_birthLabel1);
            this.Controls.Add(this.date_of_birthDateTimePicker);
            this.Controls.Add(this.sexComboBox);
            this.Controls.Add(this.typetxtbox);
            this.Controls.Add(this.type);
            this.Controls.Add(this.dateOfBirthFormat);
            this.Controls.Add(userIDLabel);
            this.Controls.Add(this.userIDTextBox);
            this.Controls.Add(roleIDLabel);
            this.Controls.Add(this.roleIDTextBox);
            this.Controls.Add(this.contactInfoGroupBox);
            this.Controls.Add(this.memberFormHeading);
            this.Controls.Add(sexLabel);
            this.Controls.Add(this.sexTextBox);
            this.Controls.Add(this.cancelMemberFormButton);
            this.Controls.Add(this.acceptMemberFormButton);
            this.Controls.Add(first_nameLabel);
            this.Controls.Add(this.first_nameTextBox);
            this.Controls.Add(last_nameLabel);
            this.Controls.Add(this.last_nameTextBox);
            this.Name = "MemberForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Member Form";
            this.Load += new System.EventHandler(this.MemberForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).EndInit();
            this.contactInfoGroupBox.ResumeLayout(false);
            this.contactInfoGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.locationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private _cs6232_g4DataSet _cs6232_g4DataSet;
        private System.Windows.Forms.BindingSource userBindingSource;
        private _cs6232_g4DataSetTableAdapters.userTableAdapter userTableAdapter;
        private _cs6232_g4DataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox first_nameTextBox;
        private System.Windows.Forms.TextBox last_nameTextBox;
        private System.Windows.Forms.TextBox phone_numberTextBox;
        private System.Windows.Forms.TextBox address1TextBox;
        private System.Windows.Forms.TextBox address2TextBox;
        private System.Windows.Forms.TextBox zipTextBox;
        private System.Windows.Forms.Button acceptMemberFormButton;
        private System.Windows.Forms.Button cancelMemberFormButton;
        private System.Windows.Forms.TextBox sexTextBox;
        private System.Windows.Forms.Label memberFormHeading;
        private System.Windows.Forms.GroupBox contactInfoGroupBox;
        private System.Windows.Forms.TextBox userIDTextBox;
        private System.Windows.Forms.TextBox roleIDTextBox;
        private System.Windows.Forms.Label dateOfBirthFormat;
        private System.Windows.Forms.Label phoneFormatLabel;
        private System.Windows.Forms.Label type;
        private System.Windows.Forms.TextBox typetxtbox;
        private System.Windows.Forms.ComboBox sexComboBox;
        private System.Windows.Forms.DateTimePicker date_of_birthDateTimePicker;
        private System.Windows.Forms.BindingSource locationBindingSource;
        private _cs6232_g4DataSetTableAdapters.locationTableAdapter locationTableAdapter;
        private System.Windows.Forms.TextBox cityTextBox;
        private System.Windows.Forms.ComboBox stateComboBox;
        private _cs6232_g4DataSet _cs6232_g4DataSet1;
        private System.Windows.Forms.BindingSource stateBindingSource;
        private _cs6232_g4DataSetTableAdapters.stateTableAdapter stateTableAdapter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label zipLocationLabel;
    }
}