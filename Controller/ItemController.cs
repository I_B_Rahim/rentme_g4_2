﻿using RentMe_G4.DAL;
using RentMe_G4.Model;
using System;
using System.Collections.Generic;

namespace RentMe_G4.Controller
{
    public class ItemController
    {
        private ItemDAL ItemDBSource;

        /// <summary>
        /// Default constructor for the ItemController
        /// </summary>
        public ItemController()
        {
            this.ItemDBSource = new ItemDAL();
        }

        /// <summary>
        /// Creates a new rental by insertain a transaction and rental items for the transaction
        /// </summary>
        /// <param name="orderList">The list of int arrays containing the serial and quantity</param>
        /// <param name="member_userID">The member this rental is for</param>
        /// <param name="employee_userID">The employee submiting this rental</param>
        /// <param name="daysToRent">The number of days from the current date to set the rental due date</param>
        public void SubmitOrder(List<int[]> orderList, int member, int employee, int daysToRent)
        {
            ItemDAL.SubmitOrder(orderList, member, employee, daysToRent);
        }

        public List<Item> GetListOfItems()
        {
            return ItemDAL.GetListOfItems();
        }

        /// <summary>
        /// Returns the quantity of an item in stock by serial number
        /// </summary>
        /// <param name="serialNumber">The serial number to check</param>
        /// <returns>The quantity of an item in stock by serial number</returns>
        public int GetQuantityBySerial(int serialNumber)
        {
            return this.ItemDBSource.GetQuantityBySerial(serialNumber);
        }
    }
}
