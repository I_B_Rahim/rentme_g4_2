﻿namespace RentMe_G4.UserControls
{
    partial class SearchFurnitureUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SearchFurnitureDataGridView = new System.Windows.Forms.DataGridView();
            this.serialnumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoryIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.styleIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dailyrentalrateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dailyfinerateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.furnitureitemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cs6232g4DataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._cs6232_g4DataSet = new RentMe_G4._cs6232_g4DataSet();
            this.SearchCategoryComboBox = new System.Windows.Forms.ComboBox();
            this.CategorySearchLabel = new System.Windows.Forms.Label();
            this.SearchStyleLabel = new System.Windows.Forms.Label();
            this.SearchCriteriaTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.SearchStyleComboBox = new System.Windows.Forms.ComboBox();
            this.styleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SearchSerialNumberLabel = new System.Windows.Forms.Label();
            this.SearchSerialNumberTextBox = new System.Windows.Forms.TextBox();
            this.SearchSerialNumberButton = new System.Windows.Forms.Button();
            this.furniture_itemTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.furniture_itemTableAdapter();
            this.styleTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.styleTableAdapter();
            this.categoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.categoryTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.categoryTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.SearchFurnitureDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.furnitureitemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cs6232g4DataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).BeginInit();
            this.SearchCriteriaTableLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.styleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // SearchFurnitureDataGridView
            // 
            this.SearchFurnitureDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchFurnitureDataGridView.AutoGenerateColumns = false;
            this.SearchFurnitureDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SearchFurnitureDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SearchFurnitureDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.serialnumberDataGridViewTextBoxColumn,
            this.descriptionDataGridViewTextBoxColumn,
            this.categoryIDDataGridViewTextBoxColumn,
            this.styleIDDataGridViewTextBoxColumn,
            this.dailyrentalrateDataGridViewTextBoxColumn,
            this.dailyfinerateDataGridViewTextBoxColumn,
            this.quantityDataGridViewTextBoxColumn});
            this.SearchFurnitureDataGridView.DataSource = this.furnitureitemBindingSource;
            this.SearchFurnitureDataGridView.Location = new System.Drawing.Point(3, 83);
            this.SearchFurnitureDataGridView.Name = "SearchFurnitureDataGridView";
            this.SearchFurnitureDataGridView.Size = new System.Drawing.Size(758, 482);
            this.SearchFurnitureDataGridView.TabIndex = 0;
            // 
            // serialnumberDataGridViewTextBoxColumn
            // 
            this.serialnumberDataGridViewTextBoxColumn.DataPropertyName = "serial_number";
            this.serialnumberDataGridViewTextBoxColumn.HeaderText = "serial_number";
            this.serialnumberDataGridViewTextBoxColumn.Name = "serialnumberDataGridViewTextBoxColumn";
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "description";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = "description";
            this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
            // 
            // categoryIDDataGridViewTextBoxColumn
            // 
            this.categoryIDDataGridViewTextBoxColumn.DataPropertyName = "categoryID";
            this.categoryIDDataGridViewTextBoxColumn.HeaderText = "categoryID";
            this.categoryIDDataGridViewTextBoxColumn.Name = "categoryIDDataGridViewTextBoxColumn";
            // 
            // styleIDDataGridViewTextBoxColumn
            // 
            this.styleIDDataGridViewTextBoxColumn.DataPropertyName = "styleID";
            this.styleIDDataGridViewTextBoxColumn.HeaderText = "styleID";
            this.styleIDDataGridViewTextBoxColumn.Name = "styleIDDataGridViewTextBoxColumn";
            // 
            // dailyrentalrateDataGridViewTextBoxColumn
            // 
            this.dailyrentalrateDataGridViewTextBoxColumn.DataPropertyName = "daily_rental_rate";
            this.dailyrentalrateDataGridViewTextBoxColumn.HeaderText = "daily_rental_rate";
            this.dailyrentalrateDataGridViewTextBoxColumn.Name = "dailyrentalrateDataGridViewTextBoxColumn";
            // 
            // dailyfinerateDataGridViewTextBoxColumn
            // 
            this.dailyfinerateDataGridViewTextBoxColumn.DataPropertyName = "daily_fine_rate";
            this.dailyfinerateDataGridViewTextBoxColumn.HeaderText = "daily_fine_rate";
            this.dailyfinerateDataGridViewTextBoxColumn.Name = "dailyfinerateDataGridViewTextBoxColumn";
            // 
            // quantityDataGridViewTextBoxColumn
            // 
            this.quantityDataGridViewTextBoxColumn.DataPropertyName = "quantity";
            this.quantityDataGridViewTextBoxColumn.HeaderText = "quantity";
            this.quantityDataGridViewTextBoxColumn.Name = "quantityDataGridViewTextBoxColumn";
            // 
            // furnitureitemBindingSource
            // 
            this.furnitureitemBindingSource.DataMember = "furniture_item";
            this.furnitureitemBindingSource.DataSource = this.cs6232g4DataSetBindingSource;
            // 
            // cs6232g4DataSetBindingSource
            // 
            this.cs6232g4DataSetBindingSource.DataSource = this._cs6232_g4DataSet;
            this.cs6232g4DataSetBindingSource.Position = 0;
            // 
            // _cs6232_g4DataSet
            // 
            this._cs6232_g4DataSet.DataSetName = "_cs6232_g4DataSet";
            this._cs6232_g4DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // SearchCategoryComboBox
            // 
            this.SearchCategoryComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchCategoryComboBox.DataSource = this.categoryBindingSource;
            this.SearchCategoryComboBox.DisplayMember = "category_name";
            this.SearchCategoryComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchCategoryComboBox.FormattingEnabled = true;
            this.SearchCategoryComboBox.Location = new System.Drawing.Point(3, 40);
            this.SearchCategoryComboBox.Name = "SearchCategoryComboBox";
            this.SearchCategoryComboBox.Size = new System.Drawing.Size(231, 28);
            this.SearchCategoryComboBox.TabIndex = 1;
            this.SearchCategoryComboBox.ValueMember = "categoryID";
            this.SearchCategoryComboBox.SelectedIndexChanged += new System.EventHandler(this.SearchByCategory);
            // 
            // CategorySearchLabel
            // 
            this.CategorySearchLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CategorySearchLabel.AutoSize = true;
            this.CategorySearchLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CategorySearchLabel.Location = new System.Drawing.Point(3, 20);
            this.CategorySearchLabel.Name = "CategorySearchLabel";
            this.CategorySearchLabel.Size = new System.Drawing.Size(65, 17);
            this.CategorySearchLabel.TabIndex = 2;
            this.CategorySearchLabel.Text = "Category";
            // 
            // SearchStyleLabel
            // 
            this.SearchStyleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SearchStyleLabel.AutoSize = true;
            this.SearchStyleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchStyleLabel.Location = new System.Drawing.Point(252, 20);
            this.SearchStyleLabel.Name = "SearchStyleLabel";
            this.SearchStyleLabel.Size = new System.Drawing.Size(39, 17);
            this.SearchStyleLabel.TabIndex = 3;
            this.SearchStyleLabel.Text = "Style";
            // 
            // SearchCriteriaTableLayout
            // 
            this.SearchCriteriaTableLayout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchCriteriaTableLayout.ColumnCount = 6;
            this.SearchCriteriaTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 94.80519F));
            this.SearchCriteriaTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.194805F));
            this.SearchCriteriaTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 219F));
            this.SearchCriteriaTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.SearchCriteriaTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 139F));
            this.SearchCriteriaTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 134F));
            this.SearchCriteriaTableLayout.Controls.Add(this.SearchCategoryComboBox, 0, 1);
            this.SearchCriteriaTableLayout.Controls.Add(this.CategorySearchLabel, 0, 0);
            this.SearchCriteriaTableLayout.Controls.Add(this.SearchStyleLabel, 2, 0);
            this.SearchCriteriaTableLayout.Controls.Add(this.SearchStyleComboBox, 2, 1);
            this.SearchCriteriaTableLayout.Controls.Add(this.SearchSerialNumberLabel, 4, 0);
            this.SearchCriteriaTableLayout.Controls.Add(this.SearchSerialNumberTextBox, 4, 1);
            this.SearchCriteriaTableLayout.Controls.Add(this.SearchSerialNumberButton, 5, 1);
            this.SearchCriteriaTableLayout.Location = new System.Drawing.Point(3, 3);
            this.SearchCriteriaTableLayout.Name = "SearchCriteriaTableLayout";
            this.SearchCriteriaTableLayout.RowCount = 2;
            this.SearchCriteriaTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.SearchCriteriaTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.SearchCriteriaTableLayout.Size = new System.Drawing.Size(758, 74);
            this.SearchCriteriaTableLayout.TabIndex = 5;
            // 
            // SearchStyleComboBox
            // 
            this.SearchStyleComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchStyleComboBox.DataSource = this.styleBindingSource;
            this.SearchStyleComboBox.DisplayMember = "style_type";
            this.SearchStyleComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchStyleComboBox.FormattingEnabled = true;
            this.SearchStyleComboBox.Location = new System.Drawing.Point(252, 40);
            this.SearchStyleComboBox.Name = "SearchStyleComboBox";
            this.SearchStyleComboBox.Size = new System.Drawing.Size(213, 28);
            this.SearchStyleComboBox.TabIndex = 7;
            this.SearchStyleComboBox.ValueMember = "styleID";
            this.SearchStyleComboBox.SelectedIndexChanged += new System.EventHandler(this.SearchByStyle);
            // 
            // styleBindingSource
            // 
            this.styleBindingSource.DataMember = "style";
            this.styleBindingSource.DataSource = this.cs6232g4DataSetBindingSource;
            this.styleBindingSource.CurrentChanged += new System.EventHandler(this.styleBindingSource_CurrentChanged);
            // 
            // SearchSerialNumberLabel
            // 
            this.SearchSerialNumberLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SearchSerialNumberLabel.AutoSize = true;
            this.SearchSerialNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchSerialNumberLabel.Location = new System.Drawing.Point(487, 20);
            this.SearchSerialNumberLabel.Name = "SearchSerialNumberLabel";
            this.SearchSerialNumberLabel.Size = new System.Drawing.Size(98, 17);
            this.SearchSerialNumberLabel.TabIndex = 8;
            this.SearchSerialNumberLabel.Text = "Serial Number";
            // 
            // SearchSerialNumberTextBox
            // 
            this.SearchSerialNumberTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchSerialNumberTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchSerialNumberTextBox.Location = new System.Drawing.Point(487, 40);
            this.SearchSerialNumberTextBox.Name = "SearchSerialNumberTextBox";
            this.SearchSerialNumberTextBox.Size = new System.Drawing.Size(133, 26);
            this.SearchSerialNumberTextBox.TabIndex = 9;
            // 
            // SearchSerialNumberButton
            // 
            this.SearchSerialNumberButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchSerialNumberButton.Location = new System.Drawing.Point(626, 40);
            this.SearchSerialNumberButton.Name = "SearchSerialNumberButton";
            this.SearchSerialNumberButton.Size = new System.Drawing.Size(75, 28);
            this.SearchSerialNumberButton.TabIndex = 10;
            this.SearchSerialNumberButton.Text = "Search";
            this.SearchSerialNumberButton.UseVisualStyleBackColor = true;
            this.SearchSerialNumberButton.Click += new System.EventHandler(this.SearchBySerialNumber);
            // 
            // furniture_itemTableAdapter
            // 
            this.furniture_itemTableAdapter.ClearBeforeFill = true;
            // 
            // styleTableAdapter
            // 
            this.styleTableAdapter.ClearBeforeFill = true;
            // 
            // categoryBindingSource
            // 
            this.categoryBindingSource.DataMember = "category";
            this.categoryBindingSource.DataSource = this.cs6232g4DataSetBindingSource;
            // 
            // categoryTableAdapter
            // 
            this.categoryTableAdapter.ClearBeforeFill = true;
            // 
            // SearchFurnitureUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.SearchCriteriaTableLayout);
            this.Controls.Add(this.SearchFurnitureDataGridView);
            this.Name = "SearchFurnitureUserControl";
            this.Size = new System.Drawing.Size(764, 568);
            ((System.ComponentModel.ISupportInitialize)(this.SearchFurnitureDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.furnitureitemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cs6232g4DataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).EndInit();
            this.SearchCriteriaTableLayout.ResumeLayout(false);
            this.SearchCriteriaTableLayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.styleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView SearchFurnitureDataGridView;
        private System.Windows.Forms.ComboBox SearchCategoryComboBox;
        private System.Windows.Forms.Label CategorySearchLabel;
        private System.Windows.Forms.Label SearchStyleLabel;
        private System.Windows.Forms.TableLayoutPanel SearchCriteriaTableLayout;
        private System.Windows.Forms.ComboBox SearchStyleComboBox;
        private System.Windows.Forms.Label SearchSerialNumberLabel;
        private System.Windows.Forms.TextBox SearchSerialNumberTextBox;
        private System.Windows.Forms.Button SearchSerialNumberButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn serialnumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoryIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn styleIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dailyrentalrateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dailyfinerateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource furnitureitemBindingSource;
        private System.Windows.Forms.BindingSource cs6232g4DataSetBindingSource;
        private _cs6232_g4DataSet _cs6232_g4DataSet;
        private _cs6232_g4DataSetTableAdapters.furniture_itemTableAdapter furniture_itemTableAdapter;
        private System.Windows.Forms.BindingSource styleBindingSource;
        private _cs6232_g4DataSetTableAdapters.styleTableAdapter styleTableAdapter;
        private System.Windows.Forms.BindingSource categoryBindingSource;
        private _cs6232_g4DataSetTableAdapters.categoryTableAdapter categoryTableAdapter;
    }
}
