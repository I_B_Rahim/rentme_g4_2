﻿using System;

namespace RentMe_G4.Model
{
    public class Item
    {
        private int serialNumber;
        private string description;
        private int categoryID;
        private int styleID;
        private double dailyRentalRate;
        private double dailyFineRate;
        private int quantity;
        private string categoryName;
        private string styleType;

        public string CategoryName
        {
            get
            {
                return this.categoryName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("category name cannot be null or empty", "category name error");
                }
                this.categoryName = value;
            }
        }

        public string StyleType
        {
            get
            {
                return this.styleType;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("style type cannot be null or empty", "style type error");
                }
                this.styleType = value;
            }
        }

        public int SerialNumber
        {
            get
            {
                return this.serialNumber;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("serialNumber cannot be less than 0", "serialNumber error");
                }
                this.serialNumber = value;
            }
        }

        public string Description
        {
            get
            {
                return this.description;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("description cannot be null or empty", "description error");
                }
                this.description = value;
            }
        }

        public int CategoryID
        {
            get
            {
                return this.categoryID;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("categoryID cannot be less than 0", "categoryID error");
                }
                this.categoryID = value;
            }
        }

        public int StyleID
        {
            get
            {
                return this.styleID;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("styleID cannot be less than 0", "styleID error");
                }
                this.styleID = value;
            }
        }

        public double DailyRentalRate
        {
            get
            {
                return this.dailyRentalRate;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("dailyRentalRate cannot be less than 0", "dailyRentalRate error");
                }
                this.dailyRentalRate = value;
            }
        }
        public double DailyFineRate
        {
            get
            {
                return this.dailyFineRate;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("dailyFineRate cannot be less than 0", "dailyFineRate error");
                }
                this.dailyFineRate = value;
            }
        }
        public int Quantity
        {
            get
            {
                return this.quantity;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("quantity cannot be less than 0", "quantity error");
                }
                this.quantity = value;
            }
        }
    }
}
