﻿using System;
using System.Data;
using System.Windows.Forms;

namespace RentMe_G4.UserControls
{
    /// <summary>
    /// User control for searching furniture items
    /// </summary>
    public partial class SearchFurnitureUserControl : UserControl
    {
        /// <summary>
        /// Default Contructor for the SearchFurnitureUserControl
        /// </summary>
        public SearchFurnitureUserControl()
        {
            InitializeComponent();
            SetupSearchFurnitureUserControl();
        }

        /// <summary>
        /// Clear the Serial Search and Set ComboBoxes to first index
        /// </summary>
        public void SetupSearchFurnitureUserControl()
        {
            SearchCategoryComboBox.DataSource = new DataView(categoryTableAdapter.GetData());
            SearchStyleComboBox.DataSource = new DataView(styleTableAdapter.GetData());
            SearchStyleComboBox.ResetText();
            SearchCategoryComboBox.ResetText();
            SearchSerialNumberTextBox.Text = "";
        }

        private void SearchByCategory(object sender, System.EventArgs e)
        {
            SearchFurnitureDataGridView.DataSource = furnitureitemBindingSource;
            furnitureitemBindingSource.Filter = "categoryID = " + SearchCategoryComboBox.SelectedValue.ToString();
            furniture_itemTableAdapter.Fill(_cs6232_g4DataSet.furniture_item);
            SearchFurnitureDataGridView.Refresh();
            SearchStyleComboBox.ResetText();
        }

        private void SearchByStyle(object sender, System.EventArgs e)
        {
            SearchFurnitureDataGridView.DataSource = furnitureitemBindingSource;
            furnitureitemBindingSource.Filter = "styleID = " + SearchStyleComboBox.SelectedValue.ToString();
            furniture_itemTableAdapter.Fill(_cs6232_g4DataSet.furniture_item);
            SearchFurnitureDataGridView.Refresh();
            SearchCategoryComboBox.ResetText();
        }

        private void SearchBySerialNumber(object sender, System.EventArgs e)
        {
            SearchFurnitureDataGridView.DataSource = furnitureitemBindingSource;
            if (SearchSerialNumberTextBox.Text.Replace(" ", String.Empty).Length > 0) {
                furnitureitemBindingSource.Filter = "serial_number = " + SearchSerialNumberTextBox.Text;
            }
            furniture_itemTableAdapter.Fill(_cs6232_g4DataSet.furniture_item);
            SearchFurnitureDataGridView.Refresh();
            SearchStyleComboBox.ResetText();
            SearchCategoryComboBox.ResetText();
        }

        private void styleBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }
    }
}
