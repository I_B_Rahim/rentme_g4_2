﻿namespace RentMe_G4.Model
{
    /// <summary>
    /// This class models an employee
    /// </summary>
    public class Employee
    {
        public int UserID { get; set; }
        public int RoleID { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        /// <summary>
        /// Employee Constructor with required parameters
        /// </summary>
        /// <param name="userID">Database userID of Employee</param>
        /// <param name="roleID">Database roleID of Employee</param>
        /// <param name="userName">This Employee's user name</param>
        /// <param name="firstName">This Employee's first name</param>
        /// <param name="lastName">This Employee's last name</param>
        public Employee (int userID, int roleID, string userName, string firstName, string lastName)
        {
            UserID = userID;
            RoleID = roleID;
            UserName = userName;
            FirstName = firstName;
            LastName = lastName;
        }
    }
}
