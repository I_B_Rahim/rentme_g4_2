﻿namespace RentMe_G4.UserControls
{
    partial class MemberReturnsUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label first_nameLabel;
            System.Windows.Forms.Label memberIDLabel;
            System.Windows.Forms.Label returnsHistoryLabel;
            this._cs6232_g4DataSet = new RentMe_G4._cs6232_g4DataSet();
            this.return_transactionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.return_transactionTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.return_transactionTableAdapter();
            this.tableAdapterManager = new RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager();
            this.userTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.userTableAdapter();
            this.return_transactionDataGridView = new System.Windows.Forms.DataGridView();
            this.returnID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.employeeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.returnDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.viewReturnItems = new System.Windows.Forms.DataGridViewButtonColumn();
            this.memberErrorlabel = new System.Windows.Forms.Label();
            this.memberInfo = new System.Windows.Forms.GroupBox();
            this.last_nameTextBox = new System.Windows.Forms.TextBox();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.first_nameTextBox = new System.Windows.Forms.TextBox();
            this.memberIDTextBox = new System.Windows.Forms.TextBox();
            first_nameLabel = new System.Windows.Forms.Label();
            memberIDLabel = new System.Windows.Forms.Label();
            returnsHistoryLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.return_transactionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.return_transactionDataGridView)).BeginInit();
            this.memberInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // first_nameLabel
            // 
            first_nameLabel.AutoSize = true;
            first_nameLabel.Location = new System.Drawing.Point(6, 56);
            first_nameLabel.Name = "first_nameLabel";
            first_nameLabel.Size = new System.Drawing.Size(38, 13);
            first_nameLabel.TabIndex = 2;
            first_nameLabel.Text = "Name:";
            // 
            // memberIDLabel
            // 
            memberIDLabel.AutoSize = true;
            memberIDLabel.Location = new System.Drawing.Point(6, 26);
            memberIDLabel.Name = "memberIDLabel";
            memberIDLabel.Size = new System.Drawing.Size(21, 13);
            memberIDLabel.TabIndex = 0;
            memberIDLabel.Text = "ID:";
            // 
            // returnsHistoryLabel
            // 
            returnsHistoryLabel.AutoSize = true;
            returnsHistoryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            returnsHistoryLabel.Location = new System.Drawing.Point(371, 109);
            returnsHistoryLabel.Name = "returnsHistoryLabel";
            returnsHistoryLabel.Size = new System.Drawing.Size(94, 13);
            returnsHistoryLabel.TabIndex = 10;
            returnsHistoryLabel.Text = "Returns History";
            // 
            // _cs6232_g4DataSet
            // 
            this._cs6232_g4DataSet.DataSetName = "_cs6232_g4DataSet";
            this._cs6232_g4DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // return_transactionBindingSource
            // 
            this.return_transactionBindingSource.DataMember = "return_transaction";
            this.return_transactionBindingSource.DataSource = this._cs6232_g4DataSet;
            // 
            // return_transactionTableAdapter
            // 
            this.return_transactionTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.categoryTableAdapter = null;
            this.tableAdapterManager.furniture_itemTableAdapter = null;
            this.tableAdapterManager.locationTableAdapter = null;
            this.tableAdapterManager.rental_itemTableAdapter = null;
            this.tableAdapterManager.rental_transactionTableAdapter = null;
            this.tableAdapterManager.return_itemTableAdapter = null;
            this.tableAdapterManager.return_transactionTableAdapter = this.return_transactionTableAdapter;
            this.tableAdapterManager.roleTableAdapter = null;
            this.tableAdapterManager.stateTableAdapter = null;
            this.tableAdapterManager.styleTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.userTableAdapter = this.userTableAdapter;
            // 
            // userTableAdapter
            // 
            this.userTableAdapter.ClearBeforeFill = true;
            // 
            // return_transactionDataGridView
            // 
            this.return_transactionDataGridView.AllowUserToAddRows = false;
            this.return_transactionDataGridView.AllowUserToDeleteRows = false;
            this.return_transactionDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.return_transactionDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.returnID,
            this.employeeID,
            this.returnDate,
            this.viewReturnItems});
            this.return_transactionDataGridView.Location = new System.Drawing.Point(137, 125);
            this.return_transactionDataGridView.Name = "return_transactionDataGridView";
            this.return_transactionDataGridView.ReadOnly = true;
            this.return_transactionDataGridView.RowHeadersVisible = false;
            this.return_transactionDataGridView.Size = new System.Drawing.Size(529, 171);
            this.return_transactionDataGridView.TabIndex = 1;
            this.return_transactionDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Return_transactionDataGridView_CellContentClick);
            this.return_transactionDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.Return_transactionDataGridView_DataError);
            // 
            // returnID
            // 
            this.returnID.HeaderText = "Return ID";
            this.returnID.Name = "returnID";
            this.returnID.ReadOnly = true;
            // 
            // employeeID
            // 
            this.employeeID.HeaderText = "EmployeeID";
            this.employeeID.Name = "employeeID";
            this.employeeID.ReadOnly = true;
            // 
            // returnDate
            // 
            this.returnDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.returnDate.HeaderText = "Return Date";
            this.returnDate.Name = "returnDate";
            this.returnDate.ReadOnly = true;
            // 
            // viewReturnItems
            // 
            this.viewReturnItems.HeaderText = "";
            this.viewReturnItems.Name = "viewReturnItems";
            this.viewReturnItems.ReadOnly = true;
            this.viewReturnItems.Text = "View Return Items";
            this.viewReturnItems.UseColumnTextForButtonValue = true;
            // 
            // memberErrorlabel
            // 
            this.memberErrorlabel.AutoSize = true;
            this.memberErrorlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memberErrorlabel.Location = new System.Drawing.Point(594, 34);
            this.memberErrorlabel.Name = "memberErrorlabel";
            this.memberErrorlabel.Size = new System.Drawing.Size(179, 22);
            this.memberErrorlabel.TabIndex = 6;
            this.memberErrorlabel.Text = "No member selected!";
            this.memberErrorlabel.Visible = false;
            // 
            // memberInfo
            // 
            this.memberInfo.Controls.Add(this.last_nameTextBox);
            this.memberInfo.Controls.Add(this.first_nameTextBox);
            this.memberInfo.Controls.Add(this.memberIDTextBox);
            this.memberInfo.Controls.Add(first_nameLabel);
            this.memberInfo.Controls.Add(memberIDLabel);
            this.memberInfo.Location = new System.Drawing.Point(15, 13);
            this.memberInfo.Name = "memberInfo";
            this.memberInfo.Size = new System.Drawing.Size(222, 93);
            this.memberInfo.TabIndex = 8;
            this.memberInfo.TabStop = false;
            this.memberInfo.Text = "Member Info.";
            // 
            // last_nameTextBox
            // 
            this.last_nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "last_name", true));
            this.last_nameTextBox.Location = new System.Drawing.Point(122, 53);
            this.last_nameTextBox.Name = "last_nameTextBox";
            this.last_nameTextBox.ReadOnly = true;
            this.last_nameTextBox.Size = new System.Drawing.Size(56, 20);
            this.last_nameTextBox.TabIndex = 9;
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataMember = "user";
            this.userBindingSource.DataSource = this._cs6232_g4DataSet;
            // 
            // first_nameTextBox
            // 
            this.first_nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "first_name", true));
            this.first_nameTextBox.Location = new System.Drawing.Point(53, 53);
            this.first_nameTextBox.Name = "first_nameTextBox";
            this.first_nameTextBox.ReadOnly = true;
            this.first_nameTextBox.Size = new System.Drawing.Size(60, 20);
            this.first_nameTextBox.TabIndex = 9;
            // 
            // memberIDTextBox
            // 
            this.memberIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "userID", true));
            this.memberIDTextBox.Location = new System.Drawing.Point(53, 23);
            this.memberIDTextBox.Name = "memberIDTextBox";
            this.memberIDTextBox.ReadOnly = true;
            this.memberIDTextBox.Size = new System.Drawing.Size(43, 20);
            this.memberIDTextBox.TabIndex = 9;
            // 
            // MemberReturnsUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(returnsHistoryLabel);
            this.Controls.Add(this.memberInfo);
            this.Controls.Add(this.memberErrorlabel);
            this.Controls.Add(this.return_transactionDataGridView);
            this.Name = "MemberReturnsUserControl";
            this.Size = new System.Drawing.Size(787, 320);
            this.Load += new System.EventHandler(this.MemberReturnsUserControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.return_transactionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.return_transactionDataGridView)).EndInit();
            this.memberInfo.ResumeLayout(false);
            this.memberInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private _cs6232_g4DataSet _cs6232_g4DataSet;
        private System.Windows.Forms.BindingSource return_transactionBindingSource;
        private _cs6232_g4DataSetTableAdapters.return_transactionTableAdapter return_transactionTableAdapter;
        private _cs6232_g4DataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView return_transactionDataGridView;
        private System.Windows.Forms.Label memberErrorlabel;
        private System.Windows.Forms.GroupBox memberInfo;
        private _cs6232_g4DataSetTableAdapters.userTableAdapter userTableAdapter;
        private System.Windows.Forms.BindingSource userBindingSource;
        private System.Windows.Forms.TextBox memberIDTextBox;
        private System.Windows.Forms.TextBox last_nameTextBox;
        private System.Windows.Forms.TextBox first_nameTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn returnID;
        private System.Windows.Forms.DataGridViewTextBoxColumn employeeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn returnDate;
        private System.Windows.Forms.DataGridViewButtonColumn viewReturnItems;
    }
}
