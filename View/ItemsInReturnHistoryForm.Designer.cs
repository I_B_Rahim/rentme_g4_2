﻿namespace RentMe_G4.View
{
    partial class ItemsInReturnHistoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._cs6232_g4DataSet = new RentMe_G4._cs6232_g4DataSet();
            this.return_itemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.return_itemTableAdapter = new RentMe_G4._cs6232_g4DataSetTableAdapters.return_itemTableAdapter();
            this.tableAdapterManager = new RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager();
            this.RentalItemsLabel = new System.Windows.Forms.Label();
            this.returnedIDtxtbox = new System.Windows.Forms.TextBox();
            this.okbtn = new System.Windows.Forms.Button();
            this.return_itemDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.viewItemDetails = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.return_itemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.return_itemDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // _cs6232_g4DataSet
            // 
            this._cs6232_g4DataSet.DataSetName = "_cs6232_g4DataSet";
            this._cs6232_g4DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // return_itemBindingSource
            // 
            this.return_itemBindingSource.DataMember = "return_item";
            this.return_itemBindingSource.DataSource = this._cs6232_g4DataSet;
            // 
            // return_itemTableAdapter
            // 
            this.return_itemTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.categoryTableAdapter = null;
            this.tableAdapterManager.furniture_itemTableAdapter = null;
            this.tableAdapterManager.locationTableAdapter = null;
            this.tableAdapterManager.rental_itemTableAdapter = null;
            this.tableAdapterManager.rental_transactionTableAdapter = null;
            this.tableAdapterManager.return_itemTableAdapter = this.return_itemTableAdapter;
            this.tableAdapterManager.return_transactionTableAdapter = null;
            this.tableAdapterManager.roleTableAdapter = null;
            this.tableAdapterManager.stateTableAdapter = null;
            this.tableAdapterManager.styleTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = RentMe_G4._cs6232_g4DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.userTableAdapter = null;
            // 
            // RentalItemsLabel
            // 
            this.RentalItemsLabel.AutoSize = true;
            this.RentalItemsLabel.Location = new System.Drawing.Point(39, 32);
            this.RentalItemsLabel.Name = "RentalItemsLabel";
            this.RentalItemsLabel.Size = new System.Drawing.Size(149, 13);
            this.RentalItemsLabel.TabIndex = 3;
            this.RentalItemsLabel.Text = "Items Returned For Return ID:";
            // 
            // returnedIDtxtbox
            // 
            this.returnedIDtxtbox.Location = new System.Drawing.Point(194, 29);
            this.returnedIDtxtbox.Name = "returnedIDtxtbox";
            this.returnedIDtxtbox.ReadOnly = true;
            this.returnedIDtxtbox.Size = new System.Drawing.Size(43, 20);
            this.returnedIDtxtbox.TabIndex = 4;
            // 
            // okbtn
            // 
            this.okbtn.Location = new System.Drawing.Point(667, 251);
            this.okbtn.Name = "okbtn";
            this.okbtn.Size = new System.Drawing.Size(75, 23);
            this.okbtn.TabIndex = 5;
            this.okbtn.Text = "OK";
            this.okbtn.UseVisualStyleBackColor = true;
            this.okbtn.Click += new System.EventHandler(this.Okbtn_Click);
            // 
            // return_itemDataGridView
            // 
            this.return_itemDataGridView.AllowUserToAddRows = false;
            this.return_itemDataGridView.AllowUserToDeleteRows = false;
            this.return_itemDataGridView.AutoGenerateColumns = false;
            this.return_itemDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.return_itemDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.viewItemDetails});
            this.return_itemDataGridView.DataSource = this.return_itemBindingSource;
            this.return_itemDataGridView.Location = new System.Drawing.Point(42, 69);
            this.return_itemDataGridView.Name = "return_itemDataGridView";
            this.return_itemDataGridView.ReadOnly = true;
            this.return_itemDataGridView.RowHeadersVisible = false;
            this.return_itemDataGridView.Size = new System.Drawing.Size(700, 176);
            this.return_itemDataGridView.TabIndex = 5;
            this.return_itemDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Return_itemDataGridView_CellContentClick);
            this.return_itemDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.Return_itemDataGridView_DataError);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "itemID";
            this.dataGridViewTextBoxColumn1.HeaderText = "itemID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 50;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "returnID";
            this.dataGridViewTextBoxColumn2.HeaderText = "returnID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "quantity";
            this.dataGridViewTextBoxColumn3.HeaderText = "quantity";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "amount_due";
            this.dataGridViewTextBoxColumn4.HeaderText = "amount_due";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "amount_paid";
            this.dataGridViewTextBoxColumn5.HeaderText = "amount_paid";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "amount_refunded";
            this.dataGridViewTextBoxColumn6.HeaderText = "amount_refunded";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // viewItemDetails
            // 
            this.viewItemDetails.HeaderText = "";
            this.viewItemDetails.Name = "viewItemDetails";
            this.viewItemDetails.ReadOnly = true;
            this.viewItemDetails.Text = "View Item Details";
            this.viewItemDetails.UseColumnTextForButtonValue = true;
            // 
            // ItemsInReturnHistoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(771, 281);
            this.Controls.Add(this.return_itemDataGridView);
            this.Controls.Add(this.okbtn);
            this.Controls.Add(this.returnedIDtxtbox);
            this.Controls.Add(this.RentalItemsLabel);
            this.Name = "ItemsInReturnHistoryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Items In Return";
            this.Load += new System.EventHandler(this.ItemsInReturnHistoryForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.return_itemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.return_itemDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private _cs6232_g4DataSet _cs6232_g4DataSet;
        private System.Windows.Forms.BindingSource return_itemBindingSource;
        private _cs6232_g4DataSetTableAdapters.return_itemTableAdapter return_itemTableAdapter;
        private _cs6232_g4DataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Label RentalItemsLabel;
        private System.Windows.Forms.TextBox returnedIDtxtbox;
        private System.Windows.Forms.Button okbtn;
        private System.Windows.Forms.DataGridView return_itemDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewButtonColumn viewItemDetails;
    }
}