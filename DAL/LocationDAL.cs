﻿using RentMe_G4.Model;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace RentMe_G4.DAL
{
    ///<summary>
    /// The DAL that interacts with location object
    /// this gets out list of locations 
    /// </summary>
    public class LocationDAL
    {

        /// <summary>
        /// This method returns the list of zips from the location table
        /// </summary>
        /// <returns>Returns the list of zipcodes from locatoin table</returns>
        public static List<string> GetListOfZip()
        {
            List<string> listOfZip = new List<string>();
            string selectStatement =
                "SELECT DISTINCT [zip] " +
                "FROM [cs6232-g4].[dbo].[location] ";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string zip;
                            zip = reader["zip"].ToString();
                            listOfZip.Add(zip);
                        }
                    }
                }
            }
            return listOfZip;
        }

        /// <summary>
        /// Returns Location by the zip
        /// </summary>
        /// <param name="zip">the zip for the location</param>
        /// <returns>location</returns>
        public static Location GetLocationByZip(string zip)
        {
            Location location = new Location();
            string selectStatement =
                "SELECT * " +
                "FROM [location] " +
                "WHERE zip = @zip";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@zip", zip);
                    using (SqlDataReader reader = selectCommand.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        while (reader.Read())
                        {

                            location.Zip = reader["zip"].ToString();
                            location.City = reader["city"].ToString();
                            location.State = reader["state"].ToString();

                        }
                    }
                }
                return location;
            }
        }

        /// <summary>
        /// Adds an Location to the  database and returns true if added or false otherwise
        /// </summary>
        /// <param name="location">the lcoation to be added</param>
        /// <returns>true if location was added, false otherwise</returns>
        public static bool AddLocation(Location location)
        {
            bool locationAdded;
            string insertStatement =
                "INSERT location " +
                  "(zip, city, state) " +
                "VALUES (@zip, @city, @state) ";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand insertCommand = new SqlCommand(insertStatement, connection))
                {
                    insertCommand.Parameters.AddWithValue("@zip", location.Zip);
                    insertCommand.Parameters.AddWithValue("@city", location.City);
                    insertCommand.Parameters.AddWithValue("@state", location.State);

                    int count = insertCommand.ExecuteNonQuery();
                    if (count > 0)
                    {
                        locationAdded = true;
                    }
                    else
                    {
                        locationAdded = false;
                    }
                }
            }
            return locationAdded;
        }
    }
}
