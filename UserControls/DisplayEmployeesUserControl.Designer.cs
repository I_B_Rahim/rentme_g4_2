﻿namespace RentMe_G4.UserControls
{
    partial class DisplayEmployeesUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._cs6232_g4DataSet = new RentMe_G4._cs6232_g4DataSet();
            this.cs6232g4DataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.displayEmployeeListView = new System.Windows.Forms.ListView();
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cs6232g4DataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // _cs6232_g4DataSet
            // 
            this._cs6232_g4DataSet.DataSetName = "_cs6232_g4DataSet";
            this._cs6232_g4DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cs6232g4DataSetBindingSource
            // 
            this.cs6232g4DataSetBindingSource.DataSource = this._cs6232_g4DataSet;
            this.cs6232g4DataSetBindingSource.Position = 0;
            // 
            // displayEmployeeListView
            // 
            this.displayEmployeeListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.displayEmployeeListView.HideSelection = false;
            this.displayEmployeeListView.Location = new System.Drawing.Point(0, 0);
            this.displayEmployeeListView.Name = "displayEmployeeListView";
            this.displayEmployeeListView.Size = new System.Drawing.Size(398, 250);
            this.displayEmployeeListView.TabIndex = 0;
            this.displayEmployeeListView.UseCompatibleStateImageBehavior = false;
            // 
            // DisplayEmployeesUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.displayEmployeeListView);
            this.Name = "DisplayEmployeesUserControl";
            this.Size = new System.Drawing.Size(398, 250);
            this.VisibleChanged += new System.EventHandler(this.LoadDisplayEmployees);
            ((System.ComponentModel.ISupportInitialize)(this._cs6232_g4DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cs6232g4DataSetBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private _cs6232_g4DataSet _cs6232_g4DataSet;
        private System.Windows.Forms.BindingSource cs6232g4DataSetBindingSource;
        private System.Windows.Forms.ListView displayEmployeeListView;
    }
}
