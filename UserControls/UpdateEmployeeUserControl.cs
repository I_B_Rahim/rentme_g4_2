﻿using System;
using System.Windows.Forms;
using RentMe_G4.Controller;
using RentMe_G4.Model;

namespace RentMe_G4.UserControls
{

    /// <summary>
    /// User control for updating an employee
    /// </summary>
    public partial class UpdateEmployeeUserControl : UserControl
    {
        private readonly UserController UserController;

        /// <summary>
        /// Constructor to initialize the class
        /// </summary>
        public UpdateEmployeeUserControl()
        {
            InitializeComponent();
            this.UserController = new UserController();
        }

        private void UpdateEmployeeUserControl_Load(object sender, EventArgs e)
        {
            this.LoadForm();
        }

        private void LoadForm()
        {
            this.first_nameTextBox.ReadOnly = true;
            this.last_nameTextBox.ReadOnly = true;
            this.date_of_birthTextBox.ReadOnly = true;
            this.phone_numberTextBox.ReadOnly = true;
            this.address1TextBox.ReadOnly = true;
            this.address2TextBox.ReadOnly = true;
            this.cityTextBox.ReadOnly = true;
            this.zipTextBox.ReadOnly = true;
            this.codeComboBox.Enabled = false;
            this.sexComboBox.Enabled = false;
            this.clearButton.Enabled = false;
            this.updateButton.Enabled = false;
            this.statusCheckBox.Enabled = false;
            this.resetPasswrodTextBox.Enabled = false;

            this.sexComboBox.Items.Clear();
            this.sexComboBox.Items.Add("Male");
            this.sexComboBox.Items.Add("Female");
            this.sexComboBox.SelectedIndex = 1;

            this.codeComboBox.DataSource = this.UserController.GetListOfStates();

            this.userIDTextBox.Clear();
            this.first_nameTextBox.Clear();
            this.last_nameTextBox.Clear();
            this.date_of_birthTextBox.Clear();
            this.phone_numberTextBox.Clear();
            this.address1TextBox.Clear();
            this.address2TextBox.Clear();
            this.resetPasswrodTextBox.Clear();
            this.cityTextBox.Clear();
            this.zipTextBox.Clear();
        }

        private void GetUserButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.userIDTextBox.Text == "")
                {
                    throw new ArgumentException("User ID cannot be empty");
                }

                if (!(int.TryParse(this.userIDTextBox.Text, out int incidentIDOut)))
                {
                    throw new ArgumentException("User ID must be an integer");
                }

                if (!this.UserController.GetEmployeeExists(Int32.Parse(this.userIDTextBox.Text)))
                {
                    throw new ArgumentException("User ID does not exist or is not an employee");
                }
                else
                {
                    User employee = this.UserController.GetUserByUserID(Int32.Parse(this.userIDTextBox.Text));

                    this.userIDTextBox.Text = employee.UserID.ToString();
                    this.first_nameTextBox.Text = employee.FirstName;
                    this.last_nameTextBox.Text = employee.LastName;
                    this.date_of_birthTextBox.Text = employee.DateOfBirth.ToString("MM/dd/yyyy");
                    this.phone_numberTextBox.Text = employee.PhoneNumber;
                    this.address1TextBox.Text = employee.Address1;
                    if (employee.Address2 != "")
                    {
                        this.address2TextBox.Text = employee.Address2;
                    }

                    this.zipTextBox.Text = employee.Zip;
                    this.codeComboBox.SelectedItem = employee.State;
                    this.cityTextBox.Text = employee.City;
                    
                    if (employee.Sex == char.Parse("M"))
                    {
                        this.sexComboBox.SelectedItem = "Male";
                    }
                    else
                    {
                        this.sexComboBox.SelectedItem = "Female";
                    }

                    if (employee.Status)
                    {
                        this.statusCheckBox.Checked = true;
                    }
                    else
                    {
                        this.statusCheckBox.Checked = false;
                    }

                    this.first_nameTextBox.ReadOnly = false;
                    this.last_nameTextBox.ReadOnly = false;
                    this.date_of_birthTextBox.ReadOnly = false;
                    this.phone_numberTextBox.ReadOnly = false;
                    this.address1TextBox.ReadOnly = false;
                    this.address2TextBox.ReadOnly = false;
                    this.cityTextBox.ReadOnly = false;
                    this.zipTextBox.ReadOnly = false;
                    this.codeComboBox.Enabled = true;
                    this.sexComboBox.Enabled = true;
                    this.clearButton.Enabled = true;
                    this.updateButton.Enabled = true;
                    this.statusCheckBox.Enabled = true;
                    this.resetPasswrodTextBox.Enabled = true;

                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("There was an error retrieving the user: \n" + ex.Message, "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            this.LoadForm();
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            try
            {
                User employee = new User();

                employee.UserID = Int32.Parse(this.userIDTextBox.Text);
                employee.RoleID = 2;
                employee.Status = this.statusCheckBox.Checked;
                employee.FirstName = this.first_nameTextBox.Text;
                employee.LastName = this.last_nameTextBox.Text;
                employee.DateOfBirth = DateTime.Parse(this.date_of_birthTextBox.Text);
                employee.PhoneNumber = this.phone_numberTextBox.Text;
                employee.Address1 = this.address1TextBox.Text;

                if (this.address2TextBox.Text != "")
                {
                    employee.Address2 = this.address2TextBox.Text;
                }
                else
                {
                    employee.Address2 = "";
                }

                if (!Int32.TryParse(this.zipTextBox.Text, out _) || this.zipTextBox.Text.Length > 5 || this.zipTextBox.Text.Length < 5)
                {
                    throw new ArgumentException("Zip must be a 5 digit zip code.");
                }
                else
                {
                    employee.City = this.cityTextBox.Text;
                    if (!this.UserController.GetZipExists(this.zipTextBox.Text))
                    {
                        this.UserController.AddLocation(this.zipTextBox.Text, this.cityTextBox.Text, this.codeComboBox.SelectedItem.ToString());
                    }
                    employee.Zip = this.zipTextBox.Text;
                }

                if (this.sexComboBox.Text == "Male")
                {
                    employee.Sex = char.Parse("M");
                }
                else
                {
                    employee.Sex = char.Parse("F");
                }

                if (this.resetPasswrodTextBox.Text != "")
                {
                    employee.Password = this.resetPasswrodTextBox.Text;
                }

                if (this.UserController.UpdateEmployee(employee))
                {
                    MessageBox.Show("The employee has been successfully updated!", "Success",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.LoadForm();
                }
                else
                {
                    MessageBox.Show("There was an error updating the user", "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error has occurred: \n" + ex.Message, "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
