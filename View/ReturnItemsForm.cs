﻿using RentMe_G4.Controller;
using RentMe_G4.Model;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace RentMe_G4.View
{
    /// <summary>
    /// This class creates a ReturnItemsForm that allows to return items in for a transaction.
    /// </summary>
    public partial class ReturnItemsForm : Form
    {
        public static int employeeID;
        public List<string> listOfItemsToReturn;
        private decimal refund;
        private int itemIDNotPaidInFull;

        /// <summary>
        /// Initializes the Return Items form
        /// </summary>
        public ReturnItemsForm()
        {
            try
            {
                this.listOfItemsToReturn = new List<string>();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
           
            InitializeComponent();
        }

        private void ReturnItemsForm_Load(object sender, EventArgs e)
        {
            this.SetUpTheReturnTransactionInfo();
            this.SetUpTheReturnItemDataGridView();
        }

        /// <summary>
        /// Sets up the DataGridView with values in the required cells
        /// </summary>
        private void SetUpTheReturnItemDataGridView()
        {
            try
            {
                for (int index = 0; index < listOfItemsToReturn.Count; index++)
                {
                    int indexRow = return_itemDataGridView.Rows.Add();
                    return_itemDataGridView.Rows[indexRow].Cells[0].Value = listOfItemsToReturn[index];
                    return_itemDataGridView.Rows[indexRow].Cells[1].Value = returnIDTextBox.Text;
                    return_itemDataGridView.Rows[indexRow].Cells[2].Value = "1";
                    return_itemDataGridView.Rows[indexRow].Cells[3].Value = "0.00";
                    return_itemDataGridView.Rows[indexRow].Cells[4].Value = "0.00";
                    return_itemDataGridView.Rows[indexRow].Cells[5].Value = "0.00";
                }
                return_itemDataGridView.Columns["itemID"].ReadOnly = true;
                return_itemDataGridView.Columns["returnID"].ReadOnly = true;
                return_itemDataGridView.Columns["amount_due"].ReadOnly = true;
                return_itemDataGridView.Columns["amountRefunded"].ReadOnly = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        /// <summary>
        /// Gets the ammount due for the item
        /// </summary>
        /// <param name="itemID">item ID to get the amount due</param>
        /// <param name="quantity">quantity of items being returned</param>
        /// <returns>amount due</returns>
        private decimal GetAmountDue(string itemID, string quantity)
        {
            try
            {
                int quantityOFItem = int.Parse(quantity);
                decimal amountDue = decimal.Parse("0.00");
                decimal daily_fine_rate = UserController.GetDailyFineRate(int.Parse(itemID));
                decimal daily_rental_rate = UserController.GetDailyRentalRate(int.Parse(itemID));
                DateTime rentalDate = UserController.GetRentalDate(int.Parse(itemID));
                int rentalDateToDueDate = UserController.GetRentalPeriod(int.Parse(itemID));
                int rentalDateToReturnDate = (int) Math.Abs((rentalDate - Convert.ToDateTime(return_dateTextBox.Text.ToString())).TotalDays);
                int daysEarly = rentalDateToDueDate - rentalDateToReturnDate;
                daysEarly -= 1;
                refund = Math.Abs(daysEarly) * daily_rental_rate * quantityOFItem;
                if (rentalDateToReturnDate <= 1)
                {
                    return amountDue;
                }
                else if (daysEarly < 0)
                {
                    amountDue = Math.Abs(daysEarly) * daily_fine_rate * quantityOFItem;
                    refund = decimal.Parse("0.00");
                    return amountDue;
                }
                else
                {
                    return amountDue;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
                refund = decimal.Parse("0.00");
                return decimal.Parse("0.00");
            }
        }

        /// <summary>
        /// This sets up the values for the return transaction table and the return info text boxes
        /// </summary>
        private void SetUpTheReturnTransactionInfo()
        {
            try
            {
                return_transactionBindingSource.AddNew();
                int newReturnID = UserController.GetLastReturnID() + 1;
                returnIDTextBox.Text = newReturnID.ToString();
                employeeIDTextBox.Text = employeeID.ToString();
                return_dateTextBox.Text = DateTime.Now.ToString("MM/dd/yyyy");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void ReturnBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (decimal.Parse(amountOwedTxtbox.Text) > 0)
                {
                    MessageBox.Show("Please pay the entire total cost prior to making a return");
                }
                else if (!this.EachItemPaidInFull())
                {
                    MessageBox.Show("Furniture with item ID " + itemIDNotPaidInFull + ", has to be paid in full. Amount due has to be less than or equal to amount paid", 
                        "Item Not Paid In Full");
                }
                else
                {
                    this.ExecuteTheTransaction();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        /// <summary>
        /// Validate that each item is paid in full before executing a return
        /// </summary>
        /// <returns></returns>
        private bool EachItemPaidInFull()
        {
            foreach (DataGridViewRow row in return_itemDataGridView.Rows)
            {
                DataGridViewCell itemIDCell = row.Cells[0];
                DataGridViewCell amountRefundedCell = row.Cells[5];
                int itemID = int.Parse(itemIDCell.Value.ToString());
                decimal amount_refunded = decimal.Parse(amountRefundedCell.Value.ToString());
                if (amount_refunded < 0)
                {
                    itemIDNotPaidInFull = itemID;
                    return false;
                }
            }
            return true;
        }

        private void ExecuteTheTransaction()
        {
            DialogResult confirmationMessage = MessageBox.Show("Are you sure you want to return these items?", "Confirmation", MessageBoxButtons.YesNo);
            if (confirmationMessage == DialogResult.Yes)
            {
                ReturnTransaction returnTransaction = new ReturnTransaction();
                returnTransaction.EmployeeID = int.Parse(employeeIDTextBox.Text);
                if (!this.ReturnDateSet(returnTransaction))
                {
                    return_dateTextBox.Focus();
                }
                else if (UserController.ExecuteAReturn(returnTransaction, GetReturnItemsList()))
                {
                    MessageBox.Show("Return transaction was successful", "Executed return Transaction");
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("Error in DAL or initializing for return trans", "Return Trans not added");
                    this.DialogResult = DialogResult.Cancel;
                }
            }
            else
            {
                MessageBox.Show("Return transaction was not executed.", "Return Transaction Was Canceled");
            }
        }

        /// <summary>
        /// Sets the return date and checks its format
        /// </summary>
        /// <param name="returnTransaction"></param>
        /// <returns>true if return date has been set</returns>
        private bool ReturnDateSet(ReturnTransaction returnTransaction)
        {
            try
            {
                returnTransaction.ReturnDate = Convert.ToDateTime(return_dateTextBox.Text);
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show("Return date is not in the appropriate format. Please provide the appropriate format.", "Return date error.");
                return false;
            }
        }


        /// <summary>
        /// Gets the list of items to be returned from the datagridview cells
        /// The values from cells will have to be validated 
        /// </summary>
        /// <returns>list of items to be returned </returns>
        // TODO: The values from cells will have to be validated prior to assignment
        private List<ReturnItem> GetReturnItemsList()
        {
            List<ReturnItem> listOfReturnItems = new List<ReturnItem>();
            foreach (DataGridViewRow row in return_itemDataGridView.Rows)
            {
                ReturnItem returnItem = new ReturnItem();
                DataGridViewCell itemIDCell = row.Cells[0];
                DataGridViewCell returnIDCell = row.Cells[1];
                DataGridViewCell quantityCell = row.Cells[2];
                DataGridViewCell amountDueCell = row.Cells[3];
                DataGridViewCell amountPaidCell = row.Cells[4];
                DataGridViewCell amountRefundedCell = row.Cells[5];
                returnItem.ItemID = int.Parse(itemIDCell.Value.ToString());
                returnItem.ReturnID = int.Parse(returnIDCell.Value.ToString());
                returnItem.Quantity = int.Parse(quantityCell.Value.ToString());
                returnItem.Amount_due = decimal.Parse(amountDueCell.Value.ToString());
                returnItem.Amount_paid = decimal.Parse(amountPaidCell.Value.ToString());
                returnItem.Amount_refunded = decimal.Parse(amountRefundedCell.Value.ToString());
                listOfReturnItems.Add(returnItem);
            }
            return listOfReturnItems;
        }

        private void CancelReturnBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Calculates the totalcost, refund and the total paid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CalculateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                bool validAmountPaid = false;
                bool validQuantity = false;
                bool validReturnDate = false;
                decimal totalCost = 0;
                decimal totalPaid = 0;
                decimal amountOwed = 0;
                decimal totalRefund = 0;
                foreach (DataGridViewRow row in return_itemDataGridView.Rows)
                {
                    DataGridViewCell refundCell = row.Cells[5];
                    DataGridViewCell amountPaidCell = row.Cells[4];
                    DataGridViewCell amountDueCell = row.Cells[3];
                    DataGridViewCell quantity = row.Cells[2];
                    DataGridViewCell itemID = row.Cells[0];
                    validReturnDate = this.ValidateReturnDate(itemID);
                    validAmountPaid = this.ValidateAmountPaid(amountPaidCell, itemID);
                    validQuantity = this.ValidateTheQuantity(quantity, itemID);
                    if (!validAmountPaid || !validQuantity || !validReturnDate)
                    {
                        break;
                    }
                    amountDueCell.Value = this.GetAmountDue(itemID.Value.ToString(), quantity.Value.ToString());
                    decimal amountDue = decimal.Parse(amountDueCell.Value.ToString());
                    totalCost += amountDue;
                    decimal amountPaid = decimal.Parse(amountPaidCell.Value.ToString());
                    totalPaid += amountPaid;
                    decimal transactionBalance = decimal.Parse(amountPaidCell.Value.ToString()) - decimal.Parse(amountDueCell.Value.ToString());
                    if (decimal.Parse(amountPaidCell.Value.ToString()) < decimal.Parse(amountDueCell.Value.ToString()))
                    {
                        refund = transactionBalance;
                    }
                    else
                    {
                        refund += transactionBalance;
                    }
                    refundCell.Value = refund.ToString();
                    totalRefund += refund;
                }
                if (validQuantity && validAmountPaid && validReturnDate)
                {
                    this.SetTheTotals(totalPaid, totalCost, amountOwed);
                    this.SetTotalRefundBox(totalRefund);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        /// <summary>
        /// Allows to check that the return date is not before item rental date
        /// </summary>
        /// <param name="itemID">item id to validate return date</param>
        /// <returns>true if rental date is before return date</returns>
        private bool ValidateReturnDate(DataGridViewCell itemID)
        {
            try
            {
                DateTime rentalDate = UserController.GetRentalDate(int.Parse(itemID.Value.ToString()));
                int rentalDateToReturnDate = (int)(rentalDate - Convert.ToDateTime(return_dateTextBox.Text.ToString())).TotalDays;
                if (rentalDateToReturnDate < 1)
                {
                    return true;
                }
                else
                {
                    MessageBox.Show("For item with ID " + itemID.Value.ToString() + ", return date can not be before " + rentalDate.ToString() +
                        ", Please change the return date", "Return Date Invalid");
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " Please provide a return date in the appropriate format. ", "Return Date Is Invalid");
                return_dateTextBox.Clear();
                return_dateTextBox.Focus();
                return false;
            }
        }

        /// <summary>
        /// sets the value for the refund text box on the form
        /// </summary>
        /// <param name="totalRefund"> total refund of transaction</param>
        private void SetTotalRefundBox(decimal totalRefund)
        {
            if (decimal.Parse(amountOwedTxtbox.Text) > 0)
            {
                refundTxtbox.Text = "0.00";
            }
            else
            {
                refundTxtbox.Text = totalRefund.ToString();
            }
        }

        /// <summary>
        /// Sets the values in the total fields 
        /// </summary>
        /// <param name="totalPaid">paid value to help calculate the final cost/refund</param>
        /// <param name="totalCost">cost value to help calculate the final cost/refund</param>
        /// <param name="amountOwed">value to help determine balance</param>
        private void SetTheTotals(decimal totalPaid, decimal totalCost, decimal amountOwed)
        {
            try
            {
                if (totalCost > totalPaid)
                {
                    amountOwed += (totalCost - totalPaid);
                    amountOwedTxtbox.Text = amountOwed.ToString();
                }
                else
                {
                    amountOwedTxtbox.Text = "0.00";
                    decimal refund = totalPaid - totalCost;
                }
                totalCostTxtbox.Text = totalCost.ToString();
                returnBtn.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private bool ValidateAmountPaid(DataGridViewCell amountPaidCell, DataGridViewCell itemID)
        {
            try
            {
                decimal paidInput = decimal.Parse((amountPaidCell.Value.ToString()));
                if (string.IsNullOrEmpty(amountPaidCell.Value.ToString()))
                {
                    MessageBox.Show("For item with ID " + itemID.Value.ToString() + ", please enter a value for amount paid.", "Amount Paid Not Entered");
                    return false;
                }
                else if (paidInput < 0)
                {
                    MessageBox.Show("For item with ID " + itemID.Value.ToString() + ", please enter a non negative value for amount paid.", "Amount Paid Can Not Be Negative");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("For item with ID " + itemID.Value.ToString() + ", please provide a currency, eg. 8.00, for amount paid.", "Amount Paid Not Valid");
                amountPaidCell.Value = "0.00";
                return false;
            }
        }

        /// <summary>
        /// Validates the quantities in the dataGridView so that they do not exceed the original rented quantity
        /// </summary>
        /// <param name="quantity">quantity provided</param>
        /// <param name="itemID">ItemID for the item to return</param>
        /// <returns></returns>
        private bool ValidateTheQuantity(DataGridViewCell quantity, DataGridViewCell itemID)
        {
            int rentedQuantity = UserController.GetQuantityFromRental(int.Parse(itemID.Value.ToString()));
            int returnedQuantity = UserController.GetQuantityFromReturn(int.Parse(itemID.Value.ToString()));
            int remainingQuantity = rentedQuantity - returnedQuantity;
            try
            {
                if (int.Parse(quantity.Value.ToString()) > remainingQuantity)
                {
                    MessageBox.Show("For item with itemID " + itemID.Value.ToString() + ", the quantity can not be greater than " + remainingQuantity, "Quantity value error.");
                    quantity.Value = remainingQuantity.ToString();
                    return false;
                }
                else if (int.Parse(quantity.Value.ToString()) < 1)
                {
                    MessageBox.Show("For item with itemID " + itemID.Value.ToString() + ", the quantity can not be less than 1", "Quantity value error.");
                    quantity.Value = remainingQuantity.ToString();
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Please provide only numbers/digits for the quantities", "Quantity value error.");
                quantity.Value = rentedQuantity.ToString();
                return false;
            }
        }

        private void Return_itemDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 6)
                {
                    int rowIndex = e.RowIndex;
                    DataGridViewRow row = return_itemDataGridView.Rows[rowIndex];
                    DataGridViewCell cell = row.Cells[0];
                    int itemID = int.Parse(cell.Value.ToString());
                    using (ItemDetailsForm itemDetails = new ItemDetailsForm())
                    {
                        itemDetails.Tag = itemID;
                        itemDetails.ShowDialog();
                    }
                }
                else
                {
                    returnBtn.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void Return_itemDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            int row = e.RowIndex + 1;
            string errorMessage = "A data error occurred.\n" +
                "Row: " + row + "\n" +
                "Error: " + e.Exception.Message;
            MessageBox.Show(errorMessage, "Data Error");
        }

        private void Return_itemDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            returnBtn.Enabled = false;
        }

        private void Return_dateTextBox_TextChanged(object sender, EventArgs e)
        {
            returnBtn.Enabled = false;
        }
    }
}
