﻿using RentMe_G4.Controller;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace RentMe_G4.View
{
    /// <summary>
    /// This class creates a RentalItemsForm for viewing and selecting items from a rental transaction number
    /// </summary>
    public partial class RentalItemsForm : Form
    {
        public List<string> selectedItemsFromRental;

        /// <summary>
        /// Initializes the RentalItemsForm
        /// </summary>
        public RentalItemsForm()
        {
            this.selectedItemsFromRental = new List<string>();
            InitializeComponent();
        }

        private void RentalItemsForm_Load(object sender, EventArgs e)
        {
            try
            {
                rentalItemsAccept.Enabled = false;
                int rentalID = (int)this.Tag;
                RentalItemsRentalIDtxtbox.Text = this.Tag.ToString();
                this.rental_itemTableAdapter.GetItemsByRentalID(this._cs6232_g4DataSet.rental_item, rentalID);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void Rental_itemDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 4)
                {
                    bool itemDeselected = false;
                    int rowIndex = e.RowIndex;
                    DataGridViewRow row = rental_itemDataGridView.Rows[rowIndex];
                    DataGridViewCell cell = row.Cells[0];
                    int itemID = (int)cell.Value;
                    int rentedQuantity = UserController.GetQuantityFromRental(itemID);
                    int returnedQuantity = UserController.GetReturnedQuantity(itemID);
                    if (rentedQuantity - returnedQuantity <= 0)
                    {
                        MessageBox.Show("Sorry, you can not add this item to the return list because this item has been returned in the exact quantity.",
                            "Item Already Returned");
                    }
                    else
                    {
                        ListViewItem listViewItem = new ListViewItem(itemID.ToString());
                        for (int index = itemsListView.Items.Count - 1; index >= 0; index--)
                        {
                            if (itemID.ToString() == itemsListView.Items[index].Text.ToString())
                            {
                                itemsListView.Items[index].Remove();
                                itemDeselected = true;
                            }
                        }
                        if (!itemDeselected)
                        {
                            itemsListView.Items.Add(listViewItem);
                        }
                    }
                    
                }
                else if (e.ColumnIndex == 5)
                {
                    int rowIndex = e.RowIndex;
                    DataGridViewRow row = rental_itemDataGridView.Rows[rowIndex];
                    DataGridViewCell cell = row.Cells[0];
                    int itemID = (int)cell.Value;
                    using (ItemDetailsForm itemDetails = new ItemDetailsForm())
                    {
                        itemDetails.Tag = itemID;
                        itemDetails.ShowDialog();
                    }
                }
                this.HideAceptButton();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }

        /// <summary>
        /// Hides the accept button if no items are selected
        /// </summary>
        private void HideAceptButton()
        {
            try
            {
                if (itemsListView.Items.Count == 0)
                {
                    rentalItemsAccept.Enabled = false;
                }
                else
                {
                    rentalItemsAccept.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void RentalItemsAccept_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (ListViewItem item in itemsListView.Items)
                {
                    selectedItemsFromRental.Add(item.Text);
                }
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void RentalItemsCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void Rental_itemDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            int row = e.RowIndex + 1;
            string errorMessage = "A data error occurred.\n" +
                "Row: " + row + "\n" +
                "Error: " + e.Exception.Message;
            MessageBox.Show(errorMessage, "Data Error");
        }
    }
}
