﻿using RentMe_G4.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace RentMe_G4.DAL
{
    public class ItemDAL
    {
        /// <summary>
        /// Creates a new rental by insertain a transaction and rental items for the transaction
        /// </summary>
        /// <param name="orderList">The list of int arrays containing the serial and quantity</param>
        /// <param name="member_userID">The member this rental is for</param>
        /// <param name="employee_userID">The employee submiting this rental</param>
        /// <param name="daysToRent">The number of days from the current date to set the rental due date</param>
        public static void SubmitOrder(List<int[]> orderList, int member_userID, int employee_userID, int daysToRent)
        {
            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                string insertStatement =
                "INSERT INTO rental_transaction (member_userID, employee_userID, rental_date, due_date)"
                + "OUTPUT INSERTED.rentalID "
                + "VALUES(@memberID, @employeeID, GETDATE(), DATEADD(DAY, @daysToRent, GETDATE()))";

                int rentalID = -1;
                using (SqlCommand insertCommand = new SqlCommand(insertStatement, connection))
                {
                    SqlTransaction theTransaction = connection.BeginTransaction("CompleteRentalOrder");
                    try
                    {
                        insertCommand.Transaction = theTransaction;
                        insertCommand.Parameters.Add("@memberID", SqlDbType.Int).Value = member_userID;
                        insertCommand.Parameters.Add("@employeeID", SqlDbType.Int).Value = employee_userID;
                        insertCommand.Parameters.Add("@daysToRent", SqlDbType.Int).Value = daysToRent;
                        rentalID = (int)insertCommand.ExecuteScalar();
                        theTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                        theTransaction.Rollback();
                    }
                }

                if (rentalID >= 0) {
                    foreach (int[] order in orderList)
                    {
                        insertStatement =
                        "INSERT INTO rental_item (rentalID, serial_number, quantity) "
                        + "VALUES(@rentalID, @serial_number, @quantity)";

                        using (SqlCommand insertCommand = new SqlCommand(insertStatement, connection))
                        {
                            SqlTransaction theTransaction = connection.BeginTransaction("CompleteRentalOrder");
                            try
                            {
                                insertCommand.Transaction = theTransaction;
                                insertCommand.Parameters.Add("@rentalID", SqlDbType.Int).Value = rentalID;
                                insertCommand.Parameters.Add("@serial_number", SqlDbType.Int).Value = order[0];
                                insertCommand.Parameters.Add("@quantity", SqlDbType.Int).Value = order[1];
                                insertCommand.ExecuteScalar();
                                theTransaction.Commit();
                            }
                            catch (Exception e)
                            {
                                MessageBox.Show(e.Message);
                                theTransaction.Rollback();
                            }
                        }
                        string updateStatement =
                        "UPDATE furniture_item "
                        + "SET quantity = quantity - @quantity "
                        + "WHERE serial_number = @serial_number";

                        using (SqlCommand updateCommand = new SqlCommand(updateStatement, connection))
                        {
                            SqlTransaction theTransaction = connection.BeginTransaction("CompleteRentalOrder");
                            try
                            {
                                updateCommand.Transaction = theTransaction;
                                updateCommand.Parameters.Add("@serial_number", SqlDbType.Int).Value = order[0];
                                updateCommand.Parameters.Add("@quantity", SqlDbType.Int).Value = order[1];
                                updateCommand.ExecuteScalar();
                                theTransaction.Commit();
                            }
                            catch (Exception e)
                            {
                                MessageBox.Show(e.Message);
                                theTransaction.Rollback();
                            }
                        }
                    }
                }
                connection.Close();
            }
        }

        /// <summary>
        /// Returns a list of all furniture Items in inventory
        /// </summary>
        /// <returns>The list of furniture Items</returns>
        public static List<Item> GetListOfItems()
        {
            List<Item> itemList = new List<Item>();
            string selectStatement =
                "SELECT serial_number, description, f.categoryID, f.styleID, c.category_name, s.style_type, daily_rental_rate, daily_fine_rate, quantity " +
                "FROM furniture_item f " +
                "JOIN category c " +
                "ON f.categoryID = c.categoryID " +
                "JOIN style s " +
                "ON f.styleID = s.styleID " +
                "ORDER BY f.categoryID";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Item item = new Item
                            {
                                SerialNumber = (int)reader["serial_number"],
                                Description = reader["description"].ToString(),
                                CategoryID = (int)reader["categoryID"],
                                StyleID = (int)reader["styleID"],
                                Quantity = (int)reader["quantity"],
                                CategoryName = reader["category_name"].ToString(),
                                StyleType = reader["style_type"].ToString(),
                                DailyRentalRate = Convert.ToDouble(reader["daily_rental_rate"].ToString()),
                                DailyFineRate = Convert.ToDouble(reader["daily_fine_rate"].ToString())
                            };
                            itemList.Add(item);
                        }
                    }
                }
            }

            return itemList;
        }

        /// <summary>
        /// Returns the quantity of an item in stock by serial number
        /// </summary>
        /// <param name="serialNumber">The serial number to check</param>
        /// <returns>The quantity of an item in stock by serial number</returns>
        public int GetQuantityBySerial(int serialNumber)
        {
            int quantity = -1;

            string selectStatement =
                "SELECT quantity " +
                "FROM [furniture_item]  " +
                "WHERE serial_number = @serial_number";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@serial_number", serialNumber);
                    using (SqlDataReader reader = selectCommand.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        while (reader.Read())
                        {
                            quantity = (int)reader["quantity"];
                        }
                    }
                }
            }
            return quantity;
        }
    }
}
