﻿using RentMe_G4.Model;
using RentMe_G4.Controller;
using System;
using System.Windows.Forms;

namespace RentMe_G4.View
{
    /// <summary>
    /// This class sets up the form to either edit a member or register a customer
    /// </summary>
    public partial class MemberForm : Form
    {
        public bool addMember;
        public User user;
        public int userID;
        public string userPhone;
        private string originalZip;
        private bool errorAddingLocation;

        /// <summary>
        /// This method allows to initialize the form component
        /// </summary>
        public MemberForm()
        {
            this.errorAddingLocation = false;
            InitializeComponent();
        }

        private void userBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.UpdateTheMember();
        }

        private void UpdateTheMember()
        {
            try
            {
                this.Validate();
                this.userBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this._cs6232_g4DataSet);
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
                this.DialogResult = DialogResult.Cancel;
            }
        }

        private void MemberForm_Load(object sender, EventArgs e)
        {
            this.stateTableAdapter.Fill(this._cs6232_g4DataSet1.state);
            this.SetUpAddEditForm();
            this.SetSexCombobox();
        }

        private void SetSexCombobox()
        {
            try
            {
                sexComboBox.Items.Add("Male");
                sexComboBox.Items.Add("Female");
                if (sexTextBox.Text == "M" || sexTextBox.Text == "")
                {
                    sexComboBox.SelectedIndex = 0;
                }
                else
                {
                    sexComboBox.SelectedIndex = 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }

        private void SetUpAddEditForm()
        {
            try
            {
                if (addMember)
                {
                    this.RegisterACustomer();
                }
                else
                {
                    memberFormHeading.Text = "Update Customer";
                    this.EditCurrentMember();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }

        private void EditCurrentMember()
        {
            try
            {
                this.userTableAdapter.FillByUserID(this._cs6232_g4DataSet.user, userID);
                originalZip = zipTextBox.Text;
                this.locationTableAdapter.FillByZip(this._cs6232_g4DataSet.location, originalZip);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void RegisterACustomer()
        {
            try
            {
                this.date_of_birthDateTimePicker.Value = DateTime.Parse("1/1/1753");
                this.userBindingSource.AddNew();
                roleIDTextBox.Text = "3";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void AcceptMemberFormButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (date_of_birthDateTimePicker.Value == DateTime.Parse("1/1/1753"))
                {
                    MessageBox.Show("Please select a date before accepting.", "Date Selection Error");
                    date_of_birthDateTimePicker.Focus();
                }
                else if (AllFieldsValid())
                {
                    if (addMember)
                    {
                        this.userPhone = phone_numberTextBox.Text;
                    }
                    this.AddNewLocation();
                    if (!errorAddingLocation)
                    {
                        this.UpdateTheMember();
                    }
                    else
                    {
                        MessageBox.Show("Please provide the appropriate values for your address.", "Address Input Error");
                        cityTextBox.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        /// <summary>
        /// Adds the new location values to database
        /// </summary>
        private void AddNewLocation()
        {
            bool zipInDatabase = false;
            foreach (string zip in LocationController.GetListOfZip())
            {
                if (zip == zipLocationLabel.Text)
                {
                    zipInDatabase = true;
                }
            }
            if (!zipInDatabase)
            {
                Location location = new Location();
                location.Zip = zipLocationLabel.Text;
                location.City = cityTextBox.Text;
                location.State = stateComboBox.SelectedValue.ToString();
                if (LocationController.AddLocation(location))
                {
                    errorAddingLocation = false;
                }
                else
                {
                    errorAddingLocation = true;
                }
            }
        }

        /// <summary>
        /// Checks that all required fields are valid
        /// </summary>
        /// <returns> Returns true if all fields are valid</returns>
        private bool AllFieldsValid()
        {

            if (Validator.IsPresent(userIDTextBox) &&
                Validator.IsPresent(address1TextBox) &&
                Validator.IsPresent(roleIDTextBox) &&
                Validator.IsPresent(cityTextBox) &&
                Validator.IsPresent(first_nameTextBox) &&
                Validator.IsPresent(last_nameTextBox) &&
                Validator.IsPresent(phone_numberTextBox) &&
                Validator.IsPresent(zipTextBox) &&
                Validator.IsPresent(sexTextBox))
            {
                if (Validator.IsPhoneNumber(phone_numberTextBox))
                {
                    if (Validator.IsGender(sexTextBox))
                    {
                        if (Validator.IsZipCode(zipTextBox))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        private void CancelMemberFormButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }

        private void SexComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (sexComboBox.SelectedIndex == 0)
                {
                    sexTextBox.Text = "M";
                }
                else
                {
                    sexTextBox.Text = "F";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void ZipTextBox_TextChanged(object sender, EventArgs e)
        {
            zipLocationLabel.Text = zipTextBox.Text;
        }
    }
}
