﻿namespace RentMe_G4.UserControls
{
    partial class EmployeeRentalsUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UserCartInformationTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.MemberUserIDLookupTextBox = new System.Windows.Forms.TextBox();
            this.MemberUserIDLookupLabel = new System.Windows.Forms.Label();
            this.CartMessageLabel = new System.Windows.Forms.Label();
            this.CreateMemberCartButton = new System.Windows.Forms.Button();
            this.ReviewCartButton = new System.Windows.Forms.Button();
            this.FurnitureItemListView = new System.Windows.Forms.ListView();
            this.AddFurnitureTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.AddToCartButton = new System.Windows.Forms.Button();
            this.ItemToAddQuantityTextBox = new System.Windows.Forms.TextBox();
            this.QuantityToAddLabel = new System.Windows.Forms.Label();
            this.SerialNumberToAddTextBox = new System.Windows.Forms.TextBox();
            this.SerialNumberToAddLabel = new System.Windows.Forms.Label();
            this.UserCartInformationTableLayout.SuspendLayout();
            this.AddFurnitureTableLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // UserCartInformationTableLayout
            // 
            this.UserCartInformationTableLayout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UserCartInformationTableLayout.ColumnCount = 7;
            this.UserCartInformationTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.UserCartInformationTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.UserCartInformationTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 104F));
            this.UserCartInformationTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.UserCartInformationTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.UserCartInformationTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.UserCartInformationTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 152F));
            this.UserCartInformationTableLayout.Controls.Add(this.MemberUserIDLookupTextBox, 1, 0);
            this.UserCartInformationTableLayout.Controls.Add(this.MemberUserIDLookupLabel, 0, 0);
            this.UserCartInformationTableLayout.Controls.Add(this.CartMessageLabel, 6, 0);
            this.UserCartInformationTableLayout.Controls.Add(this.CreateMemberCartButton, 2, 0);
            this.UserCartInformationTableLayout.Controls.Add(this.ReviewCartButton, 5, 0);
            this.UserCartInformationTableLayout.Location = new System.Drawing.Point(3, 3);
            this.UserCartInformationTableLayout.Name = "UserCartInformationTableLayout";
            this.UserCartInformationTableLayout.RowCount = 1;
            this.UserCartInformationTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.UserCartInformationTableLayout.Size = new System.Drawing.Size(822, 42);
            this.UserCartInformationTableLayout.TabIndex = 0;
            // 
            // MemberUserIDLookupTextBox
            // 
            this.MemberUserIDLookupTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.MemberUserIDLookupTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MemberUserIDLookupTextBox.Location = new System.Drawing.Point(137, 9);
            this.MemberUserIDLookupTextBox.Name = "MemberUserIDLookupTextBox";
            this.MemberUserIDLookupTextBox.Size = new System.Drawing.Size(36, 23);
            this.MemberUserIDLookupTextBox.TabIndex = 0;
            // 
            // MemberUserIDLookupLabel
            // 
            this.MemberUserIDLookupLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.MemberUserIDLookupLabel.AutoSize = true;
            this.MemberUserIDLookupLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MemberUserIDLookupLabel.Location = new System.Drawing.Point(55, 12);
            this.MemberUserIDLookupLabel.Name = "MemberUserIDLookupLabel";
            this.MemberUserIDLookupLabel.Size = new System.Drawing.Size(76, 17);
            this.MemberUserIDLookupLabel.TabIndex = 1;
            this.MemberUserIDLookupLabel.Text = "Member ID";
            // 
            // CartMessageLabel
            // 
            this.CartMessageLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.CartMessageLabel.AutoSize = true;
            this.CartMessageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CartMessageLabel.Location = new System.Drawing.Point(679, 12);
            this.CartMessageLabel.Name = "CartMessageLabel";
            this.CartMessageLabel.Size = new System.Drawing.Size(140, 17);
            this.CartMessageLabel.TabIndex = 2;
            this.CartMessageLabel.Text = "No Member Selected";
            // 
            // CreateMemberCartButton
            // 
            this.CreateMemberCartButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CreateMemberCartButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateMemberCartButton.Location = new System.Drawing.Point(179, 3);
            this.CreateMemberCartButton.Name = "CreateMemberCartButton";
            this.CreateMemberCartButton.Size = new System.Drawing.Size(98, 36);
            this.CreateMemberCartButton.TabIndex = 1;
            this.CreateMemberCartButton.Text = "New Cart";
            this.CreateMemberCartButton.UseVisualStyleBackColor = true;
            this.CreateMemberCartButton.Click += new System.EventHandler(this.CreateMemberCartButton_Click);
            // 
            // ReviewCartButton
            // 
            this.ReviewCartButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReviewCartButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReviewCartButton.Location = new System.Drawing.Point(573, 3);
            this.ReviewCartButton.Name = "ReviewCartButton";
            this.ReviewCartButton.Size = new System.Drawing.Size(94, 36);
            this.ReviewCartButton.TabIndex = 6;
            this.ReviewCartButton.Text = "ReviewCart";
            this.ReviewCartButton.UseVisualStyleBackColor = true;
            this.ReviewCartButton.Click += new System.EventHandler(this.ReviewCartButton_Click);
            // 
            // FurnitureItemListView
            // 
            this.FurnitureItemListView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FurnitureItemListView.HideSelection = false;
            this.FurnitureItemListView.Location = new System.Drawing.Point(4, 121);
            this.FurnitureItemListView.Name = "FurnitureItemListView";
            this.FurnitureItemListView.Size = new System.Drawing.Size(818, 221);
            this.FurnitureItemListView.TabIndex = 1;
            this.FurnitureItemListView.UseCompatibleStateImageBehavior = false;
            this.FurnitureItemListView.ItemActivate += new System.EventHandler(this.FurnitureItemListView_ItemActivate);
            // 
            // AddFurnitureTableLayout
            // 
            this.AddFurnitureTableLayout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AddFurnitureTableLayout.ColumnCount = 5;
            this.AddFurnitureTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.AddFurnitureTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.AddFurnitureTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.AddFurnitureTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.92344F));
            this.AddFurnitureTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.07655F));
            this.AddFurnitureTableLayout.Controls.Add(this.AddToCartButton, 4, 0);
            this.AddFurnitureTableLayout.Controls.Add(this.ItemToAddQuantityTextBox, 3, 0);
            this.AddFurnitureTableLayout.Controls.Add(this.QuantityToAddLabel, 2, 0);
            this.AddFurnitureTableLayout.Controls.Add(this.SerialNumberToAddTextBox, 1, 0);
            this.AddFurnitureTableLayout.Controls.Add(this.SerialNumberToAddLabel, 0, 0);
            this.AddFurnitureTableLayout.Location = new System.Drawing.Point(4, 48);
            this.AddFurnitureTableLayout.Name = "AddFurnitureTableLayout";
            this.AddFurnitureTableLayout.RowCount = 1;
            this.AddFurnitureTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.AddFurnitureTableLayout.Size = new System.Drawing.Size(819, 39);
            this.AddFurnitureTableLayout.TabIndex = 2;
            // 
            // AddToCartButton
            // 
            this.AddToCartButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AddToCartButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddToCartButton.Location = new System.Drawing.Point(666, 3);
            this.AddToCartButton.Name = "AddToCartButton";
            this.AddToCartButton.Size = new System.Drawing.Size(150, 33);
            this.AddToCartButton.TabIndex = 5;
            this.AddToCartButton.Text = "Add To Cart";
            this.AddToCartButton.UseVisualStyleBackColor = true;
            this.AddToCartButton.Click += new System.EventHandler(this.AddToCartButton_Click);
            // 
            // ItemToAddQuantityTextBox
            // 
            this.ItemToAddQuantityTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ItemToAddQuantityTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemToAddQuantityTextBox.Location = new System.Drawing.Point(618, 8);
            this.ItemToAddQuantityTextBox.Name = "ItemToAddQuantityTextBox";
            this.ItemToAddQuantityTextBox.Size = new System.Drawing.Size(42, 23);
            this.ItemToAddQuantityTextBox.TabIndex = 4;
            // 
            // QuantityToAddLabel
            // 
            this.QuantityToAddLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.QuantityToAddLabel.AutoSize = true;
            this.QuantityToAddLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuantityToAddLabel.Location = new System.Drawing.Point(551, 11);
            this.QuantityToAddLabel.Name = "QuantityToAddLabel";
            this.QuantityToAddLabel.Size = new System.Drawing.Size(61, 17);
            this.QuantityToAddLabel.TabIndex = 2;
            this.QuantityToAddLabel.Text = "Quantity";
            // 
            // SerialNumberToAddTextBox
            // 
            this.SerialNumberToAddTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.SerialNumberToAddTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SerialNumberToAddTextBox.Location = new System.Drawing.Point(403, 8);
            this.SerialNumberToAddTextBox.Name = "SerialNumberToAddTextBox";
            this.SerialNumberToAddTextBox.Size = new System.Drawing.Size(134, 23);
            this.SerialNumberToAddTextBox.TabIndex = 3;
            // 
            // SerialNumberToAddLabel
            // 
            this.SerialNumberToAddLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.SerialNumberToAddLabel.AutoSize = true;
            this.SerialNumberToAddLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SerialNumberToAddLabel.Location = new System.Drawing.Point(299, 11);
            this.SerialNumberToAddLabel.Name = "SerialNumberToAddLabel";
            this.SerialNumberToAddLabel.Size = new System.Drawing.Size(98, 17);
            this.SerialNumberToAddLabel.TabIndex = 4;
            this.SerialNumberToAddLabel.Text = "Serial Number";
            // 
            // EmployeeRentalsUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.AddFurnitureTableLayout);
            this.Controls.Add(this.FurnitureItemListView);
            this.Controls.Add(this.UserCartInformationTableLayout);
            this.Name = "EmployeeRentalsUserControl";
            this.Size = new System.Drawing.Size(828, 345);
            this.Load += new System.EventHandler(this.EmployeeRentalsUserControl_Load);
            this.UserCartInformationTableLayout.ResumeLayout(false);
            this.UserCartInformationTableLayout.PerformLayout();
            this.AddFurnitureTableLayout.ResumeLayout(false);
            this.AddFurnitureTableLayout.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel UserCartInformationTableLayout;
        private System.Windows.Forms.TextBox MemberUserIDLookupTextBox;
        private System.Windows.Forms.Label MemberUserIDLookupLabel;
        private System.Windows.Forms.Label CartMessageLabel;
        private System.Windows.Forms.Button CreateMemberCartButton;
        private System.Windows.Forms.Button ReviewCartButton;
        private System.Windows.Forms.ListView FurnitureItemListView;
        private System.Windows.Forms.TableLayoutPanel AddFurnitureTableLayout;
        private System.Windows.Forms.Button AddToCartButton;
        private System.Windows.Forms.TextBox ItemToAddQuantityTextBox;
        private System.Windows.Forms.Label QuantityToAddLabel;
        private System.Windows.Forms.TextBox SerialNumberToAddTextBox;
        private System.Windows.Forms.Label SerialNumberToAddLabel;
    }
}
