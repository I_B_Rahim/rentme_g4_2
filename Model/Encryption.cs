﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace RentMe_G4.Model
{
    /// <summary>
    /// This class handles encryption and decryption of credentials
    /// </summary>
    public class Encryption
    {
        // Normaly this key would be stored securely outside of the program
        // and database where the password is stored.
        private static string key = "W+T7RPm4QOcSWSBeGw4xHLaBjFBvG2Lt";

        /// <summary>
        /// This encrypts a password using a key and IV
        /// </summary>
        /// <param name="password">The password in plaintext to encrypt</param>
        /// <returns>The encrypted password</returns>
        public static string EncryptPassword(string password)
        {
            // Check arguments.
            if (password == null || password.Length <= 0)
                throw new ArgumentNullException("password should not be empty");

            byte[] encrypted;
            byte[] IV = new byte[16];

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Encoding.UTF8.GetBytes(key);
                aesAlg.IV = IV;

                // Create an encryptor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(password);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Return the encrypted bytes from the memory stream.
            return Convert.ToBase64String(encrypted);
        }

        /// <summary>
        /// This decrypts a password using a key and IV
        /// </summary>
        /// <param name="password">The encrypted password to compare</param>
        /// <returns>The decrypted password</returns>
        public static string DecryptPassword(string password)
        {
            // Check arguments.
            if (password == null || password.Length <= 0)
                throw new ArgumentNullException("password should not be empty");

            byte[] IV = new byte[16];
            byte[] cipherText = Convert.FromBase64String(password);

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Encoding.UTF8.GetBytes(key);
                aesAlg.IV = IV;

                // Create a decryptor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            return srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
        }
    }
}
