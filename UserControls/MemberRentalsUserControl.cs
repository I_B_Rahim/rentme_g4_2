﻿using System;

using System.Windows.Forms;
using RentMe_G4.View;

namespace RentMe_G4.UserControls
{
    /// <summary>
    /// This class created the MemberREntalsUserControl to allow an employee to view a members rentals and start a return
    /// </summary>
    public partial class MemberRentalsUserControl : UserControl
    {
        public string memberID;
        private string additionalItemsToAdd;

        /// <summary>
        /// Initializes the MemberRentalsUserControl
        /// </summary>
        public MemberRentalsUserControl()
        {
            InitializeComponent();
        }

        private void MemberRentalsUserControl_Load(object sender, EventArgs e)
        {
            try
            {
                this.SetUpMemberRentalFieldsAndDataGridView();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        /// <summary>
        /// This method allows to setup the values for the member info fields and the data for the data grid view
        /// </summary>
        public void SetUpMemberRentalFieldsAndDataGridView()
        {
            if (string.IsNullOrEmpty(memberID))
            {
                makeReturnBtn.Enabled = false;
                memberErrorlabel.Visible = true;
                this.userTableAdapter.FillBy(this._cs6232_g4DataSet.user, 0);
                this.rental_transactionTableAdapter.GetRentalsByMemberID(this._cs6232_g4DataSet.rental_transaction, 0);
            }
            else
            {
                memberErrorlabel.Visible = false;
                this.userTableAdapter.FillBy(this._cs6232_g4DataSet.user, ((int)(System.Convert.ChangeType(memberID, typeof(int)))));
                this.rental_transactionTableAdapter.GetRentalsByMemberID(this._cs6232_g4DataSet.rental_transaction, ((int)(System.Convert.ChangeType(memberID, typeof(int)))));
            }
        }

        private void Rental_transactionDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 5)
                {
                    int rowIndex = e.RowIndex;
                    DataGridViewRow row = rental_transactionDataGridView.Rows[rowIndex];
                    DataGridViewCell cell = row.Cells[0];
                    int rentalID = (int)cell.Value;
                    using (RentalItemsForm rentalItemsForm = new RentalItemsForm())
                    {
                        rentalItemsForm.Tag = rentalID;
                        DialogResult result = rentalItemsForm.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            this.AddRentalItemsSelectedItemsToAllItems(rentalItemsForm);
                            if (!string.IsNullOrEmpty(this.additionalItemsToAdd))
                            {
                                MessageBox.Show("Items ID: " + this.additionalItemsToAdd + " have been added to the list.", "Items Added Successfully");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Additional items weren't added to the list", "No New Items Added");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }

        private void AddRentalItemsSelectedItemsToAllItems(RentalItemsForm rentalItemsForm)
        {
            try
            {
                this.additionalItemsToAdd = "";
                string redundantItems = "";
                bool itemAlreadyInList = false;
                foreach (string item in rentalItemsForm.selectedItemsFromRental)
                {

                    for (int index = allItemsListView.Items.Count - 1; index >= 0; index--)
                    {
                        if (item == allItemsListView.Items[index].Text.ToString())
                        {
                            itemAlreadyInList = true;
                        }
                    }
                    if (!itemAlreadyInList)
                    {
                        this.additionalItemsToAdd += item + ", ";
                        ListViewItem listViewItem = new ListViewItem(item);
                        allItemsListView.Items.Add(listViewItem);
                        makeReturnBtn.Enabled = true;
                    }
                    else
                    {
                        redundantItems += item + ", ";
                    }
                }
                if (!string.IsNullOrEmpty(redundantItems))
                {
                    MessageBox.Show("Items with ID: " + redundantItems + " are already added");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }

        private void Rental_transactionDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            int row = e.RowIndex + 1;
            string errorMessage = "A data error occurred.\n" +
                "Row: " + row + "\n" +
                "Error: " + e.Exception.Message;
            MessageBox.Show(errorMessage, "Data Error");
        }

        private void ClearListBtn_Click(object sender, EventArgs e)
        {
            this.ClearListViewItems();
        }

        /// <summary>
        /// Allows to clear the listView items and sets the make return button enable property to false
        /// </summary>
        public void ClearListViewItems()
        {
            try
            {
                allItemsListView.Clear();
                makeReturnBtn.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void MakeReturnBtn_Click(object sender, EventArgs e)
        {
            try
            {
                using (ReturnItemsForm returnItemsForm = new ReturnItemsForm())
                {
                    for (int index = allItemsListView.Items.Count - 1; index >= 0; index--)
                    {
                        returnItemsForm.listOfItemsToReturn.Add(allItemsListView.Items[index].Text.ToString());
                    }
                    DialogResult result = returnItemsForm.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        this.ClearListViewItems();
                        MessageBox.Show("The Items have been successfully returned", "Items Returned Successfully");
                    }
                    else
                    {
                        MessageBox.Show("The Items have not been returned", "No Items returned");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void RemoveItemBtn_Click(object sender, EventArgs e)
        {
            if (allItemsListView.SelectedItems.Count > 0)
            {
                int itemID = int.Parse(allItemsListView.SelectedItems[0].Text);
                allItemsListView.SelectedItems[0].Remove();
                MessageBox.Show("The item with id " + itemID + ", has been removed from the list");
            }
            else
            {
                MessageBox.Show("Please select an Item in the list before clicking to remove");
            }
            
        }
    }
}
