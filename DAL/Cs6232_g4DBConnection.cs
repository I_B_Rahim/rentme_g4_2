﻿using System.Data.SqlClient;

namespace RentMe_G4.DAL
{
    /// <summary>
    /// Allows to establish a connection to the database cs6232-g4
    /// </summary>
    public class Cs6232_g4DBConnection
    {
        /// <summary>
        /// Establishes a connection to the cs6232-g4 database
        /// </summary>
        /// <returns>Returns a connection to the database cs6232-g4</returns>
        public static SqlConnection GetConnection()
        {
            string connectionString =
                "Data Source=localhost;Initial Catalog=cs6232-g4;" +
                "Integrated Security=True";


            SqlConnection connection = new SqlConnection(connectionString);
            return connection;
        }
        
    }
}
