﻿using RentMe_G4.DAL;
using RentMe_G4.Model;
using System.Collections.Generic;

namespace RentMe_G4.Controller
{
    ///<summary>
    /// The controller class for accessing locationDAL methods
    /// </summary>
    public class LocationController
    {

        /// <summary>
        /// Gets the list of zip codes available to use
        /// </summary>
        /// <returns>List of zipcodes</returns>
        public static List<string> GetListOfZip()
        {
            return LocationDAL.GetListOfZip();
        }

        /// <summary>
        /// Returns Location by the zip
        /// </summary>
        /// <param name="zip">the zip for the location</param>
        /// <returns>location</returns>
        public static Location GetLocationByZip(string zip)
        {
            return LocationDAL.GetLocationByZip(zip);
        }

        /// <summary>
        /// Adds an Location to the  database and returns true if added or false otherwise
        /// </summary>
        /// <param name="location">the lcoation to be added</param>
        /// <returns>true if location was added, false otherwise</returns>
        public static bool AddLocation(Location location)
        {
            return LocationDAL.AddLocation(location);
        }
    }
}
