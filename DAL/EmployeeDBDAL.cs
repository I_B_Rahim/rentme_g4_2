﻿using RentMe_G4.DAL;
using RentMe_G4.Model;
using System.Data.SqlClient;

namespace RentMeG4.DAL
{
    /// <summary>
    /// Data access layer class for employees
    /// </summary>
    class EmployeeDBDAL
    {
        /// <summary>
        /// Returns the employee found by username and password
        /// </summary>
        /// <param name="theUserName">The username to match</param>
        /// <param name="thePassword">The password to match</param>
        /// <returns>The employee found by username and password</returns>
	    public Employee GetEmployeeByUsernameAndPassword(string theUserName, string thePassword)
        {
            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                SqlParameter userName = new SqlParameter("@userName", System.Data.SqlDbType.VarChar)
                {
                    Value = theUserName
                };
                SqlParameter password = new SqlParameter("@password", System.Data.SqlDbType.VarChar)
                {
                    Value = thePassword
                    //Value = "0xF5CBC3CA8ACEAD07046F36E4F652EE4F35C60CD0B4D3A4090A409E61C690BE15"
                };
                string selectStatement =
                    "SELECT * " +
                    "FROM credential AS c " +
                        "INNER JOIN [user] AS u ON c.userID = u.userID " +
                    "WHERE c.userName = @userName AND c.password = @password AND c.is_active = 1";
                SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
                selectCommand.Parameters.Add(userName);
                selectCommand.Parameters.Add(password);
                connection.Open();
                SqlDataReader reader = selectCommand.ExecuteReader();

                while (reader.Read())
                {
                    if ((int)reader["roleID"] == 2) {
                        Employee theEmployee = new Employee((int)reader["userID"],
                                (int)reader["roleID"],
                                reader["userName"].ToString(),
                                reader["first_name"].ToString(),
                                reader["last_name"].ToString());
                        return theEmployee;
                    }
                }
            }
            return null;
        }
    }
}
