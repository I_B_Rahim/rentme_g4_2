﻿using System;
using System.Windows.Forms;

namespace RentMe_G4.View
{
    /// <summary>
    /// Stores the methods to validate the input fields in a form
    /// </summary>
    public class Validator
    {
        /// <summary>
        /// Returns if a control is present
        /// </summary>
        /// <param name="control">The control to validate</param>
        /// <returns>If a control is present</returns>
        public static bool IsPresent(Control control)
        {
            if (control.GetType().ToString() == "System.Windows.Forms.TextBox")
            {
                TextBox textBox = (TextBox)control;
                if (textBox.Text == "")
                {
                    MessageBox.Show(textBox.Tag.ToString() + " is a required field.", "Input Error");
                    textBox.Focus();
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (control.GetType().ToString() == "System.Windows.Forms.ComboBox")
            {
                ComboBox comboBox = (ComboBox)control;
                if (comboBox.SelectedIndex == -1)
                {
                    MessageBox.Show(comboBox.Tag.ToString() + " is a required field.", "Input Error");
                    comboBox.Focus();
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return true;
        }

        /// <summary>
        /// validates the phone number textbox
        /// </summary>
        /// <param name="textBox">textbox to check</param>
        /// <returns>true if valid phone number</returns>
        public static bool IsPhoneNumber(TextBox textBox)
        {
            try
            {
                long phoneNumber = Convert.ToInt64(textBox.Text);
                if (phoneNumber.ToString().Length == 10)
                {
                    return true;
                }
                else
                {
                    MessageBox.Show(textBox.Tag.ToString() + " has to be exactly 10 digits ", "Invalid Phone Number");
                    return false;
                }
                
            }
            catch (FormatException)
            {
                MessageBox.Show(textBox.Tag.ToString() + " is not valid. Please enter only numbers and try again.", "Phone Number Error");
                textBox.Focus();
                return false;
            }
        }

        /// <summary>
        /// validates the zipcode textbox
        /// </summary>
        /// <param name="textBox">textbox to validate</param>
        /// <returns>true if zipcode is valid, false otherwise</returns>
        public static bool IsZipCode(TextBox textBox)
        {
            try
            {
                int zip = int.Parse(textBox.Text);
                if (textBox.Text.Length == 5)
                {
                    return true;
                }
                else
                {
                    MessageBox.Show(textBox.Tag.ToString() + " has to be exactly 5 digits ", "Invalid Zipcode");
                    return false;
                }

            }
            catch (FormatException)
            {
                MessageBox.Show(textBox.Tag.ToString() + " is not valid. Please enter only 5 numbers and try again.", "Zipcode Error");
                textBox.Focus();
                return false;
            }
        }

        /// <summary>
        /// Validates the Gender textbox
        /// </summary>
        /// <param name="textBox">textbox to validate</param>
        /// <returns>true if gender is valid and false otherwise</returns>
        public static bool IsGender(TextBox textBox)
        {

            try
            {
                if (textBox.Text.Length == 1)
                {
                    if (textBox.Text == "M" || textBox.Text == "F")
                    {
                        return true;
                    }
                    else
                    {
                        MessageBox.Show(textBox.Tag.ToString() + " is not valid. Please enter either Uppercase M or F. and try again.", "Invalid Sex");
                        textBox.Focus();
                        return false;
                    }
                    
                }
                else
                {
                    MessageBox.Show(textBox.Tag.ToString() + " has to be uppercase M or F", "Invalid Phone Number");
                    return false;
                }

            }
            catch (FormatException)
            {
                MessageBox.Show(textBox.Tag.ToString() + " is not valid. Please enter either Uppercase M or F.", "Sex Error");
                textBox.Focus();
                return false;
            }
        }
    }
}
