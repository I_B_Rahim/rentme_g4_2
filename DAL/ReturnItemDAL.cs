﻿
using System.Data.SqlClient;

namespace RentMe_G4.DAL
{
    ///<summary>
    /// The DAL that interacts with returnItem object
    /// </summary>
    public class ReturnItemDAL
    {

        /// <summary>
        /// Gets the serial number of the item 
        /// </summary>
        /// <param name="itemID">itemID to get serial number</param>
        /// <returns>serial number</returns>
        public static int GetSerialNumberByItemID(int itemID)
        {
            int serialNumber = 0;
            string selectStatement =
                "SELECT TOP 1 rei.serial_number " +
                "FROM rental_item rei " +
                "WHERE rei.itemID = @itemID ";

            using (SqlConnection connection = Cs6232_g4DBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@itemID", itemID);
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            serialNumber = (int)reader["serial_number"];
                        }
                    }
                }
            }
            return serialNumber;
        }
    }
}
