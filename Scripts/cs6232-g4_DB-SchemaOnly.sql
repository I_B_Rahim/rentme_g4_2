﻿USE [master]
GO
/****** Object:  Database [cs6232-g4]    Script Date: 4/26/2020 1:48:07 PM ******/
CREATE DATABASE [cs6232-g4]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'cs6232-g4', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\cs6232-g4.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'cs6232-g4_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\cs6232-g4_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [cs6232-g4] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [cs6232-g4].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [cs6232-g4] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [cs6232-g4] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [cs6232-g4] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [cs6232-g4] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [cs6232-g4] SET ARITHABORT OFF 
GO
ALTER DATABASE [cs6232-g4] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [cs6232-g4] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [cs6232-g4] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [cs6232-g4] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [cs6232-g4] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [cs6232-g4] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [cs6232-g4] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [cs6232-g4] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [cs6232-g4] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [cs6232-g4] SET  DISABLE_BROKER 
GO
ALTER DATABASE [cs6232-g4] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [cs6232-g4] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [cs6232-g4] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [cs6232-g4] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [cs6232-g4] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [cs6232-g4] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [cs6232-g4] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [cs6232-g4] SET RECOVERY FULL 
GO
ALTER DATABASE [cs6232-g4] SET  MULTI_USER 
GO
ALTER DATABASE [cs6232-g4] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [cs6232-g4] SET DB_CHAINING OFF 
GO
ALTER DATABASE [cs6232-g4] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [cs6232-g4] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [cs6232-g4] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'cs6232-g4', N'ON'
GO
ALTER DATABASE [cs6232-g4] SET QUERY_STORE = OFF
GO
USE [cs6232-g4]
GO
/****** Object:  UserDefinedFunction [dbo].[getReturnAmountDue]    Script Date: 4/26/2020 1:48:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jake Spain
-- Create date: 2/23/2020
-- Description:	Calculates the amount due for a returned item
-- =============================================
CREATE FUNCTION [dbo].[getReturnAmountDue] 
(
	-- Add the parameters for the function here
	@transactionID int,
	@serial_number int,
	@return_date datetime,
	@quantity int
)
RETURNS decimal(9, 2)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @due_date datetime
	DECLARE @return_days_difference int
	DECLARE @daily_fine_rate decimal(9, 2)
	DECLARE @amount_due decimal(9, 2)

	-- Add the T-SQL statements to compute the return value here
	SET @due_date = (
	  SELECT due_date
      FROM rental
      WHERE transactionID = @transactionID
      AND serial_number = @serial_number
	)

	SET @return_days_difference = DATEDIFF(DAY, @due_date, @return_date)

	SET @daily_fine_rate = (
	  SELECT daily_fine_rate
      FROM furniture_kind fk
      JOIN furniture_item fi
      ON fk.furnitureID = fi.furnitureID
      WHERE fi.serial_number = @serial_number
	)

	SET @amount_due = @return_days_difference * @daily_fine_rate * @quantity

	-- Return the result of the function
	RETURN @amount_due

END
GO
/****** Object:  Table [dbo].[category]    Script Date: 4/26/2020 1:48:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[category](
	[categoryID] [int] IDENTITY(1,1) NOT NULL,
	[category_name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_category] PRIMARY KEY CLUSTERED 
(
	[categoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[credential]    Script Date: 4/26/2020 1:48:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[credential](
	[userID] [int] NOT NULL,
	[username] [varchar](50) NOT NULL,
	[password] [varchar](100) NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_credential] PRIMARY KEY CLUSTERED 
(
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [IX_credential_username] UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[furniture_item]    Script Date: 4/26/2020 1:48:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[furniture_item](
	[serial_number] [int] NOT NULL,
	[description] [varchar](50) NOT NULL,
	[categoryID] [int] NOT NULL,
	[styleID] [int] NOT NULL,
	[daily_rental_rate] [decimal](9, 2) NOT NULL,
	[daily_fine_rate] [decimal](9, 2) NOT NULL,
	[quantity] [int] NOT NULL,
 CONSTRAINT [PK_furniture_item] PRIMARY KEY CLUSTERED 
(
	[serial_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[location]    Script Date: 4/26/2020 1:48:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[location](
	[zip] [char](5) NOT NULL,
	[city] [varchar](50) NOT NULL,
	[state] [char](2) NOT NULL,
 CONSTRAINT [PK_location] PRIMARY KEY CLUSTERED 
(
	[zip] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rental_item]    Script Date: 4/26/2020 1:48:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rental_item](
	[itemID] [int] IDENTITY(1,1) NOT NULL,
	[rentalID] [int] NOT NULL,
	[serial_number] [int] NOT NULL,
	[quantity] [int] NOT NULL,
 CONSTRAINT [PK_rental_item] PRIMARY KEY CLUSTERED 
(
	[itemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rental_transaction]    Script Date: 4/26/2020 1:48:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rental_transaction](
	[rentalID] [int] IDENTITY(1,1) NOT NULL,
	[member_userID] [int] NOT NULL,
	[employee_userID] [int] NOT NULL,
	[rental_date] [datetime] NOT NULL,
	[due_date] [date] NOT NULL,
 CONSTRAINT [PK_rental_transaction] PRIMARY KEY CLUSTERED 
(
	[rentalID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[return_item]    Script Date: 4/26/2020 1:48:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[return_item](
	[itemID] [int] NOT NULL,
	[returnID] [int] NOT NULL,
	[quantity] [int] NOT NULL,
	[amount_due] [decimal](9, 2) NOT NULL,
	[amount_paid] [decimal](9, 2) NOT NULL,
	[amount_refunded] [decimal](9, 2) NOT NULL,
 CONSTRAINT [PK_return_item] PRIMARY KEY CLUSTERED 
(
	[itemID] ASC,
	[returnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[return_transaction]    Script Date: 4/26/2020 1:48:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[return_transaction](
	[returnID] [int] IDENTITY(1,1) NOT NULL,
	[employee_userID] [int] NOT NULL,
	[return_date] [datetime] NOT NULL,
 CONSTRAINT [PK_return_transaction] PRIMARY KEY CLUSTERED 
(
	[returnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[role]    Script Date: 4/26/2020 1:48:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[role](
	[roleID] [int] IDENTITY(1,1) NOT NULL,
	[type] [varchar](50) NOT NULL,
 CONSTRAINT [PK_role] PRIMARY KEY CLUSTERED 
(
	[roleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[state]    Script Date: 4/26/2020 1:48:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[state](
	[code] [char](2) NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_state] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[style]    Script Date: 4/26/2020 1:48:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[style](
	[styleID] [int] IDENTITY(1,1) NOT NULL,
	[style_type] [varchar](50) NOT NULL,
 CONSTRAINT [PK_style] PRIMARY KEY CLUSTERED 
(
	[styleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user]    Script Date: 4/26/2020 1:48:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user](
	[userID] [int] IDENTITY(1,1) NOT NULL,
	[roleID] [int] NOT NULL,
	[date_of_birth] [date] NOT NULL,
	[first_name] [varchar](50) NOT NULL,
	[last_name] [varchar](50) NOT NULL,
	[phone_number] [char](10) NOT NULL,
	[address1] [varchar](50) NOT NULL,
	[address2] [varchar](50) NULL,
	[zip] [char](5) NOT NULL,
	[sex] [char](1) NOT NULL,
 CONSTRAINT [PK_employee] PRIMARY KEY CLUSTERED 
(
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_category]    Script Date: 4/26/2020 1:48:07 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_category] ON [dbo].[category]
(
	[category_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_credential]    Script Date: 4/26/2020 1:48:07 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_credential] ON [dbo].[credential]
(
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_style]    Script Date: 4/26/2020 1:48:07 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_style] ON [dbo].[style]
(
	[style_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[credential] ADD  CONSTRAINT [DF_credential_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[credential]  WITH CHECK ADD  CONSTRAINT [FK_credential_user] FOREIGN KEY([userID])
REFERENCES [dbo].[user] ([userID])
GO
ALTER TABLE [dbo].[credential] CHECK CONSTRAINT [FK_credential_user]
GO
ALTER TABLE [dbo].[furniture_item]  WITH CHECK ADD  CONSTRAINT [FK_furniture_item_category] FOREIGN KEY([categoryID])
REFERENCES [dbo].[category] ([categoryID])
GO
ALTER TABLE [dbo].[furniture_item] CHECK CONSTRAINT [FK_furniture_item_category]
GO
ALTER TABLE [dbo].[furniture_item]  WITH CHECK ADD  CONSTRAINT [FK_furniture_item_style] FOREIGN KEY([styleID])
REFERENCES [dbo].[style] ([styleID])
GO
ALTER TABLE [dbo].[furniture_item] CHECK CONSTRAINT [FK_furniture_item_style]
GO
ALTER TABLE [dbo].[location]  WITH CHECK ADD  CONSTRAINT [FK_location_state] FOREIGN KEY([state])
REFERENCES [dbo].[state] ([code])
GO
ALTER TABLE [dbo].[location] CHECK CONSTRAINT [FK_location_state]
GO
ALTER TABLE [dbo].[rental_item]  WITH CHECK ADD  CONSTRAINT [FK_rental_item_furniture_item] FOREIGN KEY([serial_number])
REFERENCES [dbo].[furniture_item] ([serial_number])
GO
ALTER TABLE [dbo].[rental_item] CHECK CONSTRAINT [FK_rental_item_furniture_item]
GO
ALTER TABLE [dbo].[rental_item]  WITH CHECK ADD  CONSTRAINT [FK_rental_item_rental_transaction] FOREIGN KEY([rentalID])
REFERENCES [dbo].[rental_transaction] ([rentalID])
GO
ALTER TABLE [dbo].[rental_item] CHECK CONSTRAINT [FK_rental_item_rental_transaction]
GO
ALTER TABLE [dbo].[rental_transaction]  WITH CHECK ADD  CONSTRAINT [FK_rental_transaction_employeeID] FOREIGN KEY([employee_userID])
REFERENCES [dbo].[user] ([userID])
GO
ALTER TABLE [dbo].[rental_transaction] CHECK CONSTRAINT [FK_rental_transaction_employeeID]
GO
ALTER TABLE [dbo].[rental_transaction]  WITH CHECK ADD  CONSTRAINT [FK_rental_transaction_memberID] FOREIGN KEY([member_userID])
REFERENCES [dbo].[user] ([userID])
GO
ALTER TABLE [dbo].[rental_transaction] CHECK CONSTRAINT [FK_rental_transaction_memberID]
GO
ALTER TABLE [dbo].[return_item]  WITH CHECK ADD  CONSTRAINT [FK_return_item_rental_item] FOREIGN KEY([itemID])
REFERENCES [dbo].[rental_item] ([itemID])
GO
ALTER TABLE [dbo].[return_item] CHECK CONSTRAINT [FK_return_item_rental_item]
GO
ALTER TABLE [dbo].[return_item]  WITH CHECK ADD  CONSTRAINT [FK_return_item_return_transaction] FOREIGN KEY([returnID])
REFERENCES [dbo].[return_transaction] ([returnID])
GO
ALTER TABLE [dbo].[return_item] CHECK CONSTRAINT [FK_return_item_return_transaction]
GO
ALTER TABLE [dbo].[return_transaction]  WITH CHECK ADD  CONSTRAINT [FK_return_transaction_employeeID] FOREIGN KEY([employee_userID])
REFERENCES [dbo].[user] ([userID])
GO
ALTER TABLE [dbo].[return_transaction] CHECK CONSTRAINT [FK_return_transaction_employeeID]
GO
ALTER TABLE [dbo].[user]  WITH CHECK ADD  CONSTRAINT [FK_employee_location1] FOREIGN KEY([zip])
REFERENCES [dbo].[location] ([zip])
GO
ALTER TABLE [dbo].[user] CHECK CONSTRAINT [FK_employee_location1]
GO
ALTER TABLE [dbo].[user]  WITH CHECK ADD  CONSTRAINT [FK_user_role] FOREIGN KEY([roleID])
REFERENCES [dbo].[role] ([roleID])
GO
ALTER TABLE [dbo].[user] CHECK CONSTRAINT [FK_user_role]
GO
/****** Object:  StoredProcedure [dbo].[GetPopularFurniture]    Script Date: 4/26/2020 1:48:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPopularFurniture]
@start_date DATETIME,
@end_date DATETIME

AS
	/** Overrides date parameters for testing
	SET @start_date = '2015-03-28';
	SET @end_date = '2015-12-10';
	**/

	DECLARE @total_rentals SMALLINT;
	SET @total_rentals = (
		SELECT COUNT(*) 
		FROM rental_item AS ri
			LEFT JOIN rental_transaction AS rt ON ri.rentalID = rt.rentalID
		WHERE rt.rental_date >= @start_date AND rt.rental_date <= @end_date
	)

	SELECT fi.serial_number 'SerialNumber', c.category_name 'Category', fi.description 'Description',
		(SELECT COUNT(*) 
		 FROM rental_item AS ri1 
			LEFT JOIN rental_transaction AS rt1 ON ri1.rentalID = rt1.rentalID
		 WHERE ri1.serial_number = fi.serial_number
			AND rt1.rental_date >= @start_date AND rt1.rental_date <= @end_date AND (SELECT COUNT(*) FROM rental_item WHERE ri.serial_number = fi.serial_number) > 1) AS 'TimesRented',
		 @total_rentals AS 'TotalRentals',
		 CAST(((SELECT COUNT(*) 
		 FROM rental_item AS ri1 
			LEFT JOIN rental_transaction AS rt1 ON ri1.rentalID = rt1.rentalID
		 WHERE ri1.serial_number = fi.serial_number
			AND rt1.rental_date >= @start_date AND rt1.rental_date <= @end_date AND (SELECT COUNT(*) FROM rental_item WHERE ri.serial_number = fi.serial_number) > 1) / CAST(@total_rentals AS FLOAT) * 100) AS INT) AS 'PercentOfTotalRentals',
		 (SELECT COUNT(*)
			FROM furniture_item AS fi
				LEFT JOIN rental_item AS ri ON ri.serial_number = fi.serial_number
					LEFT JOIN rental_transaction AS rt ON rt.rentalID = ri.rentalID
						LEFT JOIN "user" AS u ON rt.member_userID = u.userID 
			WHERE rt.rental_date >= @start_date AND rt.rental_date <= @end_date 
				AND (SELECT COUNT(*) FROM rental_item WHERE ri.serial_number = fi.serial_number) > 1
				AND (FLOOR(DATEDIFF(DAY, u.date_of_birth, rt.rental_date) / 365) BETWEEN 18 AND 29)
		 ) 'PercentBetween18And29'
	FROM furniture_item AS fi
		LEFT JOIN category AS c ON fi.categoryID = c.categoryID
			LEFT JOIN rental_item AS ri ON ri.serial_number = fi.serial_number
				LEFT JOIN rental_transaction AS rt ON rt.rentalID = ri.rentalID
	WHERE rt.rental_date >= @start_date AND rt.rental_date <= @end_date AND 
		(
		SELECT COUNT(*) 
		FROM rental_item AS ri2
			LEFT JOIN rental_transaction AS rt2 ON rt2.rentalID = ri2.rentalID
		WHERE ri2.serial_number = fi.serial_number AND rt2.rental_date >= @start_date AND rt2.rental_date <= @end_date
		) > 1
	GROUP BY fi.serial_number, fi.description, c.category_name, ri.serial_number
RETURN
GO
USE [master]
GO
ALTER DATABASE [cs6232-g4] SET  READ_WRITE 
GO
